
% edit these as needed
setenv('JH_OHBA_UKMP','/Users/jhadida/Data/dataset/ukmp/proc');

% set console encoding
try
    slCharacterEncoding('UTF-8');
catch 
    warning('Could not set character encoding; is Simulink installed?');
end

% seed random number generator
rng('shuffle','twister');

% add libraries to path and call startup scripts 
p_ = jh_path();
addpath( p_.root, p_.core, p_.deck, p_.lsbm, p_.bayte );
clear p_

dk_startup();
lsbm_startup();
bt_startup();
