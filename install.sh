#!/usr/bin/env bash

# source utilities
HERE=$(cd $(dirname "${BASH_SOURCE}") && pwd -P)
source "$HERE/bashlib.sh" || { echo "Could not source utilities."; exit 1; }
qpushd "$HERE"
msg_bG "\n  --- OHBA INSTALL START ($HERE) ---"

# clone repositories
EXT_FOLDER=$HERE/external
clone_repo() {
    local name=$1
    local url=$2

    if [ -d "$EXT_FOLDER/$name" ]; then
        msg_bG "Repo ${name}: ok"
    else
        msg_bB "Repo ${name}: cloning..."
        git clone "$url" "$EXT_FOLDER/$name" || echoerr "Could not clone '$name'"
        msg_bG "Repo ${name}: done!"
    fi
}

clone_repo deck https://gitlab.com/jhadida/deck.git
clone_repo lsbm https://gitlab.com/jhadida/lsbm.git
# clone_repo bayte https://gitlab.com/jhadida/bayte.git

# install dependencies
qpushd "$EXT_FOLDER/deck/jmx"
msg_bB "Installing Deck..."
./install.sh || echoerrx1 "Could not install Deck."
msg_bG "Install Deck: done!"
qpopd 

qpushd "$EXT_FOLDER/lsbm"
msg_bB "Installing LSBM..."
./install.sh || echoerrx1 "Could not install LSBM."
msg_bG "Install LSBM: done!"
qpopd

# download data
DATA_LINK=https://users.fmrib.ox.ac.uk/~jhadida/ohba/
msg_bB "Downloading data..."
if iscmd rsync; then 
    rsync -rltpuP "$DATA_LINK" data
else 
    echoerrx1 "rsync is required to download data (on OSX, install with Homebrew: brew install rsync)."
fi
msg_bG "Downloading data: done!"

# final notice
msg_bG "\n  --- OHBA INSTALL END ---"
msg_G "Now start Matlab and run:\n>> cd('$HERE'); jh_startup(); jh_compile();"
