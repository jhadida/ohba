#!/usr/bin/env bash

# source utilities
HERE=$(cd $(dirname "${BASH_SOURCE}") && pwd -P)
source "$HERE/bashlib.sh" || { echo "Could not source utilities."; exit 1; }
qpushd "$HERE"
msg_bG "\n  --- OHBA UPDATE START ($HERE) ---"

# update repos
EXT_FOLDER=$HERE/external

qpushd "$EXT_FOLDER/deck"
msg_bB "Updating Deck..."
git pull || echoerrx1 "Could not update Deck."
msg_bG "Updating Deck: done!"
qpopd

qpushd "$EXT_FOLDER/lsbm"
msg_bB "Updating LSBM..."
git pull || echoerrx1 "Could not update LSBM."
msg_bG "Updating LSBM: done!"
qpopd

# update data
msg_bB "Updating data..."
REMOTE=https://users.fmrib.ox.ac.uk/~jhadida/ohba/
rsync -rltpuP "$@" "$REMOTE" data || echoerrx1 "Could not update data."
msg_bG "Updating data: done!"

# final notice
msg_bG "\n  --- OHBA UPDATE END ---"
msg_G "Now start Matlab and run:\n>> cd('$HERE'); jh_startup(); jh_compile();"
