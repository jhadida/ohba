function bt_startup()

    here = fileparts(mfilename('fullpath'));
    assert( ~isempty(getenv('DECK_ROOT')), 'Required dependency "Deck" is not on the path.' );
    
    dk.print('[BT] Starting up from folder "%s".',here);
    dk.env.path_flag( 'BT_ROOT', here );

end
