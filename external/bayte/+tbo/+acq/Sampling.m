classdef Sampling < handle
%
% For a given cell of elements, return a subset using a combination of:
%   - deterministic selection based on highest weights (skimming)
%   - random selection using importance sampling or uniform sampling
%
% Properties to be set are:
%
%   skim    DEFAULT: 0.5
%           Ratio of samples that should be selected deterministically.
%           Note that the skimming ratio is honoured in a rolling manner, 
%           across multiple calls to sample().
%
%   unif    DEFAULT: true
%           If true, the random sampling considers equal weights for all items.
%
%
% See also: datasample
%
% JH

    properties
        skim    % ratio of samples to be selected deterministically
        unif    % uniform random selection if true, importance sampling otherwise
    end
    
    properties (Hidden,SetAccess=private)
        dcount
        rcount
    end
    
    methods
        
        function self = Sampling(varargin)
            if nargin==1 && isstruct(varargin{1})
                self.restore(varargin{1});
            else
                self.reset(varargin{:});
            end
        end
        
        function reset(self,skim,unif)
            
            if nargin < 2, skim = 0.5; end
            if nargin < 3, unif = true; end
            
            assert( dk.num.between(skim,0,1), 'Skim ratio should be between 0 and 1.' );
            
            self.skim = skim;
            self.unif = all(logical(unif));
            
            self.dcount = 0;
            self.rcount = 0;
            
        end
        
        function s = to_struct(self)
            s.type = 'tbo.acq.Sampling';
            s.ver = '0.1';
            
            s.prop.skim = self.skim;
            s.prop.unif = self.unif;
            s.prop.dcount = self.dcount;
            s.prop.rcount = self.rcount;
        end
        
        function self = restore(self,s)
            [ver,prop] = bt.priv.restore(s,'tbo.acq.Sampling');
            switch lower(ver)
                case '0.1'
                    self.skim = prop.skim;
                    self.unif = prop.unif;
                    
                    self.dcount = prop.dcount;
                    self.rcount = prop.rcount;
                otherwise
                    error( 'Unknown version: %s', ver );
            end
        end
        
        function out = select(self,in,w,ns)
            
            assert( iscell(in), 'Expected a cell of elements.' );
            assert( numel(in) == numel(w), 'Weights size mismatch.' );
            
            ne = numel(in);
            if ns > ne
                warning( 'Cannot sample more than the number of input items.' );
                ns = ne;
            end
            
            % avoid numerical artefacts
            [in,w] = tbo.util.shuffle(in,w);
            
            % split between deterministic and random sampling
            cd = self.dcount;
            cr = self.rcount;
            
            nsd = floor( self.skim * (cr + cd + ns) - cd );
            nsd = dk.num.clamp( nsd, 0, ns );
            nsr = ns - nsd;
            
            % sample deterministically (skimming)
            [w,k] = sort(w,'descend');
            outd = in(k(1:nsd));
            
            % sample randomly
            m = nsd+1;
            if nsr > 0
                if self.unif
                    outr = in(datasample( k(m:end), nsr, 'Replace', false ));
                else
                    outr = in(datasample( k(m:end), nsr, 'Replace', false, 'Weights', w(m:end) ));
                end
            else
                outr = {};
            end
            
            % concatenate selections and update counters
            out = [outd, outr];
            
            self.dcount = self.dcount + nsd;
            self.rcount = self.rcount + nsr;
            
        end
        
    end
    
end