function obj = factory(type,varargin)
%
% Create instance of acquisition strategy.
%
% JH

    if nargin==1 && isstruct(type)
        args = {type};
        type = type.type;
    else
        args = varargin;
    end

    % convert tbo.acq.<Name> to <Name>
    type = strsplit(type,'.');
    type = type{end};
    
    % instanciate
    switch lower(type)
        case {'sampling','s'}
            obj = tbo.acq.Sampling(args{:});
        case {'traversal','t'}
            obj = tbo.acq.Traversal(args{:});
        otherwise
            error( 'Unknown type: %s', type );
    end

end