classdef Traversal < handle
%
% For a given cell of elements with associated depths, select one element at each depth,
% using one of the following methods:
%
%   max     select element with maximum weight
%   imp     importance sampling
%   unif    uniform random sampling
%
% JH

    properties
        meth
    end
    
    methods
        
        function self = Traversal(varargin)
            if nargin==1 && isstruct(varargin{1})
                self.restore(varargin{1});
            else
                self.reset(varargin{:});
            end
        end
        
        function reset(self,meth)
            
            if nargin < 2, meth='max'; end
            self.meth = meth;
            
        end
        
        function s = to_struct(self)
            s.type = 'tbo.acq.Traversal';
            s.ver = '0.1';
            
            s.prop.meth = self.meth;
        end
        
        function self = restore(self,s)
            [ver,prop] = bt.priv.restore(s,'tbo.acq.Traversal');
            switch ver
                case '0.1'
                    self.meth = prop.meth;
                otherwise
                    error( 'Unknown version: %s', ver );
            end
        end
        
        function out = select(self,in,w,d)
            
            assert( iscell(in), 'Expected a cell of elements.' );
            assert( numel(in) == numel(w), 'Weights size mismatch.' );
            assert( numel(in) == numel(d), 'Depths size mismatch.' );
            
            % avoid numerical artefacts
            [in,w,d] = tbo.util.shuffle(in,w,d);
            
            % group by label
            d = dk.grouplabel(d);
            n = numel(d);
            
            % select one element at each depth
            out = cell(1,n);
            mask = false(1,n);
            best = -Inf;
            
            for k = 1:n
                
                % current group
                g = d{k};
                if isempty(g), continue; end
                
                % select element
                switch lower(self.meth)
                    case {'max','m'}
                        [~,s] = max(w(g));
                        s = g(s);
                    case {'imp','w'}
                        s = datasample( g, 1, 'Weights', w(g) );
                    case {'unif','u'}
                        s = datasample( g, 1 );
                    otherwise
                        error( 'Unknown selection method: %s', self.meth );
                end
                
                % enforce increasing constraint
                if w(s) >= best
                    mask(k) = true;
                    out{k} = in{s};
                    best = w(s);
                end
                
            end
            
            % selected elements
            out = out(mask);
            
        end
        
    end
    
end