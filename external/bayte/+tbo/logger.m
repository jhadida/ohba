function L = logger()
%
% Return logger used within the toolbox.

    L = dk.logger.get( 'BayTe', 'nodate', true, 'lvlchar', true );
end