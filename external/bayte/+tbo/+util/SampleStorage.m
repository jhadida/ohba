classdef SampleStorage < handle
%
% Attach to an instance of Optimiser, and act as a data store for simulations.
%
% JH

    properties (SetAccess = private)
        root
        conf
    end
    properties (Transient,SetAccess = private)
        item
    end
    
    methods
        
        function self = SampleStorage(root,varargin)
            
            assert( ~dk.fs.isfile(root), 'Input should be a folder name.' );
            
            self.root = root;
            self.conf = dk.getopt( varargin, ...
                'format', 'sample_%s', 'regex', 'sample_([\w\d]+)', 'savename', 'storage.mat' );
        
            dk.fs.mkdir( self.root );
            b = self.path( self.conf.savename );
            if dk.fs.isfile(b)
                self.restore(b);
            end
            
            names = self.search( self.conf.regex, false, 'folder' );
            self.item = dk.mapfun( @self.parseid, names, false );
        end
        
        function n = count(self)
            n = numel(self.item);
        end
        
        % i/o methods
        function save(self)
            dk.save( self.path(self.conf.savename), self.to_struct() );
        end
        function saveobj(self)
            error( 'This object cannot be saved like a regular variable.' );
        end
        
        function s = to_struct(self)
            s = struct();
            s.type = 'tbo.util.SampleStorage';
            s.ver = '0.1';
            
            s.prop.root = self.root;
            s.prop.conf = self.conf;
        end
        
        function self = restore(self,s)
            [v,p] = bt.priv.restore(s,'tbo.util.SampleStorage');
            switch v
            case '0.1'
                self.root = p.root;
                self.conf = p.conf;
            otherwise
                error( 'Unknown version: %s', v );
            end
        end
        
    end
    
    % -----------------------------------------------------------------------------------------
    % deal with file and folder names
    % -----------------------------------------------------------------------------------------
    methods
        
        % path relative to root
        function p = path(self,varargin)
        	p = fullfile( self.root, varargin{:} );
        end
        
        % path relative to sample
        function [s,id] = sname(self,id,varargin)
            if dk.is.integer(id)
                id = num2str(id);
            end
            assert( dk.is.string(id), 'id should be a string.' );
            s = sprintf( self.conf.format, id );
        end
        
        function [p,id] = spath(self,id,varargin)
            [p,id] = self.sname(id);
            p = self.path(p,varargin{:});
        end
        
        function ok = sfind(self,id)
            ok = dk.fs.isdir(self.spath(id));
        end
        
        % parse sample ID from name or path
        function id = parseid(self,name)
            
            % extract folder name from path
            name = dk.str.rstrip( name, filesep );
            [~,name] = fileparts(name);
            
            % extract ID from folder name
            id = regexp( name, self.conf.regex, 'tokens' );
            try
                id = id{1}{1};
            catch
                error( 'Could not parse item ID from: %s', name );
            end
        end
        
        % find file or folder matching input query
        function q = find(self,name,type)
            if nargin < 3, type='file'; end
            assert( dk.is.string(name), 'Input should be a string.' ); 
            
            switch type
                case {'f','file'}
                    test = @dk.fs.isfile;
                otherwise
                    test = @dk.fs.isdir;
            end
            
            q = self.path(name);
            assert( test(q), 'Not found: "%s"', name );
        end
        
        function r = search(self,varargin)
        %
        % res = search( location, pattern, recurse=true, type=file )
        % res = search( pattern, recurse=true, type=file )
        %
        % Search for files and/or folders matching the regex pattern.
        %
        % Optionally specify subfolder as location (default: self.root).
        % Optionally restrict results to being files or folders only:
        %   type = file / folder / any
        %
        % Output is a cellstr of absolute paths.
        
            % shift arguments if location is omitted
            if nargin >= 3 && ischar(varargin{2})
                args = varargin;
            else
                args = [ {self.root}, varargin ];
            end
            nargs = numel(args);
            
            % extract args
            loc = args{1};
            pat = args{2};
            if nargs > 2
                recurse = args{3};
            else
                recurse = true;
            end
            if nargs > 3
                type = args{4};
            else
                type = 'file';
            end
            
            % try to find location relative to storage root
            if ~dk.fs.isdir(loc)
                loc = self.path(loc);
            end
            dk.fs.chkdir(loc);
            
            % determine filter based on expected result type
            switch lower(type)
                case {'f','file'}
                    filter = @(x) ~x.isdir;
                case {'d','dir','folder'}
                    filter = @(x) x.isdir;
                otherwise
                    filter = @(x) true;
            end
            
            % search folder
            r = dk.fs.search( loc, pat, filter, recurse );
        end
        
    end
    
    % -----------------------------------------------------------------------------------------
    % deal with files and folders
    % -----------------------------------------------------------------------------------------
    methods
        
        function [fpath,id] = create(self,id)
        % Create folder for a new item
            
            [fpath,id] = self.spath(id);
            dk.fs.mkdir( fpath, false );
            self.item{end+1} = id;
        end
        
        function p = store(self,id,relpath,varargin)
            assert( ~isempty(relpath), 'Relpath should be a string.' );
            p = self.spath(id,relpath);
            dk.save( p, varargin{:} );
        end
        
        function [s,p] = retrieve(self,id,relpath)
            assert( ~isempty(relpath), 'Relpath should be a string.' );
            p = self.spath(id,relpath);
            s = load(p);
        end
        
        function out = iter(self,callback)
        % Iterate all samples, and call:
        %   callback(id,path)
        %
        % If an output is collected, then a cell is returned.
        % No error if the callback returns void, and no output is collected.
        
            n = numel(self.item);
            out = cell(1,n);
            for i = 1:n
                id = self.item{i};
                if nargout > 0
                    out{i} = callback( id, self.spath(id) );
                else
                    callback( id, self.spath(id) );
                end
            end
        end
        
    end
    
end
