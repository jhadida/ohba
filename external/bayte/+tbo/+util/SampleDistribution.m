classdef SampleDistribution < handle
%
% For a set of data being given incrementally (e.g. objective evaluations of 
% elements during optimisation), compute empirical sample densities (mass, 
% and cumulative), and provide efficient methods to evaluate them at any point.
%
% The densities are computed using ksdensity, and updated every time new data 
% are appended. The methods {pdf,cdf} use interpolation (pchip) to efficiently 
% compute values at any point.
%
%
% See also: ksdensity, interp1
% 
% JH

    properties
        data
        dist
        args
    end
    
    methods
        
        function self = SampleDistribution(v,varargin)
            self.clear();
            if nargin > 0
                self.args = varargin;
                if isstruct(v)
                    self.restore(v);
                else
                    self.append(v);
                end
            end
        end
        
        function clear(self)
            self.data = {};
            self.dist = struct();
            self.args = {};
        end
        
        function self = append(self,v)
            assert( isnumeric(v), 'Input should be numeric.' );
            if isempty(v), return; end
            self.data{end+1} = v(:);
            self.update();
        end
        
        function s = to_struct(self)
            s.type = 'tbo.util.SampleDistribution';
            s.ver = '0.1';
            
            s.prop.data = self.data;
            s.prop.dist = self.dist;
            s.prop.args = self.args;
        end
        
        function self = restore(self,s)
            [ver,prop] = bt.priv.restore( s, 'tbo.util.SampleDistribution' );
            switch lower(ver)
                case '0.1'
                    self.data = prop.data;
                    self.dist = prop.dist;
                    self.args = prop.args;
                otherwise
                    error( 'Unknown version: %s', ver );
            end
        end
        
        function self = update(self,varargin)
            
            self.dist = struct();
            
            % update args if given
            if nargin > 1
                self.args = varargin;
            end
            
            % concatenate all data
            v = vertcat(self.data{:});
            if isempty(v), return; end
            
            % estimate PDF
            [p,x] = ksdensity( v, 'Function', 'pdf', self.args{:} );
            
            % extend functions before and after for smooth extrapolation
            dx = x(2)-x(1);
            x = [ x(1) - [2,1]*dx, x, x(end) + [1,2]*dx ];
            p = [ 0, 0, p, 0, 0 ];
            
            % compute CDF
            g = cumsum(p);
            g = g / g(end);
            
            % assign member properties
            self.dist.x = x(:);
            self.dist.p = p(:);
            self.dist.g = g(:);
            self.dist.w = trapz(x,p);
            
            % experimental
            %self.update_icdf(v);
            
        end
        
    end
    
    methods
        
        function v = pdf(self,x)
            v = interp1( self.dist.x, self.dist.p, x, 'pchip' );
        end
        
        function v = cdf(self,x)
            v = interp1( self.dist.x, self.dist.g, x, 'pchip' );
        end
        
        function v = rcdf(self,x)
            v = 1 - self.cdf(x);
        end
        
        function h = plot_pdf(self,varargin)
            h = plot( self.dist.x, self.dist.p, varargin{:} );
        end
        
        function h = plot_cdf(self,varargin)
            h = plot( self.dist.x, self.dist.g, varargin{:} );
        end
                
    end
    
    methods (Hidden) % experimental
        
        function update_icdf(self,v)
            [u,y] = ksdensity( v, 'Function', 'icdf', self.args{:} );
            self.dist.u = u(:);
            self.dist.y = y(:);
        end
        
        function v = icdf(self,x)
            v = interp1( self.dist.y, self.dist.u, x, 'pchip' );
        end
        
        function h = plot_icdf(self,varargin)
            h = plot( self.dist.y, self.dist.u, varargin{:} );
        end

    end
    
end