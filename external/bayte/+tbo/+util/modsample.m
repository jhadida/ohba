function ns = modsample(elm,base)
%
% Modulate sample-size as a function of the dimensionality and scale (normalised) 
% of input elements. Effect of dimensionality is linear, and effect of scale is
% reciprocal of logarithm. This works as intended because all scales are strictly
% less than sqrt(d) (elements are in the unit box), so larger scale correspond to 
% more samples, and vice-versa. The division by exp(1) is to ensure the denominator 
% is always larger than 1.
%

    p = dk.mapfun( @(x) [x.scale,x.depth], elm );
    p = vertcat(p{:});
    s = p(:,1);
    d = p(:,2);
    
    ns = s ./ (sqrt(d)*exp(1));
    ns = - d ./ log(ns);
    ns = ceil( base*ns );

end