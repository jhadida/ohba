classdef RatioCounter < handle
    
    properties (SetAccess = private)
        dratio
        dcount
        rcount
    end
    
    methods
        
        function self = RatioCounter(varargin)
            self.clear();
            if nargin > 0
                arg = varargin{1};
                if isstruct(arg)
                    self.restore(arg);
                else
                    self.ratio(arg);
                end
            end
        end
        
        function clear(self)
            self.dratio = 0.5;
            self.dcount = 0;
            self.rcount = 0;
        end
        
        function self = ratio(self,r)
            assert( dk.is.number(r) && dk.num.between(r,0,1), 'Ratio should be between 0 and 1.' );
            self.dratio = r;
        end
        
        function s = to_struct(self)
            s.type = 'tbo.util.RatioCounter';
            s.ver = '0.1';
            
            a.prop.dratio = self.dratio;
            s.prop.dcount = self.dcount;
            s.prop.rcount = self.rcount;
        end
        
        function self = restore(self,s)
            [ver,prop] = bt.priv.restore(s,'tbo.util.RatioCounter');
            switch lower(ver)
                case '0.1'
                    self.dratio = prop.dratio;
                    self.dcount = prop.dcount;
                    self.rcount = prop.rcount;
                otherwise
                    error( 'Unknown version: %s', ver );
            end
        end
        
        function [nd,nr] = sample(self,n)
            cd = self.dcount;
            cr = self.rcount;
            
            nd = floor( self.dratio * (cr + cd + n) - cd );
            nd = dk.num.clamp( nd, 0, n );
            nr = n - nd;
            
            self.dcount = self.dcount + nd;
            self.rcount = self.rcount + nr;
        end
        
    end
    
end