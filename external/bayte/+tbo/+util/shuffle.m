function varargout = shuffle(elm,w,varargin)
%
% Shuffle elements and associated weights to avoid numerical artefacts.
%

    assert( iscell(elm), 'First input should be a cell.' );
    varargout = cell(1,nargin);
    
    % permute input cell of elements
    p = randperm(numel(elm));
    varargout{1} = elm(p);
    
    % round and permute associated weights
    if nargin > 1
        varargout{2} = dk.num.round( w(p), 8 ); % round to 8 decimal places
    end
    
    % shuffle any other input
    for k = 3:nargin
        varargout{k} = varargin{k-2}(p);
    end

end