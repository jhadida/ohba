function x = positify(x,a)
%
% y = tbo.util.positify(x,a=1)
%
% Smooth transform from R to R+
%
%    (1-x)^(-a)  if  x < 0
%    (1+x)^a     otherwise
%
% JH

    if nargin < 2, a=1; end
    assert( a > eps, 'a should be positive.' );

    m = x < 0;
    x(m) = (1 - x(m)).^(-a);
    
    m = ~m;
    x(m) = (1 + x(m)).^a;

end