function u = ei(ms,best)
%
% u = tbo.util.ei(ms,best)
%
% Compute expected improvement.
% 
% JH

    assert( dk.is.matrix(ms,[0,2]), 'First input should be nx2' );
    mu = ms(:,1);
    sigma = ms(:,2);
    
    u = (mu-best) ./ max( sqrt(2)*sigma, eps );
    u = (mu-best) .* erfc(u)/2 + (sigma/sqrt(2*pi)) .* exp(- u.*u);
    
end