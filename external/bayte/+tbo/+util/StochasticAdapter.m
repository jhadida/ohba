classdef StochasticAdapter < tbo.obj.Objective
%
% Wrap an instance of tbo.obj.Objective to evaluate the objective several
% times under the hood at each call to value(x). Only calls with a single 
% point are supported, and the value output is a vector of variable size.
%
% See also: ant.math.sample_stats
%
% JH

    properties
        obj
        arg
    end
    
    methods
        
        function self = StochasticAdapter(obj,varargin)
            assert( isa(obj,'tbo.obj.Objective'), 'Input should be an Objective.' );
            self.obj = obj;
            self.arg = varargin;
        end
        
        function v = value(self,x)
            assert( size(x,1)==1, 'Only accepts single points.' );
            [~,~,v] = ant.math.sample_stats( @() self.obj.value(x), self.arg{:} );
        end
        
    end
    
end