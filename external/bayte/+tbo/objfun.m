function obj = objfun(varargin)
%
% obj = tbo.objfun( fun, varargin )
% 
% Create instance of tbo.obj.Objective from input function handle.
% Wrap into StochasticAdapter if more inputs are provided.
%
% See also: tbo.obj.ObjectiveHandle, tbo.util.StochasticAdapter
%
% JH

    arg = dk.wrap(varargin{:});
    
    % convert first input to Objective if needed
    if dk.is.fhandle(arg{1})
        obj = tbo.obj.ObjectiveHandle(arg{1});
    else
        obj = arg{1};
        assert( isa(obj,'tbo.obj.Objective'), 'Bad input type.' );
    end
    
    % wrap into StochasticAdapter if more inputs are provided
    if numel(arg) > 1
        obj = tbo.util.StochasticAdapter(obj,arg{2:end});
    end
    
end