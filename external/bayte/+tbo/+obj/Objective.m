classdef Objective < handle
    
    methods (Abstract)
        
        % evaluate the objective (possibly at multiple points)
        y = value(~,x);
        
    end
    
end