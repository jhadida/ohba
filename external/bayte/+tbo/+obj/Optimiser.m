classdef Optimiser < handle

    properties (Constant,Abstract)
        NAME    % algorithm identifier
    end
    
    properties (SetAccess=protected)
        tess    % tessellation
        meta    % metadata
        prop    % runtime properties
        cfg     % options for algorithm
    end
    
    properties (Transient, SetAccess=protected)
        obj     % objective wrapper
        val     % value-class
        log     % message logging
    end
    
    events
        BeforeInit
        AfterInit
        BeforeIter
        AfterIter
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % abstract interface
    % -----------------------------------------------------------------------------------------
    methods (Abstract, Hidden) % to be implemented
        
        runInit(~);
        runIter(~,crit); % takes a Terminator
        
    end
    methods (Hidden) % to be overridden
        
        % saveMembers / loadMembers(p)
        %
        %   These methods are called during serialisation / restoration.
        %   You can use them to save and retrieve any additional class 
        %   properties that derived classes may define.
        
        function p = saveMembers(self)
            p = struct();
        end
        
        function loadMembers(self,p)
        end
        
        
        % postSetup() / postEval(elm)
        %
        %   Called after setting the objective function, value-class, 
        %   and config.
        %
        function postSetup(self)
        end
        
        % postEval(elm)
        %
        %   Called after the evaluation of EACH element in evaluate().
        %   Note that there might be several evaluations of the objective 
        %   e.g. if StochasticAdapter is used.
        %
        function postEval(self,elm)
        end
        
        % postIter(varargin)
        %
        %   To be called after initialisation, and after each iteration.
        %   Updates metadata and logs information.
        %
        function postIter(self,varargin)
            
            self.meta.inc_niter();
            self.meta.nprior = self.tess.pred.prior.nact;
            self.meta.nelem = numel(self.tess.store.leaf_ids());
            self.meta.record( varargin{:} );
            self.log.info( self.meta.to_string() );
            
        end
        
    end
    methods
        
        function defaultConfig(self)
            self.cfg = struct();
        end
        
        function setConfig(self,varargin)
            self.cfg = dk.setopt( self.cfg, varargin{:} );
        end
        
        function value = setOption(self,name,value)
            assert( isfield(self.cfg,name), 'Unknown option: %s', name );
            self.cfg.(name) = value;
        end
        
        function value = getOption(self,name,value)
            if isfield(self.cfg,name)
                value = self.cfg.(name);
            else
                assert( nargin > 2, 'Unknown option: %s', name );
            end
        end
        
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % core methods
    % -----------------------------------------------------------------------------------------
    methods (Hidden)
        
        function self = init(self,tess,varargin)
            assert( nargin > 2, 'At least two inputs required.' );
            self.clear();
            
            if isa(tess,'bt.obj.Tessellation')
                self.tess = tess;
                self.setup( varargin{:} );
            else
                assert( ischar(tess) || isstruct(tess), 'Bad input for restoration.' );
                self.restore( tess, varargin{:} );
            end
        end
        
    end
    methods
        
        function self = clear(self)
            self.tess = [];
            self.meta = tbo.obj.Metadata(); % metadata monitors key properties during the optimisation
            self.prop = struct();           % runtime properties may be used for implementations
            self.defaultConfig();
            
            self.obj = [];                  % the objective function (see tbo.obj.Objective)
            self.val = [];                  % the value-class (see tbo.obj.Value)
            self.log = tbo.logger();        % logging object
        end
        
        function self = setup(self,varargin)
            
            % parse inputs
            p = inputParser;
            v = @validateattributes;
            p.CaseSensitive = false;
            p.KeepUnmatched = true;
            
            p.addRequired( 'obj', @(x) v(x, {'function_handle','bt.obj.Objective'}, {}) );
            p.addOptional( 'val', @tbo.obj.Value, @(x) v(x, {'function_handle'}, {}) );
            
            p.parse(varargin{:});
            r = p.Results;
            
            % set members
            self.obj = tbo.objfun(r.obj);
            self.val = r.val;
            
            % forward options 
            self.setConfig( p.Unmatched );
            self.postSetup();
            
        end
        
        function self = run(self,varargin)
            
            % termination criterion
            crit = tbo.obj.Terminator(varargin{:});
            crit.offset_counters(self.meta);
            
            % initialise (counts as iter #1)
            if self.meta.niter == 0
                tstart = tic;
                dk.notify( self, 'BeforeInit' );
                self.runInit();
                dk.notify( self, 'AfterInit' );
                self.postIter( 'runtime', toc(tstart) );
            end
            
            % run optimisation
            while ~crit.stop(self.meta)
                
                tstart = tic;
                dk.notify( self, 'BeforeIter' );
                self.runIter(crit);
                dk.notify( self, 'AfterIter' );
                self.postIter( 'runtime', toc(tstart) );
                
            end
            
        end
        
        function [val,coord,eid] = result(self,nbest)
            
            if nargin < 2, nbest='best'; end
            
            % iterate all nodes of tessellation
            eid = self.tess.store.elem_ids();
            val = self.tess.iter( eid, @(x) [x.centre, x.value.value()] );
            val = vertcat(val{:});
            
            % convert text input
            switch lower(nbest)
                case 'best'
                    nbest = 1;
                case 'all'
                    nbest = numel(eid);
            end
            
            % sort decreasing and return
            [~,r] = sort(val(:,end-1));
            r = r(1:nbest);
            nd = self.tess.ndims;
            
            eid = eid(r);
            coord = val(r,1:nd);
            val = val(r,nd+1:end);
            
        end
        
        % i/o methods
        function s = save(self,file)
            s.type = 'tbo.obj.Optimiser';
            s.NAME = self.NAME;
            s.ver = '0.1';
            
            s.prop = self.saveMembers();
            s.prop.tess = self.tess.save();
            s.prop.meta = self.meta.to_struct();
            s.prop.prop = self.prop;
            s.prop.cfg = self.cfg;
            s.prop.log = self.log.saveState();
            
            if nargin > 1
                dk.save( file, s );
            end
        end
        
        function restore(self,s,varargin)
            if ischar(s), s = load(s); end
            
            [v,p] = bt.priv.restore(s,'tbo.obj.Optimiser');
            assert( strcmp(s.NAME,self.NAME), ...
                'Algorithm mismatch: %s (self) != %s (file)', self.NAME, s.NAME );
            
            % retrieve saved properties
            switch v
                case '0.1'
                    self.tess = bt.obj.Tessellation( p.tess );
                    self.meta = tbo.obj.Metadata( p.meta );
                    self.prop = p.prop;
                    self.cfg = p.cfg;
                    self.log.resetState(p.log);
                    self.loadMembers(p);
                otherwise
                    error( 'Unknown version: %s', v );
            end
            
            % reset objective, value-class and override options
            self.setup(varargin{:});
        end
        
        function saveobj(self)
            error([ 
                'Optimiser instances cannot be saved/loaded as regular objects.' newline ...
                'To save an Optimiser, use: obj.save( filename )' newline ...
                'To load an Optimiser, use: tbo.load( filename, objective, [valueclass] )' ...
            ]);
        end
        
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % helper functions
    % -----------------------------------------------------------------------------------------
    methods (Hidden) % can be overridden if needed
        
        function L = active_leaves(self,crit)
        %
        % Fetch all leaves satisfying non-terminal criteria MaxDepth and MinScale.
        %
            
            % get all leaves with their depth and scale
            L = self.tess.leaves();
            p = dk.mapfun( @(elm) [elm.depth; elm.scale], L, false );
            p = horzcat(p{:});
            
            % remove leaves that are too deep or too small
            keep = (p(1,:) < crit.MaxDepth) & (p(2,:) > crit.MinScale);
            L = L(keep);
            
        end
        
        function evaluate(self,elm)
        %
        % Evaluate a cell of elements, or a single element:
        %   - call objective function on denormalised centre
        %   - update elements' properties and call postEval
        %   - update metadata after all evaluations
        %
            
            elm = dk.wrap(elm);
            ctr = self.tess.centre(elm,true);
            ne = numel(elm);
            ev = cell(1,ne);
            
            for i = 1:ne
                
                Ei = elm{i};
                xi = ctr(i,:);
                
                if Ei.has_value()
                    continue;
                else
                    vi = self.obj.value(xi);
                end
                
                % record new evaluation(s)
                ev{i} = vi(:);
                try
                    Ei.value.append(vi);
                catch
                    Ei.value = self.val(vi);
                end
                self.postEval(Ei);
            
            end
            
            % update metadata
            ev = vertcat(ev{:});
            
            self.meta.update_eval(numel(ev),ev);
            self.tess.prior_disable(ctr);
            
        end
        
        function sub = subdivide(self,elm,d,full)
        %
        % subdivide(elm)
        %   Subdivide each leaf once.
        %
        % subdivide(elm,d)
        %   Ensure each element's offspring is d levels deep.
        %   Inputs can be non-leaf.
        %
        % subdivide(elm,d,true)
        %   Full subdivision of depth d for each input leaf.
        %   All inputs must be leaves.
        %
        % New children elements (and only new ones) are returned.
        %
        
            if nargin < 4, full=false; end
            if nargin < 3, d=0; end
            
            elm = dk.wrap(elm);
            if full
                [sub,ns] = self.tess.fullsub(elm,d);
            elseif d > 0
                [sub,ns] = self.tess.densoff(elm,d);
            else
                [sub,ns] = self.tess.subdivide(elm);
            end
            
            % initialise the value of all unshared subelements
            nc = numel(sub);
            for i = 1:nc
                if sub{i}.shared
                    % redundant (see Element::add_children), but kept for clarity
                    sub{i}.value = sub{i}.parent.value;
                else
                    sub{i}.value = self.val();
                end
            end
            
            % update metadata
            p = dk.mapfun( @(x) [x.depth, x.scale], sub, false );
            p = vertcat(p{:});
            self.meta.update_split( ns, p(:,1), p(:,2) );
            
        end
        
        % predictive sampling methods:
        %   - n points sampled randomly within each element;
        %   - optionally prepend centre of element (on 1st row);
        %   - Predictor called either for value or gradient.
        %
        function [m,s,x] = sample_value(self,E,n,inc_centre)
            if nargin < 4, inc_centre=true; end
            x = self.tess.sample(E,n,false);
            if inc_centre
            	x = [E.centre; x];
            end
            [m,s] = self.tess.value( E, x, true );
        end
        
        function [g,x] = sample_gradient(self,E,n,inc_centre)
            if nargin < 4, inc_centre=true; end
            x = self.tess.sample(E,n,false);
            if inc_centre
            	x = [E.centre; x];
            end
            g = self.tess.gradient( E, x, true );
        end
        
        % lookahead sampling methods:
        %   - offspring computed down to (relative) depth d
        %   - if n==1, take faircentre for each polytope (default)
        %   - if n>1, compute several instances of noisy centre
        %   - w can be set to modulate the sampling distribution
        %     default is 10 (near barycentre) and ignored if n==1
        %
        function x = lookahead(self,E,d,n,w)
            if nargin < 4, n=1; end
            if nargin < 5, w=10; end
            
            p = bt.part.adhoc( self.tess.part.type, E, d );
            if n > 1
                x = dk.mapfun( @(pp) pp.sample(n,w), p );
            else
                x = dk.mapfun( @(pp) pp.faircentre(), p );
            end
            x = vertcat(x{:});
        end
        
    end
    
end