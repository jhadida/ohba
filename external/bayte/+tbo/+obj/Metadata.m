classdef Metadata < handle
%
% Summary state of the optimiser during optimisation.
%
% JH

    properties (Hidden, Constant)
        proplst = {'best', 'neval', 'niter', 'nprior', 'depth', 'scale', ...
            'nsplit', 'nelem', 'dist', 'hist'};
    end

    properties
        best
        neval
        niter
        nprior
        depth
        scale
        nsplit
        nelem
    end
    
    properties (SetAccess = private)
        dist
        hist
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % core + i/o methods
    % -----------------------------------------------------------------------------------------
    methods
        
        function self = Metadata(varargin)
            self.clear();
            if nargin > 0
                self.restore(varargin{:});
            end
        end
        
        function clear(self)
            self.best = -inf;
            self.neval = 0;
            self.niter = 0;
            self.nprior = 0;
            self.depth = 0;
            self.scale = inf;
            self.nsplit = 0;
            self.nelem = 0;
            
            self.dist = tbo.util.SampleDistribution();
            self.hist = {};
        end
        
        function s = to_string(m)
            s = sprintf( '{iter: %d, depth: %d, scale: %g, elmts: %d, eval: %d, best: %g}', ...
                m.niter, m.depth, m.scale, m.nelem, m.neval, m.best );
        end
        
        function disp(self)
            disp(self.to_string());
        end
        
        function s = to_struct(self)
            s.type = 'tbo.obj.Metadata';
            s.ver = '0.1';
            
            f = self.proplst;
            n = numel(f);
            for i = 1:n
                s.prop.(f{i}) = self.(f{i});
            end
        end
        
        function self = restore(self,s)
            [ver,p] = bt.priv.restore(s,'tbo.obj.Metadata');
            switch ver
                case '0.1'
                    f = self.proplst;
                    n = numel(f);
                    for i = 1:n
                        fi = f{i};
                        self.(fi) = p.(fi);
                    end
                otherwise
                    error( 'Unknown version: %s', v );
            end
        end
        
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % main methods
    % -----------------------------------------------------------------------------------------
    methods
        
        % update metadata following an evaluation sequence
        function update_eval(self,ne,val)
            self.neval = self.neval + ne;
            self.best = max( [self.best; val(:)] );
            self.dist.append(val(:));
        end
        
        % update metadata following a subdivision sequence
        function update_split(self,ns,depth,scale)
            self.nsplit = self.nsplit + ns;
            self.depth = max( [self.depth; depth(:)] );
            self.scale = min( [self.scale; scale(:)] );
        end
        
        % increment iteration counter
        function inc_niter(self,ni)
            if nargin < 2, ni=1; end
            self.niter = self.niter + ni;
        end
        
        % record current state into history
        % additional key/val pairs are saved as field "extra"
        function data = record(self,varargin)
            f = setdiff( self.proplst, {'dist','hist'} );
            n = numel(f);
            data = struct();
            for i = 1:n
                fi = f{i};
                data.(fi) = self.(fi);
            end
            data.extra = dk.c2s(varargin{:});
            self.hist{end+1} = data;
        end
        
    end
    
end
