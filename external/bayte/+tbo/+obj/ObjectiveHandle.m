classdef ObjectiveHandle < tbo.obj.Objective
%
% Concrete implementation of tbo.obj.Objective when using a function handle.
% The function should accept calls:
%   y = fun(x)
%
% where x is either
%   a 1xd vector, with d is the dimensionality, 
%   or a nxd matrix, with n the number of points.
%
% If the function handle does not accept matrices, it should throw an error.
% The ObjectiveHandle detects such errors and iterates over individual points
% instead. 
% 
% JH

    properties
        fun
    end
    
    methods
        
        function self = ObjectiveHandle(fun)
            assert( dk.is.fhandle(fun), 'Input should be a function handle.' );
            self.fun = fun;
        end
        
        function y = value(self,x)
            try
                % use vectorisation if possible
                y = self.fun(x);
            catch
                % otherwise call several times
                n = size(x,1);
                y = nan(n,1);
                for i = 1:n
                    y(i) = self.fun(x(i,:));
                end
            end
        end
        
    end
    
end