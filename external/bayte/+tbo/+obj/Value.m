classdef Value < bt.abc.Value
%
% An implementation of value-class for optimisation.
%
%   eval    vector of objective evaluations
%   srgt    structure with fields {x,y}
%   util    scalar value updated manually for selecting elements
%
% JH

    properties
        eval
        srgt
        util
    end
    
    methods
        
        % --------------------------------------------------
        % general stuff
        
        function self = Value(varargin)
            if nargin > 0
                self.assign(varargin{:});
            else
                self.clear();
            end
        end
        
        function clear(self)
            self.eval = [];
            self.util = [];
            self.srgt = [];
        end
        
        % override isempty for ELement:has_value
        function e = isempty(self)
            e = isempty(self.eval);
        end
        
        function assign(self,v)
            if isa(v,'tbo.obj.Value')
                self.eval = v.eval;
                self.srgt = v.srgt;
                self.util = v.util;
            else
                self.clear();
                self.eval = v(:);
            end
        end
        
        function append(self,v)
            if self.isempty()
                self.assign(v);
            else
                self.eval = vertcat( self.eval, v(:) );
            end
        end
        
        
        % --------------------------------------------------
        % jQuery-style properties
        
        function [x,y] = surrogate(self,x,y)
            if nargin > 1
                assert( ismatrix(x) && ismatrix(y) && size(x,1)==size(y,1), 'Bad input.' );

                self.srgt.x = x;
                self.srgt.y = y;
            else
                x = self.srgt.x;
                y = self.srgt.y;
            end
        end
        
        function u = utility(self,u)
            if nargin > 1
                self.util = u;
            else
                u = self.util;
            end
        end
        
        
        % --------------------------------------------------
        % abstract methods
        
        function m = mean(self)
            m = mean(self.eval);
        end
        
        function s = std(self)
            s = std(self.eval);
        end
        
        function md = value(self)
            md = [ self.mean(), self.std() ];
        end
        
        % used to draw utilities
        function u = drawutil(self)
            if isempty(self.util)
                u = nan(1,2);
            else
                u = [self.util,0];
            end
        end
        
        
        % --------------------------------------------------
        % save/restore
        
        function s = saveobj(self)
            s.type = 'tbo.obj.Value';
            s.ver = '0.1';
            s.prop.eval = self.eval;
            s.prop.util = self.util;
            s.prop.srgt = self.srgt;
        end
        
    end
    
    methods (Static)
        
        function obj = loadobj(s)
            [ver,s] = bt.priv.restore(s,'tbo.obj.Value');
            switch ver
                case '0.1'
                    obj = tbo.obj.Value();
                    obj.eval = s.eval;
                    obj.util = s.util;
                    obj.srgt = s.srgt;
                otherwise
                    error( 'Unknown version: %s', ver );
            end
        end
        
    end
    
end

% function update_props(self,E)
% %
% % update_props(E)
% %   Set basic element properties using its value(s):
% %       nv  number of values
% %       tr  mean and std of values (1x2 vec)
% %       mw  deviation of sample mean
% %
% 
%     % sample mean/std of values
%     mu = mean(E.value);
%     sigma = std(E.value);
%     E.prop.tr = [ mu, sigma ];
% 
%     % deviation of sample mean distribution
%     nv = numel(E.value);
%     mw = sigma / sqrt(nv);
%     E.prop.nv = nv;
%     E.prop.mw = mw;
% 
% end