classdef Terminator < handle
    
    properties
        EvalCount
        IterCount
        SplitCount
        
        % non-terminal
        MinScale
        MaxDepth
    end
    
    properties (Hidden)
        log
    end
    
    methods
        
        function self = Terminator(varargin)
            
            self.log = tbo.logger();
            
            % process inputs and apply defaults
            arg = dk.unwrap(varargin{:});
            if isempty(arg)
                error( 'Input(s) required.' );
            elseif isnumeric(arg)
                arg = { 'EvalCount', arg };
            end
            arg = dk.obj.kwArgs(arg{:});
            
            % assign member properties
            self.EvalCount = arg.get( 'EvalCount', [] );
            self.IterCount = arg.get( 'IterCount', [] );
            self.SplitCount = arg.get( 'SplitCount', [] );
            self.MinScale = arg.get( 'MinScale', 0 );
            self.MaxDepth = arg.get( 'MaxDepth', Inf );
            
            arg.restrict();
            
            % validate properties
            assert( self.has('EvalCount') || self.has('IterCount') || self.has('SplitCount'), ...
                'Missing required criterion.' );
            
        end
        
        function self = offset_counters(self,meta)
            self.EvalCount = offcount( self.EvalCount, meta.neval );
            self.IterCount = offcount( self.IterCount, meta.niter );
            self.SplitCount = offcount( self.SplitCount, meta.nsplit );
        end
        
        function yes = stop(self,meta)
            
            self.log.trace('stop_START');
            yes = false;
            
            if ~yes && self.has('EvalCount')
                yes = meta.neval >= self.EvalCount;
                if yes, self.log.debug( 'Termination: EvalCount' ); end
            end
            
            if ~yes && self.has('IterCount')
                yes = meta.niter >= self.IterCount;
                if yes, self.log.debug( 'Termination: IterCount' ); end
            end
            
            if ~yes && self.has('SplitCount')
                yes = meta.nsplit >= self.SplitCount;
                if yes, self.log.debug( 'Termination: SplitCount' ); end
            end
            
            self.log.trace('stop_END');
            
        end
        
    end
    
    methods (Hidden)
        
        function y = has(self,name)
            y = ~isempty(self.(name));
        end
        
        function ok = check_posnum(self,name)
            ok = false;
            if self.has(name)
                self.(name) = floor(self.(name));
                assert( self.(name) >= 0, '% should be positive.', name );
                ok = true;
            end
        end
        
    end
    
end

% -----------------------------------------------------------------------------------------

function cnt = offcount(cnt,off)
%
% Offset counters of termination criteria using current metadata.
% This is to allow a more natural parameterisation of the run() method.
%
% Specifying an absolute value for a particular counter (i.e. without offset) 
% can be done using negative numbers instead of positive ones.
%

    if isempty(cnt), return; end
    if cnt < 0
        cnt = -cnt;
    else
        cnt = cnt + off;
    end
    
end
