function opt = new( fun, dom, algo, par, pre, varargin )
%
% opt = tbo.new( fun, dom, algo, par, gp, [options...] )
%
%
% Create an instance of tbo.obj.Optimiser with specified algorithm implementation.
% 
%   !THIS DOES NOT RUN OPTIMISATION!
%   To do so, type: 
%       opt.run( EvalCount )                or
%       opt.run( 'field', value, ... )      where valid fields are:
%
%            Field:      Terminal?    Description
%            ------      ---------    -----------
%
%           EvalCount       yes       Max number of objective evaluations
%           IterCount       yes       Max number of iterations
%           SplitCount      yes       Max number of element split
%           MinScale         no       Ignore elements smaller than.. (normalised units)
%           MaxDepth         no       Ignore elements deeper than..
%           
%
% INPUTS
% ------
%
%    fun    Objective function
%           1. Function handle: 
%               @(x) f(x)                               (in: vector, out: scalar)
%           3. Instance of tbo.obj.Objective                     (see tbo.objfun)
%           3. Instance of tbo.obj.Objective                     (see tbo.objfun)
%
%    dom    Optimisation domain (open interval for each param)
%               [ min_x1   min_x2   min_x3 ... ;
%                 max_x1   max_x2   max_x3 ...  ]
%
%   algo    Name of optimisation algorithm
%           See: tbo.algo.factory
%
%    par    Partition function
%           1. String for partition name                    (see bt.part.factory)
%           2. Cell: 
%               { name, options... }                       (see bt.abc.Partition)
%           3. Instance of bt.abc.Partition                       (see bt.part.*)
%
%    pre    Prediction function
%           1. String for *kernel* name                      (see bt.ker.factory)
%           2. Cell:
%               { name, options... }                       (see bt.abc.Predictor)
%           3. Instance of bt.abc.Predictor                       (see bt.pred.*)
%
%    ...    Options defined by concrete algorithm implementations
%           See tbo.algo.*
%
%
% MORE INFO
% ---------
%
%   Logging verbose level can be adjusted and/or streamed to a file.
%   See: dk.logger.Logger 
%
%
% See also: bt.algo.select
%
% JH

    if nargin < 5 || isempty(pre), pre='default'; end

    tess = bt.tess( par, dom, pre );    
    opt = tbo.algo.factory( algo, tess, fun, varargin{:} );
    
end