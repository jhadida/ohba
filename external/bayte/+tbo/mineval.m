function [n,p] = mineval( par, ndim, fac )
% 
% [n,p] = tbo.mineval( par, ndim, fac=100 )
%
% Estimate minimum number of evaluations for reducing the size of the 
% search-space by a given factor, for a given partitioning method and 
% dimensionality. The number returned is a lower bound; multiply that 
% number by 10-100 to stand a reasonable chance of finding an optimum 
% in your problem.
%
% Second output p is the corresponding minimum depth, and can be used
% to set the optimiser's MaxDepth setting (see also MinScale).
%
% JH

    if nargin < 3, fac=100; end
    
    % select partitioning method
    switch lower(par)
        case {'t','tern','ternary'}
            count = @count_ternary;
            minev = @minev_ternary;
        case {'b','bary','barycentric'}
            count = @count_barycentric;
            minev = @minev_barycentric;
        case {'s','site','simplicial'}
            count = @count_simplicial;
            minev = @minev_simplicial;
        otherwise
            error( 'Unknown partition: %s', par );
    end
    
    % compute minimum number of evaluations
    p = find_p( count, ndim, fac );
    n = minev( p, ndim );
    
%     n = 100;
%     c = zeros(1,n);
%     for p = 1:n
%         c(p) = count(p,ndim);
%     end
%     plot(c);
    
end

function p = find_p(fun,ndim,fac)
    b = ndim*log(fac);
    p = 1;
    while fun(p,ndim) < b
        p = p+1;
    end
end

function [q,r] = divmod(a,b)
    q = fix(a/b);
    r = rem(a,b);
end

% --------------------------------------------------------------------------------
% --------------------------------------------------------------------------------
%
% log-counts of elements after p steps of subdivision:

function c = count_ternary(p,d)
    c = p*log(3);
end

function c = count_barybox(p,d)
    assert( p <= d, 'Barycentric subdivision on d-box cannot exceed d steps.' );
    c = p*log(2) + sum(log(d-p+1:d));
end

function c = count_barycentric(p,d)
    [q,r] = divmod(p,d);
    if q == 0
        c = count_barybox(r,d);
    else
        c = count_barybox(d,d) + (q-1)*sum(log(2:d+1)) + sum(log(d+2-r:d+1));
    end
end

function c = count_simplicial(p,d)
    [q,r] = divmod(p,d);
    if q == 0
        c = count_barybox(r,d);
    else
        c = (d-2)*log(2) + sum(log(2:d+2)) - log(6);
        c = count_barybox(d,d) + (q-1)*c + (p-1)*log(2) + sum(log(d+3-r:d+2));
    end
end

% --------------------------------------------------------------------------------
% --------------------------------------------------------------------------------
%
% minimum number of evaluations after p steps of subdivision:

function n = minev_ternary(p,d)
    n = 3*p;
end

function n = minev_barybox(p,d)
    assert( p <= d, 'Barycentric subdivision on d-box cannot exceed d steps.' );
    n = p*(2*d-p+1);
end

function n = minev_barycentric(p,d)
    [q,r] = divmod(p,d);
    if q == 0
        n = minev_barybox(r,d);
    else
        n = minev_barybox(d,d) + (q-1)*d*(d+3)/2 + r*(2*d-r+3)/2;
    end
end

function n = minev_simplicial(p,d)
    [q,r] = divmod(p,d);
    if q == 0
        n = minev_barybox(r,d);
    else
        n = d*(d+2) + 4*(d-1);
        n = minev_barybox(d,d) + (q-1)*n + r*(2*d-r+5) - (d+2);
    end
end
