classdef Base < tbo.obj.Optimiser
%
% Requires configuration option "idepth" to be defined.
%

    methods (Abstract,Hidden)
        
        % initialise sample distribution and acquisition strategy
        initMembers(self);
        
        % update surrogate predictions for each element
        predict(self,elm);
        
        % select a subset of elements using the acquisition strategy
        sel = acquire(self,elm);
        
    end
    
    methods (Hidden)
        
        function p = saveMembers(self)
            % serialise subdfields, prop will be saved later on
            self.prop.acq = self.prop.acq.to_struct();
            p = struct();
        end
        
        function loadMembers(self,p)
            % restore subfields
            self.prop.acq = tbo.acq.factory(p.prop.acq);
        end
        
        function runInit(self)
            
            assert( self.tess.nelem == 1, 'Tessellation should only have a root element.' );
            
            % helper objects
            self.initMembers();
            
            % initial full subdivision
            R = self.tess.root;
            C = self.subdivide( R, self.getOption('idepth',1), true );
            E = [ {R}, C ];
            
            self.evaluate(E);
            self.predict(E);
            
        end
        
        function runIter(self,crit)
            
            % select amongst active leaves
            L = self.active_leaves(crit);
            S = self.acquire(L);
            
            % subdivide these elements and evaluate their children
            C = self.subdivide(S);
            self.evaluate(C);
            
            % fetch all affected neighbours to update their utility
            n = numel(C);
            r = self.tess.pred.opt.brad;
            B = cell(1,n);
            
            for i = 1:n
                B{i} = C{i}.blanket(r,true);
            end
            B = bt.priv.uniqid(horzcat( B{:} ));
            self.predict([B,S]);
            
        end
        
    end
    
end