function obj = factory(name,varargin)
%
% Match algorithm by name, and return instance of tbo.algo.*
%
% All these instance should derive from tbo.obj.Optimiser, meaning
% that they should be capable of running optimisation.
%
%
%  Name    Class            Acquisition     Description
%  ----    -----            -----------     -----------
%
%  tis1    tbo.algo.TIS1    Sampling        Tessellated importance sampling.
%  tis2    tbo.algo.TIS2    Sampling        Modified TIS with Lipschitz boosts.
%
%  ucb1    tbo.algo.UCB1    Traversal       Upper-confidence bound.
%  ucb1r   tbo.algo.UCB1r   Sampling        Same as UCB1 with random sampling.
%
%  ei1     tbo.algo.EI1     Traversal       Expected improvement.
%  ei2     tbo.algo.EI2     Traversal       Modified EI combining local+global estimates.
%  ei2r    tbo.algo.EI2r    Sampling        Same as EI2 with random sampling.
%
%
% See also: tbo.obj.Optimiser, tbo.algo.Base
%
% JH

    switch lower(name)
        case 'tis1'
            obj = tbo.algo.TIS1(varargin{:});
        case 'tis2'
            obj = tbo.algo.TIS2(varargin{:});
        case 'ucb1'
            obj = tbo.algo.UCB1(varargin{:});
        case 'ei1'
            obj = tbo.algo.EI1(varargin{:});
        case 'ei2'
            obj = tbo.algo.EI2(varargin{:});
        otherwise
            error('Unknown algo: %s',name)
    end

end
