function opt = demo(fname,varargin)
%
% opt = tbo.demo( fun, algo, par, gp, [options...] );
%
% Run optimisation on one of the test-functions in bt.fun.*
% 
%
% EXAMPLE:
%
%   opt = tbo.demo( 'peaks', 'ei2', 'site', 'mat32', 'nsample', 30 ); 
%   opt.run('EvalCount',50,'MaxDepth',10);
%
%
% See also: tbo.new
%
% JH

    GRID_SIZE = [100,100];
    SHOW_UTIL = false; % uncomment EI:predict if true


    % select test function
    F = bt.fun.factory(fname);
    
    % build optimiser
    if nargin > 2
        opt = tbo.new( F.fun, F.dom(:)*[1,1], varargin{:} );
    else
        opt = varargin{1};
    end
    
    % create grid for display
    gsz = GRID_SIZE;
    xr = [ F.dom, gsz(1) ];
    yr = [ F.dom, gsz(2) ];
    grd = dk.obj.Grid( xr, yr );
    [Vref,xq] = grd.eval( F.fun );
    
    % attach display function to events
    addlistener( opt, 'AfterInit', @callback );
    addlistener( opt, 'AfterIter', @callback );
    
    % change colours at night
    h = hour(datetime());
    if h >= 21 || h < 7
        figbg = (60 + [5,10,0])/255;
        cmap = 0.75*jet(128);
    else
        figbg = (210 + [0,10,0])/255;
        cmap = jet(128);
    end
    
    % display function
    fig = nan;
    function callback(src,~)
        
        % generate predicted map
        Member = bt.priv.tess_map2d(src.tess,gsz);
        Vpred = bt.priv.tess_pred2d(src.tess,Member,xq);
        if SHOW_UTIL
            Vutil = bt.priv.tess_pred2d(src.tess,Member,xq,false,'drawutil');
        end

        % create figure
        if ~ishandle(fig)
            fig_title = sprintf( 'TBO demo - %s (%s)', F.name, src.NAME );
            fig = dk.fig.new( fig_title, 0.75, [], 'Color', figbg );
            set( datacursormode(fig), 'UpdateFcn', @tooltip );
            colormap(cmap);
        else
            figure(fig); % select the right figure
        end
        
        subplot(1,2,1);
        h = grd.image( Vpred, F.val ); hold on;
        draw_tessellation( src.tess ); hold off;
        title(sprintf('Tessellated surrogate (iter %d)', src.meta.niter));
        h.UserData.fun = F.fun;
        
        subplot(1,2,2);
        if SHOW_UTIL
            ca = ant.img.range(Vutil);
            h = grd.surface( Vpred, Vutil, ca, 'EdgeAlpha', 0.1 );
        else
            h = grd.surface( Vref, Vref-Vpred, F.err, 'EdgeAlpha', 0.1 );
        end
        %cb = colorbar(gca); cb.Label.String = 'Regret (Truth - Predicted)';
        title( 'True objective & regret' ); 
        h.UserData.fun = F.fun;
        
        pause(0.8);

    end

end

function draw_tessellation(T)
    
    % draw leaves' boundary
    L = T.leaves();
    P = T.to_polytope(L);
    n = numel(L);
    
    for i = 1:n
        P{i}.plot( 'line', [0,0,0] );
    end
    hold on;
    
    % draw evaluated centres
    k = T.store.eval_ids();
    x = T.centre(k);
    h = dk.ui.plot( x, 'k*', 'MarkerSize', 5 );
    
    h.UserData.id = k;
    h.UserData.pts = x;
    
    % draw non-evaluated centres
    k = setdiff( T.store.elem_ids(), k );
    if isempty(k), return; end
    
    x = T.centre(k);
    h = dk.ui.plot( x, 'kd', 'MarkerSize', 5 );
    
    h.UserData.id = k;
    h.UserData.pts = x;
    
end

function txt = tooltip(~,evt)
    try
        pos = evt.Position;
        dat = evt.Target.UserData;
        
        % if the target is a point, print its ID
        % see how draw_tessellation() defined fields {pts,id}
        if isfield(dat,'pts')
            
            m = ant.mex.matchrow( dat.pts, pos, 1e-3 );
            if m > 0
                txt = { sprintf('ID %d',dat.id(m)); };
            else
                txt = { 'none' };
            end
            
        % otherwise print value of underlying image
        else
            
            v = dat.fun(pos);
            txt = { sprintf('Value: %g',v); };
            
        end
            
    catch err
        txt = { 'error' };
        warning( err.message );
    end
end