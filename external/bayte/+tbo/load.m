function obj = load(file, varargin)
%
% Load saved instance of Optimiser.
%

    obj = load(file);
    obj = tbo.algo.factory( obj.NAME, obj, varargin{:} );

end
