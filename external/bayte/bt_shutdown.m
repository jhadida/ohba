function bt_shutdown()

    root = getenv('BT_ROOT');
    if dk.fs.isdir(root)
        setenv( 'BT_ROOT', '' );
        dk.print( '[BT] Shutting down from folder "%s".', root );
    else
        warning( 'Bayte does not appear to have started.' );
    end

end
