function obj = from_elem( varargin )
%
% obj = bt.poly.from_elem( elm, tfun=[] )
% obj = bt.poly.from_elem( type, shape, store, tfun=[] )
% 
% Unpack shape structure stored in Element instance into a Polytope instance.
% Optionally apply transformation to coordinates (see bt.abc.Polytope:transform).
%
% JH

    % deconstruct input elements
    if nargin < 3
        elm = varargin{1};
        assert( isa(elm,'bt.obj.Element'), 'Bad input type.' );
        obj = bt.poly.from_elem( elm.type, elm.shape, elm.store, varargin{2:end} );
        return;
    end
    
    % validate inputs
    [type,shape,store] = deal(varargin{1:3});
    assert( ischar(type) && isstruct(shape) && isa(store,'bt.obj.Storage'), 'Bad input types.' );
    
    % unpack polytope shape to restore coordinates
    switch lower(type)
        case 'simplex'
            unpack = @bt.poly.Simplex.unpack;
        case 'subbox'
            unpack = @bt.poly.Subbox.unpack;
        case 'subcore'
            unpack = @bt.poly.Subcore.unpack;
        otherwise
            error( 'Unknown polytope type: %s', type );
    end
    obj = unpack( shape, store );
    
    % apply optional transformation
    if nargin > 3
        obj.transform( varargin{4} );
    end

end