classdef Subcore < bt.abc.Polytope
%
% A subcore is a convex polytope composed of:
%   the midpoints of an embedded simplex (not fully connected)
%   a set of intermediary barycentres
%
% JH

    properties (Constant)
        type = 'subcore';
    end
    
    % -----------------------------------------------------------------------------------------
    % static methods
    % -----------------------------------------------------------------------------------------
    methods (Static,Hidden)
        
        % throw error if structure is not a subcore
        function check(x)
            assert( dk.is.struct( x, {'s','b'} ), 'Missing required shape fields.' );
            
            [rs,cs] = size(x.s);
            [rb,cb] = size(x.b);
            assert( (rs+rb)==cs+1 && (rb==0 || cb==cs), 'Bad subcore shape.' );
        end
        
        function x = make(s,b)
        %
        % s: enclosing simplex
        % b: intermediary barycentres
        
            if nargin < 2, b=[]; end
            x.s = s;
            x.b = b;
        end
        
        % insert vertices into storage
        function p = pack(x,store)
            try
                p = x.shape;
            catch
                p = x;
            end
            bt.poly.Subcore.check(p);
            p.s = store.insert(p.s);
            p.b = store.insert(p.b);
            warning( 'Storing Subcores manually is not recommended; ensure midpoints are inserted in the Partition.' );
        end
        
        % convert vertex indices to coordinates
        function p = unpack(x,store)
            assert( bt.priv.vecfields(x,'s','b'), 'Shape is not packed.' );
            x.s = store.vert(x.s);
            x.b = store.vert(x.b);
            p = bt.poly.Subcore(x);
        end
        
    end

    % -----------------------------------------------------------------------------------------
    % instance methods
    % -----------------------------------------------------------------------------------------
    methods

        function self = Subcore(varargin)
            assert( nargin >= 1, 'Not enough inputs.' );
            if isstruct(varargin{1})
                self.assign(varargin{1});
            else
                self.assign(self.make(varargin{:}));
            end
        end
        
        % number of midpoints
        function n = nmidp(self)
            n = size(self.shape.s,1); % NOTE: n = d+1 vertices..
            n = n*(n-1)/2;
        end
        

        function d = ndims(self)
            d = size(self.shape.s,2);
        end

        function c = barycentre(self)
            c = sum(self.shape.s,1);
            nm = self.nmidp();
            
            ns = size(self.shape.s,1);
            nb = size(self.shape.b,1);

            c = (c/ns + sum(self.shape.b,1)/nm) / (1 + nb/nm);
        end

        function c = faircentre(self)
            c = sum(self.shape.s,1);
            ns = size(self.shape.s,1);
            nb = size(self.shape.b,1);

            c = (c + sum(self.shape.b,1)) / (ns + nb);
        end
        
        function x = noisycentre(self,w)
            if nargin < 2, w=25; end
            x = self.sample(1,w);
        end

        function x = sample(self,n,w)
        %
        % This is a bit heavy (quadratic time+storage in dimension), but no incentive to improve
        % for now; we sample from a Dirichlet distribution with as many dimensions as there are
        % of vertices in total (number of midpoints is the bottleneck).

            if nargin < 3, w=1; end
        
            ns = size( self.shape.s, 1 );
            nb = size( self.shape.b, 1 );
            nm = self.nmidp();
            np = nm + nb;

            s = ant.math.dirichlet( w*ones(1,np), n );
            x = s(:,1:nb) * self.shape.b;
            s = s(:,nb+1:end);

            p = bt.priv.pairs( 1:ns );
            x = x + s * ( self.shape.s(p(:,1),:) + self.shape.s(p(:,2),:) )/2;

        end

        % approximate distance from centre to vertices
        function s = scale(self)
            c = self.centre();
            u = self.shape.s(1:end-1,:) + self.shape.s(2:end,:); % don't compute all midpoints
            u = vertcat( u/2, self.shape.b );
            u = dk.bsx.sub( u, c );
            u = sqrt(dot( u, u, 2 ));
            s = ant.mean(u);
        end
        function s = scales(self)
            c = self.centre();
            u = self.shape.s(1:end-1,:) + self.shape.s(2:end,:); % don't compute all midpoints
            u = vertcat( u/2, self.shape.b );
            u = dk.bsx.sub( u, c );
            s = ant.mean(abs(u));
        end
        
        % apply function to all vertex coordinates
        function self = transform(self,fun)
            self.shape.s = fun( self.shape.s );
            self.shape.b = fun( self.shape.b );
        end

        % plot subcore
        function plot(self,type,col,varargin)
            
            if nargin < 3, col=0.2*[1,1,1]; end
            if nargin < 2, type='line'; end
            
            % make output
            d = self.ndims();
            p = bt.obj.Drawing( self.ndims(), 6 );
            
            % draw
            hold on;
            switch d
            case 2
                error('There are no subcores in dimension 2.');
            case 3
                
                % list v-facets and f-facets
                % in 3D, both f- and v-facets are simplices
                x = self.shape.s;
                V = cell(1,4);
                F = cell(1,4);
                for i = 1:4
                    cur = i;
                    oth = [1:i-1,i+1:4];
                    y = x(oth,:);
                    V{i} = dk.bsx.add( y, x(cur,:) )/2;
                    F{i} = (y + circshift(y,-1))/2;
                end
                
                % the facets are defined with the same vertices
                %   - identify unique vertices
                %   - then encode all pairs of vertices within each facet
                x = vertcat( V{:}, F{:} );
                [L,U] = ant.mex.labelrows(x);
                n = size(x,1);
                g = [ 1,2; 2,3; 3,1 ];
                E = zeros(n,1);
                for i = 1:3:n
                    k = i + (0:2);
                    E(k) = ant.math.sympair( L(k(g(:,1))), L(k(g(:,2))) );
                end
                [i,j] = ant.math.symunpair(unique(E));
                
                switch lower(type)
                case 'line'
                    
                    % iterate over edges to draw
                    n = numel(i);
                    col = p.reset( n, col );
                    for k = 1:n
                        y = [ x(U(i(k)),:); x(U(j(k)),:) ];
                        p.gobj(i) = dk.ui.plot( y, 'Color', col(k,:), varargin{:} ); 
                        %hold on;
                    end
                    
                case 'patch'
                    
                    % iterate over facets
                    if size(col,1) == 2
                        col = col([1,1,1,1,2,2,2,2],:);
                    end
                    col = p.reset( 8, col );
                    for k = 1:4
                        y = V{k}([1,2,3,1],:);
                        p.gobj(k) = dk.ui.fill( y, col(k,:), varargin{:} ); 
                        %hold on;
                        
                        y = F{k}([1,2,3,1],:);
                        p.gobj(k+4) = dk.ui.fill( y, col(k+4,:), varargin{:} );
                    end
                    
                otherwise
                    error('Unknown plot type: %s',type);
                end
                
            otherwise
                error('Cannot plot in dimension >3.');
            end
            hold off;
            
        end
        
    end

end
