function x = unit_box( d, L )
%
% x = unit_box( d, L )
%
% Create a d-dimensional box with side-lengths L.
% If L is scalar, it specifies the length for all sides.
% L defaults to ones(1,d).
%
% JH

    if nargin < 2, L = ones(1,d); end

    if isscalar(L), L = L*ones(1,d); end
    L = L(:)';
    assert( numel(L) == d, 'Bad number of lengths' );

    x = bt.poly.Subbox( zeros(1,d), L );

end
