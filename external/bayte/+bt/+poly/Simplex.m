classdef Simplex < bt.abc.Polytope
%
% A simplex in dimension D is a set of D+1 points not embedded in
% some lower-dimensional space. For the purpose of subdivision, the
% vertices are split into two sets:
%   a "vertex" set corresponding to the "original" vertices
%   a "barycentre" set corresonding to the "intermediary" barycentres
%
   
    properties (Constant)
        type = 'simplex';
    end

    
    methods (Static,Hidden)
        
        % throw error if structure is not a simplex
        function check(x)
            assert( dk.is.struct( x, {'v','b'} ), 'Missing required fields.' );
            
            [~,d] = size(x.v);
            s = size(x.v,1) + size(x.b,1);
            r = vertcat( x.v, x.b );
            r = rank(dk.bsx.sub( r(2:end,:), r(1,:) ));

            assert( s == d+1, 'Bad dimensionality.' );
            assert( r == d, 'Bad rank.' );
        end
        
        function s = make(v,b)
        %
        % v: vertices
        % b: intermediary barycentres
        
            if nargin < 2, b=[]; end
            s.v = v;
            s.b = b;
        end
        
        % insert vertices into storage
        function p = pack(x,store)
            try
                p = x.shape;
            catch
                p = x;
            end
            bt.poly.Simplex.check(p);
            p.v = store.insert(p.v);
            p.b = store.insert(p.b);
        end
        
        % convert vertex indices to coordinates
        function p = unpack(s,store)
            assert( bt.priv.vecfields(s,'v','b'), 'Shape is not packed.' );
            s.v = store.vert(s.v);
            s.b = store.vert(s.b);
            p = bt.poly.Simplex(s);
        end
        
    end
    
    
    methods

        function self = Simplex( varargin )
            assert( nargin >= 1, 'Not enough inputs.' );
            if isstruct(varargin{1})
                self.assign(varargin{1});
            else
                self.assign(self.make(varargin{:}));
            end
        end
        
        function v = vertices(self)
            v = vertcat( self.shape.v, self.shape.b );
        end
        

        function d = ndims(self)
            d = max( size(self.shape.v,2), size(self.shape.b,2) );
        end

        function x = barycentre(self)
            x = ant.mean(self.vertices());
        end
        
        function x = faircentre(self)
            x = self.barycentre();
        end
        
        function x = noisycentre(self,w)
            if nargin < 2, w=25; end
            x = self.sample(1,w);
        end

        % normalised positive weighted sum of vertices
        %
        % NOTE: set w to a large value (eg 25) to sample near centre
        function x = sample(self,n,w)
            if nargin < 3, w=1; end
            x = bt.priv.dcomb( self.vertices(), n, w );
        end

        % average distance from barycentre to vertices
        function s = scale(self)
            s = self.vertices();
            s = ant.pts.sqdist( s, ant.mean(s) );
            s = ant.mean(sqrt(s));
        end
        function s = scales(self)
            s = self.vertices();
            s = dk.bsx.sub( s, ant.mean(s) );
            s = ant.mean(abs(s));
        end
        
        function r = eqbr(self)
            v = self.vertices;
            v = dk.bsx.sub( v(2:end,:), v(1,:) );
            d = size(v,2);
            
            [~,U] = lu(v);
            r = sum(log(abs(diag(U)))) - gammaln( (d+1)/2 ); % log-determinant
            r = exp(r/d - log(2) - (d-1)*log(pi)/(2*d));
        end
        
        % apply function to all vertex coordinates
        function self = transform(self,fun)
            self.shape.v = fun( self.shape.v );
            self.shape.b = fun( self.shape.b );
        end
        
        % plot simplex
        function p = plot(self,type,col,varargin)
            
            if nargin < 3, col=0.2*[1,1,1]; end
            if nargin < 2, type='line'; end
            
            % make output
            V = self.vertices();
            d = size(V,2);
            p = bt.obj.Drawing( size(V,2), size(V,1) );
            
            % draw
            hold on;
            switch d
            case 2
                
                switch lower(type)
                case 'line'
                    col = p.reset( 3, col );
                    for i = 1:3
                        x = [1:i-1, i+1:3];
                        x = V(x,:);
                        p.gobj(i) = dk.ui.plot( x, 'Color', col(i,:), varargin{:} );
                        %hold on;
                    end
                case 'patch'
                    x = [ V; V(1,:) ];
                    col = p.reset( 1, col );
                    p.gobj = dk.ui.fill( x, col, varargin{:} );
                otherwise
                    error('Unknown plot type: %s',type);
                end
                
            case 3
                
                switch lower(type)
                case 'line'
                    q = bt.priv.pairs( 1:4 );
                    n = size(q,1);
                    col = p.reset( n, col );
                    for i = 1:n
                        x = V(q(i,:),:);
                        p.gobj(i) = dk.ui.plot( x, 'Color', col(i,:), varargin{:} );
                        %hold on;
                    end
                case 'patch'
                    col = p.reset( 4, col );
                    for i = 1:4
                        x = [1:i-1, i+1:4];
                        x = V(x,:);
                        x = [x; x(1,:)];
                        p.gobj(i) = dk.ui.fill( x, col(i,:), varargin{:} );
                        %hold on;
                    end
                otherwise
                    error('Unknown plot type: %s',type);
                end
                
            otherwise
                error('Cannot plot in dimension >3.');
            end
            hold off;
            
        end

    end

end
