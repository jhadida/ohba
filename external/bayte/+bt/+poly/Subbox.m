classdef Subbox < bt.abc.Polytope
%
% A box can be described by a lower and upper bounds.
%
% The number of coordinates determines the dimensionality.
% The barycenter is the mean of the bounds.
% Sampling can be done by linear interpolation.
%
% A sub-box is a polytope composed of:
%    an embedded box
%    a set of fully-connected vertices
%

    properties (Constant)
        type = 'subbox';
    end
    
    
    methods (Static,Hidden)
        
        % throw error if structure is not a subbox
        function check(x)
            assert( dk.is.struct( x, {'lo','up','b'} ), 'Missing required shape fields.' );
            
            b = size(x.b,1);
            d = numel(x.lo);
            p = nnz( x.lo ~= x.up );

            assert( numel(x.up)==d, 'Bound dimensions mismatch.' );
            assert( all(x.lo <= x.up), 'Lower bound should be <= uppper bound.' );
            assert( b == d-p, 'Issue with p-box size.' );
        end
        
        function s = make(lo,up,b)
        %
        % lo: lower-bound
        % up: upper-bound
        %  b: intermediary barycentres
        
            if nargin < 3, b=[]; end
            s.lo = lo(:)';
            s.up = up(:)';
            s.b = b;
        end
        
        % insert vertices into storage
        function p = pack(x,store)
            try
                p = x.shape;
            catch
                p = x;
            end
            bt.poly.Subbox.check(p);
            p.lo = store.insert(p.lo);
            p.up = store.insert(p.up);
            p.b = store.insert(p.b);
        end
        
        % convert vertex indices to coordinates
        function p = unpack(s,store)
            assert( bt.priv.vecfields(s,'lo','up','b'), 'Shape is not packed.' );
            s.lo = store.vert(s.lo);
            s.up = store.vert(s.up);
            s.b = store.vert(s.b);
            p = bt.poly.Subbox(s);
        end
        
    end

    
    % -----------------------------------------------------------------------------------------
    % instance methods
    % -----------------------------------------------------------------------------------------
    methods

        function self = Subbox(varargin)
            assert( nargin >= 1, 'Not enough inputs.' );
            if isstruct(varargin{1})
                self.assign(varargin{1});
            else
                self.assign(self.make(varargin{:}));
            end
        end

        function d = delta(self)
            d = self.shape.up - self.shape.lo;
        end

        % a subbox is a cuboid if there are no intermediary barycentres
        function y = is_cuboid(self)
            y = isempty( self.shape.b );
        end
        
        % an embedded subbox has a p-box base with p < d
        function y = is_embedded(self)
            y = ~self.is_cuboid();
        end
        
        
        function [d,p] = ndims(self)
        %
        % WARNING:
        % This way of calculating p only works if the box is aligned with canonical axes.
        % I.e. this relies on the assumption that the only box encountered is the original 
        % normalised space (0,1)^d.
        
            d = numel(self.shape.lo);
            p = nnz( self.shape.lo ~= self.shape.up ); % p-box dimension
        end

        function b = barycentre(self)
            [d,p] = self.ndims();
            
            b = (self.shape.lo + self.shape.up)/2;
            if p < d
                u = ant.mean(self.shape.b);
                f = exp( log(d-p) - p*log(2) );
                b = (f*u + b)/(1+f);
            end
        end

        function r = faircentre(self)
            [d,p] = self.ndims();
            
            r = (self.shape.lo + self.shape.up)/2;
            if p < d
                u = ant.mean(self.shape.b);
                r = (p*r + (d-p)*u)/d;
            end
        end
        
        function x = noisycentre(self,w)
            if nargin < 2, w=25; end
            x = self.sample(1,w,true);
        end

        function x = sample(self,n,w,heuristic)
        %
        % Sample a point randomly within the p-box
        % Then form a polytope with the remaining vertices,
        % and pick a point at random within it.
        %
        % The heuristic input controls the Dirichlet weight of the embedded p-box.
        %
        % If heuristic=false (default), the weight is set "correctly" according to
        % the number of vertices on the p-box (2^p). This yields a nearly uniform
        % sampling of the polytope, with an arbitrarily high density near the p-box
        % as the dimensionality increases.
        %
        % If heuristic=true, the weight is set to p*log(2) instead, in an attempt
        % to distribute samples in a representative subvolume of the polytope.
        %
        
            if nargin < 3 || isempty(w), w=1; end
            if nargin < 4, heuristic=false; end

            [d,p] = self.ndims();

            % trick to get Dirichlet combinations to work with box extents
            % without having to care about p<d (less efficient, but much clearer..)
            x = bt.priv.dcomb_box( self.shape.lo, self.shape.up, n, w );
            
            if p < d
                a = ones(1,d-p+1);
                assert( p >= 1, '[bug] Sanity check' );
                
                % upweight p-box intermediary
                if heuristic
                    a(1) = 2*p;
                else
                    a(1) = 2^p; 
                end
                
                r = ant.math.dirichlet( w*a, n );
                u = r(:,2:end) * self.shape.b;
                x = dk.bsx.mul( r(:,1), x ) + u;
            end
            
        end

        % approximate distance from centre to vertices
        function s = scale(self)
            c = self.barycentre();
            u = vertcat( self.shape.up, self.shape.lo, self.shape.b );
            u = dk.bsx.sub( u, c );
            u = sqrt(dot( u, u, 2 ));
            s = ant.mean(u);
        end
        function s = scales(self)
            c = self.barycentre();
            u = vertcat( self.shape.up, self.shape.lo, self.shape.b );
            u = dk.bsx.sub( u, c );
            s = ant.mean(abs(u));
        end
        
        % apply function to all vertex coordinates
        function self = transform(self,fun)
            self.shape.b = fun( self.shape.b );
            self.shape.lo = fun( self.shape.lo );
            self.shape.up = fun( self.shape.up );
        end
        
        % plot box
        function p = plot(self,type,col,varargin)
            
            if nargin < 3, col=0.2*[1,1,1]; end
            if nargin < 2, type='line'; end
            
            % make output
            bounds = [ self.shape.lo; self.shape.up ];
            d = self.ndims();
            p = bt.obj.Drawing( d, 2^d );
            
            % draw
            hold on;
            switch d
            case 2
                
                % non-cuboid 2-boxes are triangles
                if ~self.is_cuboid()
                    p = bt.poly.Simplex([ bounds; self.shape.b ]).plot(type,col,varargin{:});
                    return;
                end
                
                % draw a square
                box = ant.math.enumerate( bounds(:,1), bounds(:,2) );
                x = make_square_path(box);
                
                switch lower(type)
                case 'line'
                    col = p.reset( 4, col );
                    for i = 1:4
                        p.gobj(i) = dk.ui.plot( x( i+[0,1], : ), 'Color', col(i,:), varargin{:} ); 
                        %hold on;
                    end
                case 'patch'
                    col = p.reset( 1, col );
                    p.gobj = dk.ui.fill( x, col, varargin{:} );
                otherwise
                    error('Unknown plot type: %s',type);
                end
                
            case 3
                
                if self.is_cuboid()
                    switch lower(type)
                    case 'line'
                        % find unique pairs of distinct vertices that form edges
                        % i.e. those in which both vertices only differ by one coordinate
                        x = ant.math.enumerate( bounds(:,1), bounds(:,2), bounds(:,3) );
                        E = ant.math.pairdist( x, x, ...
                            struct( 'map', @(x)abs(x)>1e-12, 'red', @(x)x ) );
                        E = tril(E,-1);
                        [i,j] = find( E == 1 );
                        E = [ i(:), j(:) ];
                        ne = size(E,1);
                        
                        col = p.reset( ne, col );
                        for i = 1:ne
                            p.gobj(i) = dk.ui.plot( x(E(i,:),:), 'Color', col(i,:), varargin{:} ); 
                            %hold on;
                        end
                    case 'patch'
                        % iterate on facets
                        col = p.reset( 6, col );
                        for i = 1:3
                            % lower bound for dim i
                            x = bounds;
                            x = { x(:,1), x(:,2), x(:,3) };
                            x{i} = x{i}(1);
                            x = ant.math.enumerate(x{:});
                            x = make_square_path(x);
                            p.gobj(i) = dk.ui.fill( x, col(i,:), varargin{:} ); %hold on;
                            
                            % upper bound for dim i
                            x = bounds;
                            x = { x(:,1), x(:,2), x(:,3) };
                            x{i} = x{i}(2);
                            x = ant.math.enumerate(x{:});
                            x = make_square_path(x);
                            p.gobj(i+3) = dk.ui.fill( x, col(i+3,:), varargin{:} );
                        end
                    otherwise
                        error('Unknown plot type: %s',type);
                    end
                else
                    
                    % non-cuboid 3-boxes are either tetrahedra or square pyramids
                    b = self.shape.b;
                    if size(b,1) > 1
                        p = bt.poly.Simplex([ bounds; self.shape.b ]).plot(type,col,varargin{:});
                        return;
                    end
                    
                    % one of the bounds is duplicated
                    box = {bounds(:,1), bounds(:,2), bounds(:,3)};
                    box = dk.mapfun( @unique, box, false );
                    box = ant.math.enumerate(box{:});
                    x = make_square_path(box); % base of pyramid
                    
                    % triangular facets are matched to edges of the subbox
                    switch lower(type)
                    case 'line'
                        col = p.reset( 8, col );
                        for i = 1:4
                            j = i+4;
                            p.gobj(i) = dk.ui.plot( x(i+[0,1],:), 'Color', col(i,:), varargin{:} ); %hold on;
                            p.gobj(j) = dk.ui.plot( [x(i,:); b], 'Color', col(j,:), varargin{:} );
                        end
                    case 'patch'
                        col = p.reset( 5, col );
                        p.gobj(1) = dk.ui.fill( x, col(1,:), varargin{:} ); %hold on;
                        for i = 1:4
                            y = [ b; x( i+[0,1], : ); b ];
                            p.gobj(i+1) = dk.ui.fill( y, col(i+1,:), varargin{:} ); 
                        end
                    otherwise
                        error('Unknown plot type: %s',type);
                    end
                end
                
            otherwise
                error('Cannot plot in dimension >3.');
            end
            hold off;
            
        end

    end

end

function y = make_square_path(x)
    assert( size(x,1) == 4, 'Should have 4 points.' );
    
    % two adjacent vertices cannot differ by more than one coordinate
    t = 1e-12;
    y = [x; x(1,:)];
    if nnz(abs(y(2,:)-y(1,:)) > t) > 1
        tmp = y(3,:);
        y(3,:) = y(2,:);
        y(2,:) = tmp;
    end
    if nnz(abs(y(3,:)-y(2,:)) > t) > 1
        tmp = y(4,:);
        y(4,:) = y(3,:);
        y(3,:) = tmp;
    end
    assert( all(sum(abs(diff(y,1,1)) > t,2) == 1), 'Not a square path.' );
end
