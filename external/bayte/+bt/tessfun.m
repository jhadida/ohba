function [T,F] = tessfun( fname, par, tmake, varargin )
%
% [T,F] = bt.tessfun( fname, par, tmake, gp )
%
% Select test function, tesselate domain, and evaluate elements.
% 
% INPUTS
%
%    fname  Test-function name, one of: 
%               peaks, waves, dixon, tang, himmel
%
%      par  Partition method, one of: 
%               tern, bary, site
%
%    tmake  either scalar specifying number of subelements
%               or 
%           function handle modifying the input tessellation
%               (Tessellation) => void
%
%       gp  GP kernel, one of: 
%               se, mat32, mat52
%
%
% See also: bt.tess, bt.rand, bt.fun.factory
%
% JH

    % select test function
    F = bt.fun.factory(fname);
    
    % build tessellation
    T = bt.tess(par, F.dom(:)*[1,1], varargin{:});
    if dk.is.fhandle(tmake)
        tmake(T);
    else
        bt.rand(T,[],1,tmake);
    end
    
    % evaluate all elements
    function elmeval(elm)
        elm.value = bt.obj.Value(F.fun(T.denormalise( elm.centre )));
    end
    T.iter_all( @elmeval );

end
