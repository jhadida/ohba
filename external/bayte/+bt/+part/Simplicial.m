classdef Simplicial < bt.abc.Partition
%
% Simplicial subdivision is similar to barycentric subdivision, in that it
% proceed cyclically with d steps per cycles, and that the shapes procuced
% in the last step are all simplices.
%
% However, it introduces a new intermediary shape that we call a subcore, 
% and relaxes the assumption that elements have a similar shape/volume at 
% a given depth in the partition tree.
% 
% JH

    properties (Constant)
        type = 'simplicial';
    end
    properties
        pmap;
    end
    
    methods (Static)
        
        function obj = restore(prop,ver)
            if nargin < 2, ver='latest'; end
            assert( strcmp(prop.type,bt.part.Simplicial.type), 'Partition type mismatch.' );
            switch lower(ver)
                case {'latest','0.1'}
                    obj = bt.part.Simplicial( prop.opt );
                    obj.reset(bt.priv.PairMap( prop.pmap ));
                otherwise
                    error( 'Unknown version: %s', ver );
            end
        end
        
    end
    
    methods
        
        function self = Simplicial(varargin)
            self.setopt( varargin );
            self.valopt();
            self.reset();
        end
        
        function self = reset(self,pmap)
            if nargin > 1
                assert( isa(pmap,'bt.priv.PairMap'), 'Bad input type.' );
                self.pmap = pmap;
            else
                self.pmap = bt.priv.PairMap();
            end
        end
        
        function out = subdivide(self,elm)
            
            self.pre_checks( elm, {'subbox','subcore','simplex'} );
            
            % call specialised subdivision methods
            switch elm.type
                case 'subbox'
                    out = bt.part.barybox(self,elm);
                case 'subcore'
                    out = self.subdivide_subcore(elm);
                case 'simplex'
                    out = self.subdivide_simplex(elm);
            end
            
            self.post_checks( elm, out );

        end
        
        function s = to_struct(self)
            s.type = 'bt.abc.Partition';
            s.ver = '0.1';
            s.prop.pmap = self.pmap.to_struct();
            s.prop.type = self.type;
            s.prop.opt = self.opt;
        end

    end
    
    methods (Hidden)
        
        function out = subdivide_simplex(self,elm)
            
            DEBUG = false;
            store = elm.store;
            
            % gather all vertices
            s = [elm.shape.v, elm.shape.b];
            n = numel(s);
            d = elm.nd;
            assert( n >= 3 && n == d+1, '[bug] Sanity check' );
            
            % compute and insert all midpoints
            mid = zeros(n);
            store.reserve_vert( n*(n-1)/2 );
            for j = 1:n 
            for i = j+1:n 
                
                x = store.vert( s([i,j]) );
                if self.opt.noise > eps
                    x = bt.priv.dcomb( x, 1, 1/self.opt.noise );
                else
                    x = ant.mean(x);
                end
                
                mid(i,j) = store.insert(x);
                mid(j,i) = mid(i,j);
                
            end
            end
            
            % register pairs of vertex indices with corresp. midpoint indices
            % NOTE: the order of these pairs is obviously important
            [ij,T] = bt.priv.pairs(s);
            v = mid(T); 
            self.pmap.add( ij, v );
            v = dk.torow(v);
            
            % create children subelements
            mks = @bt.poly.Simplex.make;
            mkc = @bt.poly.Subcore.make;
            ctr = self.opt.centre;
            out = store.new_elems(n+1); % this is (d+1) + 1
            
            % start by cutting off the peaks
            for i = 1:n
                
                % separate current vertex from the other
                cur = s(i);
                oth = [1:i-1, i+1:n];
                mpt = mid(i,oth); % corresponding midpoints

                % create peak
                out{i}.set_polytope( 'simplex', mks([cur, mpt]), ctr );
                
            end
            
            % then join the midpoints to create the core
            if n > 3
                out{end}.set_polytope( 'subcore', mkc(s), ctr );
            else
                out{end}.set_polytope( 'simplex', mks(v), ctr );
            end
            elm.add_children(out);
            
            % terminate if noedge=true
            if self.opt.noedge, return; end
            
            [E,eid] = elm.group_edges_by_facet();
            assert( isreal(eid) && all(eid > 0), '[bug] Facet IDs should be positive.' );
            
            % plot in debug mode
            if DEBUG
                
                figure( 'name', sprintf('Subdivision of element #%d',elm.id) );
                
                % plot element, its neighbours, and its children
                dbg_elm = elm.to_polytope();
                dbg_nei = elm.neighbours();
                dbg_nei = dk.mapfun( @(x) x.to_polytope(), dbg_nei, false ); 
                dbg_chi = dk.mapfun( @(x) x.to_polytope(), out, false );
                
                dbg_elm.plot( 'line', [0,0,0] );
                dk.mapfun( @(x) x.plot( 'line', [1,1,1]/3, 'LineStyle', '--' ), dbg_nei );
                dk.mapfun( @(x) x.plot( 'line', [0,0,1]/2, 'LineWidth', 1 ), dbg_chi );
                
                axis equal tight; axis([0,1,0,1]); hold on;
                
            end
            
            for i = 1:n
                
                % core inherits all edges in dimension > 2, none otherwise
                % subpeak {i} inherit all but s(i), and s(j) corresonds to midpoint i-j
                
                % FIDs for inherited edges below
                oth = [1:i-1, i+1:n];
                mpt = mid(i,oth); 
                
                % 1: new core-to-peaks edges
                if n > 3
                    link( out{end}, -s(i), out{i}, s(i) );
                    Ci = out([ oth, n+1 ]); % include core
                    Fi = [mpt, s(i)]; 
                else
                    link( out{end}, v(n-i+1), out{i}, s(i) );
                    Ci = out(oth); % do not include core
                    Fi = mpt;
                end
                
                % 2: inherit edges associated with original facets
                bt.part.inherit( elm, E(eid == s(i)), Ci, Fi );
                
            end
            
        end
        
        
        function subdivide_subcore(self,elm)
            
            store = elm.store;
            
            % enclosing simplex
            s = elm.shape.s;
            d = elm.nd;
            n = numel(s);
            assert( n > 3, '[bug] No subcores in 2D' );
            
            % introduce barycentre B at centre of enclosing simplex
            b = store.vert(s);
            if self.opt.noise > eps
                b = bt.priv.dcomb( b, 1, 1/self.opt.noise );
            else
                b = ant.mean(b);
            end
            b = store.insert(b);
            t = [ elm.shape.b, b ];

            % one sub-core + sub-peak per vertex of the enclosing simplex
            mks = @bt.poly.Simplex.make;
            mkc = @bt.poly.Subcore.make;
            ctr = self.opt.centre;
            out = cell(1,2*n);
            
            for i = 1:n 

                % retrieve midpoints
                cur = s(i);
                oth = s([1:i-1, i+1:n]);
                mpt = [cur*ones(n-1,1), oth(:)]; % all pairs with vertex i
                mpt = self.pmap.get( mpt ); % corresponding midpoint indices
                mpt = dk.torow(mpt); 

                % sub-peak
                x = [ b, mpt ];
                assert( length(x)==d+1, '[bug] Sanity check' );
                out{i}.set_polytope( 'simplex', mks(x), ctr );

                % sub-core
                if n > 4
                    out{n+i}.set_polytope( 'subcore', mkc(oth,t), ctr );
                else
                    % in 3D all F-facets are triangles, so subcores are simplices
                    mpt = bt.priv.pairs(oth); % pairs of vertices on facet
                    mpt = self.pmap.get( mpt ); % indices of corresponding midpoints
                    mpt = dk.torow(mpt); 

                    x = [ t, mpt ];
                    assert( length(x)==d+1, '[bug] Sanity check' );
                    out{n+i}.set_polytope( 'simplex', mks(x), ctr );
                end

            end
            
            % terminate if noedge=true
            if self.opt.noedge, return; end
            
            [E,eid] = elm.group_edges_by_facet();
            assert( isreal(eid), '[bug] Facet IDs should be real.' );
            
            % all edges can be added in one loop
            for i = 1:n
                
                oth = [1:i-1, i+1:n];
                
                % 1: edges between subcore {n+i} and all sub-peaks except {i}
                for j = 1:n-1
                    fid_p = self.pmap.get([ s(oth(j)), s(i) ]);
                    if n > 4
                        fid_c = -s(oth(j));
                    else
                        fid_c = self.pmap.get(s(oth([ 1:j-1, j+1:end ])));
                    end
                    link( out{n+i}, fid_c, out{oth(j)}, fid_p );
                end
            
                % 2: inherit edge(s) for F-facet s(i) to subcore {n+i} with fid b
                self.transfer( elm, E(eid == s(i)), out{n+i}, b );
                
                % 3: inherit edge(s) for V-facet -s(i) to subpeak {i} with fid b
                self.transfer( elm, E(eid == -s(i)), out{i}, b );
                
            end
            
            % 4: remaining edges correspond to facets associated with interm. barycentres
            bt.part.inherit( elm, E(~ismember( abs(eid), s )), out );
            
        end
        
    end
    
end

% define new edge between two elements
function link( elm1, fid1, elm2, fid2 )
    elm1.add_edge( elm2, fid1 );
    elm2.add_edge( elm1, fid2 );
end
