function par = factory(par,varargin)
%
% par = bt.part.factory( par, varargin )
%
%
% Match partition by name, and return instance of bt.part.*
%
% All these instances should derive from bt.abc.Partition, meaning
% that they should be accepted by bt.obj.Tessellation as partition functions.
%
% 
%       Name                    Class               Description
%       ----                    -----               -----------
%
%   t|tern|ternary          bt.part.Ternary          Ternary splits
%   b|bary|barycentric      bt.part.Barycentric      Barycentric subdivison
%   s|site|simplicial       bt.part.Simplcial        Simplicial subdivison
%
% JH

    if ~isa(par,'bt.abc.Partition')
        assert( ischar(par), 'Expected a partition name.' );
        switch lower(par)
            case {'t','tern','ternary'}
                par = bt.part.Ternary(varargin{:});
            case {'b','bary','barycentric'}
                par = bt.part.Barycentric(varargin{:});
            case {'s','site','simplicial'}
                par = bt.part.Simplicial(varargin{:});
            otherwise
                error( 'Unknown partition: %s', par );
        end
    end
    
    assert( isa(par,'bt.abc.Partition'), 'Non-string inputs should be Partition instances.' );

end