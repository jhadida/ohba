function inherit( parent, edges, children, newfid, lnorm )
%
% Used internally by partition methods to either:
%   - replicate each edge to each child
%   - distribute edges amongst children by matching them with the 
%       parent's neighbours
%
%
% INPUTS
% ------
%
%     parent    instance of bt.obj.Element 
%
%   children    cell of children to whom the edges should be inherited
%               NOTE: beware that the parent-child relationship is NOT 
%               checked internally
%
%     newfid    DEFAULT: []
%               if specified, a vector of FIDs, one for each child
%
%      lnorm    DEFAULT: euclidean
%               The norm used for edge-matching (only when neighbours are
%               at least as deep as children), currently one of:
%                   euc, euclidean, L2
%                   abs, manhattan, L1
%
%
% USAGE
% -----
%
% bt.part.inherit( parent, edges, children, [], lnorm='euclidean' )
% bt.part.inherit( parent, edges, children, newfid, lnorm='euclidean' )
%
%   Where edges is a struct-array with fields {fid,nei}, e.g. a subset of edges 
%   from the parent to be replicated/distributed amongst children. The norm is 
%   used only when edges are distributed (i.e. when the neighbours are at least 
%   as deep as the children).
%
%
% bt.part.inherit( parent, edges, children, [], lnorm='euclidean' )
%
%   Where edges is a cell of edges with same fid in each cell on the parent side.
%   See bt.obj.Element::group_edges_by_facet.
%
%
% JH
    
    if nargin < 4, newfid = []; end
    if nargin < 5, lnorm = 'euclidean'; end
    
    % early cancelling
    edges = dk.unwrap(edges);
    if isempty(edges), return; end

    % check inputs
    assert( isa(parent,'bt.obj.Element'), 'Parent should be an element.' );
    assert( iscell(children), 'Expected a cell of children.' );
    
    if iscell(edges)
        
        assert( isempty(newfid), 'New FIDs cannot be specified for multi-facet matching.' );
        
        nf = numel(edges); % number of facets
        for i = 1:nf
            connect_facet( parent, edges{i}, children, lnorm );
        end
        
    else
        
        if isempty(newfid)
            connect_facet( parent, edges, children, lnorm );
        else
            connect_facet_fid( parent, edges, children, newfid, lnorm );
        end
        
    end     
    
end

function connect_facet( parent, edges, children, lnorm )

    ne = numel(edges);
    nc = numel(children);
    
    switch ne
        case 0
            return;
    
        % If there is only one neighbour, then replicate the edge for each child 
        % (depth of facing neighbour is <= parent in the partition tree)
        case 1
            nei = parent.store.getelem( edges.nei );
            tmp = nei.edge_with( parent );
            for i = 1:nc-1
                children{i}.insert_edge( edges );
                tmp.nei = children{i}.id;
                nei.insert_edge(tmp);
            end
            children{end}.insert_edge( edges );
            nei.update_edge( parent, children{end} );
        
        % If there are more than one, then there should be at least as many as there
        % are of children (conjecture), and they can be matched by distance (conjecture)
        otherwise
            assert( ne >= nc, '[bug] Not sure what to do, can this happen?' );

            M = find_matching( parent, edges, children, lnorm );
            nei = parent.store.elem([ edges.nei ]);
            for i = 1:ne
                children{M(i)}.insert_edge( edges(i) );
                nei{i}.update_edge( parent, children{M(i)} );
            end
            
    end

end

function connect_facet_fid( parent, edges, children, newfid, lnorm )

    ne = numel(edges);
    nc = numel(children);
    
    assert( dk.is.vector(newfid,nc), 'Bad FIDs.' );
    
    switch ne
        case 0
            return;
            
        % If there is only one neighbour, then replicate the edge for each child 
        % (depth of facing neighbour is <= parent in the partition tree)
        case 1
            nei = parent.store.getelem( edges.nei );
            tmp = nei.edge_with( parent );
            for i = 1:nc-1
                children{i}.insert_edge( edges, newfid(i) );
                tmp.nei = children{i}.id;
                nei.insert_edge(tmp);
            end
            children{end}.insert_edge( edges, newfid(end) );
            nei.update_edge( parent, children{end} );
        
        % If there are more than one, then there should be at least as many as there
        % are of children (conjecture), and they can be matched by distance (conjecture)
        otherwise
            assert( ne >= nc, '[bug] Not sure what to do, can this happen?' );
            
            M = find_matching( parent, edges, children, lnorm );
            nei = parent.store.elem([ edges.nei ]);
            for i = 1:ne
                children{M(i)}.insert_edge( edges(i), newfid(M(i)) );
                nei{i}.update_edge( parent, children{M(i)} );
            end

    end

end

function idx = find_matching( parent, edges, children, lnorm )

    ne = numel(edges);
    nc = numel(children);

    % gather neighbour elements
    neighbours = parent.store.elem([ edges.nei ]);
    
    % find ancestors at the same depth as children
    d = parent.depth + 1;
    for i = 1:ne
        while neighbours{i}.depth > d
            neighbours{i} = neighbours{i}.parent;
        end
    end
    
    % find unique ancestors
    [~,ia,ic] = unique(cellfun( @(a) a.id, neighbours ));
    
    % sanity checks
    assert( all(cellfun( @(c) c.depth, children )==d), '[bug] Unexpected children depths.' );
    assert( all(cellfun( @(a) a.depth, neighbours )==d), '[bug] Unexpected neighbours depths.' );
    assert( numel(ia) == nc, 'Mismatch between number of neighbours and children.' );
    
    % extract centres
    ref = dk.mapfun( @(c) c.centre, children, false );
    ref = vertcat( ref{:} );
    
    qry = dk.mapfun( @(a) a.centre, neighbours(ia), false );
    qry = vertcat( qry{:} );
    
    % select specified metric, and try fast matching first
    switch lower(lnorm)
        case {'euc','euclidean','l2'}
            idx = ant.mex.nearestL2( ref, qry );
            metric.map = @(x) x.^2;
            metric.red = @(x) sqrt(x);
        case {'abs','manhattan','l1'}
            idx = ant.mex.nearestL1( ref, qry );
            metric.map = @(x) abs(x);
            metric.red = @(x) x;
        otherwise
            error( 'Unknown metric: %s', lnorm );
    end
    
    % if fast matching did not succeed, solve assignment problem
    if ~dk.num.isperm(idx)
        cost = ant.math.pairdist( ref, qry, metric );
        idx = ant.math.munkres( cost, false );
        [idx,~] = find(idx);
        assert( all(idx), 'Something went wrong with assignment problem.' );
    end
    idx = idx(ic); % restore duplicates
    
end
