classdef Ternary < bt.abc.Partition
%
% With ternary splits, any parent box is split into 3 children boxes:
%
%        lower                     upper
%          \                         /
%           =--------parent---------=
%                                           ===> dimension "s"
% 
%           =---L---=---M---=---R---=
%          /        |       |        \
%        Plo       Lup     Rlo       Pup
%                  Mlo     Mup
%
% The seemingly duplicated bounds {Lup/Mlo} and {Rlo/Mup} are actually 
% different in the remaining dimensions.
%
% Facets are identified by their orthogonal dimension.
%
% Here facet "s" refers to:
%   - the rightmost boundary of all children
%   - the rightmost boundary of the parent
%     which is also the righmost boundary of the right child
%
% Similarly "-s" refers to leftmost boundaries orthogonal to dimension s.
% Hence the facet identifier is NOT unique across elements.
%
% JH

    properties (Constant)
        type = 'ternary';
    end
    
    methods (Static)
        
        function obj = restore(prop,ver)
            if nargin < 2, ver='latest'; end
            assert( strcmp(prop.type,bt.part.Ternary.type), 'Partition type mismatch.' );
            switch lower(ver)
                case {'latest','0.1'}
                    obj = bt.part.Ternary( prop.opt );
                otherwise
                    error( 'Unknown version: %s', ver );
            end
        end
        
    end

    methods
        
        function self = Ternary(varargin)
            self.setopt( varargin );
            self.valopt();
        end
        
        function s = to_struct(self)
            s.type = 'bt.abc.Partition';
            s.ver = '0.1';
            s.prop.type = self.type;
            s.prop.opt = self.opt;
        end
        
        function out = subdivide(self,elm)
            
            store = self.pre_checks(elm,'subbox');
            assert( isempty(elm.shape.b), 'Cartesian partitions only apply to cuboids.' );
            
            % extract parent bounds
            t = [ elm.shape.lo, elm.shape.up ];
            P = store.vert(t);
            
            % separate lower/upper
            Plo = P(1,:);
            Pup = P(2,:);
            
            % determine splitting points
            if self.opt.noise > eps
                w = ant.math.dirichlet( [1,1,1]/self.opt.noise, 1 );
                a = w(1);
                b = w(1) + w(2);
            else
                a = 1/3;
                b = 2/3;
            end

            % split along largest dimension
            % NOTE: truncate to 10 digits to avoid precision issues
            [~,s] = max(dk.num.floor( Pup-Plo, 10 )); 

            % children bounds
            Lup = Pup;
            Rlo = Plo;
            Mlo = Plo;
            Mup = Pup;

            %Lup(s) = (2*Plo(s) +   Pup(s))/3.0;
            %Rlo(s) = (  Plo(s) + 2*Pup(s))/3.0;
            Lup(s) = Plo(s) + a*(Pup(s)-Plo(s));
            Rlo(s) = Plo(s) + b*(Pup(s)-Plo(s));
            Mlo(s) = Lup(s);
            Mup(s) = Rlo(s);
            
            % create new vertices
            v = store.insert([ Lup; Rlo; Mlo; Mup ]);
            v = dk.torow(v);
            
            % convert back to indices
            [Plo,Pup,Lup,Rlo,Mlo,Mup] = dk.deal([t,v]);

            % create children elements
            out = elm.store.new_elems(3);
            mks = @bt.poly.Subbox.make;
            ctr = self.opt.centre;
            out{1}.set_polytope( 'subbox', mks( Plo, Lup ), ctr );
            out{2}.set_polytope( 'subbox', mks( Mlo, Mup ), ctr );
            out{3}.set_polytope( 'subbox', mks( Rlo, Pup ), ctr );
            elm.add_children(out);
            
            % terminate here if noedge=true
            if self.opt.noedge
                self.post_checks(elm,out);
                return;
            end
            
            % work out neighbourhood
            [E,eid] = elm.group_edges_by_facet();
            n = numel(E);
            for i = 1:n
            switch eid(i)
                case -s % all edges along the lower split-dimension go to the "left" child
                    self.transfer( elm, E{i}, out{1} );
                    
                case s % all edges along the upper split-dimension go to the "right" child
                    self.transfer( elm, E{i}, out{3} );
                    
                otherwise
                    bt.part.inherit( elm, E{i}, out, [], 'L1' );
                    
            end
            end
            
            % link middle child to siblings
            % NOTE: this needs to be outside the previous loop
            link( out{1}, s, out{2}, -s );
            link( out{2}, s, out{3}, -s );
            
            % sanity checks
            self.post_checks(elm,out);

        end

    end

end

% define new edge between two elements
function link( elm1, fid1, elm2, fid2 )
    elm1.add_edge( elm2, fid1 );
    elm2.add_edge( elm1, fid2 );
end
