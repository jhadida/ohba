function out = adhoc( meth, elm, depth )
%
% out = bt.part.adhoc( meth, elm, depth=1 )
%
%   Purely geometric and recursive implementation of the different 
%   partitioning algorithms, returning a cell of Polytope objects.
%   Note that this does NOT compute edges between neighbours.
%
%   This is used mainly for drawing polytopes, and for "lookahead" 
%   during optimisation, without altering the over-arching tessellation.
%
%   Both Elements and Polytopes are accepted in input, but note that
%   Elements are converted to Polytopes WITHOUT denormalisation.
%
% JH

    if nargin < 3, depth=1; end
    assert( depth < 10, 'This is probably a mistake.' );
    
    % convert element to polytope
    if isa(elm,'bt.abc.Polytope')
        poly = elm;
    else
        poly = bt.poly.from_elem(elm);
    end
    
    % compute subdivsion
    switch lower(meth)
        case {'t','tern','ternary'}
            out = adhoc_ternary(poly,depth);
        case {'b','bary','barycentric'}
            out = adhoc_barycentric(poly,depth);
        case {'s','site','simplicial'}
            out = adhoc_simplicial(poly,depth);
        otherwise
            error( 'Unknown method: %s', meth );
    end

end

% ----------------------------------------------------------------------
% Recursive partitioning gateways calling concrete implementations below
% ----------------------------------------------------------------------

function out = recursive_subdivision(poly,depth,callback)
    out = cell(1,depth+1);
    out{1} = {poly};
    
    for i = 1:depth
        n = numel(out{i});
        t = cell(1,n);
        for j = 1:n
            t{j} = callback( out{i}{j} );
        end
        out{i+1} = horzcat(t{:});
    end
    out = horzcat(out{2:end});
end

function out = adhoc_ternary(poly,depth)
    assert( strcmp(poly.type,'subbox') && poly.is_cuboid(), 'Input should be a cuboid.' );
    out = recursive_subdivision( poly, depth, @subdivide_ternary );
end

function out = adhoc_barycentric(poly,depth)
    assert( ismember(poly.type,{'subbox','simplex'}), 'Bad input polytope.' );
    out = recursive_subdivision( poly, depth, @barycentric_switch );
end
function out = barycentric_switch(poly)
    switch poly.type
        case 'subbox'
            out = subdivide_barybox(poly);
        case 'simplex'
            out = subdivide_barycentric(poly);
        otherwise
            error( 'Invalid polytope type: %s', poly.type );
    end
end

function out = adhoc_simplicial(poly,depth)
    assert( ismember(poly.type,{'subbox','simplex','subcore'}), 'Bad input polytope.' );
    out = recursive_subdivision( poly, depth, @simplicial_switch );
end
function out = simplicial_switch(poly)
    switch poly.type
        case 'subbox'
            out = subdivide_barybox(poly);
        case 'simplex'
            out = subdivide_simplicial_simplex(poly);
        case 'subcore'
            out = subdivide_simplicial_subcore(poly);
        otherwise
            error( 'Invalid polytope type: %s', poly.type );
    end
end

% ----------------------------------------------------------------------
% Implementations of partitioning algorithms
% ----------------------------------------------------------------------

function out = subdivide_ternary(in)

    % lower/upper bounds
    Plo = in.shape.lo;
    Pup = in.shape.up;
    
    % split along largest dimension
    % NOTE: truncate to 10 digits to avoid precision issues
    [~,s] = max(dk.num.floor( Pup-Plo, 10 )); 

    % children bounds
    Lup = Pup;
    Rlo = Plo;
    Mlo = Plo;
    Mup = Pup;

    Lup(s) = (2*Plo(s) +   Pup(s))/3.0;
    Rlo(s) = (  Plo(s) + 2*Pup(s))/3.0;
    Mlo(s) = Lup(s);
    Mup(s) = Rlo(s);

    % create children polytopes
    make = @bt.poly.Subbox;
    out = { make(Plo,Lup), make(Mlo,Mup), make(Rlo,Pup) };

end

function out = subdivide_barybox(in)
    
    % unpack embedded box
    L = in.shape.lo;
    U = in.shape.up;
    B = in.shape.b;
    
    % the bounds of an (axis-aligned) embedded p-box only differ by p coordinates
    d = numel(L);
    m = L ~= U;
    p = nnz(m);
    q = size(B,1);
    k = find(m);
    assert( p == d-q, 'Unexpected box dimensionality; is it axis-aligned?' );
    
    % insert point at the centre of the embedded box
    B = [ B; (L + U)/2 ];
    
    % create children polytopes
    makeS = @bt.poly.Simplex;
    makeB = @bt.poly.Subbox;
    out = cell(1,2*p);
    
    for i = 1:p
        
        % update bounds by collapsing dimension k(i)
        ki = k(i);
        Li = L; Li(ki) = U(ki);
        Ui = U; Ui(ki) = L(ki);

        % form polytope shapes with current facets
        %
        % NOTE: no need to insert vertices if p=1, because in that case, the
        % two facets are 0-faces (i.e. vertices), corresponding to the bounds
        % of the parent box (which have already been inserted previously).
        if p == 1
            out{1} = makeS([ B; L ]);
            out{2} = makeS([ B; U ]);
        else
            out{i} = makeB( L, Ui, B );
            out{p+i} = makeB( Li, U, B );
        end
        
    end

end

function out = subdivide_barycentric(in)

    % unpack simplex
    V = in.shape.v;
    B = in.shape.b;
    B = [ B; ant.mean(V) ];
    
    d = in.ndims();
    n = size(V,1);
    m = size(B,1);
    assert( m+n == d+2, '[Bug] Sanity check' );
    
    % create children polytopes
    make = @bt.poly.Simplex;
    out = cell(1,n);
    
    if n == 2
        out{1} = make([ V(2,:); B ]);
        out{2} = make([ V(1,:); B ]);
    else
        for i = 1:n
            out{i} = make( V([ 1:i-1, i+1:n ],:), B );
        end
    end
    
end

function out = subdivide_simplicial_simplex(in)

    % unpack simplex
    S = [ in.shape.v; in.shape.b ];
    n = size(S,1);
    d = in.ndims();
    assert( n >= 3 && n == d+1, '[bug] Sanity check' );
    
    % create children polytopes
    makeS = @bt.poly.Simplex;
    makeC = @bt.poly.Subcore;
    out = cell(1,n+1);
    
    % cut off the peaks
    for i = 1:n
        cur = S(i,:);
        mid = dk.bsx.add( S([1:i-1, i+1:n],:), cur )/2;
        out{i} = makeS([ cur; mid ]);
    end
    
    % join midpoints to create core
    if n > 3
        out{end} = makeC(S);
    else
        out{end} = makeS( (S + S([2,3,1],:))/2 );
    end

end

function out = subdivide_simplicial_subcore(in)

    % unpack subcore
    S = in.shape.s;
    n = size(S,1);
    d = in.ndims();
    assert( n > 3, '[bug] No subcores in 2D' );
    
    % introduce barycenter inside enclosing simplex
    B = ant.mean(S);
    T = [ in.shape.b; B ];
    
    % create children polytopes
    makeS = @bt.poly.Simplex;
    makeC = @bt.poly.Subcore;
    out = cell(1,2*n);
    
    for i = 1:n
        
        % compute midpoints
        cur = S(i,:);
        oth = S([1:i-1, i+1:n],:);
        
        % sub-peak
        mid = dk.bsx.add( oth, cur )/2;
        pts = [ B; mid ];
        
        assert( size(pts,1)==d+1, '[bug] Sanity check' );
        out{i} = makeS(pts);
        
        % sub-core
        if n > 4
            out{n+i} = makeC( oth, T );
        else
            mid = (oth + oth([2,3,1],:))/2;
            pts = [ T; mid ];

            assert( size(pts,1)==d+1, '[bug] Sanity check' );
            out{n+i} = makeS(pts);
        end
        
    end

end