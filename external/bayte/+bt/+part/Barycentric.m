classdef Barycentric < bt.abc.Partition
%
% Barycentric subdivision operates cyclicly on the facets of polytopes; in dimension
% d, each cycle of subdivision consists of d steps, described as follows:
%
%   - Initially list all vertices of the polytope.
%
%   - At step  1 <= k <= d, list all (d-k)-faces involving ONLY inital vertices; 
%     within a given element, these should form one and only one (d-k+1)-face.
%
%   - Insert a new vertex at the barycentre of that (d-k+1)-face, and combine all 
%     non-initial vertices together with this new barycentre, as well as ONE of the
%     (d-k)-faces, to obtain a new subelement. This results in exactly (d-k) new 
%     elements.
%
%   - If k=d, then the element has exactly d+1 vertices (i.e. it is a simplex), and 
%     a new cycle begins by considering it as a new "initial" polytope.
% 
% For any initial polytope, the subelements obtained at ANY step after the first cycle
% are ALL simplices.
%
% JH

    properties (Constant)
        type = 'barycentric';
    end
    
    methods (Static)
        
        function obj = restore(prop,ver)
            if nargin < 2, ver='latest'; end
            assert( strcmp(prop.type,bt.part.Barycentric.type), 'Partition type mismatch.' );
            switch lower(ver)
                case {'latest','0.1'}
                    obj = bt.part.Barycentric( prop.opt );
                otherwise
                    error( 'Unknown version: %s', ver );
            end
        end
        
    end

    methods
        
        function self = Barycentric(varargin)
            self.setopt( varargin );
            self.valopt();
        end
        
        function out = subdivide(self,elm)
            
            self.pre_checks( elm, {'subbox','simplex'} );
            
            % call specialised subdivision methods
            switch elm.type
                case 'subbox'
                    out = bt.part.barybox(self,elm);
                case 'simplex'
                    out = self.subdivide_simplex(elm);
            end
            
            self.post_checks( elm, out );

        end
        
        function s = to_struct(self)
            s.type = 'bt.abc.Partition';
            s.ver = '0.1';
            s.prop.type = self.type;
            s.prop.opt = self.opt;
        end

    end

    methods (Access=private,Hidden)
        
        function out = subdivide_simplex(self,elm)
            
            store = elm.store;
            
            % new point at barycentre of current vertex set
            s = elm.shape;
            n = numel(s.v);
            b = store.vert( s.v );
            if self.opt.noise > eps
                b = bt.priv.dcomb( b, 1, 1/self.opt.noise );
            else
                b = ant.mean(b);
            end
            
            % prepare new simplex shape
            b = store.insert(b);
            s.b = [ s.b, b ];
            assert( numel(s.b)+n == elm.nd+2, '[bug] Sanity check' );

            % create children elements
            mks = @bt.poly.Simplex.make;
            ctr = self.opt.centre;
            out = store.new_elems(n);
            if n == 2
                % intermediate barycentres become original vertices
                out{1}.set_polytope( 'simplex', mks([ s.v(2), s.b ]), ctr );
                out{2}.set_polytope( 'simplex', mks([ s.v(1), s.b ]), ctr );
            else
                % facets are defined by their opposite vertex
                for i = 1:n
                    si = s;
                    si.v = si.v([ 1:i-1, i+1:n ]); % faster than using setdiff(1:n,i)
                    out{i}.set_polytope( 'simplex', si, ctr );
                end
            end 
            elm.add_children(out);
            
            % terminate if noedge=true
            if self.opt.noedge, return; end
            
            [E,eid] = elm.group_edges_by_facet();
            assert( isreal(eid), '[bug] Facet IDs should be real.' );
            
            % simplest edges first
            for i = 1:n
                
                % 1: all-to-all edges between siblings
                for j = i+1:n
                    link( out{i}, out{i}.shape.v(j-1), out{j}, out{j}.shape.v(i) );
                end
                
                % 2: inherit parent edge(s) for facets associated with original vertices:
                %   (one-to-one mapping by construction between these facets and the children)
                self.transfer( elm, E(eid == s.v(i)), out{i}, b );
                
            end
            
            % IMPORTANT NOTE:
            %
            % Because the parent and all children share the intermediary barycenters as common 
            % vertices, there is no need to reassign the facet IDs of the following edges.
            
            % 3: the remaining edges correspond to facets associated with interm. barycentres
            bt.part.inherit( elm, E(~ismember( eid, s.v )), out );
            
        end
        
    end
    
end

% define new edge between two elements
function link( elm1, fid1, elm2, fid2 )
    elm1.add_edge( elm2, fid1 );
    elm2.add_edge( elm1, fid2 );
end
