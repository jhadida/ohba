function obj = restore( in )
%
% obj = bt.part.restore( in )
%
% Load serialised partition, and restore corresponding instance.
% Input should be either:
%   - the filename of the serialised partition
%   - the corresponding serialised structure with fields {ver,type,prop}
%
%
% JH

    [ver,prop] = bt.priv.restore( in, 'bt.abc.Partition' );
    switch lower(prop.type)
        case 'ternary'
            obj = bt.part.Ternary.restore(prop,ver);
        case 'barycentric'
            obj = bt.part.Barycentric.restore(prop,ver);
        case 'simplicial'
            obj = bt.part.Simplicial.restore(prop,ver);
        otherwise
            error( 'Unknown partition type: %s', prop.type );
    end

end