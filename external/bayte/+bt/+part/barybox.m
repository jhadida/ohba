function out = barybox(obj,elm)
%
% Barycentric subdivision of subbox Element.
%
% Implemented as separate function to allow sharing between Barycentric and 
% Simplicial partition functions. Note that this implementation assumes that
% boxes are AXIS-ALIGNED, and will fail if that is not the case.
%
% JH

    store = elm.store;

    % extract upper and lower bound coordinates
    s = elm.shape;
    t = [ s.lo, s.up ];
    V = store.vert(t);
    d = size(V,2);
    L = V(1,:);
    U = V(2,:);
    
    % the bounds of an (axis-aligned) embedded p-box only differ by p coordinates
    m = L ~= U;
    p = nnz(m);
    k = find(m);
    assert( p == d-numel(s.b), 'Unexpected box dimensionality; is it axis-aligned?' );

    % compute barycentre of p-box
    if obj.opt.noise > eps
        b = bt.priv.dcomb_box( L, U, 1, 1/obj.opt.noise );
    else
        b = (L + U)/2;
    end

    % insert barycentre of p-box base and prepare polytope shape for children
    b = store.insert(b);
    s.b = [s.b, b];

    % list the facets of the p-box
    store.reserve_vert(2*p);
    out = store.new_elems(2*p);
    mks = @bt.poly.Simplex.make;
    ctr = obj.opt.centre;
    for i = 1:p

        % update bounds by collapsing dimension k(i)
        ki = k(i);
        Li = L; Li(ki) = U(ki);
        Ui = U; Ui(ki) = L(ki);

        % form polytope shapes with current facets
        %
        % NOTE: no need to insert vertices if p=1, because in that case, the
        % two facets are 0-faces (i.e. vertices), corresponding to the bounds
        % of the parent box (which have already been inserted previously).
        if p == 1
            out{1}.set_polytope( 'simplex', mks([ s.b, t(1) ]), ctr );
            out{2}.set_polytope( 'simplex', mks([ s.b, t(2) ]), ctr );
        else
            sL = s; sL.up = store.insert(Ui); 
            sU = s; sU.lo = store.insert(Li);

            out{i}.set_polytope( 'subbox', sL, ctr );
            out{p+i}.set_polytope( 'subbox', sU, ctr );
        end

    end
    elm.add_children(out);

    % terminate if noedge=true
    if obj.opt.noedge, return; end

    [E,eid] = elm.group_edges_by_facet();

    % iterate embedded dimensions
    for i = 1:p

        ki = k(i);

        % new edges between siblings
        if p == 1
            link( out{1}, t(1), out{2}, t(2) );
        else
            for j = i+1 : p
                link( out{i},   -k(j), out{j},   -ki );
                link( out{i},    k(j), out{j+p}, -ki );
                link( out{i+p}, -k(j), out{j},    ki );
                link( out{i+p},  k(j), out{j+p},  ki );
            end
        end

        % inherited p-box edges along non-collapsed dimension, typically links
        % between children and their uncles/cousins
        Ichk = abs(real(eid)) == ki;
        assert( all(isreal(eid(Ichk))), ...
            '[bug] Facets along dim %d should have a real ID.', ki );

        obj.transfer( elm, E(eid == -ki), out{i},   complex(-ki,b) );
        obj.transfer( elm, E(eid ==  ki), out{i+p}, complex( ki,b) );

    end

    % barycentric edges (previously collapsed p-box dimensions) inherited to 
    % all children
    Ibar = isreal(eid) | ismember( abs(real(eid)), k );
    Ibar = ~Ibar;
    
    bt.part.inherit( elm, E(Ibar), out );

    % if children elements are simplices; take the imaginary part of facet 
    % IDs to convert from <dim> + i <vtx> to <vtx>
    if p == 1
        extract_imaginary_fid(out{1}); 
        extract_imaginary_fid(out{2}); 
    end

end

% define new edge between two elements
function link( elm1, fid1, elm2, fid2 )
    elm1.add_edge( elm2, fid1 );
    elm2.add_edge( elm1, fid2 );
end

% extract imaginary part of fid for each edge
function extract_imaginary_fid(elm)
    function e = fid_filter(e)
        if ~isreal(e.fid)
            e.fid = imag(e.fid);
        end
    end
    elm.apply_edges(@fid_filter);
end