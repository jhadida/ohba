function gpr_value(name,varargin)
%
% bt.test.gpr_value( name, varargin )
%
% Draw predictive mean estimated by bt.pred.GaussianProcess.
%
%
% OPTIONS:
%
%   npts    number of training points for the GP
%           Default: 100
%
%   ker     kernel name: sqexp, mat32, mat52
%           Default: mat32
%
%   len     isotropic length-scale in relative units to domain-width
%           Default: 0.1
%
%   sigma   uniform sigma in relative units to value-width
%           Default: 0.01
% 
%
% See also: bt.fun.factory
%
% JH

    if nargin < 1, name = 'peaks'; end
    opt = dk.getopt( varargin, 'npts', 100, 'ker', 'mat32', 'len', 0.1, 'sigma', 0.01 );

    % select test function
    F = bt.fun.factory(name);
    opt.len = opt.len * diff(F.dom);
    opt.sigma = opt.sigma * diff(F.val);
    
    % grid and random points
    [G,Fg,Xg] = bt.priv.fungrid(F);
    
    nr = opt.npts;
    Xr = G.rand(opt.npts);
    Yr = [ F.fun(Xr), opt.sigma*ones(nr,1) ];
    Ft = bt.gpr( Xr, Yr, Xg, opt.ker, opt.len );
    
    % draw results
    figure; colormap('jet');
    
    subplot(1,2,1); 
    G.image( Fg, F.val ); hold on; 
    dk.ui.scatter( Xr, 'k*' ); hold off; title('Ground-truth');
    
    subplot(1,2,2); 
    G.image( Ft, F.val ); title('Regressed');

end