function tgpr( T, F, r, sz )
%
% bt.test.tgpr( T, F, r=<option brad>, n=80 )
%
% Tessellated GP regression using all leaves.
%
% Create a grid of size n spanning the domain of the input function.
% For each pixel, assign the ID of the enclosing leaf (see bt.test.map_inside).
% For each unique ID in the leafmap, run tessellated GP prediction at 
% the coordinates of the corresponding pixels.
% 
% INPUTS:
%
%   T,F     obtained with bt.tessfun
%   r       blanket radius
%   n       grid size
%
% EXAMPLE:
%
%   [T,F] = bt.tessfun( 'peaks', 'bary', 30 );
%   bt.test.tgpr( T, F, 2 );
%
% NOTE:
%   Best predictions seem to be obtained with matern32 kernel and blanket
%   radius 3.
%
%
% See also: bt.tessfun, bt.test.map_inside
%
% JH

    % set blanket radius
    if nargin > 3 && ~isempty(r)
        T.pred.opt.brad = r;
    end
    
    % side of the grid
    if nargin < 4
        sz = 80;
    end
    
    % for each grid element, find enclosing leaf
    [M,G] = bt.priv.tess_map2d(T,sz);
    
    % for each grid element, evaluate function
    [gz,xq] = G.eval( F.fun );
    
    % for each grid element generate prediction
    M = bt.priv.tess_pred2d(T,M,xq);
    
    % centres of evaluated elements
    C = T.centre(T.store.eval_ids());
    
    % draw results
    figure; colormap('jet');
    
    subplot(1,2,1);
    G.image( gz, F.val ); hold on;
    dk.ui.plot( C, 'k*' ); hold off; 
    title('Ground-truth');
    
    subplot(1,2,2);
    G.image( M, F.val ); hold on;
    T.draw( 'LineWidth', 1 ); 
    T.draw_edges( 'r-' );
    hold off;
    title( 'Predicted mean' );

end
