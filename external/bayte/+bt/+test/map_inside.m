function M = map_inside(T,sz)
%
% M = bt.test.map_inside( T, n=80 )
%
% For each pixel in a grid of size n, assign ID of enclosing element.
% Yields a membership-map corresponding to the boundaries of leaf elements.
%
%
% ALGORITHM:
%
%   Sample each leaf and convert samples to grid indices, 
%   Set corresponding pixels to the ID of the leaf element. 
%   Remaining pixels are assigned the ID of their nearest neighbour.
%
% EXAMPLE:
%
%   T = bt.tess( {'b','noise',1/15}, 2, 'mat32' );
%   bt.test.randsum( T, 7 );
%   bt.test.map_inside( T );
%
%
% JH

    if nargin < 2, sz=80; end
    
    % get membership map
    [M,G] = bt.priv.tess_map2d(T,sz);
    
    % create optimised colormap for display
    c = bt.priv.coloptim(T,3);
    
    % remap indices
    id = unique(M);
    r(id) = 1:numel(id);
    I = r(M);
    
    % draw results
    figure; colormap(c);
    G.image(I); hold on;
    T.draw('LineWidth',1); 
    T.draw_edges('k-*'); hold off; 
    title('Matching element IDs');

end
