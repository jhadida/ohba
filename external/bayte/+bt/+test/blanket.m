function k = blanket(T,r,k)
%
% bt.test.blanket( T, r=<option brad>, k=<random leaf> )
%
% This is to ensure that blanket selection works as intended for inference:
%   - select a leaf element randomly, 
%   - call bt.pred.blanket(),
%   - draw centre of all elements in blanket,
%   - indicate the centre of the selected leaf.
%
%
% INPUTS:
%
%   T   tessellation object
%   r   blanket radius
%   k   leaf ID
%
%
% EXAMPLE:
%
%   T = bt.rand( {'b', 2}, [], 5 );
%   bt.test.blanket( T, 2 );
%
%
% See also: bt.abc.Predictor
%
% JH

    if nargin < 2
        r = T.pred.opt.brad;
    end

    % select random leaf
    L = T.leaves();
    if nargin < 3
        k = randi(numel(L));
    end
    L = L{k};
    
    % compute inference blanket
    [~,~,B] = bt.pred.blanket(L,r);
    
    % compute centres
    Lc = T.denormalise(L.centre);
    Bc = T.centre( B, true );
    
    % draw tessellation
    figure;
    T.draw(); hold on;
    dk.ui.plot(Lc,'rd','MarkerSize',10);
    dk.ui.plot(Bc,'k*');
    hold off; axis equal; grid on;

end