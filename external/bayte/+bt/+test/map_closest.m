function M = map_closest(T,n)
%
% M = bt.test.map_closest(T,n=80)
%
% For each pixel in a grid of size n, assign ID of element with closest centre.
%
%
% INPUTS
% ------
%
%   T   tessellation object
%   n   grid size
%
%
% EXAMPLE
% -------
%
%   T = bt.rand( {'t',2}, [], 8 );
%   bt.test.map_closest(T);
%
% 
% See also: bt.tess
%
% JH

    if nargin < 2, n=80; end
    
    % create grid
    assert( T.ndims == 2 );
    xr = [T.lower(1), T.upper(1)];
    yr = [T.lower(2), T.upper(2)];
    G = dk.obj.Grid(xr,yr,n);
    
    % create search map
    M = T.nearest(G.coord());
    M = G.reshape(M);
    
    % create optimised colormap for display
    c = bt.priv.coloptim(T,3);
    
    % remap indices
    id = unique(M);
    m(id) = 1:numel(id);
    M = m(M);

    % draw results
    figure; colormap(c);
    G.image(M); hold on;
    T.draw('LineWidth',1); hold off;
    title('Matching element IDs');

end