function gpr_perturb(name,varargin)
%
% bt.test.gpr_perturb( name, varargin )
%
% Study the effect of adding training points with large uncertainty close to 
% existing training points with comparatively low uncertainty.
%
% This is particularly relevant to the strategies in the case of stochastic 
% objectives, to see whether or not it is reasonable to restrict the acquisition 
% function to active leaves, instead of considering all nodes at each iteration.
%
% It seems that the effect of such "neighbouring perturbations" is really minor,
% so we can safely avoid considering multiple evaluations of ancestors, and focus
% on the evaluation of leaves instead.
%
%
% OPTIONS:
%
%   npts    number of training points for the GP
%           Default: 100
%
%   ker     kernel name: sqexp, mat32, mat52
%           Default: mat32
%
%   len     isotropic length-scale in relative units to domain-width
%           Default: 0.1
%
%   sigma   uniform sigma in relative units to value-width
%           Default: 0.01
%
%   nper    number of perturbation points
%           Default: 10% of npts
%
%   sigmap  uniform sigma for perturbation points
%           Default: sigma*10
%
%   noise   sigma for sampling perturbation points in vicinity of existing ones
%           Default: len/10
%
%
% See also: bt.fun.factory
%
% JH

    if nargin < 1, name = 'peaks'; end
    opt = dk.getopt( varargin, 'npts', 100, 'ker', 'mat32', 'len', 0.1, 'sigma', 0.01, ...
        'nper', [], 'sigmap', [], 'noise', [] );

    % select test function
    F = bt.fun.factory(name);
    opt.len = opt.len * diff(F.dom);
    opt.sigma = opt.sigma * diff(F.val);
    
    % perturbation params
    if isempty(opt.nper)
        opt.nper = ceil( opt.npts/10 );
    end
    if isempty(opt.sigmap)
        opt.sigmap = 10*opt.sigma;
    end
    if isempty(opt.noise)
        opt.noise = opt.len/10;
    end
    
    % grid and random points
    [G,Fg,Xg] = bt.priv.fungrid(F);
    
    nr = opt.npts;
    Xr = G.rand(nr);
    Yr = [ F.fun(Xr), opt.sigma*ones(nr,1) ];
    
    % perturbation points
    np = opt.nper;
    k = datasample( 1:nr, np );
    Xp = Xr(k,:) + opt.noise*randn(np,2);
    Yp = [ F.fun(Xp), opt.sigmap*ones(np,1) ];
    
    % regression with/without perturbation
    Fr = bt.gpr( Xr, Yr, Xg, opt.ker, opt.len );
    Fp = bt.gpr( [Xr; Xp], [Yr; Yp], Xg, opt.ker, opt.len );
    
    % draw results
    figure; colormap('jet');
    
    subplot(1,3,1); 
    G.image( Fg, F.val ); hold on; 
    dk.ui.scatter( Xr, 'k*' ); dk.ui.scatter( Xp, 'ko' ); 
    hold off; title('Ground-truth');
    
    subplot(1,3,2); 
    G.image( Fr, F.val ); title('Without perturbation');
    
    subplot(1,3,3); 
    G.image( Fp, F.val ); title('With perturbation');

end