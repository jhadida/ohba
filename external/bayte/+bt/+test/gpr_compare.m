function gpr_compare( name, varargin )
%
% bt.test.gpr_compare( name, varargin )
%
% Compare GP regression implementations from:
%   - Matlab    (fitrgp)
%   - GPML      (gp)
%   - BayTe     (bt.pred.GaussianProcess)
%
%
% OPTIONS:
%
%   show    information to plot: mean, sigma, regret
%           Default: mean
%
%   npts    number of training points
%           Default: 100
%
%   ker     kernel name: sqexp, mat32, mat52
%           Default: mat32
%
%   len     isotropic length-scale in relative units to domain-width
%           Default: 0.1
%
%   sigma   uniform sigma in relative units to value-width
%           Default: 0.01
%
%
% See also: bt.fun.factory
% 
% JH
    
    if nargin < 1, name = 'peaks'; end
    opt = dk.getopt( varargin, 'npts', 100, 'ker', 'mat32', 'len', 0.1, 'sigma', 0.01, 'show', 'mean' );

    % detect GPML
    has_gpml = { which('gpml_start'), which('gp') };
    has_gpml = any(~cellfun( @isempty, has_gpml ));
    if ~has_gpml
        warning( 'GPML not found, running without.' );
    end
    
    % select test function
    F = bt.fun.factory(name);
    opt.len = opt.len * diff(F.dom);
    opt.sigma = opt.sigma * diff(F.val);
    
    % draw training pointset uniformly randomly within domain
    X = F.dom(1) + rand(opt.npts,2) * diff(F.dom);
    y = F.fun(X);
    
    % generate predictions on a grid spanning the domain
    [G,gz,Xq] = bt.priv.fungrid(F);
    
    % run with bt.pred.GaussianProcess
    Pbayte = bayte_gpr(X,y,Xq,opt);
    %Pbayte = bayte_gpr_gpml(X,y,Xq,opt);
    Rbayte = format(gz,Pbayte);
    
    % run with fitrgp (no param fitting)
    Pmatlab = matlab_gpr(X,y,Xq,opt);
    Rmatlab = format(gz,Pmatlab);
    
    % run with GPML
    if has_gpml
        Pgpml = gpml_gpr(X,y,Xq,opt);
        %Pgpml = bayte_gpr_gpml(X,y,Xq,opt);
        Rgpml = format(gz,Pgpml);
    end
    
    % common colour range 
    rng.m = F.val;
    rng.s = dk.num.range( [Rmatlab.s(:); Rgpml.s(:); Rbayte.s(:)], 'pos' );
    rng.r = dk.num.range( [Rmatlab.r(:); Rgpml.r(:); Rbayte.r(:)], 'ctr' );
    
    % compare results
    figure; colormap('jet');
    
        % original function
        subplot(2,2,1); 
            G.image( gz, F.val ); title('Test function');
            hold on; dk.ui.scatter(X,'k*'); hold off;
        
        % Matlab
        [gz,cr] = select(Rmatlab,rng,opt.show);
        subplot(2,2,2); G.image( gz, cr ); title('Matlab');
        
        % GPML
        if has_gpml
            [gz,cr] = select(Rgpml,rng,opt.show);
            subplot(2,2,3); G.image( gz, cr ); 
        end
        title('GPML');
        
        % BayTe
        [gz,cr] = select(Rbayte,rng,opt.show);
        subplot(2,2,4); G.image( gz, cr ); title('BayTe');
    

end

function r = format(gz,p)
    r.m = reshape( p.m, size(gz) );
    r.s = reshape( p.s, size(gz) );
    r.r = gz - r.m;
end

function [gz,cr] = select(res,rng,sel)
    switch sel
        case {'m','mu','mean'}
            gz = res.m;
            cr = rng.m;
        case {'s','sigma'}
            gz = res.s;
            cr = rng.s;
        case {'r','regret'}
            gz = res.r;
            cr = rng.r;
        otherwise
            error('Unknown selector: %s',sel);
    end
end

function p = matlab_gpr(x,y,xq,opt)
    arg.FitMethod = 'none';
    arg.BasisFunction = 'none';
    arg.ConstantSigma = true;
    arg.Sigma = opt.sigma;
    
    switch opt.ker
        case 'sqexp'
            arg.KernelFunction = 'squaredexponential';
        case 'mat32'
            arg.KernelFunction = 'matern32';
        case 'mat52'
            arg.KernelFunction = 'matern52';
        otherwise
            error('Unknown kernel: %s',opt.ker);
    end
    arg.KernelParameters = [ opt.len, std(y)/sqrt(2) ];
    
    arg = dk.s2c(arg);
    gp = fitrgp( x, y, arg{:} );
    [p.m, p.s] = gp.predict(xq);
end

function p = gpml_gpr(x,y,xq,opt)
    gpml_start();
    hyp.lik = log(max( opt.sigma, eps ));
    hyp.cov = log([ opt.len; 1 ]);
    switch opt.ker
        case 'sqexp'
            covFun = @covSEiso;
        case 'mat32'
            covFun = {@covMaterniso, 3};
        case 'mat52'
            covFun = {@covMaterniso, 5};
        otherwise
            error('Unknown kernel: %s',opt.ker);
    end
    [p.m, p.s] = gp( hyp, @infExact, @meanZero, covFun, @likGauss, x, y, xq );
    p.s = sqrt(p.s);
    gpml_stop();
end

function p = bayte_gpr(x,y,xq,opt)
    ker = bt.ker.factory(opt.ker);
    n = numel(y);
    y = [ y, opt.sigma*ones(n,1) ];
    gp = bt.pred.GaussianProcess( ker );
    [p.m, p.s] = gp.pred(x,y,xq,opt.len);
end

function p = bayte_gpr_gpml(x,y,xq,opt)
    gp = bt.pred.GPML(opt.ker);
    hyp.sigma = opt.sigma;
    hyp.len = opt.len;
    [p.m, p.s] = gp.pred(x,y,xq,hyp);
end
