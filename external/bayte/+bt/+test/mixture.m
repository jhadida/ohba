function M = mixture( varargin )
%
% M = bt.test.mixture( varargin )
%
% Create and show 2D mixture of Gaussians.
%
% See also: bt.fun.Mixture
%
% JH

    M = bt.fun.Mixture( 2, varargin{:} );
    G = dk.obj.Grid([0,1]);
    V = M.value(G.coord());
    
    figure; G.surface(V); title('Mixture of Gaussians');
    
end