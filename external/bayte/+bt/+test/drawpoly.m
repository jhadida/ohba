function P = drawpoly( type, nd, varargin )
%
% E = bt.test.drawpoly( type, ndims, varargin )
%
% Draw polytopes defined in bt.poly.* (only in 2D and 3D..)
% Additional inputs are passed to method bt.abc.Polytope:plot.
% Output is a polytope instance.
%
%
% INPUTS:
%   
%   type    one of: simplex, box, subbox, core, subcore, subpeak
%   ndims   either 2 or 3
%
% EXAMPLE:
%
%   bt.test.drawpoly( 'simplex', 3 );
%
%
% JH

    switch nargin
        case 0
            autoargs = true;
            type = 'simplex';
            nd = 3;
        case 1
            error( 'At least two inputs required.' );
        case 2
            autoargs = true;
        otherwise
            autoargs = false;
            args = varargin;
    end
    assert( nd==2 || nd==3, 'Plotting is only possible in 2D and 3D.' );

    % create element of desired type
    cmap = @dk.cmap.jh;
    switch type
        case 'simplex'
            P = bt.poly.unit_simplex(nd);
            if autoargs
                if nd == 2
                    args = {'line',cmap(3)};
                else
                    args = {'patch',cmap(4)};
                end
            end

        case 'box'
            P = bt.poly.unit_box(nd);
            if autoargs
                if nd == 2
                    args = {'line',cmap(4)};
                else
                    args = {'patch',cmap(6)};
                end
            end

        case 'subbox'
            R = bt.poly.unit_box(nd);
            L = bt.part.adhoc( 'b', R );
            P = L{1};
            if autoargs
                if nd == 2
                    args = { 'line', cmap(3) };
                else
                    cols = cmap(2);
                    args = { 'patch', cols([1,2,2,2,2],:) };
                end
            end
            R.plot( 'line' ); hold on;

        case 'core'
            S = bt.poly.unit_simplex(nd);
            L = bt.part.adhoc( 's', S );
            P = L{end};
            if autoargs
                if nd == 2
                    args = { 'line', cmap(3) };
                else
                    args = { 'patch', cmap(2) };
                end
            end
            S.plot( 'line' ); hold on;

        case 'subcore'
            assert( nd == 3, 'Subcores do not exist in 2D' );
            S = bt.poly.unit_simplex(nd);
            L = bt.part.adhoc( 's', S );
            L = bt.part.adhoc( 's', L{end} );
            P = L{5};
            if autoargs
                cols = cmap(2);
                args = { 'patch', cols([1,2,2,2],:) };
            end
            S.plot( 'line' ); hold on;

        case 'subpeak'
            assert( nd == 3, 'Subpeaks do not exist in 2D' );
            S = bt.poly.unit_simplex(nd);
            L = bt.part.adhoc( 's', S );
            L = bt.part.adhoc( 's', L{end} );
            P = L{1};
            if autoargs
                cols = cmap(2);
                args = { 'patch', cols([1,2,2,2],:) };
            end
            S.plot( 'line' ); hold on;

        otherwise
            error('Unknown type');
    end
    
    % plot element
    P.plot( args{:} ); 
    axis equal; grid on;
    if nd==3, view(-40,20); end

end
