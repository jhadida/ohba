function tgpr_closest( T, F )
%
% bt.test.tgpr_closest( T, F )
%
% Tessellated GP regression using all leaves.
%
% Similar to bt.test.tgpr, but using a map of closest elements, instead of
% map of enclosing elements. Results in a very "patchy" overall picture.
%
%
% See also: bt.test.tgpr
%
% JH

    % create grid over function domain
    [G,gz,xq] = bt.priv.fungrid(F);
    
    % predictions over the grid based on local inference
    gm = T.predict(xq);
    gm = G.reshape(gm);
    
    % draw results
    figure; colormap('jet');
    
    subplot(1,2,1); G.image(gz,F.val); hold on;
    dk.ui.plot( T.centre(), 'k*' );
    hold off; title('Ground-truth');
    
    subplot(1,2,2); G.image(gm,F.val); hold on;
    T.draw(); %T.draw_edges('kd-'); 
    hold off; title('Predicted mean');

end
