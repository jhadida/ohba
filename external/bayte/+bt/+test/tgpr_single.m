function L = tgpr_single( T, F, r, L )
%
% bt.test.tgpr_single( T, F, r=<option brad>, L=<random leaf> )
%
% Tessellated GP regression, single-node.
%
% Randomly select a leaf from the input partition, and run GP prediction using the
% corresponding blanket information (obtained via blanket method of Prediction
% class) on a grid over the test-function's domain.
%
% INPUTS:
%
%   T,F     obtained with bt.tessfun
%   r       blanket radius
%   L       leaf element
%
% EXAMPLE:
%
%   [T,F] = bt.tessfun( 'peaks', 'bary', 30 );
%   bt.test.tgpr_single( T, F, 2 );
%
%
% See also: bt.tessfun
%
% JH

    % set blanket radius
    if nargin > 2 && ~isempty(r)
        T.pred.opt.brad = r;
    end
    
    % select leaf randomly
    if nargin < 4
        L = T.leaves();
        L = L{randi(numel(L))};
    else
        L = T.elem(L);
        L = L{1};
    end
    assert( L.is_leaf(), 'Element is not a leaf.' );

    % evaluate test function on grid
    [G,gz,xq] = bt.priv.fungrid(F);
    
    % generate prediction using its neighbourhood
    gm = T.value( L, xq );
    gm = reshape( gm, size(gz) );
    
    % plot blanket used for regression
    xn = T.denormalise(T.pred.blanket(L)); 
    
    % plot results
    figure; colormap('jet');
    
    subplot(1,2,1); G.image(gz,F.val); hold on;
    dk.ui.plot( xn, 'k*' );
    hold off; title('Ground-truth');
    
    subplot(1,2,2); G.image(gm,F.val); hold on;
    T.draw(); %T.draw_edges('kd-'); 
    hold off; title('Predicted mean');
    
end
