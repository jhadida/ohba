function [fig,F] = drawfun(fname)
%
% [fig,test] = bt.test.drawfun(fname)
% 
% Surface-plot of test-functions defined in bt.fun.* 
%
% INPUTS:
%
%   fname   one of: peaks, waves, dixon, tang, himmel
%
% EXAMPLE:
%
%   bt.test.drawfun('dixon');
%
%
% See also: bt.fun.factory
%
% JH

    if nargin < 1, fname='peaks'; end
    
    F = bt.fun.factory(fname);
    [G,z] = bt.priv.fungrid(F);
    fig = dk.fig.new( sprintf('Test-function: %s',F.name) ); colormap('jet');
    G.surface( z, [], F.val, 'EdgeAlpha', 0.5 ); title( F.name );

end
