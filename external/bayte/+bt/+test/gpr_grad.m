function gpr_grad( name, varargin )
%
% bt.test.gpr_grad( name, varargin )
%
% Draw predictive gradient estimated by bt.pred.GaussianProcess.
%
%
% OPTIONS:
%
%   ngrad   side of gradient grid
%           Default: 30
%
%   npts    number of training points for the GP
%           Default: 100
%
%   ker     kernel name: sqexp, mat32, mat52
%           Default: mat32
%
%   len     isotropic length-scale in relative units to domain-width
%           Default: 0.1
%
%   sigma   uniform sigma in relative units to value-width
%           Default: 0.01
% 
%
% See also: bt.fun.factory
%
% JH

    if nargin < 1, name = 'peaks'; end
    opt = dk.getopt( varargin, 'ngrad', 30, 'npts', 100, 'ker', 'mat32', 'len', 0.1, 'sigma', 0.01 );

    % select test function
    F = bt.fun.factory(name);
    opt.len = opt.len * diff(F.dom);
    opt.sigma = opt.sigma * diff(F.val);
    
    % grids
    G = dk.obj.Grid( F.dom );
    glin = linspace( F.dom(1), F.dom(2), opt.ngrad+2 );
    [gx,gy] = meshgrid(glin(2:end-1));
    
    % kernel
    ker = bt.ker.factory(opt.ker);
    
    % gp
    Xt = G.rand(opt.npts);
    Xp = G.coord();
    Xg = [ gx(:), gy(:) ];
    nt = size(Xt,1);
    
    Ft = [ F.fun(Xt), opt.sigma*ones(nt,1) ];
    gp = bt.pred.GaussianProcess( ker );
    Fp = gp.pred( Xt, Ft, Xp, opt.len );
    dFg = gp.grad( Xt, Ft, Xg, opt.len );
    
    % draw results
    figure; colormap('jet');
    
    subplot(1,2,1); G.image( F.fun(Xp), F.val ); hold on; 
    dk.ui.scatter( Xt, 'k*' ); hold off; title('Ground-truth');
    
    subplot(1,2,2); G.image( Fp, F.val ); hold on; 
    dk.ui.quiver( Xg, dFg, 'k-' ); hold off; title('Predictive mean and gradient');
    
end