function z = himmelblau(x)
%
% z = himmelblau(x)
%
% Himmelblau test function in 2D.
% Input x should be nx2.
%
% Dynamic range: [-5,5]
%   Value range: [-500 0]
%
% JH

    if nargin == 0
        bt.test.drawfun('himmel');
    	return
    end

    y = x(:,2);
    x = x(:,1);
    z = - (x.*x + y - 11).^2 - (x + y.*y - 7).^2;
    
end
