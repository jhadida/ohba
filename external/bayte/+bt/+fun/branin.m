function z = branin(x)
%
% z = branin(x)
%
% Branin function
%
% Dynamic range: [-5,10]
%   Value range: ?
%
% JH

    if nargin == 0
        bt.test.drawfun('branin');
    	return
    end

    a = -1;
    b = 5.1/(4*pi^2);
    c = 5/pi;
    r = 2;
    s = 10;
    t = 1/(8*pi);
    
    z = x(:,2) - b*x(:,1).^2 + c*x(:,1) - r;
    z = a*z.^2 + s*(t-1)*cos(x(:,1)) + s;

end