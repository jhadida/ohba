function z = styblinski_tang(x)
%
% z = styblinski_tang(x)
%
% d-dimensional Styblinski-Tang test function.
% Input x should be nxd.
%
% Dynamic range: [-5,5]
%   Value range: [-80 80]
%
% JH

    if nargin == 0
        bt.test.drawfun('tang');
    	return
    end

    y = x.*x;
    z = sum(16*y - 5*x - y.*y,2) / 2;
    
end
