function z = camelback(x)
%
% z = camelback(x)
%
% Six-hump camelback function
%
% Dynamic range: [-3,3]
%   Value range: ?
%

    if nargin == 0
        bt.test.drawfun('camel');
        return
    end
    
    xp = prod(x,2);
    x1 = x(:,1) .* x(:,1);
    x2 = x(:,2) .* x(:,2);
    
    z = 2.1*x1 - 4 - x1.^2/3;
    z = z.*x1 - xp + 4*(1-x2).*x2;

end