classdef Mixture < handle
%
% Create mixture of isotropic Gaussians in (0,1)^d with peaks far apart from each other,
% such that the location of the global extremum corresponds to the centre of a given peak.
% 

    properties
        peak
        seed
    end
    
    methods
        
        function self = Mixture(varargin)
            self.clear();
            switch nargin
                case 0 % nothing to do
                case 1
                    self.restore(varargin{1});
                otherwise
                    self.make(varargin{:});
            end
        end
        
        function clear(self)
            self.peak = []; 
            self.seed = [];
        end
        
        function n = npeaks(self), n = numel(self.peak); end
        function d = ndims(self)
            if self.npeaks() > 0
                d = numel(self.peak(1).m);
            else
                d = 0;
            end
        end
        
        function info(self)
            if self.npeaks
                dk.print( '%d-dimensional mixture with %d peaks:', self.ndims, self.npeaks );
                arrayfun( @(p) disp(p), self.peak );
            else
                dk.print( 'Empty mixture.' );
            end
        end
        
        function self = make(self,nd,np,sr,ar,tol,seed)
        %
        % Create a mixture of desired dimensionality, with desired number of peaks.
        % The procedure ensures that peaks are well separated, such that the global maximum
        % is necessarly one of the peaks.
        %
        %  nd: number of dimensions
        %  np: number of peaks
        %  sr: range of st-devs ([5,20]/100)
        %  ar: range of amplitudes ([1,5])
        % tol: tolerance on peak variability (max(abs(ar))/100)
        %
            
            self.clear();
            MAXTRY = 100;
            DEBUG = false;
            
            % random seed (reproducibility)
            if nargin < 7
                self.seed = rng();
            else
                rng(seed);
                self.seed = seed;
            end
            
            % debug messages
            if DEBUG
                print = @dk.print;
            else
                print = @dk.pass;
            end
            
            % parameters
            if nargin < 4 || isempty(sr), sr = [5,20]/100; end
            if nargin < 5 || isempty(ar), ar = [1,5]; end
            if nargin < 6 || isempty(tol), tol = max(abs(ar))/100; end
            tol = max( tol, 1e-6 );
            
            % initialise
            self.peak = dk.struct.repeat( {'m','s','a'}, 1, np );
            fail = false;
            
            print('Building %d-dimensional mixture with %d peaks...',nd,np);
            for i = 1:np
        
                % add a new peak
                count = 0;
                while count < MAXTRY

                    % this ensures that the mean is not within sigma of the border
                    p = make_peak( nd, sr, ar );

                    self.peak(i) = p;
                    A = vertcat(self.peak.a); % amplitude parameters
                    v = self.value( vertcat(self.peak.m) ); % actual values at the means

                    % if the actual peak values match the parameters (within tolerance)
                    % and that the amplitude of the new peak is sufficiently different from the others
                    if max(abs(v-A)) < tol && nnz(abs(A-p.a) < tol)==1
                        print( '\tpeak #%d: ok', i );
                        break;
                    else
                        print( '\tpeak #%d: retry', i );
                        count = count+1;
                    end

                end

                if count == MAXTRY
                    fail = true;
                    break;
                end

            end

            if fail
                % try again with thinner peaks
                self.make( nd, np, sr/2, ar, tol, self.seed );
            end
            
        end
        
        function v = value(self,x)
        %
        % Evaluate the mixture at points x, summing contributions of each peak.
        % 
        
            m = vertcat( self.peak.m );
            s = [ self.peak.s ];
            a = [ self.peak.a ];
            n = numel(s);
            v = 0;
            
            for i = 1:n
                v = v + a(i) * exp( - 0.5 * ant.pts.sqdist(x,m(i,:)) / s(i)^2 );
            end
            
        end
        
        function [k,d] = closest_peak(self,x)
        %
        % Return index of, and distance to, the peak nearest to x.
        %
        
            n = self.npeaks();
            m = size(x,1);
            d = zeros(m,n);

            for i = 1:n
                d(:,i) = ant.pts.sqdist( x, self.peak(i).m );
            end
            [d,k] = min( sqrt(d), [], 2 );
            
        end
        
        % basic i/o methods
        function s = saveobj(self)
            s = self.save();
        end
        function s = save(self,filename)
            s.type = 'bt.fun.Mixture';
            s.ver = '0.1';
            
            s.prop.seed = self.seed;
            s.prop.peak = self.peak;
            if nargin > 1
                dk.save( filename, s );
            end
        end
        
        function restore(self,s)
            [~,p] = bt.priv.restore(s,'bt.fun.Mixture');
            self.peak = p.peak;
            self.seed = p.seed;
        end
        
    end
    
    methods (Static)
        
        function obj = loadobj(s)
            obj = bt.fun.Mixture(s);
        end
        
    end
    
end

function p = make_peak(nd,sr,ar)
%
% Create a peak struct such that the mean is not within sigma of the border.
%

    p.s = sr(1) + diff(sr)*rand(1); % with random isotropic sigma
    p.m = p.s + (1-2*p.s)*rand(1,nd); % location picked uniformly at random
    p.a = ar(1) + diff(ar)*rand(1); % and amplitude
    
end
