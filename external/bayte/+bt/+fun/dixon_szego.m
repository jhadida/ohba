function z = dixon_szego(x)
%
% z = dixon_szego(x)
%
% Vectorised Dixon-Szego test function.
% Input x should be nx2.
%
% Dynamic range: [-2,2]
%   Value range: [-40 11]
%
% JH

    if nargin == 0
        bt.test.drawfun('dixon');
    	return
    end

    y = x(:,2);
    x = x(:,1);
    z = (4-2.1*x.^2+ x.^4/3).*x.^2 + x.*y + 4*(y.^2-1).*y.^2;
    z = 10 - z;

end
