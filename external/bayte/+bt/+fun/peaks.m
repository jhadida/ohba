function z = peaks(x,rotate,offset)
%
% z = peaks(x,rotate=pi/4,offset=0)
%
% 2D peaks test function.
% Input x should be nx2.
%
% Dynamic range: [-3,3]
%   Value range: [-8 8]
%
% JH

    if nargin == 0
        bt.test.drawfun('peaks');
    	return
    end

    if nargin < 3, offset=0; end
    if nargin < 2, rotate=pi/4; end

    y = x(:,2);
    x = x(:,1);
    
    if rotate
        ct = cos(rotate);
        st = sin(rotate);

        xn = ct*x + st*y;
        yn = ct*y - st*x;

        x = xn;
        y = yn;
    end
    
    z =  3*(1-x).^2.*exp(-(x.^2) - (y+1).^2) ...
        - 10*(x/5 - x.^3 - y.^5).*exp(-x.^2-y.^2) ...
        - 1/3*exp(-(x+1).^2 - y.^2) - offset;

end
