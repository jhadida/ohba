function z = waves(x)
%
% z = waves(x)
%
% 2D waves test function.
% Input x should be nx2.
%
% Dynamic range: [-1,1]
%   Value range: [-1 1]
%
% JH

    if nargin == 0
        bt.test.drawfun('waves');
    	return
    end

    y = x(:,2);
    x = x(:,1);
    z = -exp(x-2*x.^2-y.^2).*sin(6*(x+y+x.*y.^2));

end
