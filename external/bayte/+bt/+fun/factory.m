function out = factory(name)
%
% out = bt.fun.factory(fname)
%
% Match test-function by name, and return all associated info as a struct:
%   name   suitable for use with title()
%    fun   function handle (vectorised impl)
%    dom   relevant domain as 1x2 vec or 2x2 mat
%    err   error range (centred at 0)
%    val   value range (may not be centred at 0)
%     bf   basis function (for use with fitrgp)
%
% Valid names are:
%   peaks, waves, dixon, tang, himmel, camel, branin
%
%
% For additional functions, see: https://www.sfu.ca/~ssurjano/optimization.html
%
% JH

    bf = 'none';
    val = [];
    
    % mixture parameters
    msr = [5,20]/100;
    mar = [1,4];
    mab = max(abs(mar));
    
    switch lower(name)
        case {'p','peak','peaks'}
            nam = 'Peaks';
            fun = @bt.fun.peaks;
            dom = [-3,3];
            err = [-8,8];
            
        case {'w','wave','waves'}
            nam = 'Waves';
            fun = @bt.fun.waves;
            dom = [-1,1];
            err = [-1,1];
            
        case {'d','dixon','dixon-szego'}
            nam = 'Dixon-Szego';
            fun = @bt.fun.dixon_szego;
            dom = [-2,2];
            err = [-12,12];
            val = [-40,11];
            bf = 'pureQuadratic';
            
        case {'s','tang','styblinski-tang'}
            nam = 'Styblinski-Tang';
            fun = @bt.fun.styblinski_tang;
            dom = [-5,5];
            err = [-80,80];
            bf = 'pureQuadratic';
            
        case {'h','himmel','himmelblau'}
            nam = 'Himmelblau';
            fun = @bt.fun.himmelblau;
            dom = [-5,5];
            err = [-25,25];
            val = [-500,0];
            bf = 'pureQuadratic';
        
        case {'c','camel','camelback'}
            nam = 'Camelback';
            fun = @bt.fun.camelback;
            dom = [-3,3];
            err = [-25,25];
            val = [-100,1];
            
        case {'b','branin'}
            nam = 'Branin';
            fun = @bt.fun.branin;
            dom = [-5,10];
            err = [-25,25];
            val = [-200,1];
            
        case {'m3','mixture-3'} % mixture with 3 peaks
            nam = 'Mixture';
            mix = bt.fun.Mixture( 2, 3, msr, mar );
            fun = @(x) mix.value(x);
            dom = [0,1];
            err = mab*[-1,1];
            val = mab*[0,1];
            
        case {'m5','mixture-5'} % mixture with 5 peaks
            nam = 'Mixture';
            mix = bt.fun.Mixture( 2, 5, msr, mar );
            fun = @(x) mix.value(x);
            dom = [0,1];
            err = mab*[-1,1];
            val = mab*[0,1];
            
        otherwise
            error( 'Unknown function: %s', name );
    end

    % default value range = error range
    if isempty(val), val = err; end
    
    % pack results as struct
    out.name = nam;
    out.fun = fun;
    out.dom = dom;
    out.val = val;
    out.err = err;
    out.bf = bf;
    
end