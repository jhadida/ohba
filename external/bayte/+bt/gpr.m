function [mu,sigma,grad] = gpr( X, y, Xq, kernel, scale )
%
% [mu,sigma,grad] = gpr( X, y, Xq [, kernel, scale] )
%
% Gaussian-Process regression.
%
%
% INPUTS:
%
%        X  training coordinates as NxD matrix
%        y  training values as Nx1 matrix 
%                   or 
%           as Nx2 matrix with columns [ value, std ]
%
%       Xq  query coordinates as MxD matrix
%
%
% OPTIONS:
%
%   kernel  one of: sqexp, mat32, mat52
%           Default: mat32
%
%    scale  scale length, either as scalar or 1xD vector
%           Default: std(X,[],1)
%
%
% See also: bt.pred.GaussianProcess
%
% JH

    % validate inputs
    assert( ismatrix(X) && ismatrix(y) && ismatrix(Xq), 'Input points should be matrices.' );
    [n,d] = size(X);
    m = size(Xq,1);
    assert( n*m > 0, 'Empty coordinates.' );
    assert( size(Xq,2)==d, 'Bad num of cols in input Xq.' );
    assert( size(y,1)==n, 'Bad num of rows in input y.' );
    if size(y,2)~=2
        assert( isvector(y), 'Empty values.' );
        y = [y,zeros(n,1)];
    end
    assert( all(y(:,2) >= 0), 'Value stds should be non-negative.' );
    
    % parse kernel
    if nargin < 4, kernel='mat32'; end
    kernel = bt.ker.factory(kernel);
    
    % determine scale-lengths
    if nargin < 5, scale=std(X,[],1); end
    if isscalar(scale), scale=scale*ones(1,d); end
    assert( numel(scale)==d, 'Bad scales size.' );
    assert( all(scale > eps), 'Length-scales should be positive.' );

    % build GP instance
    gp = bt.pred.GaussianProcess(kernel);
    
    % run predictions
    [mu,sigma] = gp.pred(X,y,Xq,scale);
    if nargout > 2
        grad = gp.grad(X,y,Xq,scale);
    end

end