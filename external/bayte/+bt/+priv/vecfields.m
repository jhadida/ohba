function y = vecfields(s,varargin)
%
% checks whether all specified fields of scalar struct are vectors (or empty)
%
    y = all(cellfun( @(f) isempty(s.(f)) || isvector(s.(f)), varargin ));
end