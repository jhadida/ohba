function col = coloptim(T,p,r)
%
% Optimise colours for neighbourhood display
%

    if nargin < 2, p=1; end
    if nargin < 3, r=10; end

    A = double(T.adjacency());
    G = A;
    while p > 1
        G = G + A^p;
        p = p-1;
    end
    G = G > 0;
    
    [c,k] = ant.math.graphcolor(G,r);
    col = hsv(k);
    col = col(c,:);

end