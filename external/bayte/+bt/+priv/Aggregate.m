classdef Aggregate < handle
    
    properties
        data
    end
    
    methods
        
        function self = Aggregate()
            self.data = {};
        end
        
        function self = append(self,x)
            self.data{end+1} = x;
        end
        
        % horzcat / vertcat names are reserved
        function x = hcat(self)
            x = horzcat( self.data{:} );
        end
        function x = vcat(self)
            x = vertcat( self.data{:} );
        end
        
    end
    
end