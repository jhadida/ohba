function x = domrand( dom, n )
%
% Uniform random points within domain
%
    
    d = size(dom,2);
    assert( ismatrix(dom) && d>1 && size(dom,1)==2, 'Bad domain' );
    lo = dom(1,:);
    up = dom(2,:);
    
    x = dk.bsx.mul( rand(n,d), up-lo );
    x = dk.bsx.add( x, lo );

end