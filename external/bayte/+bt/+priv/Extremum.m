classdef Extremum < handle

    properties
        value
    end
    
    methods
        
        function self = Extremum(init)
            self.value = init;
        end
        
        function self = max(self,x)
            if x > self.value
                self.value = x;
            end
        end
        
        function self = min(self,x)
            if x < self.value
                self.value = x;
            end
        end
        
    end

end