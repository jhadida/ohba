function y = dcomb(x,nc,a)
% 
% y = dcomb(x,nc,a=1)
%
% Dirichlet combination of input vertices in nx-by-d matrix x, where d is the dimensionality.
% Parameter alpha can be specified; scalars are expanded to match number of rows in x.
%
% Output y is a nc-by-d matrix.
% 
% JH

    assert( ismatrix(x), 'x should be a NxD matrix' );
    nx = size(x,1);
    
    if nargin < 3, a=1; end
    if isscalar(a), a = a*ones(1,nx); end
    assert( numel(a)==nx && all(a > eps), 'Bad weights vector.' );
    
    y = ant.math.dirichlet( a, nc ) * x;

end