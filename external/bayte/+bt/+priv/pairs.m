function [ij,T] = pairs( v )
%
% Return a Nx2 array with unique pairs of elements in V.
% This is equivalent to nchoosek(v,2), but quicker.
%

    n = numel(v);
    [i,j] = ndgrid( 1:n );
    T = tril(true(n),-1);
    ij = v([i(T),j(T)]);
    
end