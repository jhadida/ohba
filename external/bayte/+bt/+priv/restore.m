function [v,p] = restore(s,type)
% 
% Check structure type, and extract version and properties.
%
    if ischar(s), s = load(s); end
    assert( dk.is.struct(s,{'type','ver','prop'}), 'Bad structure.' );
    assert( strcmp(s.type,type), 'Bad type (expected %s, got %s).', type, s.type );
    v = s.ver;
    p = s.prop;
end