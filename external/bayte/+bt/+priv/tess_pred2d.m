function M = tess_pred2d(T,varargin)
%
% For each pixel in a grid, generate prediction using enclosing element.
%
%   tess_pred2d(T,sz)       -> calls tess_map2d to generate membership
%   tess_pred2d(T,M,xq)     -> faster if already known
%

    % get membership map and associated coordinates for each pixel
    if ismatrix(varargin{1}) && ~isvector(varargin{1})
        M = varargin{1};
        xq = varargin{2};
        args = varargin(3:end);
    else
        [M,G] = bt.priv.tess_map2d(T,varargin{1});
        xq = G.coord();
        args = varargin(2:end);
    end
    
    % list unique elements, and generate predictions for corresponding pixels
    [grp,id] = dk.groupunique(M(:));
    n = numel(id);
    for i = 1:n
        gi = grp{i};
        Ei = T.store.getelem(id(i));
        if isempty(gi), continue; end
        M(gi) = T.value( Ei, xq(gi,:), args{:} );
    end

end