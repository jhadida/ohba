function [M,G] = tess_map2d( T, sz, fac )
%
% For each pixel in a grid of specified size, assign ID of enclosing elements.
%

    assert( T.ndims==2, 'Only works in 2D.' );
    if nargin < 3, fac=5; end
    if isscalar(sz), sz=sz*ones(1,2); end
    
    % convert all leaves to polytopes
    L = T.leaves();
    P = T.to_polytope(L);
    np = numel(P);
    
    % put together necessary information
    xr = [T.lower(1), T.upper(1), sz(1)];
    yr = [T.lower(2), T.upper(2), sz(2)];
    G = dk.obj.Grid( xr, yr );
    d = (G.wx * G.wy) / G.numel();
    
    % create index map
    M = zeros(G.size());
    for i = 1:np
        m = ceil( fac * pi * P{i}.scale()^2 / d );
        s = P{i}.sample(m);
        k = unique(G.ind(s));
        M(k) = L{i}.id;
    end
    
    % fill in the blanks
    z = M == 0;
    [~,k] = bwdist(~z);
    M(z) = M(k(z));

end