classdef Counter < handle
    
    properties
        value
    end
    
    methods
        
        function self = Counter(init)
            self.value = init;
        end
        
        function self = add(self,x)
            self.value = self.value + x;
        end
        
        function self = inc(self)
            self.add(1);
        end
        
        function self = dec(self)
            self.add(-1);
        end
        
    end
    
end