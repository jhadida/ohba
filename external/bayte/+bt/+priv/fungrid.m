function [G,gz,xq] = fungrid(F,n)
%
% Evaluate function on grid.
% Output grid object, grid of evaluation (matrix), and evaluated coordinates (Nx2).
%
% See also: dk.obj.Grid
%

    if nargin < 2, n=80; end
    G = dk.obj.Grid( F.dom, F.dom, n );
    [gz,xq] = G.eval( F.fun );
    
end