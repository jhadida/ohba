classdef PairMap < handle 
%
% Simple implementation of a symmetric associative map:
%   (i,j) => k
%   (j,i) => k
%
% where typically:
%   (i,j) denotes a pair of vertex indices
%   k is the index of the corresponding midpoint
%
% The symmetry of the association is ensured by the use of a symmetric
% pairing function (allowing diagonal elements (i,i)). This means that
% internally, pairs (i,j) are stored as a pairing number p that is unique
% for both (i,j) and (j,i).
%
% JH

    properties
        data
    end

    methods 

        function self = PairMap(varargin)
            self.clear();
            if nargin > 0
                self.restore(varargin{:});
            end
        end
        
        function clear(self)
            self.data = containers.Map( 'KeyType', 'double', 'ValueType', 'double' );
        end
        
        function n = numel(self)
            n = self.data.length();
        end

        % key/value
        function add(self,ij,v)
            [~,p] = proc_ij(ij);
            assert( ~any(self.data.isKey(p)), 'Key already exists.' );
            self.data = [ self.data; containers.Map(p,v) ];
        end

        function h = has(self,ij)
            [~,p] = proc_ij(ij);
            h = self.data.isKey(p);
        end

        function v = get(self,ij)
            [n,p] = proc_ij(ij);
            v = zeros(n,1);
            for i = 1:n 
                if self.data.isKey(p(i))
                    v(i) = self.data(p(i));
                end
            end
        end

        % i/o
        function s = to_struct(self)
            s.type = 'PairMap';
            s.ver = '0.1';
            s.data.key = self.data.keys;
            s.data.val = self.data.values;
        end

        function self = restore(self,s)
            assert( strcmp(s.type,'PairMap'), 'Bad input type.' );
            switch s.ver
            case '0.1'
                self.data = containers.Map( s.data.key, s.data.val );
            otherwise
                error( 'Unknown version: %s', s.ver );
            end
        end

    end

end

function [n,p] = proc_ij(ij)
    assert( ismatrix(ij) && size(ij,2) == 2, 'ij should be Nx2' );
    n = size(ij,1);
    p = ant.math.sympair( ij(:,1), ij(:,2) );
end
