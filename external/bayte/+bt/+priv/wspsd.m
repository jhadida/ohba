function R = wspsd(a,b,w)
%
% Weighted sum of pairwise squared differences
%
%    a   n-by-d
%    b   m-by-d
%    w   1-by-d
%    R   n-by-m
%
%    R_{ij} = \sum_{k=1}^d  w_k^2  (a_{ik} - b_{jk})^2
%
% NOTE:
%   the weights are squared in the sum
% 
% JH

    n = size(a,1);
    m = size(b,1);
    d = size(a,2);
    R = zeros(n,m);
    for i = 1:d
        t = bsxfun( @minus, a(:,i), b(:,i)' ) * w(i);
        R = R + t.*t;
    end
    
end
