function x = dcomb_box(lo,up,n,a)
% 
% x = dcomb_box(lo,up,n,a=1)
%
% Special implementation of Dirichlet combination for boxes defined by their bounds.
%
% See also: bt.priv.dcomb
% 
% JH

    d = numel(lo);
    assert( isvector(lo) && isvector(up) && numel(up)==d, 'Bad upper/lower bounds.' );
    lo = lo(:)';
    up = up(:)';
    
    if nargin < 3, a=1; end
    assert( isscalar(a) && a > eps, 'Alpha should be scalar.' );
    
    x = gamrnd( a * ones(n,d,2), 1, n, d, 2 );
    x = dk.bsx.rdiv( x, sum(x,3) );
    x = dk.bsx.mul( x(:,:,1), lo ) + dk.bsx.mul( x(:,:,2), up );

end