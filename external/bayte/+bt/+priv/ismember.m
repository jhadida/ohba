function m = ismember(a,b)
%
% Fast ismember function for numeric vector inputs.
%

    % this is slower
    %m = any(bsxfun( @eq, a(:), b(:)' ),1);
    
    n = numel(a);
    m = false(1,n);
    b = b(:);
    for i = 1:n
        m(i) = any(a(i)==b);
    end
    
end