function [x,id] = uniqid(x)
%
% Select subset of input items that have a unique ID.
% Input x can be any collection in which items define the field ".id" 
%   (e.g. struct-array or cell of objects).
% 

    [id,k] = unique(dk.mapfun( @(x) x.id, x, true ),'stable');
    x = x(k);
end