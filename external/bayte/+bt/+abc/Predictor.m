classdef Predictor < handle
%
% Predictor objects generate the following predictions at any location:
%   - average function value
%   - deviation of function value
%   - average function gradient
%
% The first two predictions are given by method .pred(), and the gradient by .grad().
% Predictions can be computed using neighbourhood information from an Element, see 
% methods .elmpred() and .elmgrad(). These methods rely on .pred() and .grad(), and 
% the neighbourhood information is derived from function bt.pred.blanket().
%
% Predictor methods require 4 inputs:
%    x   Training coordinates as NxD matrix
%    y   Associated function values as Nx1 vector
%                   OR (stochastic case)
%        mean and std of function values as Nx2 vector
%   xq   Testing coordinates, where prediction is required
%  hyp   Hyperparameters, more details in derived classes
%
% Predictor objects store properties, and typically a Kernel. 
%
%
% DEFAULT OPTIONS
% ---------------
%
%   brad    3      Blanket radius
%   lfac    1      Length-scale factor
%   minlen  1e-4   Minimum scale-length (normalised units)
%   demean  true   Centre element predictions around their mean value
%                  (affects the "long-range" behaviour)
%
% See concrete implementations for additional options.
%
%
% See also: bt.abc.Kernel, bt.obj.Element
% Implementations: bt.pred.GaussianProcess, bt.pred.GPML
%
% JH

    properties (Constant,Abstract)
        type
    end
    properties
        opt
        prior
    end
    
    methods (Abstract)
        
        % check options and return hyperparams structure for prediction
        % NOTE: should use self.chkarg and opt.minlen
        check(~,x,y,xq,hyp);
        
        % predictive value
        [v,vd] = pred(~,x,y,xq,hyp);
        
        % predictive gradient
        g = grad(~,x,y,xq,hyp);
        
        % serialise to structure
        s = to_struct(self);
        
    end
    
    
    % -----------------------------------------------------------------------------------------
    % element prediction methods
    % -----------------------------------------------------------------------------------------
    methods
        
        % predictions within an element
        function [v,vd] = elmpred(self,elm,xq,varargin)
            [x,y,m,hyp] = self.blanket(elm,varargin{:});
            [v,vd] = self.pred(x,y,xq,hyp);
            v = v + m; % recentre predicted means
        end
        
        function g = elmgrad(self,elm,xq,varargin)
            [x,y,~,hyp] = self.blanket(elm,varargin{:});
            g = self.grad(x,y,xq,hyp);
        end
        
        % inference blanket for element in the partition graph
        function [x,y,m,hyp] = blanket(self,elm,varargin)
            
            % compute inference blanket
            rad = self.opt.brad;
            rel = self.opt.demean;
            
            [x,y,m,s] = bt.pred.blanket( elm, rad, rel, varargin{:} );
            assert( size(x,1) >= 2, 'Not enough values for inference.' );
            
            % set hyper-parameters
            hyp.len = s * self.opt.lfac; 
            
        end
        
    end
    
    
    % -----------------------------------------------------------------------------------------
    % internal utils
    % -----------------------------------------------------------------------------------------
    methods (Hidden)
        
        function setopt(self,in,varargin)
            self.opt = dk.getopt( in, 'demean', true, 'brad', 3, 'lfac', 1, 'minlen', 1e-4, varargin{:} );
        end
        
        function valopt(self,varargin)
            dk.struct.restrict( self.opt, horzcat({'demean','brad','lfac','minlen'}, varargin) );
            assert( self.opt.brad >= 1, 'Blanket radius should be >= 1.' );
            assert( self.opt.lfac > eps, 'Length factor should be positive.' );
            assert( self.opt.minlen >= bt.obj.Element.xeps, 'Minimum length is too small.' );
        end
        
        function [n,m,d,y,s] = chkarg(~,x,y,xq)
            assert( ismatrix(x) && ismatrix(xq), 'Bad training or test coordinates.' );
            assert( size(x,2) == size(xq,2), 'Dimension mismatch between x and xq.' );

            n = size(x,1);
            m = size(xq,1);
            d = size(x,2);

            assert( n*m > 0, 'Empty coordinates.' );
            assert( ismatrix(y) && size(y,1)==n, 'Bad value size.' );
            if isvector(y)
                s = zeros(n,1);
            else
                s = y(:,2);
                y = y(:,1);
            end
            assert( all(s >= 0), 'Value std should be non-negative.' );
        end
        
    end
    
end
