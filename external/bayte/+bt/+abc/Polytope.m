classdef Polytope < handle
%
% Polytope instances wrap around a shape structure (with coordinates), to allow 
% manipulation of geometric properties.
%
% Polytopes are used only transiently in Tessellations, mainly to set the geometric
% properties of newly created Elements after each subdivision. This is mainly because
% Polytope instance require the actual coordinates to be stored as properties, whereas
% Elements only store the _indices_ of these coordinates; this saves a lot of memory
% because vertices can be shared between arbitrarily large numbers of Elements.
%
% See also: bt.obj.Element
% Implementations: bt.poly.Simplex, bt.poly.Subbox, bt.poly.Subcore
% 
% JH

    properties (Constant,Abstract)
        type
    end
    properties
        shape
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % abstract interface
    % -----------------------------------------------------------------------------------------
    methods (Static,Abstract)
        
        % validate unpacked shape structure
        check(shape);
        
        % create polytope struct from vertices (or vertex indices)
        make(args);
        
        % insert Polytope vertices into storage, and return packed shape
        % NOTE: 
        %   Only to be used for testing or in very rare cases.
        %   Insertion is usually done on-the-fly during subdivision.
        pack(poly,store);
        
        % unpack vertex indices as coordinates in input shape, and return Polytope instance
        unpack(shape,store);
        
    end
    methods (Abstract)
        
        % dimensionality
        d = ndims(~);
        
        % barycentre of element
        b = barycentre(~);
        
        % more representative centre
        c = faircentre(~);
        
        % noisy centre with a weight param
        c = noisycentre(~,w);
        
        % measure of spatial extent
        s = scale(~);
        
        % 1xd vector of scales in each dimension
        s = scales(~); 
        
        % transform coordinates
        transform(~);
        
        % random points within element
        x = sample(~,n,args);
        
        % plot element in 2d or 3d
        h = plot(~,type,color,args);
        
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % core methods
    % -----------------------------------------------------------------------------------------
    methods
        
        % initialise an element from a shape-struct
        function assign(self,shape)
            self.check( shape );
            self.shape = shape;
        end
        
        % to be called by Partition objects to select centre coordinates
        function x = centre(self,type,varargin)
            if nargin < 2, type='fair'; end
            switch lower(type)
                case 'bary'
                    x = self.barycentre();
                case 'fair'
                    x = self.faircentre();
                case 'noisy'
                    x = self.noisycentre(varargin{:});
                otherwise
                    error( 'Unknown centre type: %s', type );
            end
        end
        
        % i/o methods
        function s = to_struct(self)
            s.type = 'bt.abc.Polytope';
            s.ver = '0.1';
            s.prop.type = self.type;
            s.prop.shape = self.shape;
        end

        function self = restore(self,s)
            [ver,prop] = bt.priv.restore(s,'bt.abc.Polytope');
            assert( strcmp(self.type,prop.type), 'Polytope type mismatch (expected %s, got %s)', self.type, s.type );
            self.assign(prop.shape);
        end
        
    end
    
end