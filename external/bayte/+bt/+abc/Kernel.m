classdef Kernel < handle
%
% Covariance kernels should return a NxP matrix for any given sets of points X1 <Nxd> and X2 <Pxd>.
% This particular implementation requires the hyper-parameters to be given every time, rather than
% stored internally. Hence, Kernel implementations can be seen as a collection of stateless methods.
%
% Kernel instances are typically stored and used by Predictor objects.
%
% See also: bt.abc.Predictor
%
% JH

    properties (Constant,Abstract)
        type
    end

    methods (Abstract)
        
        % autocovariance function
        % returns a vector
        A = autocov(~,x,hyp);
        
        % covariance function
        % returns a matrix
        K = cov(~,x1,x2,hyp);
        
        % derivative of covariance along each dimension
        % if yr is omitted, G is a cell of matrices
        % if yr is specified (more efficient), then G is a mxd matrix
        G = dcov(~,x1,x2,hyp,yr);
        
    end
    
    methods (Static,Hidden)
        
        function [n,m,d] = check_inputs(x1,x2)
            assert( ismatrix(x1) && ismatrix(x2), 'Bad training or test coordinates.' );
            assert( size(x1,2) == size(x2,2), 'Coordinate size mismatch.' );

            n = size(x1,1);
            m = size(x2,1);
            d = size(x1,2);

            assert( n*m > 0, 'Empty coordinates.' );
        end
        
    end
    
end