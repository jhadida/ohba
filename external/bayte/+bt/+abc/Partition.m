classdef Partition < handle
%
% Partition objects mainly implement the following method: 
%   chi = par.subdivide(elm)
%
% which partitions an input Element into multiple children Elements, and returns
% them as a cell. Concrete implementations may or may not have an internal "state", 
% and in practice they are used as function objects.
%
%
% DEFAULT OPTIONS
% ---------------
%
%   centre    bary, fair (default), noisy
%             Only affects the centre coordinates of Elements, not the spatial
%             structure of the partition.
%
%   noedge    true, false (default)
%             If true, neighbourhood information is not computed, and the .edge
%             property of Elements is not modified.
%
%    noise    scalar in [0,1] (default: 0)
%             Used as inverse Dirchlet weight during subdivision, affecting the
%             spatial structure of the partition. Typical values are:
%                 1/50 = very small amounts of noise
%                 1/25 = visibly noticeable noise
%                 1/10 = noisy
%                 1    = (almost) uniform distribution within polytope
%
% See concrete implementations for additional options.
%
%
% See also: bt.obj.Element, bt.abc.Polytope
% Implementations: bt.part.Ternary, bt.part.Barycentric
%
% JH

    properties (Constant,Abstract)
        type
    end
    properties
        opt    % options
    end
    
    methods (Abstract)
        
        % unpack leaf element
        % apply split method to produce children elements
        % update neighbourhood information
        % return children
        chi = subdivide(self,elm);
        
        % serialise to structure
        s = to_struct(self);
        
    end
    
    methods (Static,Hidden)
        
        % sanity checks after subdivision
        function post_checks(parent,children)
            
            % check that children are leaves
            assert( all(cellfun( @(x) x.is_leaf(), children )), 'All children should be leaf elements.' );
            
            % check that parent has children
            cid = cellfun( @(x) x.id, children );
            assert( all(parent.is_child(cid)), 'One or several children were not found in parent.' );
            
            % check that there are no more edges attached to parent
            parent.clear_edges();
            assert( parent.ne == 0, 'The parent element should have no edges after subdivision.' );
            
            % check that all IDs are unique
            parent.check_ids();
            n = numel(children);
            for i = 1:n
                children{i}.check_ids();
            end
            
        end
        
        % transfer all edges to a child, optionally swapping fid
        function transfer( parent, edges, child, varargin )
            
            assert( isa(parent,'bt.obj.Element'), 'Parent should be an element.' );
            assert( isa(child,'bt.obj.Element'), 'Child should be an element.' );
            
            % concatenate all edges for cell inputs
            if iscell(edges)
                edges = horzcat(edges{:});
            end
            if isempty(edges), return; end
            
            % copy each edge
            n = numel(edges);
            nei = parent.store.elem([ edges.nei ]);
            for i = 1:n
                child.insert_edge( edges(i), varargin{:} );
                nei{i}.update_edge( parent, child );
            end
            
        end
        
    end
    
    methods (Hidden)
        
        % sanity checks before subdivision
        function store = pre_checks(self,elm,type,varargin)
            
            % options
            self.valopt( varargin{:} );
            assert( dk.num.between(self.opt.noise,0,1), 'Noise option should be between 0 and 1.' );
            
            % input element
            type = dk.wrap(type);
            assert( isa(elm,'bt.obj.Element'), 'Input is not an element.' );
            assert( elm.is_leaf(), 'Element is not a leaf.' ); 
            assert( ismember(elm.type,type), 'Unexpected element type (%s).', elm.type );
            store = elm.store;
            
        end
        
        % options management
        function setopt(self,in,varargin)
            self.opt = dk.getopt( in, 'centre', 'fair', 'noedge', false, 'noise', 0, varargin{:} );
        end
        
        function valopt(self,varargin)
            dk.struct.restrict( self.opt, horzcat({'centre','noedge','noise'}, varargin) );
        end
        
    end
    
end