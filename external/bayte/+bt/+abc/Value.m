classdef Value < handle
    
    methods (Abstract)
        
        % expected mean
        m = mean(self);
        
        % expected deviation
        s = std(self);
        
        % can be saved
        data = saveobj(self);
        
    end
    methods
        
        % 1x2 vector with expected mean and deviation
        function v = value(self)
            v = [ self.mean(), self.std() ];
        end
        
    end
    
    methods (Abstract, Static)
        
        % can be loaded
        loadobj(data);
        
    end
    
end