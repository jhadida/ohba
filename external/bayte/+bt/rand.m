function T = rand( T, meth, Niter, Nelm )
%
% T = bt.rand( T, method, Niter, Nelm=0 )
%
% Create a random tessellation using specified selection method.
% Random subdivision continues until BOTH Niter and Nelm are satisfied.
% Note that input Tessellation instances are modified in-place.
%
%
% INPUTS
% ------
%
%   T           Tessellation object, or cell of arguments for bt.tess.
%
%   method      Method of random leaf selection:
%                   invdepth    iteratively select leaves with probability
%                               inversely proportional to their depth
%                   traverse    iteratively select one leaf per level
%
%   Niter       Minimum number of iterations
%   Nelm        Minimum number of elements
%
%
% See also: bt.tess
%
% JH

    % create tessellation object
    if ~isa(T,'bt.obj.Tessellation')
        T = bt.tess(T{:});
    end
    
    % random leaf selection method
    if isempty(meth), meth='default'; end
    switch lower(meth)
        case {'invdepth','default'}
            sel = @invdepth;
        case 'traverse'
            sel = @traverse;
        otherwise
            error( 'Unknown selection method: %s', meth );
    end
    
    % iterate until terminal criterion is met
    if isempty(Niter), Niter=0; end
    if nargin < 4 || isempty(Nelm), Nelm=0; end

    it = 0;
    while it < Niter || T.nelem < Nelm
        it = it + 1;
        L = sel(T.leaves());
        n = numel(L);
        if Nelm > 0
            n = min( n, Nelm-T.nelem );
        end
        for j = 1:n
            T.subdivide(L{j});
        end
    end
    
end

function L = invdepth(L)
%
% Sample leaves with probability inversely proportional to their depth.
%
    n = numel(L);
    P = 1./cellfun( @(e) e.depth, L );
    L = L(rand(1,n) < P);
end

function L = traverse(L)
%
% Select one random leaf per level.
%
    d = cellfun( @(e) e.depth, L );
    g = dk.grouplabel(d+1);
    n = numel(g);
    s = zeros(1,n);
    
    for k = 1:n
        gk = g{k};
        if isempty(gk), continue; end
        s(k) = gk(randi(numel(gk)));
    end
    L = L(nonzeros(s));
end