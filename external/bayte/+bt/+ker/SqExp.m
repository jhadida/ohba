classdef SqExp < bt.abc.Kernel
    
    properties (Constant)
        type = 'se';
    end
    
    methods
        
        % autocovariance
        function A = autocov(self,x,hyp)
            A = ones(size(x,1),1);
        end
        
        % covariance function
        function K = cov(self,x1,x2,hyp)
            
            [n,m,d] = self.check_inputs(x1,x2);
            
            % get length scales
            L = hyp.len;
            if isscalar(L), L = L * ones(1,d); end
            assert( numel(L)==d && all(L > eps), 'Bad length scales.' );
            
            % compute covariance
            K = bt.priv.wspsd(x1,x2,1./L);
            K = exp(-K/2);
            
        end
        
        % derivative of covariance function
        function G = dcov(self,x1,x2,hyp,yr)
            
            [n,m,d] = self.check_inputs(x1,x2);
            
            % get length scales
            L = hyp.len;
            if isscalar(L), L = L * ones(1,d); end
            assert( numel(L)==d && all(L > eps), 'Bad length scales.' );
            
            % base covariance
            R2 = bt.priv.wspsd(x1,x2,1./L);
            K = - sqrt(R2) .* exp( -R2/2 );
            
            % compute gradient
            if nargin > 4
                
                assert( numel(yr)==m, 'Bad vector size.' );
                yr = yr(:);
                G = zeros(n,d);
                
                for i = 1:d
                    dK = (2/L(i)^2) * dk.bsx.sub( x1(:,i), x2(:,i)' ) .* K;
                    G(:,i) = dK * yr;
                end
                
            else
                
                G = cell(1,d);
                for i = 1:d
                    G{i} = (2/L(i)^2) * dk.bsx.sub( x1(:,i), x2(:,i)' ) .* K;
                end
                
            end
            
        end
        
    end
    
end

