function ker = factory(ker)
%
% ker = bt.ker.factory(ker)
% 
% Create instance of bt.abc.Kernel implementation matching input name.
% Current valid names are:
%
%   se, sqexp, squaredexponential
%   mat32, matern32 (, default)
%   mat52, matern52
%
% JH

    if ~isa(ker,'bt.abc.Kernel')
        assert( ischar(ker), 'Expected a Kernel name.' );
        switch lower(ker)
            case {'se','sqexp','squaredexponential'}
                ker = bt.ker.SqExp();
            case {'mat32','matern32','default'}
                ker = bt.ker.Matern32();
            case {'mat52','matern52'}
                ker = bt.ker.Matern52();
            otherwise
                error( 'Unknown kernel name: %s', ker );
        end
    end
    
    assert( isa(ker,'bt.abc.Kernel'), 'Non-string inputs should be Kernel instances.' );

end