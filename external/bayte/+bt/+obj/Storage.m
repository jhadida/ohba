classdef Storage < handle
%
% Vertices are stored as rows in a growing matrix.
% Elements are stored in a growing cell.
%
% See also: dk.ds.Matrix, dk.ds.Cell
%
% JH
    
    properties (Hidden,SetAccess=private)
        id
        vtx
        elm
    end
    properties (Transient,Dependent)
        ndims
        nvert
        nelem
    end
    events
        NewVertices
        NewElements
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % core methods
    % -----------------------------------------------------------------------------------------
    methods
        
        function self = Storage(varargin)
            self.id = dk.time.datestr('stamp');
            self.clear();
            switch nargin
                case 0 % nothing to do
                case 1
                    if ischar(varargin{1})
                        self.load(varargin{1});
                    else
                        self.reset(varargin{:});
                    end 
                otherwise
                    self.reset(varargin{:});
            end
        end
        
        function clear(self)
            self.vtx = dk.ds.Matrix();
            self.elm = dk.ds.Cell();
        end
        
        function reset(self,ndims,bsize)
            if nargin < 3, bsize = 100; end
            self.vtx.reset(ndims,bsize);
            self.elm.reset(bsize);
        end
        
        % dimensions
        function n = get.ndims(self), n = self.vtx.ncols; end
        function n = get.nvert(self), n = self.vtx.nrows; end
        function n = get.nelem(self), n = self.elm.count; end
        
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % access, assignment and creation
    % -----------------------------------------------------------------------------------------
    methods
        
        % return valid IDs
        function id = vert_ids(self)
            id = self.vtx.find();
        end
        function id = elem_ids(self)
            id = self.elm.find();
        end
        
        function id = leaf_ids(self)
            id = self.elm.find();
            id = id(arrayfun( @(k) self.elm.data{k}.is_leaf(), id ));
        end
        function id = eval_ids(self)
            id = self.elm.find();
            id = id(arrayfun( @(k) self.elm.data{k}.has_value(), id ));
        end
        
        % access vertices as Nxd MATRIX
        % calling without k returns all valid vertices
        function [x,k] = vert(self,k)
            if nargin < 2
                k = self.vtx.find();
            else
                self.vtx.chkind(k);
            end
            x = self.vtx.data(k,:);
        end
        
        % access elements and edges as CELL
        % calling without k returns all valid elements or edges
        function [e,k] = elem(self,k)
            if nargin < 2
                k = self.elm.find();
            else
                self.elm.chkind(k);
            end
            e = self.elm.data(k);
        end
        
        % single element/edge access by index
        function e = getelem(self,k)
            assert( isscalar(k), 'Only scalar indices are allowed, use elem() instead.' );
            self.elm.chkind(k);
            e = self.elm.data{k};
        end
        
        % access element's coordinates as Nxd MATRIX
        % calling without k returns all valid element's coordinates
        function [x,k] = centre(self,varargin)
            [E,k] = self.elem(varargin{:});
            x = dk.mapfun( @(e) e.centre, E );
            x = vertcat(x{:});
        end
        
        % get leaf elements (faster than DFS on root element)
        function [L,k] = leaves(self)
            k = self.leaf_ids();
            L = self.elm.data(k);
        end
        
        function [E,k] = eval(self)
            k = self.eval_ids();
            E = self.elm.data(k);
        end
        
        % search elements
        function [id,d] = nearest(self,x,varargin)
            [c,k] = self.centre(varargin{:});
            [id,d] = ant.mex.nearestL2( c, x );
            id = k(id);
        end
        
        function id = within(self,x,r,varargin)
            [c,k] = self.centre(varargin{:});
            id = ant.mex.withinL2( c, x, r );
            id = k(id);
        end
        
        % pre-emptive allocation
        function self = reserve_vert(self,n)
            self.vtx.reserve(n);
        end
        function self = reserve_elem(self,n)
            self.elm.reserve(n);
        end
        
        % insert vertices
        function k = insert(self,x)
            if isempty(x)
                k = [];
            else
                k = self.vtx.add(x);
                dk.notify( self, 'NewVertices', 'idx', k );
            end
        end
        
        % new elements
        function E = new_elem(self)
            E = self.new_elems(1);
            E = E{1};
        end
        function E = new_elems(self,n)
            if n <= 0
                E = {}; return;
            end
            
            k = self.elm.book(n);
            for i = 1:n
                self.elm.data{k(i)} = bt.obj.Element( k(i), self );
            end
            E = self.elem(k);
            
            dk.notify( self, 'NewElements', 'idx', k );
        end
        
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % i/o
    % -----------------------------------------------------------------------------------------
    methods
        
        function s = save(self,file)
            s = self.save_v01();
            s.type = 'bt.obj.Storage';
            if nargin > 1
                dk.save( file, s );
            end
        end
        
        function load(self,s)
            if ischar(s), s = load(s); end
            [ver,prop] = bt.priv.restore(s,'bt.obj.Storage');
            switch ver
                case '0.1'
                    self.load_v01(prop);
                otherwise
                    error('Unknown version: %s',ver);
            end
        end
        
    end
    methods (Access=private, Hidden)
        
        function s = save_v01(self)
            s.ver = '0.1';
            s.prop.id  = self.id;
            s.prop.vtx = self.vtx.serialise();
            s.prop.elm = serialise_cell( self.elm );
        end
        
        function load_v01(self,s)
            
            self.id = s.id;
            
            % vertices
            self.vtx = dk.ds.Matrix();
            self.vtx.unserialise(s.vtx);
            
            % first pass to instanciate Elements
            self.elm = dk.ds.Cell(s.elm);
            k = self.elm.find();
            n = numel(k);
            for i = 1:n
                ki = k(i);
                self.elm.data{ki} = bt.obj.Element(ki,self);
            end
            
            % second pass to restore them
            for i = 1:n
                ki = k(i);
                self.elm.data{ki}.restore(s.elm.data{ki});
            end
            
            % last pass to merge shared values
            %
            % NOTE:
            %   This is needed because serialisation does not preserve values
            %   copied by reference between shared elements.
            %
            function inherit_value(elm)
                if elm.shared
                    elm.value = elm.parent.value;
                end
            end
            if n > 0
                self.elm.data{k(1)}.root().dfs(@inherit_value);
            end
            
        end
        
    end
    
end

function s = serialise_cell(obj)
%
% Serialise instance of dk.ds.Cell.
% Then call to_struct() on used elements, and set unused elements to [].
%
    s = obj.serialise();
    n = numel(s.data);
    z = n - nnz(s.used);
    s.data( s.used) = dk.mapfun( @(x) x.to_struct(), s.data(s.used), false );
    s.data(~s.used) = cell(1,z);
end
