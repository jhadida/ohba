classdef Element < handle
%
% Elements are essential components of a Tessellation.
%
% Structurally, they contain information about both the partition hierarchy ("parent", "children" 
% and "depth" properties), and the neighbourhood ("edge" property).
%
% Functionally, they contain information about the underlying polytope ("type", "shape", "centre" 
% and "scale" properties), as well as about the objective and inference ("value" class).
% 
% They carry a reference to a storage object, which allows the shape structure to be defined with
% indices of vertices, rather than in terms of actual coordinates. This saves a lot of memory, 
% because some vertices can be shared with arbitrarily many elements.
%
% 
% -----------------------------------------------------------------------------------------
% MAIN METHODS
% 
% The methods you are most likely to be using are the following:
%
%   search(x)           For each input point, find element in subtree with closest "centre".
%   to_polytope()       Return an instance of Polytope object.
%   adj_blanket(n=1)    List neighbour elements at most n edges away.
%
% JH

    properties (Constant)
        xeps = 1e-6;
    end

    properties (SetAccess = protected)
        id      % unique storage ID
        store   % reference to memory storage
        
        type    % polytope type
        shape   % polytope structure
        centre  % coordinates for evaluation
        scale   % size of polytope
        
        parent  % parent
        shared  % is centre same as parent?
        depth   % tree depth
        child   % children
        edge    % neighbours
    end
    properties (SetAccess = public)
        value   % value class (see bt.abc.Value)
    end
    properties (Transient,Dependent)
        nd      % #dimensions
        nc      % #children
        ne      % #edges
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % core methods
    % -----------------------------------------------------------------------------------------
    methods
        
        function self = Element(id,store)
            assert( isa(store,'bt.obj.Storage'), 'Bad input store.' );
            self.id = id;
            self.store = store;
            self.clear();
        end
        
        function clear(self)
            self.shape  = [];
            self.centre = [];
            self.scale  = 0;
            self.value  = [];
            
            self.parent = [];
            self.shared = false;
            self.depth  = 0;
            self.child  = {};
            self.clear_edges();
        end
        
        % dependent props
        function n = get.nd(self), n = self.store.ndims; end
        function n = get.nc(self), n = numel(self.child); end
        function n = get.ne(self), n = numel(self.edge); end
        
        % compare IDs with another element
        function y = cmp(self,elm)
            if isa(elm,'bt.obj.Element')
                elm = elm.id;
            end
            y = self.id == elm;
        end
        
        % check value
        function y = has_value(self)
            y = ~isempty(self.value);
        end
        
        % get children or edges IDs
        function id = parent_id(self)
            if self.is_root()
                id = 0;
            else
                id = self.parent.id;
            end
        end
        function id = children_id(self)
            id = cellfun( @(x) x.id, self.child );
        end
        function id = facets_id(self)
            id = [self.edge.fid];
        end
        function id = neighbours_id(self)
            id = [self.edge.nei];
        end
        
        function check_ids(self)
            nodup = @(x) numel(unique(x)) == numel(x);
            assert( nodup(self.children_id()), 'Found duplicate children elements.' );
            assert( nodup(self.neighbours_id()), 'Found duplicate neighbours.' );
        end
        
        % search elements with nearest centre for each input point
        %
        % WARNING: this does NOT return enclosing elements!
        %
        function id = search(self,x)
            L = self.leaves();
            ctr = dk.mapfun( @(e) e.centre, L, false );
            ctr = vertcat( ctr{:} );
            k = ant.mex.nearestL2(ctr,x);
            
            id = cellfun( @(e) e.id, L );
            id = id(k);
        end
        
        % serialise
        function s = to_struct(self)
            s.type = 'bt.obj.Element';
            s.ver  = '0.1'; % dont store type to save space
            
            % copy by value (store ID to validate on restore)
            f = {'id','type','shape','centre','scale','value','depth','shared'}; 
            n = numel(f);
            for i = 1:n
                s.prop.(f{i}) = self.(f{i});
            end
            
            % extract IDs
            s.prop.parent = self.parent_id();
            s.prop.child = cellfun( @(x) x.id, self.child );
            s.prop.edge = self.edge;
        end
        
        % restore
        function self = restore(self,s)
            
            [ver,s] = bt.priv.restore(s,'bt.obj.Element');
            assert( self.id == s.id, 'ID mismatch' );
            self.clear();
            
            % copy by value
            f = {'type','shape','centre','scale','value','depth','shared'};
            n = numel(f);
            for i = 1:n
                self.(f{i}) = s.(f{i});
            end
            
            % restore linked elements
            if s.parent > 0
                self.parent = self.store.getelem(s.parent);
            else
                self.parent = [];
            end
            self.child = self.store.elem(s.child);
            self.edge = s.edge;
        end
        
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % conversions & assignments
    % -----------------------------------------------------------------------------------------
    methods
        
        % convert to polytope
        function p = to_polytope(varargin)
            p = bt.poly.from_elem(varargin{:});
        end
        
        % assign polytope structure (compressed shape)
        function self = set_polytope(self,type,shape,varargin)
            assert( isstruct(shape), 'Bad input shape.' );
            
            % assign polytope fields
            self.type = type;
            self.shape = shape;
            
            % derive properties
            P = self.to_polytope();
            self.centre = P.centre(varargin{:});
            self.scale = P.scale();
        end
        
        
        % return a nx3 array with columns: id, parent, depth
        function p = to_plist(self)
            
            % get all subnodes
            D = self.offspring();
            n = numel(D);
            p = zeros(n+1,3);
            
            % create output array
            p(1,:) = [ self.id, self.parent_id(), self.depth ];
            for i = 1:n
                p(i+1,:) = [ D{i}.id, D{i}.parent.id, D{i}.depth ];
            end
            
            % remap parent indices
            m = sparse( 1+p(:,1), 1, 1:n+1 );
            p(:,2) = m( 1+p(:,2) );
            p(1,2) = 0; % force current node as the root
            
        end
        
        % convert subtree to an instance of dk.ds.SplitTree (mainly for plotting)
        function T = to_dkTree(self)
            
            % convert to suitable representation
            p = self.to_plist();
            p = p(2:end,:); % remove the root
            
            % setup the output tree
            T = dk.ds.SplitTree();
            T.set_props(1,'elm',self);
            
            % group by parent
            g = dk.grouplabel(p(:,2));
            n = numel(g);
            for i = 1:n
                gi = g{i};
                ni = numel(gi);
                if ni > 0
                    T.split(i, ni, 'elm', self.store.elem(p(gi,1)));
                end
            end
            
        end
        
        % add children
        function self = add_children(self,varargin)
            
            % check types
            elm = dk.wrap(varargin);
            assert( all(cellfun( @(x) isa(x,'bt.obj.Element'), elm )), 'Inputs should be elements.' );
            n = numel(elm);
            
            % set inherited children properties
            self.child = [self.child, elm];
            for i = 1:n
                elm{i}.parent = self;
                elm{i}.depth = self.depth+1;
                elm{i}.shared = norm(self.centre - elm{i}.centre) < self.xeps;
                
                % if child's centre is shared, inherit value
                if elm{i}.shared
                    elm{i}.value = self.value;
                end
            end
            
        end
        
    end
    methods (Hidden) % to be called by Edge objects only
        
        function clear_edges(self)
            self.edge = dk.struct.repeat( {'fid','nei'}, 1, 0 );
        end
        
        function apply_edges(self,fun)
            n = numel(self.edge);
            for i = 1:n
                self.edge(i) = fun(self.edge(i));
            end
        end
        
        function add_edge(self,nei,fid)
            self.edge(end+1) = struct('fid',fid,'nei',nei.id);
        end
        
        function insert_edge(self,e,fid)
            if nargin > 2
                e.fid = fid;
            end
            self.edge(end+1) = e;
        end
        
        function update_edge(self,old,new)
            self.edge([self.edge.nei] == old.id).nei = new.id;
        end
        
        function duplicate_edge(self,old,new)
            e = self.edge([self.edge.nei] == old.id);
            e.nei = new.id;
            self.edge(end+1) = e;
        end
        
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % neighbour graph methods
    % -----------------------------------------------------------------------------------------
    methods
        
        % edge-listing
        function [E,u] = group_edges_by_facet(self)
        % returns a cell of struct-arrays with same fid in each cell
        
            fid = self.facets_id();
            u = unique(fid);
            n = numel(u);
            E = cell( 1, n );
            for i = 1:n
                m = fid == u(i);
                E{i} = self.edge(m);
            end
        end
        
        % test/find edge with given element
        function y = is_neighbour(self,elm)
            y = any(self.neighbours_id() == elm.id);
        end
        
        function E = edge_with(self,elm)
            E = self.edge(self.neighbours_id() == elm.id);
        end
        
        function E = edges_on_facet(self,fid)
        % returns a subset of edges
        
            m = self.facets_id() == fid;
            E = self.edge(m);
        end
        
        function E = edges_on_same_facet_as(self,elm)
        % returns a subset of edges
        
            e = self.edge_with(elm);
            assert( numel(e)==1, 'Link not found or invalid: %d <=> %d', self.id, elm.id );
            E = self.edges_on_facet( e.fid );
        end
        
        % immediate neighbours
        function nei = neighbours(self)
            nei = self.store.elem(self.neighbours_id());
        end
        
        % neighbours up to rad edges away 
        function nei = blanket(self,rad,incself)
            if nargin < 2, rad=1; end
            if nargin < 3, incself=false; end
            
            rad = rad+1;
            nei = cell(1,rad);
            
            % initialise
            nei{1} = {self};
            visited = containers.Map( 'KeyType', 'uint64', 'ValueType', 'uint8' );
            visited(self.id) = 0;
            
            % iterate over unseen neighbours
            for i = 2:rad
                k = cellfun( @(x) [x.edge.nei], nei{i-1}, 'UniformOutput', false );
                k = unique(uint64(horzcat(k{:})));
                k = k(~visited.isKey(num2cell(k)));
                if isempty(k), break; end
                
                nei{i} = self.store.elem(k);
                visited = [visited; containers.Map( k, zeros(size(k),'uint8') )];
            end
            
            % exclude self
            if incself
                nei = horzcat( nei{:} );
            else
                nei = horzcat( nei{2:end} );
            end
        end
        
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % partition tree methods
    % -----------------------------------------------------------------------------------------
    methods
        
        function y = is_root(self)
            y = isempty(self.parent);
        end
        
        function y = is_leaf(self)
            y = isempty(self.child);
        end
        
        function y = is_child(self,id)
            y = bt.priv.ismember( id, self.children_id() );
        end
        
        
        % iterate in depth-first manner (efficient)
        function dfs(self,callback,maxd)
            if nargin < 3, maxd=inf; end
            if maxd <= 0, return; end
            
            n = self.nc;
            for i = 1:n
                ci = self.child{i};
                callback(ci); 
                ci.dfs(callback,maxd-1);
            end
        end
        
        % iterate in breadth-first manner (not memory efficient)
        function bfs(self,callback,maxd)
            if nargin < 3, maxd=inf; end
            
            cur = self.child;
            n = numel(cur);
            nxt = cell(1,n);
            
            while n > 0 && maxd > 0
                for i = 1:n
                   ci = cur{i};
                   callback(ci);
                   nxt{i} = ci.child;
                end
                cur = horzcat(nxt{:});
                n = numel(cur);
                nxt = cell(1,n);
                maxd = maxd-1;
            end
        end
        
        % similar to bfs, but calling with a cell of _all_ nodes 
        % at given depth in one go
        function levels(self,callback,maxd)
            if nargin < 3, maxd=inf; end
            
            cur = self.child;
            while ~isempty(cur) && maxd > 0
                callback(cur);
                cur = dk.mapfun( @(x) x.child, cur, false );
                cur = horzcat(cur{:});
                maxd = maxd-1;
            end
        end
        
        % count elements of different types
        function c = count(self,type)
            c = bt.priv.Counter(0);
            switch lower(type)
                case {'a','all'}
                    cfun = @(x) c.add(1);
                case {'l','leaf','leaves'}
                    cfun = @(x) c.add(x.is_leaf());
                case {'n','non-leaf','non-leaves'}
                    cfun = @(x) c.add(~x.is_leaf());
                otherwise
                    error( 'Unknown type: %s', type );
            end
            self.dfs(cfun);
        end
        
        % return list of ancestor elements (excluding self)
        function A = ancestors(self)
            p = self.parent;
            d = self.depth;
            A = cell(1,d);
            
            while ~isempty(p)
                A{d} = p;
                d = d-1;
                p = p.parent;
            end
        end
        function A = ancestors_id(self)
            p = self.parent;
            d = self.depth;
            A = zeros(1,d);
            
            while ~isempty(p)
                A(d) = p.id;
                d = d-1;
                p = p.parent;
            end
        end
        
        % return the root of the tree
        function R = root(self)
            R = self;
            p = self.parent;
            
            while ~isempty(p)
                R = p;
                p = p.parent;
            end
        end
        
        % return list of leaves (not very efficient)
        function L = leaves(self)
            
            % current element is a leaf?
            if self.is_leaf()
                L = { self };
                return;
            end
            
            % assign leaf elements
            c = self.count('leaf');
            L = cell(1,c);
            function callback(elm)
                if elm.is_leaf()
                    L{c} = elm;
                    c = c-1;
                end
            end
            self.dfs( @callback );
            
        end
        
        % number of levels under
        function h = height(self)
            d = self.depth;
            h = 0;
            
            function callback(elm)
                h = max( h, elm.depth-d );
            end
            self.dfs( @callback );
        end
        
        % list of all elements under self
        function D = offspring(self)
            D = bt.priv.Aggregate();
            self.levels( @(x) D.append(x) );
            D = D.hcat();
        end
        
        % list of widths by level
        function w = widths(self)
            w = bt.priv.Aggregate();
            self.levels( @(x) w.append(numel(x)) );
            w = w.hcat();
        end
        
        % largest level under
        function w = width(self)
            w = max(self.widths());
        end
        
    end
    
end
