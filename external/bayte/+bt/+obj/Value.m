classdef Value < bt.abc.Value
%
% Basic implementation of deterministic value class.
%
    
    properties
        v
    end
    
    methods
        
        function self = Value(v)
            assert( isscalar(v), 'Only scalar values accepted in this implementation.' );
            self.v = v;
        end
        
        function m = mean(self)
            m = self.v;
        end
        
        function s = std(self)
            s = 0;
        end
        
        function s = saveobj(self)
            s.type = 'bt.obj.Value';
            s.ver = '0.1';
            s.prop.v = self.v;
        end
        
    end
    
    methods (Static)
        
        function obj = loadobj(s)
            [ver,s] = bt.priv.restore(s,'bt.obj.Value');
            switch ver
                case '0.1'
                    obj = bt.obj.Value(s.v);
                otherwise
                    error( 'Unknown version: %s', ver );
            end
        end
        
    end
    
end