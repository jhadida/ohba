classdef Drawing < handle
%
% Drawing objects wrap around Matlab's graphical objects when plotting polytopes.
%
% See also: bt.poly.*
%
% JH

    properties
        gobj
        data
        
        ndims
        nvert
    end
    properties (Transient,Dependent)
        type
        nobj
    end 
    
    methods
        
        function self = Drawing(nd,nv)
            self.ndims = nd;
            self.nvert = nv;
            self.gobj = gobjects(0);
            self.data = struct();
        end
        
        function n = get.nobj(self)
            n = numel(self.gobj); 
        end
        
        function t = get.type(self)
            switch class(self.gobj)
                case 'matlab.graphics.primitive.Patch'
                    t = 'patch';
                case 'matlab.graphics.chart.primitive.Line'
                    t = 'line';
                otherwise
                    t = 'none';
            end
        end
        
        function col = reset(self,nobj,col)
            self.gobj = gobjects(nobj,1);
            self.data = struct();
            
            if nargin > 2
                col = self.rgbmat(col);
            else
                col = [];
            end
        end
        
        function erase(self)
            n = self.nobj;
            for i = 1:n
                delete( self.gobj(i) );
            end
            self.reset(n);
        end
        
        % get figure handle containing this drawing
        function h = figobj(self)
            h = ancestor( self.gobj(1), 'figure', 'toplevel' );
        end
        
        % bring figure to the top
        function showfig(self)
            figure(self.figobj());
        end
        
        % process matrix of RGB colours
        function col = rgbmat(self,col)
            assert( ismatrix(col) && size(col,2)>=3, 'Should be Nx3 matrix of RGB colours.' );
            n = self.nobj;
            
            if size(col,1) == 1
                col = repmat(col,n,1);
            end
            
            assert( size(col,1)==n, 'Bad number of colours.' );
            assert( all(dk.num.between(col(:),0,1)), 'Intensities should be between 0 and 1.' );
        end
        
        % define data
        function setdata(self,name,value)
            dk.reject( 'w', isfield(self.data,name), 'Field "%s" will be overwritten.', name );
            self.data.(name) = value;
        end
        
        function value = getdata(self,name,value)
            if isfield(self.data,name)
                value = self.data.(name);
            end
        end
        
        % set colour(s)
        function setcolor(self,col,varargin)
            n = self.nobj;
            c = self.rgbmat(col);
            switch self.type
                case 'line'
                    for i = 1:n
                        set( self.gobj(i), 'Color', c(i,:), varargin{:} );
                    end
                case 'patch'
                    setalpha = size(c,2) > 3;
                    for i = 1:n
                        if setalpha
                            set( self.gobj(i), 'FaceColor', c(i,1:3), 'FaceAlpha', c(i,4), varargin{:} );
                        else
                            set( self.gobj(i), 'FaceColor', c(i,1:3), varargin{:} );
                        end
                    end
            end
        end
        
        % get colour(s)
        function col = getcolor(self)
            switch self.type
                case 'line'
                    col = dk.mapfun( @(x) x.Color, self.gobj, false );
                    col = vertcat( col{:} );
                case 'patch'
                    col = dk.mapfun( @(x) [x.FaceColor, x.FaceAlpha], self.gobj, false );
                    col = vertcat( col{:} );
            end
        end
        
        % blink
        function blink(self,col,w)
            if nargin < 3, w=0.6; end
            if nargin < 2, col=[1,0,0]; end
            self.showfig();
            
            n = 3;
            ref = self.getcolor();
            while n > 0
                self.setcolor(col,'LineWidth',3);
                drawnow();
                pause(w/2);
                
                self.setcolor(ref,'LineWidth',0.5);
                drawnow();
                pause(w/2);
                
                n = n-1;
            end
        end
        
    end
    
end