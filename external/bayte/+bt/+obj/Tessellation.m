classdef Tessellation < handle
%
% Tessellation objects contain:
%   - info about the original space-domain (assumed to be a cuboid), for normalisation
%   - a root Element representing the (normalised) initial domain
%   - instances of Partition and Predictor objects
%   - instance of Storage for memory management
% 
% The methods are divided into 4 categories:
%   - normalisation
%   - subdivision
%   - prediction
%   - display (only in 2d and 3d)
%
%
% See also: bt.obj.Element, bt.abc.Partition, bt.abc.Predictor, bt.obj.Storage
%
% JH

    properties (SetAccess=private)
        store
        lower
        upper
        root
        part
        pred
    end
    properties (Transient,Dependent)
        ndims
        nvert
        nelem
        nedge
    end
    events
        Subdivision
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % core methods
    % -----------------------------------------------------------------------------------------
    methods
        
        function self = Tessellation(varargin)
            switch nargin
                case 1
                    self.restore(varargin{1});
                case 4
                    self.reset(varargin{:});
                otherwise
                    error( 'Bad inputs' );
            end
        end
        
        % initialise tessellation object
        function self = reset(self,lower,upper,part,pred)
            
            % check inputs
            d = numel(lower);
            assert( d >= 2, 'Dimensionality should be at least 2.' );
            assert( isvector(lower) && isvector(upper) && numel(upper) == d, ...
                'Lower and upper should be same-sized vectors.' );
            assert( all(lower < upper), 'Lower bound should be <= upper bound.' );
            
            % assign partition
            assert( isa(part,'bt.abc.Partition'), 'Third input should be a Partition object.' );
            self.part = part;
            
            % assign inference
            assert( isa(pred,'bt.abc.Predictor'), 'Fourth input should be an Predictor object.' );
            self.pred = pred;
            
            % insert normalised bounds in storage
            self.store = bt.obj.Storage(d);
            self.lower = lower(:)';
            self.upper = upper(:)';
            lu = self.store.insert([ zeros(1,d); ones(1,d) ]);
            
            % create root element
            self.root = self.store.new_elem();
            self.root.set_polytope( 'subbox', bt.poly.Subbox.make(lu(1),lu(2)) );
            
        end
        
        % proxy methods
        function n = get.ndims(self), n = self.store.ndims; end
        function n = get.nvert(self), n = self.store.nvert; end
        function n = get.nelem(self), n = self.store.nelem; end
        function n = get.nedge(self), n = self.store.nedge; end
        
        % i/o
        function s = save(self,fname)
            s.type = 'bt.obj.Tessellation';
            s.ver = '0.1';
            
            s.prop.store = self.store.save();
            s.prop.lower = self.lower;
            s.prop.upper = self.upper;
            s.prop.root = self.root.id;
            s.prop.part = self.part.to_struct();
            s.prop.pred = self.pred.to_struct();
            
            if nargin > 1
                dk.save( fname, s );
            end
        end
        function s = saveobj(self)
            s = self.save();
        end
        
        function self = restore(self,s)
            if ischar(s), s = load(s); end
            [ver,prop] = bt.priv.restore( s, 'bt.obj.Tessellation' );
            switch lower(ver)
                case '0.1'
                    part = bt.part.restore(prop.part);
                    pred = bt.pred.restore(prop.pred);
                    self.reset( prop.lower, prop.upper, part, pred );
                    self.store.load( prop.store );
                    self.root = self.store.getelem( prop.root );
                otherwise
                    error( 'Unknown version: %s', ver );
            end
        end
        
    end
    methods (Static)
        
        function obj = loadobj(data)
            obj = bt.obj.Tessellation(data);
        end
        
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % prediction methods
    % -----------------------------------------------------------------------------------------
    methods
        
        % get ID of nearest leaf element for each query point
        function [id,x] = nearest(self,x,isnorm)
            if nargin < 3 || ~isnorm
                x = self.normalise(x);
            end
            
            elm = self.leaves();
            ctr = self.centre(elm,false);
            k = ant.mex.nearestL2( ctr, x );
            
            id = cellfun( @(e) e.id, elm(k) );
        end
        
        % proxy methods for prior management
        function prior(self,x,y,r,isnorm)
            if nargin < 5, isnorm=false; end
            if ~isnorm
                x = self.normalise(x);
                r = mean(r./self.delta());
            end
            self.pred.prior = bt.obj.Prior(x,y,r);
        end
        function prior_clear(self)
            self.pred.prior.clear();
        end
        function prior_disable(self,x,isnorm)
            if nargin < 3, isnorm=false; end
            if ~isnorm
                x = self.normalise(x);
            end
            self.pred.prior.disable(x);
        end
        
        % local predictions from given element
        function [v,vd] = value(self, elm, x, isnorm, vfun)
            if nargin < 4, isnorm=false; end
            if nargin < 5, vfun='value'; end
            
            assert( isa(elm,'bt.obj.Element'), 'Input should be an element.' );
            assert( ismatrix(x) && size(x,2)==self.ndims, 'Bad coordinates size.' );
            if ~isnorm, x = self.normalise(x); end
            
            [v,vd] = self.pred.elmpred( elm, x, vfun ); 
        end
        
        function g = gradient(self, elm, x, isnorm, vfun)
            if nargin < 4, isnorm=false; end
            if nargin < 5, vfun='value'; end
            
            assert( isa(elm,'bt.obj.Element'), 'Input should be an element.' );
            assert( ismatrix(x) && size(x,2)==self.ndims, 'Bad coordinates size.' );
            if ~isnorm, x = self.normalise(x); end
            
            g = self.pred.elmgrad( elm, x, vfun ); 
        end
        
    end
    methods (Hidden)
        
        % predictions using nearest-neighbours
        function [val,vald,grad] = predict(self,x,isnorm)
            
            warning('This method should only be used for testing.');
            if nargin < 3, isnorm=false; end
            if ~isnorm
                x = self.normalise(x);
            end
            
            % find enclosing elements
            id = self.nearest(x,true);
            
            % group by element id
            [g,id] = dk.groupunique(id);
            [nx,d] = size(x);
            ng = numel(id);
            
            % allocate output
            val = nan(nx,1);
            vald = nan(nx,1);
            for i = 1:ng
                Ei = self.store.elem(id(i));
                gi = g{i};
                [val(gi),vald(gi)] = self.pred.elmpred( Ei{1}, x(gi,:) );
            end
            
            % compute gradient only if required
            if nargout < 3, return; end
            grad = zeros(nx,d);
            for i = 1:ng
                Ei = self.store.elem(id(i));
                gi = g{i};
                grad(gi,:) = self.pred.elmgrad( Ei{1}, x(gi,:) );
            end
            
        end
        
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % element related
    % -----------------------------------------------------------------------------------------
    methods
        
        % subdivide given element(s) and return children
        function [chi,n] = subdivide(self,elm)
            
            % convert ID to element
            elm = self.elem(elm);
            assert( all(cellfun( @(x) isa(x,'bt.obj.Element') && x.is_leaf(), elm )), ...
                'Input(s) should be leaf Element instances.' );
            
            % run subdivision
            n = numel(elm);
            chi = cell(1,n);
            for i = 1:n
                Ei = elm{i};
                chi{i} = self.part.subdivide(Ei);
                dk.notify( self, 'Subdivision', 'elm', Ei );
            end
            chi = horzcat(chi{:});
            
        end
        
        % fully subdivide element down to (relative) depth d
        % all subelements are returned
        function [sub,n] = fullsub(self,elm,d)
            
            assert( d <= 10, 'This seems unreasonable.' );
            
            sub = cell(1,d+1);
            sub{1} = elm;
            n = 0;
            
            for i = 1:d
                sub{i+1} = self.subdivide( sub{i} );
                n = n + numel(sub{i});
            end
            sub = horzcat(sub{2:end});
            
        end
        
        % ensure that all offspring down to depth d are non-leaves
        % only new elements are returned, not all offspring
        function [sub,n] = densoff(self,elm,d)
            
            assert( d <= 10, 'This seems unreasonable.' );
            
            cur = { elm };
            sub = cell(1,d);
            n = 0;
            
            for i = 1:d
                L = cellfun( @(x) x.is_leaf(), cur );
                if any(L)
                    sub{i} = self.subdivide(cur(L));
                    n = n + nnz(L);
                end
                cur = dk.mapfun( @(x) x.child, cur, false );
                cur = horzcat(cur{:});
            end
            sub = horzcat(sub{:});
            
        end
        
        % sample points within an element, optionally denormalise coordinates
        function x = sample(self,elm,ns,denorm)
            
            assert( isa(elm,'bt.obj.Element'), 'Input should be an Element.' );
            if nargin < 4, denorm=true; end
        
            pol = elm.to_polytope();
            x = pol.sample(ns);
            
            if denorm
                x = self.denormalise(x);
            end
            
        end
        
        % return cell of elements, accepts indices
        function elm = elem(self,elm)
            if isnumeric(elm)
                elm = self.store.elem(elm);
            else
                elm = dk.wrap(elm);
            end
        end
        
        % get leaf elements as a cell
        function [L,k] = leaves(self)
            [L,k] = self.store.leaves();
        end
        
        % get edges as a Nx2 array of element indices
        function E = edges(self,all)
            
            if nargin < 2, all=false; end
            
            % map all edges
            M = containers.Map( 'KeyType', 'uint64', 'ValueType', 'uint8' );
            function register(elm)
                p = ant.math.sympair( elm.id, elm.neighbours_id() );
                n = numel(p);
                for i = 1:n
                    M(p(i)) = 0;
                end
            end
            if all
                k = self.store.elem_ids();
            else
                k = self.store.leaf_ids();
            end
            cellfun( @register, self.elem(k) );
            
            % convert to Nx2 matrix
            E = M.keys();
            E = ant.math.symunpair(double(vertcat(E{:})));
            
        end
        
        % shape of the current partition tree
        function d = depth(self)
            d = self.root.height();
        end
        function w = width(self)
            w = self.root.width();
        end
        
        % get element's centres (denormalised or not)
        function x = centre(self,elm,denorm)
            if nargin < 3, denorm=true; end
            if nargin < 2, elm=self.store.elem_ids(); end
            
            x = dk.mapfun( @(e) e.centre, self.elem(elm) );
            x = vertcat(x{:});
            
            if denorm 
                x = self.denormalise(x);
            end
        end
        
        % neighbour adjacency matrix
        function [A,L] = adjacency(self)
            L = self.leaves();
            n = numel(L);
            A = false(n);
            k = cellfun( @(elm) elm.id, L );
            m = sparse(k,1,1:n);
            for i = 1:n
                k = cellfun( @(elm) elm.id, L{i}.neighbours() );
                A(i,m(k)) = true;
            end
        end
        
        % iterate elements, calling function with:
        %
        %   fun( element handle, denormalised centre ) 
        %
        function varargout = iter(self,elm,fun,unif)
            if nargin < 4, unif = false; end
            varargout = cell(1,nargout);
            [varargout{:}] = dk.mapfun( @(e) fun(e), self.elem(elm), unif );
        end
        
        function varargout = iter_all(self,varargin)
            varargout = cell(1,nargout);
            [varargout{:}] = self.iter( self.store.elem_ids(), varargin{:} );
        end
        function varargout = iter_leaves(self,varargin)
            varargout = cell(1,nargout);
            [varargout{:}] = self.iter( self.store.leaf_ids(), varargin{:} );
        end
        function varargout = iter_eval(self,varargin)
            varargout = cell(1,nargout);
            [varargout{:}] = self.iter( self.store.eval_ids(), varargin{:} );
        end
        
        % convert element(s) to denormalised polytope(s)
        function P = to_polytope(self,elm,denorm)
            if nargin < 3, denorm=true; end
            if denorm
                P = dk.mapfun( @(x) x.to_polytope(@self.denormalise), self.elem(elm) );
            else
                P = dk.mapfun( @(x) x.to_polytope(), self.elem(elm) );
            end
        end
        
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % domain related
    % -----------------------------------------------------------------------------------------
    methods
        
        function d = delta(self)
            d = self.upper - self.lower;
        end
        
        function y = normalise(self,x)
            if isempty(x)
                y = []; return;
            end
            y = dk.bsx.sub( x, self.lower );
            y = dk.bsx.rdiv( y, self.delta() );
        end
        
        function y = denormalise(self,x)
            if isempty(x)
                y = []; return;
            end
            y = dk.bsx.mul( x, self.delta() );
            y = dk.bsx.add( y, self.lower );
        end
        
        % note: these only work for axis-aligned domains
        function a = is_inside(self,x,strict)
            if nargin < 3, strict=true; end
            if strict
                gt = @dk.bsx.gt;
                lt = @dk.bsx.lt;
            else
                gt = @dk.bsx.geq;
                lt = @dk.bsx.leq;
            end
            if isempty(x)
                a = [];
            else
                a = all( gt(x,self.lower) & lt(x,self.upper) );
            end
        end
        
        function a = is_outside(self,x,strict)
            if nargin < 3, strict=true; end
            a = ~self.is_inside(x,strict);
        end
        
        % random sample of points uniformly distributed within tessellation bounds
        function x = urand(self,n)
            x = rand(n,self.ndims);
            x = self.denormalise(x);
        end
        
        % create grid spanning the domain
        function varargout = meshgrid(self,n)
            d = self.ndims;
            g = cell(1,d);
            
            assert( d < 10, 'Preventing madness...' );
            for i = 1:d
                g{i} = linspace( self.lower(i), self.upper(i), n );
            end
            varargout = cell(1,d);
            [varargout{:}] = meshgrid(g{:});
        end
        
    end
    
    
    
    % -----------------------------------------------------------------------------------------
    % ui related
    % -----------------------------------------------------------------------------------------
    methods
        
        % draw centre of elements
        %
        % if rnl=false (default), draw ALL elements
        % otherwise only those of the root and leaf elements 
        function h = scatter(self,rnl,varargin)
            if nargin < 2, rnl=true; end
            if rnl
                x = self.centre([ {self.root}, self.leaves() ]);
            else
                x = self.centre();
            end
            h = dk.ui.scatter( x, varargin{:} );
            
            %axis equal; grid on;
            if self.ndims==3, view(-40,20); end
        end
        
        % draw the facets of all leaf elements
        function h = draw(self,varargin)
            E = self.leaves();
            P = self.to_polytope(E);
            h = dk.mapfun( @(x) x.plot('line',0.1*[1,1,1],varargin{:}), P, false );
            n = numel(h);
            for i = 1:n
                h{i}.data.elem = E{i};
            end
            
            % include drawings in figure with function to select by element id
            f = gcf; 
            f.UserData.dobj = h;
            f.UserData.getdobj = @(id) h{cellfun(@(x) x.data.elem.id,h) == id};
            
            %axis equal; grid on;
            if self.ndims==3, view(-40,20); end
        end
        
        % draw edges between elements
        function h = draw_edges(self,varargin)
            E = self.edges();
            n = size(E,1);
            h = gobjects(n,1);
            
            hold on;
            for i = 1:n
                x = self.centre( E(i,:) );
                h(i) = dk.ui.plot( x, varargin{:} );
            end
            hold off;
        end
        
        % draw partition tree
        % args forwarded to dk.priv.draw_tree
        function [h,T] = draw_tree(self,varargin)
            T = self.root.to_dkTree();
            function txt = tooltip(~,evt)
                try
                    dat = evt.Target.UserData;
                    elm = T.get_props(dat.id).elm;
                    txt = { ['id: ' num2str(elm.id)] };
                catch
                    txt = 'Undefined';
                end
            end
            h = T.plot('tooltip',@tooltip,varargin{:});
        end
        
    end
    
end