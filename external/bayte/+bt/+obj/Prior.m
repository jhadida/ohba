classdef Prior < handle
%
% NOTE: this should be set in NORMALISED coordinates
%

    properties
        x, y, s
        active
        radius
    end
    
    properties (Transient,Dependent)
        ndims
        ntot
        nact
    end
    
    methods
        
        function self = Prior(varargin)
            self.clear();
            switch nargin
                case 0 % nothing to do
                case 1
                    self.restore(varargin{1});
                otherwise
                    self.assign(varargin{:});
            end
        end
        
        function n = get.ndims(self)
            n = size(self.x,2);
        end
        function n = get.ntot(self)
            n = numel(self.y);
        end
        function n = get.nact(self)
            n = nnz(self.active);
        end
        
        function e = isempty(self)
            e = isempty(self.x);
        end
        
        function clear(self)
            self.x = [];
            self.y = [];
            self.s = [];
            self.active = [];
            self.radius = [];
        end
        
        function assign(self,x,y,r)
            
            assert( ismatrix(x), 'Coordinates should be a matrix.' );
            n = size(x,1);
            assert( size(y,1)==n, 'Value-size or mismatch.' );
            assert( dk.is.number(r) && r >= 0, 'Radius should be non-negative.' );
            
            if isvector(y)
                s = zeros(n,1);
            else
                s = y(:,2);
                y = y(:,1);
            end
            
            self.x = x;
            self.y = y;
            self.s = s;
            self.active = true(n,1);
            self.radius = r;
            
        end
        
        function s = to_struct(self)
            s.type = 'bt.obj.Prior';
            s.ver = '0.1';
            
            s.prop.x = self.x;
            s.prop.y = self.y;
            s.prop.s = self.s;
            s.prop.active = self.active;
            s.prop.radius = self.radius;
        end
        
        function restore(self,s)
            [ver,p] = bt.priv.restore( s, 'bt.obj.Prior' );
            switch lower(ver)
                case '0.1'
                    self.x = p.x;
                    self.y = p.y;
                    self.s = p.s;
                    self.active = p.active;
                    self.radius = p.radius;
                otherwise
                    error( 'Unknown version: %s', ver );
            end
        end
        
        function disable(self,x)
            if ~isempty(self)
                k = ant.mex.withinL2( self.x, x, self.radius );
                k = [k{:}];
                self.active(k) = false;
            end
        end
        
        function check(self,nd)
            if ~isempty(self)
                assert( self.ndims == nd, 'Bad dimensions.' );
            end
        end
        
        function [x,y,s] = append_to(self,x,y,s)
            if ~isempty(self)
                x = vertcat( x, self.x );
                y = vertcat( y, self.y );
                s = vertcat( s, self.s );
            end
        end
        
        function [x,y,s] = prepend_to(self,x,y,s)
            if ~isempty(self)
                x = vertcat( self.x, x );
                y = vertcat( self.y, y );
                s = vertcat( self.s, s );
            end
        end
        
    end
    
end