function T = tess( par, dom, pre )
%
% T = bt.tess( par, dom, pre )
%
% Helper method to create and initialise a Tessellation object.
% Options can be either k/v pairs or struct.
%
%
% INPUTS
% ------
%
%   par   Partition name/config:
%             t,tern,ternary          see bt.part.Ternary
%             b,bary,barycentric      see bt.part.Barycentric
%             s,site,simplicial       see bt.part.Simplicial
%
%         Additional options can be specified as a cell, eg:
%             par = { 'bary', 'noise', 1/15 }
%
%   dom   Lower and upper bound of initial square or cube, as 2xd array.
%         If bounds=2 or bounds=3, default to unit square or cube.
%
%   pre   Predictor name/config: 
%              se, mat32 (default), mat52
%
%         Additional options can be specified as a cell, eg:
%              ker = { 'mat32', 'brad', 3 }
%
%
% See also: bt.obj.Tessellation, bt.abc.Partition, bt.pred.GaussianProcess
%
% JH

    if nargin < 3, pre = 'mat32'; end
    
    % parse bounds
    assert( isnumeric(dom), 'Bounds should be numeric.' );
    if isscalar(dom)
        n = dom;
        assert( n > 1, 'Scalar bound should be > 1.' );
        lo = zeros(1,n);
        up = ones(1,n);
    else
        assert( ismatrix(dom) && size(dom,2) > 1 && size(dom,1)==2, 'Bad bounds size.' );
        lo = dom(1,:);
        up = dom(2,:);
    end
    
    % wrap inputs
    par = dk.wrap(par);
    pre = dk.wrap(pre);
    
    % make tessellation object
    par = bt.part.factory( par{:} );
    pre = bt.pred.GaussianProcess( pre{:} );
    T = bt.obj.Tessellation( lo, up, par, pre );

end