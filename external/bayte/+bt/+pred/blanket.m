function [x,y,m,s] = blanket( elm, rad, rel, vfun )
%
% [x,y,m,s] = bt.pred.blanket( elm, rad=1, rel=false, vfun=value )
%
%   Return coordinates x and associated values y (expected mean and deviation)
%   for all elements in an inference blanket of radius rad.
%
%   This includes:
%       - the most recent ancestor with a value (the "anchor" element)
%       - its neighbours up to radius rad
%       - its ancestors
%       - its offpring
%
%   The predicted mean of the anchor element is subtracted from the first column 
%   of y if rel==true. This affects the "long-range" behaviour of the predictions.
%
% JH

    assert( isa(elm,'bt.obj.Element'), 'Bad input element.' );
    if nargin < 2, rad = 1; end
    if nargin < 3, rel = false; end
    if nargin < 4, vfun = 'value'; end
    
    % current implementation requires input element to have a value
    assert( elm.has_value(), 'Input element must have a value.' );
    
    
    %------------------------------------
    % find elements to use for regression
    %------------------------------------
    
    % list all neighbours up to radius rad
    B = elm.blanket(rad,true);
    
    % aggregate all neighbours up to radius rad (including self), 
    % all ancestors and all offspring
    A = cellfun( @(x) x.ancestors_id(), B, 'UniformOutput', false );
    A = elm.store.elem(unique(horzcat( A{:} )));
    
    if elm.is_leaf()
        % leaf elements should only have edges with other leaf elements
        % so no need to compute offspring
        B = horzcat( B, A );
    else
        O = cellfun( @(x) x.offspring(), B, 'UniformOutput', false );
        B = horzcat( B, A, O{:} );
    end
    
    
    %--------------------------
    % compute useful properties
    %--------------------------
    
    % relative mean
    if rel
        m = feval( vfun, elm.value );
        m = m(1);
    else
        m = 0;
    end
    
    % adaptive length-scale
    s = elm.scale;
    %c = dk.mapfun( @(x) x.centre, B );
    %c = vertcat(c{:});
    %s = std(c,[],1) / sqrt(rad);

    
    %------------------------------
    % gather coordinates and values
    %------------------------------
    
    % allocate output
    n = numel(B);
    d = elm.nd;
    x = nan(n,d);
    y = nan(n,2);

    % iterate over each element in the blanket
    for i = 1:n
        Ei = B{i};

        if Ei.has_value() && ~Ei.shared
            x(i,:) = Ei.centre;
            y(i,:) = feval( vfun, Ei.value );
        end
    end

    % remove elements without value(s)
    mask = ~any(isnan(y),2);
    x = x(mask,:);
    y = y(mask,:);
    
    % demean expected values if rel==true
    y(:,1) = y(:,1) - m;
    
end