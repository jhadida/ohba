classdef GaussianProcess < bt.abc.Predictor
%
% GaussianProcess objects implement GP predictions (see Chap.2 of Rasmussen and Williams).
% They store a Kernel instance, and define a few options.
%
% Example:
%   
%   % test-function
%   F = bt.fun.factory('peaks');
%   G = dk.obj.Grid(F.dom);         % 80x80 grid over domain
%   X = G.rand(100); f = F.fun(X);  % 100 random training points
%
%   % generate predictions on the whole grid
%   gp = bt.pred.GaussianProcess( bt.ker.SqExp() );
%   sl = 0.1 * G.wx; % scale-length
%   Xq = G.coord(); 
%   [v,vd] = gp.pred(X,f,Xq,sl);
%
%   % plot results 
%   figure; colormap('jet');
%
%   subplot(1,2,1); 
%   G.image( F.fun(Xq), F.val ); title('Ground-truth');
%   hold on; dk.ui.scatter(X,'k*'); hold off;
%
%   subplot(1,2,2); 
%   G.image( v, F.val ); title('Predictive mean');
%
% JH

    properties (Constant)
        type = 'gp';
    end
    properties
        ker
    end
    
    methods (Static)
        
        function obj = restore(prop,ver)
            if nargin < 2, ver='latest'; end
            assert( strcmp(prop.type,bt.pred.GaussianProcess.type), 'Partition type mismatch.' );
            switch lower(ver)
                case {'latest','0.1'}
                    obj = bt.pred.GaussianProcess( prop.ker, prop.opt );
                otherwise
                    error( 'Unknown version: %s', ver );
            end
        end
        
    end
    
    methods
        
        function self = GaussianProcess(ker,varargin)
            if nargin < 1, ker = 'default'; end
            self.prior = bt.obj.Prior();
            self.ker = bt.ker.factory(ker);
            self.setopt( varargin, 'batch', 1000 );
            self.valopt( 'batch' );
        end
        
        function s = to_struct(self)
            s.type = 'bt.abc.Predictor';
            s.ver = '0.1';
            s.prop.type = self.type;
            s.prop.prior = self.prior.to_struct();
            s.prop.opt = self.opt;
            s.prop.ker = self.ker.type;
        end
        
        function chkopt(self)
            self.valopt( 'batch' );
            assert( isa(self.ker,'bt.abc.Kernel'), 'Bad kernel.' );
            assert( self.opt.batch >= 1, 'Batch size should be >= 1.' );
        end
        
        function [v,vd] = pred(self,x,y,xq,hyp)
            
            if nargin < 5, hyp=[]; end
            [n,m,d,y,s,hyp] = self.check(x,y,xq,hyp);
            [x,y,s] = self.prior.append_to(x,y,s);
            
            b = self.opt.batch;
            nb = ceil(m/b);
            v = cell(1,nb);
            vd = cell(1,nb);
            for i = 1:nb
                r = colon( 1 + (i-1)*b, min(m,i*b) );
                %[v{i},vd{i}] = self.pred_impl_gpml(hyp,x,y,s,xq(r,:));
                if nargout > 1
                   [v{i},vd{i}] = self.pred_impl(hyp,x,y,s,xq(r,:));
                else
                   v{i} = self.pred_impl(hyp,x,y,s,xq(r,:));
                end
            end
            v = vertcat(v{:});
            vd = vertcat(vd{:});
            
        end
        
        function g = grad(self,x,y,xq,hyp)
            
            if nargin < 5, hyp=[]; end
            [n,m,d,y,s,hyp] = self.check(x,y,xq,hyp);
            [x,y,s] = self.prior.append_to(x,y,s);
            
            b = self.opt.batch;
            nb = ceil(m/b);
            g = cell(1,nb);
            for i = 1:nb
                r = colon( 1 + (i-1)*b, min(m,i*b) );
                g{i} = self.grad_impl(hyp,x,y,s,xq(r,:));
            end
            g = vertcat(g{:});
            
        end
        
    end
    
    methods (Hidden)
        
        % process input arguments
        function [n,m,d,y,s,hyp] = check(self,x,y,xq,hyp)
            
            self.chkopt();
            [n,m,d,y,s] = self.chkarg(x,y,xq);
            
            % length-scale
            if ~isstruct(hyp)
                hyp = struct( 'len', hyp );
            end
            if isfield(hyp,'len')
                L = hyp.len;
                if isempty(L)
                    L = std(x,[],1);
                elseif isscalar(L)
                    L = L*ones(1,d);
                end
                assert( numel(L)==d && all(L >= 0), 'Bad length scale.' );
                hyp.len = L;
            end
            hyp.len = max( hyp.len, self.opt.minlen );
            
            assert( dk.is.struct(hyp,{'len'}), 'Bad hyperparams.' );
            
        end
        
        % implementation of GP regression
        function [v,vd] = pred_impl(self,hyp,x,y,s,xq)
            
            % predictive mean
            K = self.ker.cov(x,x,hyp) + diag(s.^2);
            C = self.ker.cov(xq,x,hyp);
            v = C * (K \ y);
            
            % predictive deviation
            if nargout > 1
                vd = chol(K,'lower') \ C';
                vd = self.ker.autocov(xq) - dot(vd,vd,1)';
                vd = sqrt(max(0,vd));
                
                % NOTE:
                %   "The algorithm returns the predictive mean and variance for noise free test data -- 
                %    to compute the predictive distribution for noisy test data y_*, simply add the noise 
                %    variance sigma_n^2 to the predictive variance of f_*."
                %                                                                 (GPML, sec 2.2 p.19)
                %
                % See also line 37 in gpml/lik/likGauss.m (GPML v4.2).
                %
                % This causes two issues here:
                %
                %   1. Sigma is not defined uniformly across the space, but rather for each training point 
                %      independently. We could define sigma_n^2 as an average across all training points, 
                %      but that would be a hack, and doesn't solve point number 2.
                %
                %   2. Adding sigma_n^2 to vd above is equivalent to shifting the predictive deviation by 
                %      a fixed offset across the entire space, which then effectively becomes a lower-bound.
                %      If we use vd as-is instead, the value converges towards zero gradually, as more and 
                %      more points are sampled in the vicinity of query points. This is consistent with the 
                %      way this information is used in BO; as a measure of uncertainty due to lack of 
                %      exploration, rather than noise. Note that if f is stochastic, then vd will still 
                %      converge towards a non-zero value, no matter how many points are sampled.
            end
            
        end
        
        function [v,vd] = pred_impl_gpml(self,hyp,x,y,s,xq)
            
            % Algorithm 2.1 in Rasmussen and Williams
            K = self.ker.cov(x,x,hyp) + diag(s.^2);
            L = chol(K,'lower');
            A = L' \ (L \ y);
            C = self.ker.cov(xq,x,hyp);
            v = C * A;
            
            vd = L \ C';
            vd = self.ker.autocov(xq) - dot(vd,vd,1)';
            vd = sqrt(max(0,vd));
            
        end
        
        function g = grad_impl(self,hyp,x,y,s,xq)
            
            % predictive gradient
            K = self.ker.cov(x,x,hyp) + diag(s.^2);
            g = self.ker.dcov(xq,x,hyp,K\y);
            
        end
        
    end
    
end
