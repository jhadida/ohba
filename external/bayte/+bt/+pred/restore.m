function obj = restore( in )
%
% obj = bt.pred.restore( in ) 
%
% Load serialised predictor, and restore corresponding instance.
% Input should be either:
%   - the filename of the serialised predictor
%   - the corresponding serialised structure with fields {ver,type,prop} 
%
% JH

    [ver,prop] = bt.priv.restore( in, 'bt.abc.Predictor' );
    switch lower(prop.type)
        case 'gp'
            obj = bt.pred.GaussianProcess.restore(prop,ver);
        case 'gpml'
            obj = bt.pred.GPML.restore(prop,ver);
        otherwise
            error( 'Unknown predictor type: %s', prop.type );
    end
    obj.prior = bt.obj.Prior(prop.prior);

end