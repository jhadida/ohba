classdef GPML < bt.abc.Predictor
%
% This kernel is for testing only, and requires the following repository to be on the 
% Matlab path: https://github.com/jhadida/gpml
%   
% JH

    properties (Constant)
        type = 'gpml';
    end
    properties
        ker
    end
    
    methods (Static)
        
        function obj = restore(prop,ver)
            if nargin < 2, ver='latest'; end
            assert( strcmp(prop.type,bt.pred.GPML.type), 'Partition type mismatch.' );
            switch lower(ver)
                case {'latest','0.1'}
                    obj = bt.pred.GPML( prop.ker, prop.opt );
                otherwise
                    error( 'Unknown version: %s', ver );
            end
        end
        
    end
    
    methods
        
        function self = GPML(ker,varargin)
            if nargin < 1, ker = 'mat32'; end
            self.prior = bt.obj.Prior();
            self.set_kernel(ker);
            self.setopt( varargin );
            gpml_start();
        end
        
        function s = to_struct(self)
            s.type = 'bt.abc.Predictor';
            s.ver = '0.1';
            s.prop.type = self.type;
            s.prop.prior = self.prior.to_struct();
            s.prop.opt = self.opt;
            s.prop.ker = self.ker; % fragile but ok for now
        end
        
        function chkopt(self)
            self.valopt();
            assert( strcmp(feval( self.ker{:} ),'(D+1)'), 'Kernel should be ARD.' );
        end
        
        function set_kernel(self,value)
            switch lower(value)
                case {'se','sqexp'}
                    self.ker = {@covSEard};
                case {'mat32','matern32'}
                    self.ker = {@covMaternard, 3};
                case {'mat52','matern52'}
                    self.ker = {@covMaternard, 5};
                otherwise
                    self.ker = value;
            end
        end
        
        function [v,vd] = pred(self,x,y,xq,hyp)
            
            if nargin < 5, hyp=[]; end
            [n,m,d,y,s,hyp] = self.check(x,y,xq,hyp);
            [x,y,s] = self.prior.append_to(x,y,s);
            
            [v,vd] = gp( hyp, @infGaussLik, @meanZero, self.ker, @likGauss, x, y, xq );
            vd = sqrt(vd);
            
        end
        
        function g = grad(self,x,y,xq,hyp)
            error( 'Not implemented' );
        end
        
    end
    
    methods (Hidden)
        
        % process input arguments
        function [n,m,d,y,s,hyp] = check(self,x,y,xq,hyp)
            
            self.chkopt();
            [n,m,d,y,s] = self.chkarg(x,y,xq);
            
            % length-scale
            if ~isstruct(hyp)
                hyp = struct('len',hyp);
            end
            if isfield(hyp,'len')
                L = hyp.len;
                if isempty(L)
                    L = std(x,[],1);
                elseif isscalar(L)
                    L = L*ones(1,d);
                end
                assert( numel(L)==d && all(L >= 0), 'Bad length scale.' );
                L = max( L, self.opt.minlen );
                hyp.cov = log([ L(:); 1 ]);
            end
            
            % likelihood
            if isfield(hyp,'sigma')
                hyp.lik = log(hyp.sigma);
            end
            hyp.lik = dk.struct.get( hyp, 'lik', log(mean(s)) ); % GPML only accepts scalar sigma
            
            assert( dk.is.struct(hyp,{'cov','lik'}), 'Bad hyperparams.' );
            
        end
        
    end
    
end
