classdef SurfaceBrain < handle
%
% Draw parcellated brain surfaces using a mesh and a parcellation structure.
% The mesh can be any of userdata/hcp/surf_*
% The parcellation can be any of userdata/hcp/aparc_*
%
% Drawing properties
%       openfig 
%    face_alpha
%    edge_color
%    edge_alpha
%       nocolor
%       shading
%      material
%      lighting
%
%
% Configuration methods
%   load( mesh )
%   configure( drawing options... )
%
%
% Drawing methods:
%   show( display options... )
%   showbin( id, type, color )
%   showmap( scalarMap, display options... )
%
% JH

    properties
        openfig
        face_alpha
        edge_color
        edge_alpha
        
        nocolor
        shading
        material
        lighting
    end
    
    properties (SetAccess = private)
        fig
        brain
        aparc
    end
    
    properties (Transient,Dependent)
        nleft
        nright
        nparcels
        
        % compatibility
        open_figure
    end
    
    methods
        
        function self = SurfaceBrain( varargin )
            self.clear();
            self.configure();
            self.load( varargin{:} );
        end
        
        function clear(self)
            self.fig   = gobjects();
            self.brain = [];
            self.aparc = [];
        end
        
        % dynamic props
        function n = get.nleft(self)
            n = self.aparc.left.nlabel;
        end
        function n = get.nright(self)
            n = self.aparc.right.nlabel;
        end
        function n = get.nparcels(self)
            n = self.nleft + self.nright;
        end
        
        % compatibility
        function val = get.open_figure(self)
            warning( 'Property "open_figure" is deprecated, use openfig instead.' );
            val = self.openfig;
        end
        function set.open_figure(self,val)
            warning( 'Property "open_figure" is deprecated, use openfig instead.' );
            self.openfig = val;
        end
        
        function self = unbind(self)
        %
        % Release bound figure (if any)
        %
            self.fig = gobjects();
        end
        
        function self = load(self,mesh)
        %
        % By default, show the inflated surface.
        %
        
            if nargin < 2, mesh='inflated'; end
            data = com.load_mrtrix(5,68);
            
            if dk.is.struct(mesh,{'left','right'})
                self.brain = mesh;
            else
                self.brain.left.vertices = data.left.coord.(mesh);
                self.brain.left.faces = data.left.faces;
                self.brain.left.atlas = data.left.atlas;
                
                self.brain.right.vertices = data.right.coord.(mesh);
                self.brain.right.faces = data.right.faces;
                self.brain.right.atlas = data.right.atlas;
            end
            
            self.aparc.left = data.left.parc;
            self.aparc.right = data.right.parc;
            self.compute_mapping();
            
        end

        function self = configure(self,varargin)

            args = dk.obj.kwArgs(varargin{:});

            self.openfig     = args.get('openfig',true);
            self.face_alpha  = args.get('face_alpha',0.95);
            self.edge_color  = args.get('edge_color',[1 1 1]*0.5);
            self.edge_alpha  = args.get('edge_alpha',0.5);
            
            self.nocolor     = args.get('nocolor',[1,1,1]*(2/3));
            self.shading     = args.get('shading','interp');
            self.material    = args.get('material','dull');
            self.lighting    = args.get('lighting','gouraud');
            
        end
        
        function h = showall(self,varargin)
        %
        % Show whole brain with each parcel coloured individually.
        %
            h = self.showmap( 1:self.nparcels, varargin{:} );
        end
        
        function h = showmesh(self,varargin)
        %
        % Show while brain without colouring any region.
        %
            h = self.showmap( zeros(1,self.nparcels), 'cmap', self.nocolor, varargin{:} );
        end
        
        function h = showone(self,id,type,col,varargin)
        %
        % Show selected regions with specified color
        %
            if nargin < 4 || isempty(col), col=[0.9,0,0]; end
            if nargin < 3 || isempty(type), type='default'; end
            
            switch type
                case {'both','brain','sync','byside','default'}
                    smap = zeros(1,self.nparcels);
                case 'left'
                    smap = zeros(1,self.nleft);
                case 'right'
                    smap = zeros(1,self.nright);
            end
            smap(id) = 1;
            
            h = self.showmap( smap, 'type', type, 'cmap', [ self.nocolor; col ], varargin{:} );
            delete(colorbar);
            
        end
        
        function h = showmap(self,smap,varargin)
        %
        % h = showmap(self,smap,varargin)
        %
        % Show scalar map with one value per brain region (given the current parcellation).
        % Outputs either a scalar or 1x2 vector of patch handles.
        %
        % OPTIONS:
        %
        %     type  Any of: left, right, both, sync (see draw() below)
        %           By default, the whole brain is displayed.
        %           Note that the number of values in smap depends on this option.
        %
        %     cmap  Colormap to be used for display, should be a Nx3 RGB array.
        %           By default, it is set such that increasing values in smap correspond 
        %           to colors shifting from blue to red.
        %
        %     bind  Bind the figure (either new opened, or current figure).
        %           This option automatically sets openfig to false (after opening a 
        %           new figure), and saved the current figure handle into a member property.
        %           Use unbind() to release the attached figure.
        %
        %   crange  Color range used for interpolation, should be a 1x2 vector.
        %           If unset, the input map is rescaled to fit the entire color range.
        %
        %   bright  Brightness associated to values in the scalar map.
        %           If scalar, the value is replicated for each element in smap.
        %           Otherwise, it should be a vector with the same number of elements.
        %           Default: 1
        %
        % JH
            
            % parse options
            opt = dk.obj.kwArgs(varargin{:});
            
                type = opt.get( 'type', 'default' );
                bind = opt.get( 'bind', false );
                cmap = opt.get( 'cmap', [] );
                crng = opt.get( 'crange', [] );
                val  = opt.get( 'bright', 1 );
                
            % compatibility
            dk.reject( opt.has('side'), 'Option "side" is obsolete, use "type" instead.' );
                
            % default colormap
            if isempty(cmap)
                nc = 128;
                cmap = fliplr(linspace( 0, 5/6, nc ));
                cmap = horzcat( cmap(:), ones(nc,1) * [0.9 0.8] );
                cmap = flipud(hsv2rgb(cmap));
                %cmap = flipud(dk.cmap.jh(nc));
            end
            assert( ismatrix(cmap) && size(cmap,2)==3, 'cmap should be a Nx3 matrix.' );
            
            % expected size
            smap = double(smap(:));
            ns = numel(smap);
            delta = max(smap) - min(smap);
            
            % set brightness
            if isscalar(val), val = val * ones( ns, 1 ); end
            assert( numel(val)==ns, 'Unexpected brightness size.' );
            
            % map input values to RGB colors from colormap
            nc = size(cmap,1);
            if delta < 1e-6
                col = mean(cmap,1);
                col = repmat(col,ns,1);
            elseif isempty(crng)
                col = linspace(0,1,nc);
                col = interp1( col(:), cmap, ant.math.rescale(smap,[0 1]), 'linear' );
            else
                col = linspace(crng(1),crng(2),nc);
                col = interp1( col(:), cmap, dk.num.clamp(smap,crng), 'linear' );
            end
            
            % apply brightness
            col = rgb2hsv(col);
            col(:,3) = col(:,3) .* val;
            col = hsv2rgb(col);
            
            % open figure
            figname = '[SurfaceBrain] Brain map visualisation';
            if ishandle(self.fig)
                f = figure(self.fig);
            elseif self.openfig
                f = dk.fig.new( figname, [700,800] );
            else
                f = gcf;
            end
            set( f, 'renderer', 'OpenGL' );
            if bind
                f.UserData.obj = self;
                self.fig = f; 
            end
            
            % draw brain
            h = self.draw( col, type ); 
            if self.openfig
                dk.widget.menu_3Dview(gca);
            end
            
            % show colormap
            colormap(gca,cmap); colorbar; 
            if isempty(crng)
                if std(smap) < 1e-6
                    caxis(gca,mean(smap) + 0.5*[-1 1]);
                else
                    caxis(gca,[min(smap), max(smap)]);
                end
            else
                caxis(gca,crng);
            end
            
        end
        
    end
    
    methods (Hidden)
        
        function compute_mapping(self)
        %
        % Compute left and right maps: vertex_id => label_id (and not label)
        % This mapping allows to deal with skips in the labels
        % 
        
            ml = uint16(0);
            ul = self.aparc.left.label;
            ml(1+ul) = 1:numel(ul);
            
            mr = uint16(0);
            ur = self.aparc.right.label;
            mr(1+ur) = 1:numel(ur);
            
            self.aparc.left.vmap  = ml( 1+self.aparc.left.vlabel  );
            self.aparc.right.vmap = mr( 1+self.aparc.right.vlabel );
            
        end
        
        function h = draw(self,cols,type,flip)
        %
        % h = draw(self,cols,type,flip)
        %
        % cols should be a Nx3 of RGB colours for each parcel
        % 
        % Possible types:
        %
        %    left,right  Show left or right hemisphere only.
        %                If the number of colours matches the total number of parcels across both
        %                hemispheres, then only those values within the corresponding hemispheres 
        %                are considered (this is after colour interpolation).
        %
        %    both,brain  Both hemispheres are drawn in the same axes next to each other.
        %                The number of colours should correspond to the total number of regions.
        %
        %   sync,byside  Plot hemispheres in their own axes side-by-side, and link camera rotation.
        %                The number of colours should correspond to the total number of regions.
        %
        % If flip is true, the X coordinate is flipped, and the direction of the Y axis is reversed.
        %
        % The output is either a scalar patch handle, or a 1x2 vector of handles.
        %
        % JH
            
            if nargin < 3, type='default'; end
            if nargin < 4, flip=false; end
            
            nl = self.aparc.left.nlabel;
            nr = self.aparc.right.nlabel;
            nc = size(cols,1);
            nocol = self.nocolor;
            
            switch lower(type)
                case {'sync','byside'}
                    assert( nc==(nl+nr), 'Bad number of colours.' );
                    clf; % clear current figure
                    a1 = subplot(1,5,[1,2]); h1 = self.draw( cols(1:nl,:), 'left' );
                    a2 = subplot(1,5,[3,4]); h2 = self.draw( cols(nl+1:end,:), 'right', true );
                    
                    L = linkprop( [a1,a2], {'CameraUpVector', 'CameraViewAngle', 'CameraTarget', 'CameraPosition'} );
                    setappdata( gcf, 'Link', L );
                    h = [h1,h2];
                    subplot(1,5,5); set(gca,'Visible','off'); % set current axes for colormap
                    
                case {'both','brain','default'}
                    assert( nc==(nl+nr), 'Bad number of colours.' );
                    clf; % clear current figure
                    h1 = self.draw( cols(1:nl,:), 'left' ); hold on;
                    h2 = self.draw( cols(nl+1:end,:), 'right' ); hold off;
                    h = [h1,h2];
                    
                case {'l','left'}
                    if nc==(nl+nr)
                        cols = cols(1:nl,:); 
                    else
                        assert( nc==nl, 'Bad number of colours.' );
                    end
                    cols = vertcat( nocol, cols );
                    lcol = cols( 1+self.aparc.left.vmap, : );
                    
                    V = self.brain.left.vertices;
                    if flip
                        V = dk.bsx.mul( V, [-1,1,1] );
                        set(gca,'ydir','reverse'); 
                    end
                    h = self.make_patch( V, self.brain.left.faces, lcol );
                    
                case {'r','right'}
                    if nc==(nl+nr)
                        cols = cols(nl+1:end,:); 
                    else
                        assert( nc==nr, 'Bad number of colours.' );
                    end
                    cols = vertcat( nocol, cols );
                    rcol = cols( 1+self.aparc.right.vmap, : );
                    
                    V = self.brain.right.vertices;
                    if flip
                        V = dk.bsx.mul( V, [-1,1,1] );
                        set(gca,'ydir','reverse'); 
                    end
                    h = self.make_patch( V, self.brain.right.faces, rcol );
                    
                otherwise
                    error('Unknown type: %s', type);
            end
            
        end
        
        function ph = make_patch( self, vertices, faces, cdata )
        %
        % ph = make_patch( self, vertices, faces, cdata )
        %
        % Draw patch with all the options.
        % cdata should be Nx3 where N is the number of vertices.
        %
        % JH
        
            set(gcf,'Color','w');
            ph = patch( 'Vertices', vertices, 'Faces', faces, 'FaceVertexCData', cdata, ...
                'EdgeColor', self.edge_color, 'EdgeAlpha', self.edge_alpha, 'FaceAlpha', self.face_alpha ...
            );
            
            %xlabel('x'); ylabel('y'); zlabel('z'); 
            grid off; box off; axis equal tight off; view(-90,0); axis vis3d;
            shading(self.shading); material(self.material); 
            lighting(self.lighting); com.ui.lights();
        end
        
    end
    
    
end
