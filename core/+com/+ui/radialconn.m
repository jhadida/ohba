function [node,link,radii] = radialconn( conn, varargin )
%
% [node,link,radii] = radialconn( conn, varargin )
%
% Show radial connectivity network (this opens a new figure).
% Assumes block connectivity left/right with equally sized blocks.
%
% OUTPUT:
%   
%    node    A struct-array (for each node) with fields:
%               .col  Node color as displayed.
%               .pos  Node position on the plot (ring's centre).
%               .ang  Angular position of the node (in radians).
%
%    link    A struct-array (for each displayed link) with fields:
%               .src  Node index of the source.
%               .dst  Node index of the destination
%               .val  Connectivity weight.
%               .mod  Modulation factor (between 0 and 1).
%               .col  Link color as displayed.
%               .wid  Link width as displayed.
%
%   radii    A structure with fields:
%               .node   Radius of each node.
%               .outer  Radius of the outer ring.
%
%
% OPTIONS:
%
% Colors:
%     invert    Invert colors.
%
% Ring drawing:
%     radius    Radius of gray outer circle.
%
%      group    Cell of group-indices for the left hemisphere (right-hemisphere is mirrored).
%               When specified, each group is separated from the next by inserting a ghost node,
%               and each node within the same group has the same color.
%
%     gcolor    Specify a colour for each group.
%     ncolor    Specify a colour for each node within an hemisphere.
%
% Link drawing:
%    prctile    Filter connections to be drawn (default: 95).
%               The threshold is calculated on non-zero absolute connectivity weights.
%               Percentiles should be between 0 and 100.
%
%  threshold    Overrides prctile option.
%               Manually specify the absolute threshold to be applied before selecting edges.
%
%       bend    Bend trajectories within the main ring (default: 0.3).
%               The bending factor should be between 0 and 1, higher means more bend.
%
%     maxwid,   
%     minwid    Maximum and minimum link width (default: min=3, max=7).
%               The link width is modulated accoding to their absolute connection strength AFTER THRESHOLDING.
%               See the LineWidth property of Matlab's graphical object Line.
%               NOTE: this parameter should be scaled according to the radius option above.
%
%     minsat    Minimum color saturation (default: 0.2).
%               The link color is modulated accoding to their absolute connection strength AFTER THRESHOLDING.
%               Negative weights are in blue shades, and positive weights in red.
%               This parameter should be between 0 (weakest links are gray) and 1 (no modulation).
%
%       npts    Number of points used to draw each link (default: 51).
%               The trajectory of each link is interpolated between its source and destination.
%
% JH

    % parse options
    opt = dk.obj.kwArgs(varargin{:});
    
    % connectivity matrix
    check_conn( conn );
    Nreg = size(conn,1);
    
    % color theme
    col.bg = [1 1 1];
    col.link.pos = [0.8 0 0];
    col.link.neg = [0 0 0.8];
    
    % invert colors if required
    if opt.get( 'invert', false )
        col.bg = 1-col.bg;
        col.node = 1-col.node;
        col.link.pos = 1-col.link.pos;
        col.link.neg = 1-col.link.neg;
    end
    
    % create figure
    figure( 'name','Radial connectivity UI', 'color', col.bg );
    
    % initialise nodes of the graph
    [node,radii] = draw_nodes( Nreg, opt );
    link = draw_links( conn, node, radii, opt, col.link );
    hold off; 

end

function check_conn( c )
    assert( isnumeric(c) || islogical(c), 'conn should be numeric or logical.' );
    assert( ismatrix(c) && diff(size(c))==0, 'conn should be a square numeric matrix.' );
    assert( all(dk.tocol( c-c' ) < 1e-6), 'conn should be symmetric.' );
end

function check_group( n, g )
%
% n = number of nodes in hemisphere
% g = cell of indices for each group
%
% verify that cells of g form a partition of 1:n

    c = zeros(n,1);
    m = numel(g);
    
    for i = 1:m
        c(g{i}) = c(g{i})+1;
    end
    assert( all(c==1), 'Grouping does not form a partition of all nodes.' );
end

function [node,radii] = draw_nodes( nreg, opt )

    assert( mod(nreg,2) == 0, 'There should be an even number of regions (left/right).' );
    
    node = dk.struct.repeat( {'col','pos','ang'}, 1, nreg );
    nhem = nreg/2; % # of regions in each hemisphere

    % sort out the color for each node
    rad = opt.get( 'radius', 1 );
    nodegrp = opt.get( 'group', {} );
    ngrp = numel(nodegrp);
    
    % if nodes are grouped ...
    if ngrp
        
        check_group(nhem,nodegrp);
        
        % set one color for each group and add a space betweem each group
        col = opt.get( 'gcolor', dk.cmap.jh(ngrp) );
        assert( dk.is.rgb(col,ngrp), 'Bad group colors.' );
        
        npos = nhem + 2 + ngrp-1; 
        Lang = linspace( pi/2, 3*pi/2, npos );
        Rang = linspace( pi/2, -pi/2, npos );
        
        % iterate on nodes within each group
        k = 0;
        for i = 1:ngrp
            g = nodegrp{i}; % current group
            for n = g
                k = k+1;
                node(n) = make_node( node(n), col(i,:), Lang(k+i), rad );
                node(n+nhem) = make_node( node(n+nhem), col(i,:), Rang(k+i), rad );
            end
        end
        
    % otherwise ...
    else
        
        % each node (within an hemisphere) has a different color and nodes are next to each other
        col = opt.get( 'ncolor', dk.cmap.jh(nhem) );
        if isvector(col), col = repmat(col,nhem,1); end
        assert( dk.is.rgb(col,nhem), 'Bad node colors.' );
        
        npos = nhem + 2; 
        Lang = linspace( pi/2, 3*pi/2, npos );
        Rang = linspace( pi/2, -pi/2, npos );
        
        % iterate on nodes within hemisphere
        for k = 1:nhem
            node(k) = make_node( node(k), col(k,:), Lang(k+1), rad );
            node(k+nhem) = make_node( node(k+nhem), col(k,:), Rang(k+1), rad );
        end
        
    end
    
    % compute radii
    rnode = rad*min( 0.05, pi/(2+nreg) );
    radii.node = rnode;
    radii.main = rad;
    radii.inner = 0.75*rad;
    radii.outer = 1.25*rad;
    
    % draw nodes
    draw_circle( [0 0], rad, 0.95*[1 1 1], 0.25*rad, 61 ); hold on;
    for n = 1:nreg
        draw_circle( node(n).pos, rnode, node(n).col, 0.5*rnode, 21 );
    end
    
    box off; grid off; axis equal off;

end

function link = draw_links( conn, node, rad, opt, col )

    prc  = opt.get( 'prctile', 95 ); % percentile of links to display
    thr  = opt.get( 'threshold', 0 ); % percentile of links to display
    wmax = opt.get( 'maxwid', 7 ); % maximum link width 
    wmin = opt.get( 'minwid', 3 ); % minimum link width (width modulation)
    smin = opt.get( 'minsat', 0.2 ); % minimum saturation (color modulation)
    npts = opt.get( 'npts', 51 ); % number of points used to draw links
    ctr  = opt.get( 'bend', 0.3 ); % bend paths within the disk towards the centre
    
    % prepare colors
    cneg = rgb2hsv(col.neg);
    cpos = rgb2hsv(col.pos);

    % format connectivity
    conn = tril( conn, -1 ); % keep only lower triangular values
    
    % magnitude
    mag = abs(conn);
    maxmag = max(mag(:));
    if maxmag < 1e-6, return; end % dont draw if there is nothing
    
    % filter links to display
    if islogical(conn)
        [src,dst] = find( conn );
        vals = ones(size(src));
    else
        if thr==0
            thr = prctile(nonzeros(mag),prc);
        end
        [src,dst] = find( mag >= thr );
        vals = mag(sub2ind( size(mag), src, dst ));
    end
    [~,ord] = sort(vals,'ascend');
    nlnk = numel(src);
    link = dk.struct.repeat( {'src','dst','val','mod','col','wid'}, 1, nlnk );
    
    % iterate on links
    for i = 1:nlnk
        
        % source, destination, relative magnitude, and weight
        s = src(ord(i));
        d = dst(ord(i));
        v = conn(s,d);
        if islogical(conn)
            m = v;
            w = wmin;
        else
            m = (abs(v)-thr)/(maxmag-thr);
            w = wmin + (wmax-wmin)*m;
        end
        
        % link color (modulate saturation)
        r = smin + (1-smin)*m;
        if v < 0 
            c = cneg;
        else
            c = cpos;
        end
        c(2) = c(2)*r;
        c = hsv2rgb(c);
        
        % save information
        link(i) = make_link( link(i), s, d, v, m, c, w );
        
        % attract midpoint towards the centre
        mid = (node(s).pos + node(d).pos)/2;
        mw = norm(mid)/rad.outer;
        mid = (1 - ctr*sqrt(mw))*mid;
        
        % anchor points on the nodes' circles
        sa = (1-rad.node) * node(s).pos;
        da = (1-rad.node) * node(d).pos;
        
        % draw points
        xy = interp_col( [ sa; mid; da ], npts, 'spline' );
        plot( xy(:,1), xy(:,2), '-', 'Color', c, 'LineWidth', w );
        
    end

end

function node = make_node( node, col, ang, rad )
    node.col = col;
    node.ang = ang;
    node.pos = rad*[ cos(ang), sin(ang) ];
end

function link = make_link( link, s, d, v, m, c, w )
    link.src = s;
    link.dst = d;
    link.val = v;
    link.mod = m;
    link.col = c;
    link.wid = w;
end

function draw_circle( O, R, col, width, npts )
    dk.ui.ring( O, R + width*[-1,1]/2, npts, col, 'EdgeColor', 'none' );
end

function col = interp_col( col, n, method )
    c = size(col,1);
    col = interp1( (1:c)', col, linspace(1,c,n)', method );
end
