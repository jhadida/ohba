function pa = mb_static( fc, bands, burn, norm, npts, fs )
%
% pa = mb_static( fc, band=meg(rest-overlap), burn=0, norm='', npts=5, fs=100 )
%
% Build multiband pipeline and display summary and staticfc plots.
%
% INPUTS:
%
%    fc  FC measure used for static FC panel (cf com.conn.fconz).
%  band  Frequency bands (cf com.meg.bands).
%  burn  Burn-in time for preprocessing.
%  norm  Normalisation method (cf com.pipe.preproc).
%  npts  Resampling of wavelet power envelopes (cf ant.dsp.hilbert).
%    fs  Downsampling for faster display (cf com.ui.plot.summary).
%
% Example:
% com.ui.mb_static( 'cor', [], 3 ).analyse(ts);
%
% See also: com.pipe.MultiBand, ant.dsp.hilbert, com.ui.plot.*
%
% JH

    if nargin < 6, fs=100; end
    if nargin < 5, npts=5; end
    if nargin < 4, norm=''; end
    if nargin < 3, burn=0; end
    
    if nargin < 2 || isempty(bands)
        bands = 'rest-overlap'; 
    end
    if ischar(bands)
        bands = com.meg.bands(bands);
    end

    assert( ischar(fc), 'First argument should be a string.' );
    
    opt.burn = burn;
    opt.npts = npts;
    opt.norm = norm;
    opt.bands = bands;
    
    plt(1).name   = 'Summary';
    plt(1).handle = @com.ui.plot.summary;
    plt(1).args   = struct( 'fs', fs );
    
    plt(2).name   = 'FCs';
    plt(2).handle = @com.ui.plot.staticfc;
    plt(2).args   = struct( 'fc', fc, 'bands', {bands} );
    
    pa = com.pipe.build( 'MultiBand', opt, plt );
    
end
