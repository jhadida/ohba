function p = getplot( name, head, varargin )
%
% p = com.ui.getplot( name, head, varargin )
%
% Format a plot to be displayed as a tab in PipelineAnalyser.
% Output is a structure with fields {name,handle,args}.
%
% JH

    switch lower(name)
        case 'summary'
            h = @com.ui.plot.summary;
            t = 'Summary';
        case 'static'
            h = @com.ui.plot.staticfc;
            t = 'Static FC';
        case 'sliding'
            h = @com.ui.plot.slidingfc;
            t = 'Sliding FC';
        otherwise
            error('Unknown plot: %s',name);
    end
    
    if nargin < 2 || isempty(head)
        head = t;
    end
    
    p.name = head;
    p.handle = h;
    p.args = dk.obj.kwArgs(varargin{:});

end
