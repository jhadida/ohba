function out = slidingfc( data, parent, varargin )
%
% out = slidingfc( data, parent, varargin )
%
% Within-band sliding functional connectivity analysis, showing:
% (ie, computed on sliding-window across whole band-filtered timecourse) 
%
%   - Mean, std, skew of functional connectivity
%   - Within-band power time-course for each node 
%   - Connectivity timecourse across nodes
%
% OPTIONS:
%
%     band  String (cf ant.util.eeg_bands) or 1x2 frequency band (required).
%     cmap  Colormap (default: bgr, cf dk.cmap.*)
%       fc  Connectivity measure (default: corr, cf com.conn.fconz)
%       fs  Sampling frequency used for display (default: 50)
%     nosc  Number of oscillations in each window (relative control of window-length, default: 30).
%           Relative to CENTRE FREQUENCY of the band (cf ant.dsp.TFSeries::adaptive_swin).
%
%
% See also: ant.dsp.TFSpectrum, com.conn.fconz, ant.util.eeg_bands
%
% JH

    % create a figure if no parent
    if nargin < 2 || isempty(parent)
        parent = figure('name','[nsl] Narrow-band sliding connectivity');
    end

    % parse key/value options
    opt = dk.obj.kwArgs(varargin{:});
    opt.set( 'reduce', @reduce );
    
    opt_band = opt.get('band');
    opt_cmap = opt.get('cmap', 'bgr' );
    opt_fc   = opt.get('fc',   'corr' );
    opt_fs   = opt.get('fs',   50 );
    opt_nosc = opt.get('nosc', 30 );
    
    % rebuild the parent
    delete( parent.Children );
    h.main = uix.VBox( 'parent', parent, 'spacing', 5, 'padding', 5 );
    
    % find matching band
    if ischar(opt_band)
        opt_band = ant.util.eeg_bands(opt_band);
    end
    sig = data.tf.band_select(opt_band);
    frq = sig.freq;
    
    % compute sliding fc
    [stat,tc] = sig.connectivity_stats( opt_fc, opt_nosc );
    
    out.fc = resample_tc(tc,opt_fs);
    out.fc.mean = mean(cat(3,stat.mean),3);
    out.fc.sdev = mean(cat(3,stat.sdev),3);
    out.fc.skew = mean(cat(3,stat.skew),3);
    
    
    % row 1: FC matrices
    h.row1 = uix.HBox( 'parent', h.main, 'spacing', 5 );
    
    h.fc_mean = axes( 'parent', uicontainer( 'parent', h.row1 ) );
    dk.ui.image( out.fc.mean, 'title', 'Dynamic FC - mean', 'cmap', opt_cmap );
    
    h.fc_std = axes( 'parent', uicontainer( 'parent', h.row1 ) );
    dk.ui.image( out.fc.sdev, 'title', 'Dynamic FC - std', 'cmap', opt_cmap );
    
    h.fc_skew = axes( 'parent', uicontainer( 'parent', h.row1 ) );
    dk.ui.image( out.fc.skew, 'title', 'Dynamic FC - skewness', 'cmap', opt_cmap );
    
    
    % row 2: cross-freq avg
    h.row2 = uix.HBox( 'parent', h.main, 'spacing', 5 );
    h.psd  = axes( 'parent', uicontainer( 'parent', h.row2 ) );
    [t,~,p] = sig.xfplot( 'psd', opt_fs, 'cmap', opt_cmap );
    
    out.psd.time = t;
    out.psd.vals = p;
    
    
    % row 3: connectivity timecourse
    h.row3 = uix.HBox( 'parent', h.main, 'spacing', 5 );
    h.sync = axes( 'parent', uicontainer( 'parent', h.row3 ) );
    
    if numel(frq) >= 6
        if iscell(frq), frq = cellfun( @mean, frq ); end
        dk.ui.image( {out.fc.time,frq,out.fc.vals}, ...
            'xlabel', 'Time (sec)', 'ylabel', 'Frequency', 'cmap', opt_cmap, ...
            'title', 'Connectivity timecourse' );
    else
        if iscell(frq)
            L = ant.util.band2name(frq); 
        else
            L = dk.mapfun( @(x) sprintf('%.0f Hz',x), frq, false ); 
        end
        
        plot(out.fc.time,out.fc.vals); xlabel('Time (sec)'); ylabel('Connectivity'); 
        title('Connectivity timecourse'); legend(L{:});
    end
    
end

function r = reduce(c)
    
    if isempty(c) || isnumeric(c{1})
        error('Unexpected connectivity type.');
    else
        n = numel(tc);
        t = dk.mapfun( @(x) [x.time(1), x.time(end)], tc, false );
        t = vertcat(t{:});
        t = [max(t(:,1)), min(t(:,2))];
        t = t(1): 1/fs :t(end);
        m = numel(t);
        v = zeros(m,n);
    end

end

function tc = resample_tc( tc, fs )

    n = numel(tc);
    t = dk.mapfun( @(x) [x.time(1), x.time(end)], tc, false );
    t = vertcat(t{:});
    t = [max(t(:,1)), min(t(:,2))];
    t = t(1): 1/fs :t(end);
    m = numel(t);
    v = zeros(m,n);
    
    for i = 1:n
        v(:,i) = ant.ts.resample( tc(i).vals, tc(i).time, t );
    end
    
    tc = struct( 'time', t, 'vals', v );
    
end
