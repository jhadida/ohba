function out = envelope( data, parent, varargin )
%
% out = envelope( data, parent, varargin )
%
% Envelope plot showing:
%   - timecourses and mixed correlation (Pearson\partial)
%   - normalised-value distributions and envelope spectra
%   - envelope temporal statistics and raw spectra
%
%
% OPTIONS
% -------
%
%     cmap  Colormap (default: bgr, cf dk.cmap.*)
%       fs  Sampling frequency used for display (default: 50)
%     nbin  Number of bins for value distributions (default: 50)
%     freq  Frequencies at which PSD should be computed (default: 1:50)
%
%
% See also: com.conn.fcon
%
% JH

    % create a figure if no parent
    if nargin < 2 || isempty(parent)
        parent = figure('name','[nsl] Summary plot');
    end

    % parse key/value options
    opt = dk.obj.kwArgs(varargin{:});
    
    opt_cmap = opt.get('cmap', 'bgr' );
    opt_fs   = opt.get('fs',   100 );
    opt_nbin = opt.get('nbin', 50 ); 
    opt_freq = opt.get('freq', 1:50 );
    
    
    % ------------------------------------------------------------
    % compute features
    
    env = data.env;
    pre = data.preproc;
    raw = data.input;
    ns = pre.ns;
    
    out.fc = com.conn.fcon( env.vals, 'mcorr' );
    
    [out.vald.count, out.vald.edge] = ant.dsp.normvalhist( env, opt_nbin );
    out.psd.env = spectrum( env.vals, opt_freq, env.fs );
    out.psd.pre = spectrum( pre.vals, opt_freq, pre.fs );
    
    [lo,up] = ant.ts.envelope( raw.vals );
    raw_env = (up-lo)/2;
    
    out.prc.bin = [1,25,50,75,99];
    out.prc.val = prctile( raw_env, out.prc.bin, 1 );
    
    
    % ------------------------------------------------------------
    % rebuild the parent
    delete( parent.Children );
    h.main = uix.VBox( 'parent', parent, 'spacing', 5, 'padding', 5 );
    
    % ------------------------------------------------------------
    % row 1: envelope TCs and connectivity
    h.row1 = uix.HBox( 'parent', h.main, 'spacing', 5 );
    
    h.env.tc = axes( 'parent', uicontainer('parent',h.row1) );
    env.resample(opt_fs).plot_image( 'ylabel', 'Channel', 'cmap', opt_cmap, ...
        'title', 'Broadband envelopes' );
    
    h.env.fc = axes( 'parent', uicontainer('parent',h.row1) );
    dk.ui.image( out.fc, 'title', 'Broadband AEC', 'cmap', opt_cmap );
    
    h.row1.Widths = [-3,-2];
    
    
    % ------------------------------------------------------------
    % row 2: envelope shape and spectra
    h.row2 = uix.HBox( 'parent', h.main, 'spacing', 5 );
    
    h.vald = axes( 'parent', uicontainer('parent',h.row2) );
    dk.ui.image( {1:ns, out.vald.edge, out.vald.count}, 'xlabel', 'Channel', ...
        'ylabel', 'Normalised env.', 'cmap', opt_cmap, 'positive', true, ...
        'title', 'Distributions of normalised envelopes' );
    
    h.psd.env = axes( 'parent', uicontainer('parent',h.row2) );
    dk.ui.image( {1:ns, opt_freq, log1p(out.psd.env)}, 'xlabel', 'Channel', ...
        'ylabel', 'Frequency (Hz)', 'clabel', 'log-PSD', 'cmap', opt_cmap, ...
        'positive', true, 'title', 'Envelope spectra' );
    
    
    % ------------------------------------------------------------
    % row 3: envelope stats and raw spectra
    h.row3 = uix.HBox( 'parent', h.main, 'spacing', 5 );
    
    h.stat = axes( 'parent', uicontainer('parent',h.row3) );
    plot( 1:ns, out.prc.val ); axis tight;
    xlabel('Channel'); ylabel('Value'); title('Raw amplitude stats');
    legend( '1%', '25%', '50%', '75%', '99%' );
    
    h.psd.raw = axes( 'parent', uicontainer('parent',h.row3) );
    dk.ui.image( {1:ns, opt_freq, log1p(out.psd.pre)}, 'xlabel', 'Channel', ...
        'ylabel', 'Frequency (Hz)', 'clabel', 'log-PSD', 'cmap', opt_cmap, ...
        'positive', true, 'title', 'Preprocessed spectra' );
    
    
end

function psd = spectrum( x, f, fs )
    
    x = dk.bsx.sub(x, mean(x,1));
    
    % good but slow..
    %psd = pmtm( x, 4, f, fs );
    
    n = size(x,1);
    L = ceil(n/4);
    o = floor(L/2);
    w = tukeywin(L);
    psd = pwelch( x, w, o, f, fs );
    
end

function reduce()
    if isstruct(c{1})
        % in the sliding/adaptive cases, we cannot average the 
        % vectorised matrix tcs, because they are not sampled
        % at the same rate; so we just aggregate them
        t = [c{:}];
    else
        t = cat( 3, c{:} );
    end
end
