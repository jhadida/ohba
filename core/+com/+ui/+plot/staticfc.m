function out = staticfc( data, parent, varargin )
%
% out = staticfc( data, parent, varargin )
%
% Show static (ie, computed across all whole timecourse) band-specific 
% functional connectivity matrices.
%
% OPTIONS:
%
%     cmap  Colormap (default: bgr, cf dk.cmap.*)
%    bands  Frequency bands in which functional connectivity should be computed.
%           Default: {[4 8],[6 10],[8 13],[10 20],[13 30],[20 40]}
%       fc  Connectivity measure (default: corr, cf com.conn.fconz)
%
% See also: ant.dsp.TFSeries, com.conn.fconz
%
% JH

    % create a figure if no parent
    if nargin < 2 || isempty(parent)
        parent = figure('name','[nsl] Narrow-band static connectivity');
    end

    % parse key/value options
    opt = dk.obj.kwArgs(varargin{:});
    opt.default( 'bands', com.meg.bands('rest-overlap') );
    opt.set( 'reduce', @reduce );
    
    opt_cmap  = opt.get( 'cmap', 'bgr' );
    opt_fc    = opt.get( 'fc', 'corr' );
    
    % compute functional connectivity
    out = com.dsp.bandfc( data, opt_fc, opt );
    
    % rebuild the parent
    delete( parent.Children ); axes( 'parent', uicontainer('parent',parent) );
    ant.img.grid( {out.(opt_fc)}, {out.name}, true, 'cmap', opt_cmap );

end

function r = reduce(c)

    if isempty(c) || isstruct(c{1})
        error('Unexpected connectivity type (did you set sliding options?).');
    else
        r = mean(cat(3,c{:}),3);
    end

end

% function fc = average_FC( tf, bnd, fcfun )
% 
%     id = find(tf.band_match(bnd));
%     fc = zeros(tf.ns);
% 
%     n = numel(id);
%     for i = 1:n
%        fc = fc + fcfun(tf.signal( id(i) ));
%     end
%     fc = fc / max(n,1);
% 
% end
% 
% function fc = average_SP( tf, bnd, fcfun )
% 
%     % average magnitude and phase
%     [m,p,t] = tf.band_select(bnd).xfsigavg(10);
%     
%     % this is a wavelet aggregate, so we know the frequency band
%     s = ant.dsp.TFSeries( t, m .* exp(1i * p), bnd );
%     fc = fcfun(s);
% 
% end
