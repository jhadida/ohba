function out = summary( data, parent, varargin )
%
% out = summary( data, parent, varargin )
%
% Summary plot showing:
%   - raw timecourses and mixed correlation (Pearson\partial)
%   - power timecourse for each node across frequencies
%   - power timecourse for each frequency across nodes
%
% OPTIONS:
%
%     cmap  Colormap (default: bgr, cf dk.cmap.*)
%     envc  If false, correlate raw signals instead of Hilbert envelope (default: true)
%       fc  Connectivity measure (default: mcorr, cf com.conn.fcon)
%       fs  Sampling frequency used for display (default: 50)
%     vald  If nonzero, number of bins used for value histogram across nodes at each timepoint.
%           If null, the raw timecourses are displayed instead (default: 0).
%
% See also: com.conn.fcon, ant.ui.ts2image
%
% JH

    % create a figure if no parent
    if nargin < 2 || isempty(parent)
        parent = figure('name','[nsl] Summary plot');
    end

    % parse key/value options
    opt = dk.obj.kwArgs(varargin{:});
    
    opt_cmap = opt.get('cmap', 'bgr' );
    opt_envc = opt.get('envc', true); % if false, correlate processed signals
    opt_fc   = opt.get('fc',   'mcorr' );
    opt_fs   = opt.get('fs',   50 );
    opt_vald = opt.get('vald', 0 ); % if >0, plot value distribution instead of tcs
    
    % rebuild the parent
    delete( parent.Children );
    h.main = uix.VBox( 'parent', parent, 'spacing', 5, 'padding', 5 );
    
    % compute features to display
    pre = data.preproc;
    if opt_envc
        signal = data.env;
        fcstr = 'Broadband FC';
    else
        signal = pre;
        fcstr = 'Raw FC';
    end
    
    time = pre.time;
    chan = 1:pre.ns;
    
    out.fc = com.conn.fcon( signal.vals, opt_fc );
    
    
    % row 1: timecourses and connectivity
    h.row1 = uix.HBox( 'parent', h.main, 'spacing', 5 );
    
    % Raw time-courses
    h.preproc = axes( 'parent', uicontainer( 'parent', h.row1 ) );
    if opt_vald > 0
        ncols = fix( opt_fs*pre.tspan );
        [D,t,v] = ant.ui.ts2image( pre, 1, opt_vald, ncols );
        D = dk.bsx.rdiv( D, max(1,sum(D,1)) );
        dk.ui.image( {t,v,D}, ...
            'xlabel', 'Time (sec)', 'ylabel', 'Value', 'clabel', 'Density', ...
            'title', 'Value Distribution', 'cmap', opt_cmap );
    else
        dk.ui.image( {time,chan,pre.vals}, ...
            'xlabel', 'Time (sec)', 'ylabel', 'Channel', 'clabel', 'Value', ...
            'title', 'Processed time-courses', 'cmap', opt_cmap );
    end
    
    
    % functional connectivity
    h.fc = axes( 'parent', uicontainer( 'parent', h.row1 ) );
    dk.ui.image( out.fc, 'title', fcstr, 'cmap', opt_cmap );
    
    h.row1.Widths = [-2 -1];
    
    
    % row 2: power across frequency
    h.row2 = uix.HBox( 'parent', h.main, 'spacing', 5 );
    
    h.cross_freq = axes( 'parent', uicontainer( 'parent', h.row2 ) );
    [t,c,p] = data.tf.xfplot( 'psd', opt_fs, 'cmap', opt_cmap, 'positive', true );
    out.xf.time = t;
    out.xf.chan = c;
    out.xf.vals = p;
    
    h.cross_freq_box = axes( 'parent', uicontainer( 'parent', h.row2 ) );
    data.tf.xfdist(p,c);
    title('Cross-frequency power');
    
    h.row2.Widths = [-2 -1];
    
    
    % row 3: power across nodes
    h.row3 = uix.HBox( 'parent', h.main, 'spacing', 5 );
    
    h.cross_node = axes( 'parent', uicontainer( 'parent', h.row3 ) );
    [t,f,p] = data.tf.xcplot( 'psd', opt_fs, 'cmap', opt_cmap, 'positive', true);
    out.xc.time = t;
    out.xc.freq = f;
    out.xc.vals = p;

    h.cross_node_box = axes( 'parent', uicontainer( 'parent', h.row3 ) );
    data.tf.xcdist(p,f);
    title('Cross-channel power');
    
    h.row3.Widths = [-2 -1];
    
end

