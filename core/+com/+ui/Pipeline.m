classdef Pipeline < handle
    
    properties (SetAccess=protected)
        pipeline;
        results;
    end
    
    properties (SetAccess=protected, Hidden)
        tabs;
        settings;
        listener;
    end
    
    properties (Transient)
        data;
    end
    
    properties (Dependent)
        has_results, has_data;
    end
    
    methods
        
        function self = Pipeline(varargin)
            self.clear();
            if nargin > 0
                self.set_pipeline(varargin{:});
            end
        end
        
        function clear(self)
            if isempty(self.tabs)
                self.tabs = dk.widget.MultiTab();
            else
                self.tabs.clear();
            end
            
            if ~isempty(self.listener)
                delete(self.listener);
            end
            
            self.data     = [];
            self.pipeline = [];
            self.results  = struct();
            self.settings = [];
            self.listener = [];
        end
        
        % is there something in the results/data property?
        function y = get.has_results(self)
            y = numel(fieldnames(self.results)) > 0;
        end
        function y = get.has_data(self)
            y = ~isempty(self.data);
        end
        
        % assign pipeline object (needs to be in displayable state)
        function self = set_pipeline(self,p)
            
            assert( isa(p,'com.pipe.Abstract'), 'Input is not a pipeline.' );
            self.clear();
            
            % set the pipeline, and listen to changes to its properties
            self.pipeline = p;
            self.listener = addlistener( p, 'param', 'PostSet', @(varargin) self.rerun() );
            
        end
        
        % process the data
        function self = process(self,data)
            
            if nargin > 1
                self.data = data;
            end
            
            dk.print('[PipelineAnalyser] Analysing data...'); tstart = tic;
            self.results = self.pipeline.process( self.data );
            dk.print('[PipelineAnalyser] Done! Runtime: %.3f sec', toc(tstart));
        end
        
        % rerun pipeline on existing data
        function self = rerun(self)
            if self.has_data
                self.process();
                self.redraw();
            end
        end
        
        % run the pipeline and refresh tabs
        function self = analyse(self,data,results)
            
            % set data
            self.data = data;
            
            % analyse, or set analysis results
            if nargin < 3
                self.process();
            else
                if ischar(results)
                    dk.print('[PipelineAnalyser] Loading results from "%s"...',results);
                    self.results = load(results);
                else
                    self.results = results;
                end
            end
            
            % redraw
            self.redraw();
            
        end
        
        % add a new tab to the current UI
        %  name      - title of the new tab
        %  callback  - function handle that supports the prototype:
        %               callback( data, parent, options )
        %              where parent is a handle, and options a dk.obj.kwArgs object
        function self = add_tab(self,name,callback,varargin)
            
            self.open();
            tdat = struct( ...
                'callback', callback, ...
                'options', dk.obj.kwArgs(varargin{:}), ...
                'output', struct() ...
            );
            
            n = self.tabs.add( name, tdat );
            self.redraw_tab(n);
        end
        
        % remove tab by number (tab #1 is the Settings) or by name
        function self = rem_tab(self,id)
            self.tabs.rem(id);
        end
        
    end
    
    % ui related methods
    methods
        
        % open or select the window
        function self = open(self)
            
            assert( isa(self.pipeline,'com.pipe.Abstract'), 'Please set a pipeline before opening the UI.' );
            
            if self.tabs.is_open
                self.tabs.open(); % bring figure forward
            else
                
                % save analysis tabs
                old_tabs = self.tabs.tab(2:end);
                self.tabs.clear();
                
                % open new figure
                self.tabs.open(); 
                
                % resize figure
                dk.fig.resize( self.tabs.figure, 700, 1200 );
                
                % settings in the first tab
                [~,self.settings] = self.tabs.add('Settings');
                self.pipeline.ui_open( self.settings );
                
                % iterate over analysis tabs if any
                for i = 1:numel(old_tabs)
                    tab = old_tabs(i);
                    self.add_tab( tab.name, tab.data.callback, tab.data.options );
                end
                
            end
        end
        
        % close the window
        function self = close(self)
            self.tabs.close();
        end
        
        % get output data generated by each tab
        function data = all_data(self)
            data = self.tabs.all_data();
            data = dk.mapfun( @(x) x.output, data(2:end), false );
        end
        
        % shorthand aliases for common operations
        function self = select_tab(self,k)
            self.tabs.select(k);
        end
        function self = update_tab(self,k,varargin)
            self.redraw_tab(k, [], varargin{:} );
        end
        function self = undock_tab(self,k,varargin)
            self.redraw_tab(k, figure, varargin{:} );
        end
        
    end
    
    % hidden methods
    methods (Hidden)
        
        % redraw a given tab
        function self = redraw_tab(self,k,parent,varargin)
            
            % accept tab names
            if ischar(k)
                k = self.tabs.find_tab(k);
            end
            
            if self.has_results
                
                % temporarily extract tab data
                tdat = self.tabs.tab_data(k);
                
                % default parent is the existing one
                if nargin < 3 || isempty(parent)
                    parent = self.tabs.tab_handle(k);
                end
                
                % new options are merged with the old one
                options = tdat.options;
                if nargin > 3
                    options.merge(varargin{:});
                end
                
                % if the parent hasn't changed, just select the tab
                if parent == self.tabs.tab_handle(k)
                    self.open();
                    self.tabs.select(k);
                    tdat.options = options;
                end
                
                % callback with the parent and updated options
                tdat.output = tdat.callback( self.results, parent, options );
                
                % update tab data
                self.tabs.tab_data(k,tdat);
                drawnow;
                
            end
        end
        
        % iterate over each tab and redraw it
        function self = redraw(self)
            if self.tabs.is_open
                n = self.tabs.ntabs;
                for i = 2:n
                    self.redraw_tab(i);
                end
            else
                self.open();
            end
        end
                
    end
    
end
