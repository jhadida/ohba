function [node,link,radii] = connwheel( conn, varargin )
%
% [node,link,radii] = connwheel( conn, varargin )
%
% Show connectivity network as a wheel graph (this opens a new figure).
% Assumptions made about the input matrix: 
%
%   1. Block connectivity left/right with equally sized blocks, 
%      so square matrix with even number of rows/columns 2N; 
%
%   2. Same ordering of nodes within each hemisphere,
%      so nodes k and N+k for k=1..N should be homotopic regions.
%
%
% OUTPUTS:
%   
%    node    A struct-array (for each node) with fields:
%               .col  Node color as displayed.
%               .pos  Node position on the plot (ring's centre).
%               .ang  Angular position of the node (in radians).
%
%    link    A struct-array (for each displayed link) with fields:
%               .src  Node index of the source.
%               .dst  Node index of the destination
%               .val  Connectivity weight.
%               .mod  Modulation factor (between 0 and 1).
%               .col  Link color as displayed.
%               .wid  Link width as displayed.
%
%   radii    A structure with fields:
%               .node   Radius of each node.
%               .outer  Radius of the outer ring.
%
%
% OPTIONS:
%
% Colors:
% -------
%     invert    Invert colors.
%
%      colbg    Color for figure background (default: [1,1,1]).
%     colpos    Color for positive edges (default: [0.8,0,0]).
%     colneg    Color for negative edges (default: [0,0,0.8]).
%     coltxt    Text color for node names (default: [0,0,0]).
%
%     gcolor    Specify a colour for each group.
%     ncolor    Specify a colour for each node in left hemisphere (RH mirrored).
%
% Ring drawing:
% -------------
%     radius    Radius of gray outer circle.
%
%      group    Cell of group-indices for the left hemisphere (RH mirrored).
%               When specified, each group is separated from the next by inserting a ghost node,
%               and each node within the same group has the same color.
%
%       name    Cell of names for each region.
%   showname    Write the name of each region directly on the plot (default: false).
%    namesep    Space between ring and node name, as percentage of radius (default: 0.1).
%
% Link drawing:
% -------------
%    prctile    Filter connections to be drawn (default: 95).
%               The threshold is calculated on non-zero absolute connectivity weights.
%               Percentiles should be between 0 and 100.
%
%    edgethr    Overrides prctile option.
%               Manually specify the absolute threshold to be applied before selecting edges.
%
%       bend    Bend trajectories within the main ring (default: 0.3).
%               The bending factor should be between 0 and 1, higher means more bend.
%
%     maxwid,   
%     minwid    Maximum and minimum link width (default: min=3, max=7).
%               The link width is modulated accoding to their absolute connection strength AFTER THRESHOLDING.
%               See the LineWidth property of Matlab's graphical object Line.
%               NOTE: this parameter should be scaled according to the radius option above.
%
%     minsat    Minimum color saturation (default: 0.2).
%               The link color is modulated accoding to their absolute connection strength AFTER THRESHOLDING.
%               Negative weights are in blue shades, and positive weights in red.
%               This parameter should be between 0 (weakest links are gray) and 1 (no modulation).
%
%       npts    Number of points used to draw each link (default: 51).
%               The trajectory of each link is interpolated between its source and destination.
%
%   arrowlen    Length arrow in the case of directed graphs, relative to npts option (default: 4).
%
% JH

    % parse options
    opt = dk.getopt( varargin, ...
        'eps', 1e-6, ... 
        'invert', false, ...
        'colbg', [1,1,1], ...
        'colpos', [0.8,0,0], ...
        'colneg', [0,0,0.8], ...
        'coltxt', [0,0,0], ...
        'radius', 1, ...
        'group', {}, ...
        'gcolor', [], ...
        'ncolor', [], ...
        'name', {}, ...
        'showname', false, ...
        'namesep', 0.1, ...
        'prctile', 95, ... 
        'edgethr', 0, ... 
        'bend', 0.3, ... 
        'maxwid', 7, ... 
        'minwid', 3, ... 
        'minsat', 0.2, ... 
        'npts', 51, ... 
        'arrowlen', 4 ... 
    );
    [opt,col.node] = check_conn( conn, opt );
    
    % color theme
    col.bg = opt.colbg;
    col.txt = opt.coltxt;
    col.link.pos = opt.colpos;
    col.link.neg = opt.colneg;
    
    % invert colors if required
    if opt.invert
        col.bg = 1-col.bg;
        col.node = 1-col.node;
        col.link.pos = 1-col.link.pos;
        col.link.neg = 1-col.link.neg;
    end
    
    % create figure
    figure( 'name','Connectivity Wheel', 'color', col.bg );
    
    % initialise nodes of the graph
    hold on;
    [node,radii] = draw_nodes( opt, col.node );
    link = draw_links( conn, node, radii, opt, col.link );
    hold off; 

end

function [opt,col] = check_conn( c, opt )

    % check connectivity matrix
    assert( isnumeric(c) || islogical(c), 'conn should be numeric or logical.' );
    assert( ismatrix(c) && diff(size(c))==0, 'conn should be a square numeric matrix.' );
    assert( dk.is.even(size(c,1)), 'conn should have an even number of rows.' );
    
    opt.sym = all(dk.tocol( abs(c-c') ) < opt.eps); % is the connectivity matrix symmetric?
    nreg = size(c,1);
    nhem = nreg/2;
    
    % check groupings and set node colors
    if isempty(opt.group)
        if isempty(opt.ncolor)
            col = dk.cmap.jh(nhem);
        else
            col = opt.ncolor;
        end
        
        if isrow(col)
            col = repmat(col,nhem,1);
        end
    else
        assert( all(cellfun(@isrow,opt.group)), 'Group indices should be row-vectors.' );
        assert( dk.num.isperm([opt.group{:}],nhem), 'Nodes missing in group indices.' );
        
        ngrp = numel(opt.group);
        if isempty(opt.gcolor)
            grp = dk.cmap.jh(ngrp);
        else
            grp = opt.gcolor;
        end
        
        col = zeros(nhem,3);
        for i = 1:ngrp
            gi = opt.group{i};
            col(gi,:) = repmat(grp(i,:),numel(gi),1);
        end
    end
    assert( dk.is.rgb(col,nhem), 'Bad node colors.' );
    
    % determine node names
    if isempty(opt.name)
        opt.name = dk.mapfun( @(k) sprintf('Node %d',k), 1:nreg, false );
    end
    assert( numel(opt.name)==nreg, 'There should be one name for each region in BOTH hemispheres.' );
    
    % fix prctile if between 0 and 1
    if opt.prctile < 1
        opt.prctile = floor(100*opt.prctile);
    end
    
end

function [node,radii] = draw_nodes( opt, col )

    nhem = size(col,1); % #regions in each hemisphere
    nreg = 2*nhem;
    node = dk.struct.repeat( {'col','pos','ang','name'}, 1, nreg );
    name = opt.name;

    % determine node positions
    rad = opt.radius;
    nodegrp = opt.group;
    ngrp = numel(nodegrp);
    
    % if nodes are grouped ...
    if ngrp
        
        % node positions (add a space betweem each group)
        npos = nhem + 2 + ngrp-1; 
        Lang = linspace( pi/2, 3*pi/2, npos );
        Rang = linspace( pi/2, -pi/2, npos );
        
        % iterate on nodes within each group
        k = 0;
        for i = 1:ngrp
            g = nodegrp{i}; % current group
            for n = g
                k = k+1;
                node(n) = make_node( node(n), col(n,:), Lang(k+i), rad, name{n} );
                node(n+nhem) = make_node( node(n+nhem), col(n,:), Rang(k+i), rad, name{n+nhem} );
            end
        end
        
    % otherwise ...
    else
        
        % node positions
        npos = nhem + 2; 
        Lang = linspace( pi/2, 3*pi/2, npos );
        Rang = linspace( pi/2, -pi/2, npos );
        
        % iterate on nodes within hemisphere
        for k = 1:nhem
            node(k) = make_node( node(k), col(k,:), Lang(k+1), rad, name{k} );
            node(k+nhem) = make_node( node(k+nhem), col(k,:), Rang(k+1), rad, name{k+nhem} );
        end
        
    end
    
    % compute radii
    rnode = rad*min( 0.05, pi/(2+nreg) );
    radii.node = rnode;
    radii.main = rad;
    radii.inner = 0.75*rad;
    radii.outer = 1.25*rad;
    
    % draw nodes
    draw_circle( [0 0], rad, 0.95*[1 1 1], 0.25*rad, 61 );
    for n = 1:nreg
        draw_circle( node(n).pos, rnode, node(n).col, 0.5*rnode, 21 );
        if opt.showname
            xy = (1+opt.namesep) * node(n).pos;
            if n > nhem % right hem
                text( xy(1), xy(2), node(n).name, 'Rotation', rad2deg(node(n).ang) );
            else % left hem
                text( xy(1), xy(2), node(n).name, 'Rotation', rad2deg(pi+node(n).ang), ...
                    'HorizontalAlignment', 'right' );
            end
        end
    end
    
    box off; grid off; axis equal off;

end

function link = draw_links( conn, node, rad, opt, col )

    prc  = opt.prctile; % percentile of links to display
    thr  = opt.edgethr; % percentile of links to display
    wmax = opt.maxwid; % maximum link width 
    wmin = opt.minwid; % minimum link width (width modulation)
    smin = opt.minsat; % minimum saturation (color modulation)
    npts = opt.npts; % number of points used to draw links
    ctr  = opt.bend; % bend paths within the disk towards the centre
    aln  = opt.arrowlen; % arrow length for directed graphs
    sym  = opt.sym; % is graph undirected?
    
    % prepare colors for saturation modulation
    cneg = rgb2hsv(col.neg);
    cpos = rgb2hsv(col.pos);
    
    % start/end points for arrow
    ab = floor(npts/3 - aln/2);
    ae = floor(npts/3 + aln/2);

    % format connectivity
    if sym
        conn = tril( conn, -1 ); % keep only lower triangular values
    else
        conn = ant.mat.setdiag( conn, 0 ); % ignore self connections
    end 
    
    % magnitude
    mag = abs(conn);
    maxmag = max(mag(:));
    if maxmag < opt.eps, return; end % dont draw if there is nothing
    
    % filter links to display
    if islogical(conn)
        [src,dst] = find( conn );
        vals = ones(size(src));
    else
        if thr==0, thr = prctile(nonzeros(mag),prc); end
        [src,dst] = find( mag >= thr );
        vals = mag(sub2ind( size(mag), src, dst ));
    end
    [~,ord] = sort(vals,'ascend');
    nlnk = numel(src);
    link = dk.struct.repeat( {'src','dst','val','mod','col','wid'}, 1, nlnk );
    
    % iterate on links
    for i = 1:nlnk
        
        % source, destination, relative magnitude, and weight
        s = src(ord(i));
        d = dst(ord(i));
        v = conn(s,d);
        if islogical(conn)
            m = v;
            w = (wmin+wmax)/2;
        else
            m = (abs(v)-thr)/(maxmag-thr);
            w = wmin + (wmax-wmin)*m;
        end
        
        % link color (modulate saturation)
        r = smin + (1-smin)*m;
        if v < 0 
            c = cneg;
        else
            c = cpos;
        end
        c(2) = c(2)*r;
        c = hsv2rgb(c);
        
        % save information
        link(i) = make_link( link(i), s, d, v, m, c, w );
        
        % attract midpoint towards the centre
        mid = (node(s).pos + node(d).pos)/2;
        mw = norm(mid)/rad.outer;
        mid = (1 - ctr*sqrt(mw))*mid;
        
        % anchor points on the nodes' circles
        sa = (1-rad.node) * node(s).pos;
        da = (1-rad.node) * node(d).pos;
        
        % draw points
        xy = interp_col( [ sa; mid; da ], npts, 'pchip' );
        plot( xy(:,1), xy(:,2), '-', 'Color', c, 'LineWidth', w );
        
        % draw arrow
        if ~sym, draw_arrow( xy(ab,:), xy(ae,:), c, m ); end
        
    end

end

function node = make_node( node, col, ang, rad, name )
    node.col = col;
    node.ang = ang;
    node.pos = rad*[ cos(ang), sin(ang) ];
    node.name = name;
end

function link = make_link( link, s, d, v, m, c, w )
    link.src = s;
    link.dst = d;
    link.val = v;
    link.mod = m;
    link.col = c;
    link.wid = w;
end

function draw_circle( O, R, col, width, npts )
    dk.ui.ring( O, R + width*[-1,1]/2, npts, col, 'EdgeColor', 'none' );
end

function draw_arrow( h, a, col, m )
    v = a-h;
    v = [-v(2),v(1)];
    w = (2+m)/3;
    b = h+w*v/2;
    c = h-w*v/2;
    p = [a;b;c;a];
    fill(p(:,1),p(:,2),'k'); % fill with black looks better
end

function col = interp_col( col, n, method )
    c = size(col,1);
    col = interp1( (1:c)', col, linspace(1,c,n)', method );
end
