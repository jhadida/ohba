function fc = fcon( ts, method, varargin )
%
% fc = ant.dsp.plot.fcon( ts, method, varargin )
% 
% Compute functional connectivity on input time-series and display image.
% Varargs are forwarded to dk.ui.image.
%
% JH

    fc = com.conn.fcon( ts.vals, method );
    dk.ui.image( fc, varargin{:} );

end
