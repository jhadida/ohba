function lights(h)
%
% Create 6 infinite lights from each direction in the current (or given) axes.
%
% JH

    if nargin > 0
        dk.fig.select(h);
    end
    
    h = dk.ui.light( ...
        { [0,0], 'infinite' }, ...
        { [180,0], 'infinite' }, ...
        { [0,90], 'infinite' }, ...
        { [0,-90], 'infinite' }, ...
        { [90,0], 'infinite' }, ...
        { [-90,0], 'infinite' } ...
    );

    n = numel(h);
    for i = 1:n
        h(i).Color = 0.63*[1,1,1];
    end

end