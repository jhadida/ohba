classdef OrthoView < handle
    
    properties
        handle;
        volume;
        option;
        position;
    end
    
    methods
        
        function self = OrthoView(varargin)
            self.handle = struct();
            self.clear();
            if nargin > 0
                self.set_volume(varargin{:});
            end
        end
        
        function clear(self)
            self.close();
            self.reset_options();
            self.volume   = zeros(1,1,0); % force 3D size
            self.position = [];
            self.handle   = struct();
        end
        
        % close the window
        function self = close(self)
            
            if self.ui_is_open
                close( self.handle.fig );
            end
            self.handle.fig = NaN;
        end
        
        % set brain image (reverse Y direction and update labels)
        function self = set_brain(self,vol,varargin)
            
            opt = [ {'xlabel', 'L-R', 'ylabel', 'A-P', 'zlabel', 'I-S', 'convention', [1,-1,1]}, varargin ];
            self.set_volume(vol,opt{:});
            
        end
        
        % set volumetric image
        function self = set_volume(self,vol,varargin)
            
            % ensure that we can draw this
            assert( isnumeric(vol) && ndims(vol)==3 && ~isempty(vol), 'Expects a non-empty numeric 3D array in input.' );
            
            % process options
            self.reset_options();
            self.position = [];
            if nargin > 2
                self.set_options(varargin{:});
            end
            
            % apply convention
            c = in.get('convention',[1,1,1]);
            if c(1) < 0, vol = flip(vol,1); end
            if c(2) < 0, vol = flip(vol,2); end
            if c(3) < 0, vol = flip(vol,3); end
            self.volume = vol;
            
            % redraw figure
            self.redraw();
            
        end
        
        % update views using current position
        function self = redraw(self)
            
            % build UI window if not already open
            if self.ui_is_open
                figure(self.handle.fig);
            else
                self.build();
            end
            
            % check that there is a volume to display
            if isempty(self.volume)
                return;
            end
            
            % if no position is set, set the middle point
            if isempty(self.position)
                self.position = floor(size(self.volume)/2);
            end
            
            % check that options are valid
            assert( self.check_options(), 'Invalid options.' );
            
            % compute color range and map
            r = self.option.range;
            t = lower(self.option.type);
            v = self.volume(~isnan(self.volume));
            if ~isempty(r)
                t = 'manual';
            end
            if strcmp(t,'auto')
                if islogical(self.volume)
                    t = 'logical';
                elseif all(dk.torow( v == int32(v) ))
                    t = 'integer';
                else
                    t = 'continuous';
                end
            end
            switch t
                case {'real','continuous'}
                    r = [min(v), max(v)];
                    c = jet(64);
                case {'logical','bw'}
                    r = [0,1];
                    c = [0,0,0; 1,1,1];
                case {'integer','label'}
                    u = unique(v);
                    r = u([1,end]);
                    n = numel(u);
                    c = linspace(0,4/5,n);
                    c = hsv2rgb([ c(:), [0,0; ones(n-1,2)] ]);
                case {'pos','positive'}
                    r = [0,max(v)];
                    c = hot(64);
                case {'neg','negative'}
                    r = [min(v),0];
                    c = flipud(hot(64));
                case {'sym','symmetric'}
                    r = max(abs(v))*[-1,1];
                    c = dk.cmap.rdb2(128,true);
                case 'manual'
                    % nothing to do
                    c = jet(64);
                otherwise
                    error( 'Unknown range type: %s', t );
            end
            if isnumeric(self.option.colormap)
                c = self.option.colormap;
            end
            
            % draw views
            colormap(c);
            self.draw_view('x'); caxis(r);
            self.draw_view('y'); caxis(r);
            self.draw_view('z'); caxis(r);
            
        end
        
        % set options
        function in = set_options(self,varargin)
            
            in  = dk.obj.kwArgs(varargin{:});
            opt = self.option;
            
            % resample image
            tmp_resample = in.get( 'resample', [] );
            if ~isempty(tmp_resample)
                self.volume = com.util.imresize3( self.volume, tmp_resample, in.get('method','cubic') );
            end
            
            % set color range
            opt.range = in.get( 'range', opt.range );
            
            % range type
            opt.type = in.get( 'type', opt.type );
            
            % colormap
            opt.colormap = in.get( 'colormap', opt.colormap );
            
            % value formatting
            opt.format = in.get( 'format', opt.format );
            
            % disable labels
            opt.xlabel = in.get('xlabel',opt.xlabel);
            opt.ylabel = in.get('ylabel',opt.ylabel);
            opt.zlabel = in.get('zlabel',opt.zlabel);
            
            if in.get('nolabel',false)
                opt.xlabel = [];
                opt.ylabel = [];
                opt.zlabel = [];
            end
            
            % remember options
            self.option = opt;
            
        end
        
        % reset options to default values
        function self = reset_options(self)
            
            % default options
            opt.type       = 'auto';
            opt.colormap   = 'auto';
            opt.format     = '%g';
            opt.range      = [];
            opt.xlabel     = 'x';
            opt.ylabel     = 'y';
            opt.zlabel     = 'z';
            
            % reset member property
            self.option = opt;
            
        end
        
        % check options
        function ok = check_options(self)
            ok = true; % TODO
        end
        
        % print info about the current volume
        function info_volume(self)
            dk.print('[OrthoView.info_volume]');
                info.size  = size(self.volume);
                info.range = [min(self.volume(:)),max(self.volume(:))];
                disp(info);
        end
        
        % print current values of options
        function info_options(self)
            dk.print('[OrthoView.info_options]');
                disp(self.option);
        end
        
    end
    
    methods (Hidden)
        
        % check that handle 'name' exists and is active
        function ok = ui_check_handle(self,name)
            ok = isfield( self.handle, name ) && ishandle( self.handle.(name) );
        end
        
        % check that main figure is open
        function y = ui_is_open(self)
            y = self.ui_check_handle('fig');
        end
        
        % build figure
        function build(self,parent)
            
            if nargin < 2
                parent = dk.fig.new('[OrthoView]',0.8);
            end
            
            % clear parent handle
            delete( parent.Children );
            self.handle.fig = parent;
            
            % main grid with 3 axes
            self.handle.grid = uix.HBox( 'parent', parent, 'spacing', 5, 'padding', 5 );
            self.handle.view.x = axes( 'parent', uicontainer( 'parent', self.handle.grid ), 'ButtonDownFcn', @(varargin) self.cb_select(1,varargin{:}) );
            self.handle.view.y = axes( 'parent', uicontainer( 'parent', self.handle.grid ), 'ButtonDownFcn', @(varargin) self.cb_select(2,varargin{:}) );
            self.handle.view.z = axes( 'parent', uicontainer( 'parent', self.handle.grid ), 'ButtonDownFcn', @(varargin) self.cb_select(3,varargin{:}) );
            
            % build menu
            self.handle.menu.tools = uimenu( self.handle.fig, 'Label', 'Tools' );
            
            self.handle.menu.tools_info = uimenu( self.handle.menu.tools, 'Label', 'Info' );
            self.handle.menu.tools_info_volume = uimenu( self.handle.menu.tools_info, 'Label', 'Volume', 'Callback', @self.cb_info_volume );
            self.handle.menu.tools_info_option = uimenu( self.handle.menu.tools_info, 'Label', 'Options', 'Callback', @self.cb_info_options );
            
            self.handle.menu.tools_snapshot = uimenu( self.handle.menu.tools, 'Label', 'Snapshot', 'Callback', @self.cb_snapshot );
            
        end
        
        % draw view
        function draw_view(self,name)
            
            % select axes to draw into
            axes( self.handle.view.(name) );
            
            % split position into three distinct variables
            [vx,vy,vz] = size(self.volume);
            [x,y,z] = dk.deal(self.position);
            
            % get pretty colours for the lines
            col = dk.cmap.matlab;
            col = struct( 'r', col(end,:), 'g', col(end-2,:), 'b', col(end-1,:) );
            %col = struct( 'r', [1 0 0], 'g', [0 1 0], 'b', [0 0 1] );
            
            % draw views
            switch name
                case 'x'
                    f=@(varargin) self.cb_select(1,varargin{:});
                    imagesc( permute(self.volume(x,:,:),[3,2,1]), 'ButtonDownFcn', f ); hold on;
                    plot( [0,vy], z*[1 1], 'Color', col.b, 'LineWidth', 2, 'ButtonDownFcn', f );
                    plot( y*[1,1], [0,vz], 'Color', col.g, 'LineWidth', 2, 'ButtonDownFcn', f ); hold off;
                    xlabel( self.option.ylabel ); ylabel( self.option.zlabel );
                case 'y'
                    f=@(varargin) self.cb_select(2,varargin{:});
                    imagesc( permute(self.volume(:,y,:),[3,1,2]), 'ButtonDownFcn', f ); hold on;
                    plot( [0,vx], z*[1 1], 'Color', col.b, 'LineWidth', 2, 'ButtonDownFcn', f );
                    plot( x*[1,1], [0,vz], 'Color', col.r, 'LineWidth', 2, 'ButtonDownFcn', f ); hold off;
                    xlabel( self.option.xlabel ); ylabel( self.option.zlabel ); title(sprintf('(%d,%d,%d)',x,y,z));
                case 'z'
                    f=@(varargin) self.cb_select(3,varargin{:});
                    imagesc( permute(self.volume(:,:,z),[2 1 3]), 'ButtonDownFcn', f ); hold on;
                    plot( [0,vx], y*[1 1], 'Color', col.g, 'LineWidth', 2, 'ButtonDownFcn', f );
                    plot( x*[1,1], [0,vy], 'Color', col.r, 'LineWidth', 2, 'ButtonDownFcn', f ); hold off;
                    xlabel( self.option.xlabel ); ylabel( self.option.ylabel );
            end
            set( gca, 'YDir', 'normal' );
            
        end
        
        
    end
    
    methods (Hidden)
        
        function cb_select(self,viewid,hobj,edat)
            
            % get position from "Hit" object
            pos = max(1,round(edat.IntersectionPoint(1:2)));

            % update member position and redraw views
            vsz = size( self.volume );
            switch viewid

                case 1
                    if all( pos>=1 & pos<=vsz([2,3]) )
                        self.position([2,3]) = pos;
                        self.redraw();
                    end
                case 2
                    if all( pos>=1 & pos<=vsz([1,3]) )
                        self.position([1,3]) = pos;
                        self.redraw();
                    end
                case 3
                    if all( pos>=1 & pos<=vsz([1,2]) )
                        self.position([1,2]) = pos;
                        self.redraw();
                    end
                
            end
            
            % Print info in console
            pos = self.position;
            ind = [1,cumprod(vsz)];
            ind = 1+dot( pos-1, ind(1:3) );
            dk.print( ['Voxel(%d,%d,%d) = ' self.option.format], pos(1), pos(2), pos(3), self.volume(ind) );
            
        end
        
        function cb_info_volume(self,hobj,edat)
            self.info_volume();
        end
        function cb_info_options(self,hobj,edat)
            self.info_options();
        end
        
        function cb_snapshot(self,hobj,edat)
            dk.warn('[OrthoView.snapshot] Not implemented yet.'); % TODO
        end
        
    end
    
end
