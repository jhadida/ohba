function fig = structural( weight, length, names )
%
% fig = com.ui.structural( weight, length, names )
%
% Display grid of structural connectivity matrices, combining both weight (lower-triangle) 
% and length (upper-triangle).
%
% Weight and length should be cells of square matrices.
% Names should be a cellstr of same length with region names.
%
% Colour is harmonized separately across weights and lengths matrices (assumed positive).
% Two additional figures are opened with the corresponding colour bars.
%
% JH

    % check inputs
    assert( iscell(weight) && iscell(length) && numel(weight)==numel(length), ...
        'Input weight/length should be cells of matrices.' );
    
    squarepos = @(x) dk.is.squarepos(x,false);
    assert( all(cellfun( squarepos, weight )) && all(cellfun( squarepos, length )), ...
        'Bad input matrices (should be square with non-negative elements.' );
    
    assert( iscellstr(names) && numel(names)==numel(weight), ...
        'Names should be a cellstr with one name per matrix.' );
    
    Nmat = numel(weight);
    [Nrow,Ncol] = dk.gridfit(Nmat);
    
    % build RGB images for each slice, combining both weight and length
    wmap = dk.cmap.bgr(64);
    lmap = jet(64);
    lmap = wmap;
    
    [mat,wrng,lrng] = ant.ui.combmat( weight, length, 'lr', 'pos', 'ur', 'pos', 'cmap', wmap );
    
    % open figure and show images
    fig = figure;
    for i = 1:Nmat
        subplot( Nrow, Ncol, i ); imshow( mat{i} ); title( names{i} ); 
    end
    
    fig.UserData.mat = mat;
    fig.UserData.wrng = wrng;
    fig.UserData.lrng = lrng;
    
    % show colorbars in separate figures
    dk.ui.colorbar( wrng, 'log(1 + Count)', 'orient', 'h', 'reverse', true, 'txtopt', {'FontWeight','bold'}, 'cmap', wmap );
    dk.ui.colorbar( lrng, 'Length (cm)', 'orient', 'v', 'txtopt', {'FontWeight','bold'}, 'cmap', lmap );
    
end
