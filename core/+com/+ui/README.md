
# Graphics submodule

## Brain plots

Example:
```
com.ui.SurfaceBrain( parc, mesh )       % calls assign()

    .configure( options... )
    .assign( parc, mesh )
      .set_parcellation( name )
      .set_mesh( mesh )

    .showmap( vec, options... )         % show any scalar map
      .showall()                        % show all regions with different colours
      .showmesh()                       % brain without color
      .showone( id, type, col, options...)    % show one region
```

Config options:

| Name | Default | Description |
|---|---|---|
| `openfig` | `true` | Open new figure when drawing |
| `face_alpha` | `0.95` | Mesh transparency (1=opaque) |
| `edge_color` | `[1,1,1]*0.95` | - |
| `edge_alpha` | `0.5` | - |
| `nocolor` | `[1,1,1]*2/3` | Color of unassigned faces |
| `shading` | `interp` | `flat, interp, faceted` |
| `material` | `dull` | `dull, metal, shiny` |
| `lighting` | `gouraud` | `flat, gouraud, none` |
| `light_type` | `infinite` | `local, infinite` |

Drawing options:

| Name | Default | Description |
|---|---|---|
| `type` | `both` | `sync, both, left, right` |
| `bind` | `false` | Bind instance to figure handle |
| `cmap` | `hsv` | Colors associated with values |
| `crange` | `[]` | Constrain values of scalar map |
| `bright` | `1` | Modulate brightness |

---

## Volume explorer

Example:
```
com.ui.OrthoView( vol, options )        % calls set_volume

    .set_volume( vol, options... )      % default volume drawing
    .set_brain( vol, options... )       % force axes labels
    .set_options( options... )          % change options
```

Options:

| Name | Default | Description |
|---|---|---|
| `resample` | `[]` | Resample input volume to target size |
| `method` | `cubic` | Resampling method (see `com.util.imresize3`) |
| `range` | `[0,1]` | Colormap range |
| `dynrange` | `[0,100]` | Set `range` using percentiles |
| `positive` | `false` | Force lower range to 0 |
| `negative` | `false` | Force upper range to 0 |
| `colormap` | `com.cmap.hot` | Set colormap (`sc, randsc, gray, hot`) |
| `format` | `%g` | Print voxel values to console |
| `(x,y,z)label` | `x,y,z` | Axes labels |
| `nolabel` | `false` | Remove labels |

---

## Other

 - `barwitherr`: third-party script combining `bar` plot with `errorbar`.
 - `scgrid`: display grid of structural connectivity matrices with weights in tril, and length in triu.
