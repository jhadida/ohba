function pa = wav_sliding( fc, freq, burn, nosc, norm, npts, fs )
%
% pa = wav_sliding( fc, freq, burn=0, nosc=30, norm='', npts=5, fs=100 )
%
% Build wavelet pipeline and display summary and slidingfc plots.
% The frequency bands for which the sliding FC is computed are:
%   [4,8], [8,13], [13,30], [20,40]
%
% INPUTS:
%
%    fc  FC measure used for static FC panel (cf com.ui.plot.staticfc).
%  freq  Frequencies for the wavelet decomposition (cf com.pipe.Wavelet).
%  burn  Burn-in time for preprocessing.
%  nosc  Number of oscillations in each window (cf ant.dsp.TFSeries::adaptive_swin).
%  norm  Normalisation method (cf com.pipe.preproc).
%  npts  Resampling of wavelet power time-courses (cf ant.dsp.wavelet).
%    fs  Downsampling for faster display (cf com.ui.plot.*).
%
% Example:
% com.ui.wav_sliding( 'cor', 4:40, 3, 6 ).analyse(ts);
%
% See also: com.pipe.Wavelet, ant.dsp.wavelet, com.ui.plot.*
%
% JH

    if nargin < 7, fs=100; end
    if nargin < 6, npts=5; end
    if nargin < 5, norm=''; end
    if nargin < 4, nosc=30; end
    if nargin < 3, burn=0; end

    opt.burn = burn;
    opt.npts = npts;
    opt.freq = freq;
    opt.norm = norm;
    
    arg = struct( 'fc', fc, 'nosc', nosc, 'fs', fs );

    plt(1).name   = 'Summary';
    plt(1).handle = @com.ui.plot.summary;
    plt(1).args   = struct( 'fs', fs, 'burn', burn );

    arg.band = [4,8];
    plt(2).name   = 'Theta';
    plt(2).handle = @com.ui.plot.slidingfc;
    plt(2).args   = arg;
    
    arg.band = [8,13];
    plt(3).name   = 'Alpha';
    plt(3).handle = @com.ui.plot.slidingfc;
    plt(3).args   = arg;
    
    arg.band = [13,30];
    plt(4).name   = 'Beta';
    plt(4).handle = @com.ui.plot.slidingfc;
    plt(4).args   = arg;
    
    arg.band = [20,40];
    plt(5).name   = 'Beta hi';
    plt(5).handle = @com.ui.plot.slidingfc;
    plt(5).args   = arg;
    
    pa = com.pipe.build( 'Wavelet', opt, plt );
    
end
