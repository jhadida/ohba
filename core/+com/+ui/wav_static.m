function pa = wav_static( fc, freq, burn, norm, npts, fs )
%
% pa = wav_static( fc, freq, burn=0, normalise='', npts=5, fs=100 )
%
% Build wavelet pipeline and display summary and staticfc plots.
%
% INPUTS:
%
%    fc  FC measure used for static FC panel (cf com.ui.plot.staticfc).
%  freq  Frequencies for the wavelet decomposition (cf com.pipe.Wavelet).
%  burn  Burn-in time for preprocessing.
%  norm  Normalisation method (cf com.pipe.preproc).
%  npts  Resampling of wavelet power time-courses (cf ant.dsp.wavelet).
%    fs  Downsampling for faster display (cf com.ui.plot.summary).
%
% Example:
% com.ui.wav_static( 'cor', 4:40, 3 ).analyse(ts);
%
% See also: com.pipe.Wavelet, ant.dsp.wavelet, com.ui.plot.*
%
% JH

    if nargin < 6, fs=100; end
    if nargin < 5, npts=5; end
    if nargin < 4, norm=''; end
    if nargin < 3, burn=0; end

    opt.burn = burn;
    opt.npts = npts;
    opt.freq = freq;
    opt.norm = norm;

    plt(1).name   = 'Summary';
    plt(1).handle = @com.ui.plot.summary;
    plt(1).args   = struct( 'fs', fs );
    
    plt(2).name   = 'FCs';
    plt(2).handle = @com.ui.plot.staticfc;
    plt(2).args   = struct( 'fc', fc );
    
    pa = com.pipe.build( 'Wavelet', opt, plt );
    
end
