function rotate_gif( fig, folder, el, n, style )
%
% rotate_gif( fig, folder, el=15, n=61, style=normal )
%
% Take figure with single 3D axes; rotate axes saving figure for each angle, and create a gif.
%
% Then call (using ImageMagick):
%   convert -resize 50% -delay 20 -loop 0 img{1..61}.png output.gif
%
% See: see: https://askubuntu.com/a/940447/135465
% See also: dk.fig.export
%
% JH
    
    if nargin < 5, style='normal'; end
    if nargin < 4, n=61; end
    if nargin < 3, el=15; end
    
    assert( dk.fs.isdir(folder), 'Folder not found: %s', folder );
    
    f = figure(fig);
    az = linspace( -180, 180, n );

%     for i = 1:n
%         view( az(i), el );
%         file = fullfile( folder, sprintf( 'img%d.png', i ) );
%         dk.fig.export( f, file, style );
%     end
    
    dk.print( 'Now run:\n\t cd "%s"\n\t convert -resize 50%% -delay 20 -loop 0 img{1..61}.png output.gif\n', folder );
    
end
