function fig = ei_unit( ts, title_str )

    if nargin < 2, title_str = 'EI unit time-course'; end
    fig = figure( 'name', '[com.ui.sim] EI Unit UI' );
    
    % check whether both E and I have a time-course for this node
    eok = ts.ns >= 1; tse = ant.TimeSeries();
    iok = ts.ns >= 2; tsi = ant.TimeSeries();
    
    if eok, tse = ts.select(1); end
    if iok, tsi = ts.select(2); end

    % show raw time-courses
    subplot(2,3,[1 2 3]); L = {};
    if eok
        tse.plot_lines( 'b-', 'LineWidth', 1.5 ); hold on; L{end+1} = 'E';
    end
    if iok
        tsi.plot_lines( 'r-', 'LineWidth', 1.0 ); hold on; L{end+1} = 'I';
    end
    hold off; legend(L{:}); xlabel('Time (sec)'); title(title_str); 

    % show phase-plane if both E and I are set
    subplot(2,3,4);
    if eok && iok
        plot( tse.vals, tsi.vals );
        xlabel('E'); ylabel('I'); axis equal; 
        title('Orbit');
    else
        [ari,dt] = ts.is_arithmetic();
        assert( ari, 'Cannot differentiate irregular time-series.' );
        x = tse.vals;
        y = ant.ts.diff( tse.time, 1/dt );
        plot(x,y); xlabel('E'); ylabel('E'''); axis equal; 
        title('Phase Plane');
    end

    % show spectral analysis of the excitatory time-course
    subplot(2,3,[5 6]);
    if eok, ant.ui.periodogram(tse); end
    
    % stash the timecourses for easy retrieval
    fig.UserData.tse = tse;
    fig.UserData.tsi = tsi;

end
