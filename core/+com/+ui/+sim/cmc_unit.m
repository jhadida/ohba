function fig = cmc_unit( ts, title_str )

    if nargin < 2, title_str = 'CMC unit time-course'; end
    
    % extract data
    time = ts.time;
    if ts.ns == 8
        vals = ts.vals(:,1:2:end);
    elseif ts.ns == 4
        vals = ts.vals;
    else
        error('Input time-series should have 4 or 8 channels.');
    end
    
    % create figure
    fig = figure( 'name', '[com.ui.sim] CMC Unit UI' );
    
    
    % draw time-courses
    subplot(3,3,[1 2 3]);
    
    plot( time, vals(:,1), '-', 'Color', [0 190 60]/255 ); hold on;
    plot( time, vals(:,2), '-', 'Color', [0 180 235]/255 ); 
    plot( time, vals(:,3), 'b-', 'LineWidth', 1.5 ); 
    plot( time, vals(:,4), 'r-', 'LineWidth', 1.5 ); hold off;
    
    legend( 'Spiny Stellate', 'Interneuron', 'Pyramidal Esyn', 'Pyramidal Isyn' );
    xlabel('Time (sec)'); ylabel('Value');
    title( title_str );
    
    
    % draw MEG source
    subplot(3,3,[4 5 6]);
    
    plot( time, vals(:,3)-vals(:,4), 'k-', 'LineWidth', 2 );
    xlabel('Time (sec)'); ylabel('Value');
    title('Net pyramidal synaptic current (MEG source equivalent)');

    
    % show phase-plane if both E and I are set
    subplot(3,3,7);
    plot( vals(:,3), vals(:,4) );
    xlabel('E'); ylabel('I'); axis equal tight; title('Orbit');

    % show spectral analysis of the excitatory time-course
    subplot(3,3,[8 9]);
    ant.ui.periodogram(ant.TimeSeries( time, vals(:,3) - vals(:,4) ));
    
    % stash the timecourses for easy retrieval
    fig.UserData.time = time;
    fig.UserData.vals = vals;

end
