function fig = cb_unit( ts, title_str )

    if nargin < 2, title_str = 'CB unit time-course'; end
    fig = figure( 'name', '[com.ui.sim] CB Unit UI' );
    assert( mod(ts.ns,3) == 0, 'Number of time-courses should be a multiple of 3.' );
    
    % build the UI
    vplot = subplot(3,3,[1 2 3]);
    gplot = subplot(3,3,[4 5 6]);
    oplot = subplot(3,3,7);
    
    % legends
    vleg = {};
    gleg = {};
    
    
    % check whether both E and I have a time-course for this node
    eok = ts.ns >= 3; 
    iok = ts.ns >= 6;
    t   = ts.time;
    
    if eok
        Ve  = ts.vals(:,1);
        gee = ts.vals(:,2);
        gie = ts.vals(:,3);
        
        plot( vplot, t, Ve, 'b-', 'LineWidth', 1.5 ); vleg{end+1}='Ve';
        plot( gplot, t, gee, 'b-' ); hold(gplot,'on'); 
        plot( gplot, t, gie, 'c-' ); hold(gplot,'off'); gleg=[gleg,{'gee','gie'}];
    else
        Ve = []; gee = []; gie = [];
    end
    if iok
        Vi  = ts.vals(:,4);
        gei = ts.vals(:,5);
        gii = ts.vals(:,6);
        
        hold(vplot,'on'); 
            plot( vplot, t, Vi, 'r-' ); 
        hold(vplot,'off'); vleg{end+1}='Vi';
        hold(gplot,'on'); 
            plot( gplot, t, gei, 'm-' );
            plot( gplot, t, gii, 'r-' );
        hold(gplot,'off'); gleg=[gleg,{'gei','gii'}];
    else
        Vi = []; gei = []; gii = [];
    end
    
    
    % axis/legends
    xlabel( vplot, 'Time (sec)' );
    ylabel( vplot, 'Potential (mV)' );
    legend( vplot, vleg{:} );
    title(vplot,title_str);
    
    %xlabel( gplot, 'Time (sec)' );
    ylabel( gplot, 'Conductance (nS)' );
    legend( gplot, gleg{:} );
    
    
    % orbits
    if iok
        plot( oplot, Ve, Vi );
        xlabel('Ve'); ylabel('Vi'); axis equal; title('Orbit');
    elseif eok
        [ari,dt] = ts.is_arithmetic();
        assert( ari, 'Cannot differentiate irregular time-series.' );
        x = Ve;
        y = ant.ts.diff( Ve, 1/dt );
        plot( oplot, x, y ); 
        xlabel('Ve'); ylabel('Ve'''); axis equal; 
        title('Phase Plane');
    end
    
    
    % frequency analysis
    subplot(3,3,[8 9]);
    if eok, ant.ui.periodogram(ant.TimeSeries(t,Ve)); end
    
    
    % export data
    dat.t   = t;
    dat.Ve  = Ve;
    dat.gee = gee;
    dat.gie = gie;
    dat.Vi  = Vi;
    dat.gei = gei;
    dat.gii = gii;
    fig.UserData = dat;
    
end

