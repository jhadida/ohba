function [f1,f2] = analysis( ana, fctype, order )
%
% [f1,f2] = com.ui.analysis( ana, fctype=cor, order=[] )
%
% Display results produced by ant.dsp.analysis.
% 
% Opens TWO figures, one with FC matrices, the other with spectral stats.
%
% See also: ant.dsp.analysis
%
% JH 

    if nargin < 3, order = []; end
    if nargin < 2, fctype = 'cor'; end
    
    is_squaremat = @(x) isnumeric(x) && ismatrix(x) && (diff(size(x)) == 0);
    is_volume = @(x) isnumeric(x) && ndims(x)==3;

    
    % extract connectivity
    band = { ana.con.band };
    con = { ana.con.(fctype) };
    if isempty(con)
        error( 'Empty field: %s', fctype );
    elseif isscalar(con)
        con = con{1};
        band = band{1};
    elseif dk.is.struct(con{1},{'time','mats'})
        dk.info('Taking average FC across windows.');
        con = dk.mapfun( @(x) ant.mat.col2sym( mean(x.mats,2), true ), con, false );
    elseif dk.is.struct(con{1},{'mean','sdev'})
        dk.info('Selecting FC stats field: mean.');
        con = dk.mapfun( @(x) ant.mat.col2sym(x.mean,true), con, false );
    elseif isvector(con{1})
        con = dk.mapfun( @(x) ant.mat.col2sym(x(:),true), con, false );
    elseif ~is_squaremat(con{1})
        dk.info('Taking average FC within each band.');
        con = dk.mapfun( @(x) ant.mat.col2sym( mean(x,2), true ), con, false );
    end
    
    assert( all(cellfun( is_squaremat, con )), 'Could not extract connectivity matrices.' );
    
    nn = size(con{1},1);
    if isempty(order)
        order = 1:nn;
    else
        con = dk.mapfun( @(x) x(order,order), con, false );
    end
    
    f1 = com.meg.show_fc( con, band, true );
    
    
    % extract spectral stats
    if isfield(ana,'sps')
        
        sps = ana.sps;
        
        if is_volume(sps.mean)
            dk.info('Taking average spectral stats for each frequency.');
            sps.mean = mean( sps.mean, 3 );
            sps.sdev = mean( sps.sdev, 3 );
            sps.skew = mean( sps.skew, 3 );
        end

        assert( dk.is.struct(sps,{'freq','mean','sdev'}), 'Could not extract spectral stats.' );

        nf = numel(sps.freq);
        [rs,cs] = size(sps.mean);
        assert( rs==nf && cs==nn, 'Unexpected stats size.' );
        clear rs cs;
        
        if ~isempty(order)
            sps.mean = sps.mean(:,order);
            sps.sdev = sps.sdev(:,order);
            sps.skew = sps.skew(:,order);
        end

        f2 = dk.fig.new( 'Spectral stats', 0.75 );
        f2.UserData = sps;
        
        x = 1:nn;
        y = sps.freq;
        r = dk.num.range( [sps.mean(:); sps.sdev(:)], 'pos' );
        
        subplot(2,3,[1 2]); dk.ui.image( {x,y,sps.mean}, 'crange', r, ...
            'title', 'Time-average of wavelet magnitude', 'ylabel', 'Frequency (Hz)' );
        subplot(2,3,3); dk.ui.image( {y,y,corrcoef(sps.mean')}, 'rmticks', false );
        
        subplot(2,3,[4 5]); dk.ui.image( {x,y,sps.sdev}, 'crange', r, ...
            'title', 'Time-std of wavelet magnitude', 'ylabel', 'Frequency (Hz)', 'xlabel', 'Brain region' ); 
        subplot(2,3,6); dk.ui.image( {y,y,corrcoef(sps.sdev')}, 'rmticks', false );
        
    else
        f2 = [];
    end
    
end
