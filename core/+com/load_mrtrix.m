function [out,file] = load_mrtrix( nstream, nregion )
%
% [out,file] = com.load_mrtrix( nstream=10, nregion=68 )
%
%   Load bundle archive from the data/ folder.
%   Output is a struct with fields:
%   
%       sconn           structural connectivity matrices for each subject
%       left/right      meshes and parcellation info for each hemisphere
%       group           groupings of nodes into different lobes + subcortical structures
%       disp            ordering or rows/columns to be used for display
%
%
%   More info about field sconn:
%
%       The connectivity matrices are given as NxM rectangular arrays, where each column
%       corresponds to the lower triangular weights (diagonal excluded) for each subject.
%       
%           rows = lower triangular weights
%           columns = subjects
%
%       E.g. to form the full connectivity matrix of streamline counts for subject k:
%           C = ant.mat.col2sym( out.sconn.count(:,k), true )
%
%
%   More info about field left/right:
%
%       The coordinates of the vertices for the inflated mesh of the left hemisphere are:
%           data.left.coord.inflated
%
%       The faces are common to all meshes in a given hemisphere, and given by:
%           data.left.faces   (rows of indices to 3 vertices)
%
%       The parcellation of the mesh is done by assigning an integer label to vertices:
%           data.left.parc.vlabel
%
%       The list of all label values is given by:
%           data.left.parc.label
%
%       The names corresponding to each label are in:
%           data.left.parc.name
%           data.left.parc.code
%           data.left.parc.long
%
%       Some vertices do not have a label (value 0), this is what the atlas is for:
%           all( data.left.parc.vlabel(~data.left.atlas) == 0 )   is true
%
%       E.g. to display the parcellated left hemisphere:
%           gray = [1,1,1]/2;
%           col = [ gray; jet(data.left.parc.nlabel) ];
%           col = col( 1+data.left.parc.vlabel, : );
%           patch( 'Vertices', data.left.coord.inflated, 'Faces', data.left.faces, ...
%               'FaceVertexCdata', col, 'EdgeColor', 'none', 'FaceColor', 'flat' );
%           grid off; box off; axis equal tight off vis3d; lighting phong;
%
% JH

    if nargin < 2, nregion=68; end
    if nargin < 1, nstream=10; end
    
    file = sprintf('mrtrix_%dM_dk%d.mat',nstream,nregion);
    out = com.load(file);
    
end