function [t,f,p] = show_crosschan( wav, name, fs, varargin )

    figure;
    
    subplot(1,3,[1 2]);
    [t,f,p] = wav.xcplot( name, fs, varargin{:} );
    
    subplot(1,3,3);
    wav.xcdist( p, f );

end