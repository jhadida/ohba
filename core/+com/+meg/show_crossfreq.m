function [t,c,p] = show_crossfreq( wav, name, fs, varargin )

    figure;
    
    subplot(1,3,[1 2]);
    [t,c,p] = wav.xfplot( name, fs, varargin{:} );
    
    subplot(1,3,3);
    wav.xfdist( p, c );

end