function data = load_analysis()
%
% data = com.meg.load_analysis()
%
% Load summary group-analysis of UK MEG Partnership resting-state dataset.
%
% JH

    data = com.load('ukmp_1to45Hz_dk_norm.mat');

end