function [data,file] = load_ukmp(sid)
%
% [data,file] = com.meg.load_ukmp(sid)
%
% Load beamformed parcellated restin-state data from the MEG UK partnership dataset.
% Requires the environment variable JH_OHBA_UKMP to be set correctly (see jh_startup).
%
% JH

    file = fullfile( getenv('JH_OHBA_UKMP'), sprintf('sub%02d',sid), 'bf_1to45hz_dk_norm_8mm.mat' );
    try
        data = load(file);
    catch
        error('Could not load UKMP data; did you set JH_OHBA_UKMP correctly in jh_startup()?')
    end
    
end