classdef Analysis < handle
%
% Wrapper class to:
%   - analyse MEG (or MEG-like) data, 
%   - store results in a compressed format,
%   - and display them.
%
%
% EXAMPLE:
%
%   % we have a time-series object
%   ts = ...
%
%   % Run Analysis
%   %
%   %   Required: freq, bands
%   %   Optional: all other options
%   % 
%   %   order causes reordering of both conmats and spstats (stored internally)
%   %   bands are stored as a cell of vectors internally (even if given as string)
%   %
%   order = com.parc.dk.order('lobes',true);
%   an = com.meg.Analysis( ts, 4:40, 'rest-overlap', 'orth', 'closest', 'order', order );
%
%       % access results
%       an.con
%       an.sps
%       an.pre
% 
%       % options are saved
%       an.band     % cell of 1x2 vectors
%       an.freq     % row-vector
%       an.fc       % cellstr
% 
%       % results can be compressed
%       [con,sps] = an.compress()       % non-mutating
%       an.compress()                   % mutating
%
%   % Display Results
%   %
%   %   figures UserData contains compressed results
%   %
%   [fc,fs] = an.show( 'cor' );     % fc measure required, figure handles returned
%   an.show( 'cor', [1,3,5] );      % select bands to display
%
% See also: com.pipe.analysis, com.meg.band, com.meg.show_fc
%
% JH

    properties
        
        % options
        order
        bands
        freq
        fc
        
        burn
        norm
        orth
        envfs
        npts
        dsl
        
        % results
        con
        sps
        pre
        
    end
    
    properties (Hidden,Dependent)
        popt
        mopt
        wopt
    end
    
    methods
        
        function self = Analysis(varargin)
            if nargin > 0
                self.run(varargin{:});
            end
        end
        
        function p = get.popt(self)
            p = self.props( 'orth', 'norm', 'burn', 'envfs' );
        end
        function p = get.mopt(self)
            p = self.props( 'npts', 'dsl', 'bands', 'fc' );
        end
        function p = get.wopt(self)
            p = self.props( 'npts', 'freq' );
        end
        
        function self = config(self,varargin)
        %
        % Set processing options.
        % bands and freq are required.
        %
            
            arg = dk.obj.kwArgs(varargin{:});
            
            % required params
            self.bands = arg.get( 'bands' );
            self.freq = arg.get( 'freq' );
            
            % optional params
            self.fc = arg.get( 'fc', {'cor'} );
            self.order = arg.get( 'order', [] );
            
            self.burn = arg.get( 'burn', 0 );
            self.norm = arg.get( 'norm', 'maxstd' );
            self.orth = arg.get( 'orth', 'closest' );
            
            self.dsl = arg.get( 'dsl', 3 );
            self.npts = arg.get( 'npts', 5 );
            self.envfs = arg.get( 'envfs', 0 );
            
            % post-proc bands
            if ischar(self.bands)
                self.bands = com.meg.bands( self.bands );
            end
            
        end
        
        function [self,tf] = run(self,ts,freq,bands,varargin)
        %
        % [self,tf] = obj.run( ts, freq, bands, varargin )
        %
        % Run analysis with specified options.
        % Wavelet frequencies and frequency bands are required.
        % TFSpectrum is returned separately because it is big.
        %
        % FORMAT OF: con, sps, pre?
        % 
        
            self.config( 'freq', freq, 'bands', bands, varargin{:} );
            [ana,pre,tf] = com.pipe.analysis( ts, self.popt, self.mopt, self.wopt );
            self.con = ana.con.out;
            self.sps = ana.sps.out;
            self.pre = pre;
            self.reorder();
            
        end
        
        function [con,sps] = compress(self)
        %
        % FORMAT OF: con, sps, pre?
        %
        
            dk.reject( isempty(self.con), 'No data.' );
            
            % early canceling if properties are compressed
            if isscalar(self.con)
                con = self.con;
                sps = self.sps;
                return;
            end
        
            % compress connectivity
            nfc = numel(self.fc);
            con = struct();
            con.band = {self.con.band};
            con.fs   = [self.con.fs];
            
            for i = 1:nfc
                f = self.fc{i};
                con.(f) = dk.mapfun( @(x) ant.mat.sym2col(x,true), {self.con.(f)}, false );
            end
            
            % compress spectral statistics
            sps = struct();
            sps.freq = [self.sps.freq];
            sps.mean = vertcat(self.sps.mean);
            sps.sdev = vertcat(self.sps.sdev);
            sps.skew = vertcat(self.sps.skew);
            
            % apply to self if no output
            if nargout == 0
                self.con = con;
                self.sps = sps;
            end
            
        end
        
        function [Fcon,Fsps] = show(self,fc,idx)
            
            % get compressed data
            [con,sps] = self.compress();
            
            nb = numel(con.band); 
            if nargin < 3, idx = 1:nb; end
            
            % unpack specified FC
            band = con.band(idx);
            cmat = con.(fc);
            cmat = dk.mapfun( @(x) ant.mat.col2sym(x,true), cmat(idx), false );
            
            % unpack spectral stats
            sx = 1:size(sps.mean,2);
            sy = sps.freq;
            sr = dk.num.range( [sps.mean(:); sps.sdev(:)], 'pos' );
            
            % show matrices in separate figures
            Fcon = com.meg.show_fc( cmat, band, true );
            Fcon.UserData = con;
            
            % show spectral stats
            Fsps = dk.fig.new( 'Wavelet magnitude stats', 0.75 );
            Fsps.UserData = sps;
            
            subplot(2,3,[1 2]); dk.ui.image( {sx,sy,sps.mean(:,ord)}, 'crange', sr, ...
                'clabel', 'Average', 'ylabel', 'Frequency (Hz)' );
            subplot(2,3,3); dk.ui.image( {sy,sy,corrcoef(sps.mean')}, 'rmticks', false );

            subplot(2,3,[4 5]); dk.ui.image( {sx,sy,sps.sdev(:,ord)}, 'crange', sr, ...
                'clabel', 'Std-Deviation', 'ylabel', 'Frequency (Hz)', 'xlabel', 'Brain region' ); 
            subplot(2,3,6); dk.ui.image( {sy,sy,corrcoef(sps.sdev')}, 'rmticks', false );
            
        end
        
        function showfc(self,fc,idx)
            
            % get compressed data
            con = self.compress();
            nb = numel(con.band); 
            if nargin < 3, idx = 1:nb; end
            
            % unpack specified FC
            band = con.band(idx);
            name = ant.util.band2name( band, '%.1f-%.1f Hz' );
            cmat = con.(fc);
            cmat = horzcat( cmat{idx} );
            crng = dk.num.range( cmat, 'pos' );
            
            % show matrices in separate figures
            h = gobjects(1,nb);
            for i = 1:nb
                h(i) = dk.fig.new( name{i}, [700,800] );
                c = ant.mat.col2sym( cmat(:,i), true );
                
                dk.ui.image( c, 'crange', crng, 'rmbar', true );
                h(i).UserData = c;
            end
            
        end
        
    end
    
    methods (Hidden, Access=private)
        
        function x = props(self,varargin)
        %
        % x = filtprop( obj, field1, field2, ... )
        %
        % Filter object properties to a struct.
        %
        
            x = struct();
            n = numel(varargin);
            for i = 1:n
                f = varargin{i};
                x.(f) = self.(f);
            end
            
        end
        
        function reorder(self)
            
            % abort if no order was specified
            if isempty(self.order)
                return;
            end
            
            % otherwise reorder matrices and stats
            nc = numel(self.con);
            nf = numel(self.sps);
            nfc = self.fc;
            ord = self.order;
            
            for i = 1:nc
            for j = 1:nfc
                f = self.fc{j};
                M = self.con(i).(f);
                self.con(i).(f) = M( ord, ord );
            end
            end
            
            for i = 1:nf
                self.sps(i).mean = self.sps(i).mean( ord );
                self.sps(i).sdev = self.sps(i).sdev( ord );
                self.sps(i).skew = self.sps(i).skew( ord );
            end
            
        end
        
    end
    
end
