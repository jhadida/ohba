function [ana,pre,tf] = analyse( ts, popt, mopt, wopt )
%
% [ana,pre,tf] = com.meg.analyse( ts, popt, mopt, wopt )
%
% Analyse parcellated MEG-like data.
%
% PARAMETERS
%
% o PRE-PROCESSING
%
%   All parameters in com.pipe.preproc, notably:
%        orth  Orthogonalisation (default: closest)
%        norm  Normalisation (default: maxstd)
%       envfs  Resampling frequency of broadband envelopes (default: 200)
%      + burn  Burn-in time in seconds (default: 0)
%
% o MULTI-BAND ANALYSIS
%
%        npts  Number of points per oscillation, for resampling (default: 5)
%       bands  Frequency bands for connectivity analysis (default: rest-overlap)
%              Bands can be cell of 1x2 vectors, or string (forwarded to com.meg.bands).
%         dsl  Desynchronisation length for coherence-weighted corr (default: 3 cycles).
%        + fc  Cell-string of FC measures to compute in each band (default: {cor,pcoh}).
%
% o WAVELET ANALYSIS
%
%        npts  Number of points per oscillation, for resampling (default: 5)
%        freq  Wavelet frequencies (default: 4:40 Hz)
%      + fold  Run wavelet analysis on a large sliding window (less memory).
%              Should be a 1x3 vector [len,step,burn] in seconds.
%
% o BOTH ANALYSES
%
%   Analysis-specific parameters:
%        nosc  Number of oscillations per window (default: -)
%          fs  Override npts to manually force sampling rates in each band
%        swin  Override nosc to specify a fixed sliding window across bands (in seconds).
%
% See also: com.pipe.analysis
%
% JH

    if nargin < 4, wopt=struct(); end
    if nargin < 3, mopt=struct(); end
    if nargin < 2, popt=struct(); end

    % process options
    popt = dk.obj.kwArgs( 'orth', 'closest', 'norm', 'maxstd', 'envfs', 200 ).merge(popt);
    mopt = dk.obj.kwArgs( 'npts', 5, 'dsl', 3, 'bands', 'rest-overlap', 'fc', {'cor','pcoh'} ).merge(mopt);
    wopt = dk.obj.kwArgs( 'npts', 5, 'freq', 4:40 ).merge(wopt);
    
    mopt.sanitise( 'bands', @resolve_bands ); 
    
    % analysis
    [ana,pre,tf] = com.pipe.analysis( ts, popt, mopt, wopt );
    
end

function bands = resolve_bands( bands )
    if ischar(bands)
        bands = com.meg.bands(bands);
    end
end
