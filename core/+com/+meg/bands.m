function b = bands( label )
%
% b = com.meg.bands( label )
%
% LABEL in:
%
%  o all, eeg
%       delta, theta, alpha, beta, gamma
%  o rest
%       theta, alpha, beta
%  o wrest
%       delta, theta, alpha, beta
%  o rest-overlap
%       { [4 8], [6 10], [8 13], [10 20], [13 30], [20 40] }
%  o wrest-overlap
%       { [1 4], [3 6], [4 8], [6 10], [8 13], [10 20], [13 30], [20 40] }
%   
% OUTPUT:
%   b is a cell array with a 1x2 vector [low,high] for each frequency band.
%
% See also: ant.util.eeg_bands
%
% JH

    switch lower(label)
        
        case {'all','eeg'}
            b = ant.util.eeg_bands( 'delta', 'theta', 'alpha', 'beta', 'gamma' );
            
        case 'rest'
            b = ant.util.eeg_bands( 'theta', 'alpha', 'beta' );

        case 'wrest'
            b = ant.util.eeg_bands( 'delta', 'theta', 'alpha', 'beta' );
            
        case {'rest-overlap','paper'}
            b = { [4 8], [6 10], [8 13], [10 20], [13 30], [20 40] };

        case 'wrest-overlap'
            b = { [1 4], [3 6], [4 8], [6 10], [8 13], [10 20], [13 30], [20 40] };
            
        case 'rest-old'
            b = ant.mat.pairelm([4 6 8 10 13 20 20 40],2);
            
        otherwise
            error('Unknown label: %s', label);
    end

    % extract values only
    if isstruct(b)
        b = dk.struct.values(b);
    end

end
