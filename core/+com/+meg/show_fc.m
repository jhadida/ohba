function fig = show_fc( con, bands, samerng, trans, varargin )
%
% fig = show_fc( fc, bands=[], samerg=[], trans=[], varargin )
%
% Flexible method to show functional connectivity from a variety of inputs, 
% with optional transformations before display.
%
% INPUTS
%
%         fc   Either a 3D matrix, or a cell array, or a struct-array.
%              3D matrix:
%                   One FC matrix per slice.
%              Cell-array:
%                   One FC measurement per cell (can be sliding or symvectorised).
%              Struct-array:
%                   FC measurement should be stored in one of the fields: {fc,mat}
%
%      bands   Either a cell-string, or empty, or a string.
%              Cell-string:
%                   Titles associated with FC matrices.
%              Empty or omitted:
%                   We try to extract band information 
%                     from fields fc:{band,freq,name}, and issue a warning if 
%                     nothing is found.
%                   o A string, in which case we try to extract band information
%                     from fc (as previously), and bands is used instead to specify
%                     the connectivity field in fc (eg 'cor').
%
%    samerng   Display all matrices with the same color range.
%
%      trans   Transformation to be applied to each FC measurement prior to display.
%              This is useful if the measurements are sliding and one wants to show 
%              the average FC across windows for instance.
%   
%   varargin   Additional inputs are forwarded to dk.ui.image.
%
% OUTPUT
%   fig is the figure handle
%
% See also: ant.img.grid, ant.util.band2name
%
% JH

    if nargin < 2, bands = []; end
    if nargin < 3, samerng = []; end
    if nargin < 4, trans = []; end
    
    % if input is a struct, try to extract
    if isstruct(con)
        
        scalar = numel(con);
        
        if ischar(bands)
            if scalar
                fc = con.(bands);
            else
                fc = { con.(bands) };
            end
            bands = [];
        else
            field = intersect( fieldnames(con), {'fc','mat'}, 'stable' );
            fc = { con.(field{1}) };
        end
        
        if isempty(bands)
            field = intersect( fieldnames(con), {'band','freq','name'}, 'stable' );
            if isempty(field)
                warning( 'Unable to find field corresponding to band info.' );
                bands = [];
            else
                if scalar
                    bands = con.(field{1});
                else
                    bands = {con.(field{1})};
                end
            end
        end
        
    elseif isnumeric(con)
        
        % force fc to be a cell
        if ismatrix(con)
            con = ant.mat.col2sym(con,true);
        end
        assert( ndims(con) <= 3, 'Numeric inputs should be 3D.' );
        fc = reshape( num2cell(con, [1 2]), 1, [] );
        
    else
        fc = con;
    end
    assert( iscell(fc), 'Connectivity input should be a cell.' );
    
    % process band names
    if ~iscellstr(bands)
        bands = ant.util.band2name(bands,'%.0f - %.0f Hz');
    end
    
    % apply transformation, if any
    if isempty(trans) && iscolumn(fc{1})
        trans = @(x) ant.mat.col2sym(x,true); % interpret columns as compressed symmetric matrices
    end
    if ~isempty(trans)
        fc = dk.mapfun( trans, fc, false );
    end
    
    % show FC
    fig = dk.fig.new( 'Band-specific functional connectivity', 0.8 ); 
    ant.img.grid( fc, bands, samerng, varargin{:} );
    
end
