function out = show_summary( env, wav, varargin )
%
% out = com.meg.show_summary( env, wav, varargin )
%
% OPTIONS
%
%   prop  Spectral property to be displayed (default: magnitude)
%   cmap  Colormap (default: bgr)
%     fs  Sampling rate for comparisong across frequencies (default: 50)
%
% JH

    opt = dk.obj.kwArgs(varargin{:});
    
    opt_cmap = opt.get( 'cmap', 'bgr' );
    opt_prop = opt.get( 'prop', 'magnitude' );
    opt_fs   = opt.get( 'fs', 50 );
    
    fig = figure;
    
    % First row: broadband envelope
    
        % broadband envelopes
        subplot(3,3,[1 2]);
        env.plot_image( ...
            'xlabel', 'Time (sec)', 'ylabel', 'Signal #', 'title', 'Broadband envelope', ...
            'positive', true, 'cmap', opt_cmap );

        % broadband fcon
        %subplot(3,3,3);
        %out.fc = env.fcon( 'mcorr', 'title', 'Broadband FC', 'cmap', opt_cmap );
    
    
    % Second row: power across frequency
    o = 3;
    
        subplot(3,3,o+[1 2]); 
        [t,c,p] = wav.xfplot( opt_prop, opt_fs, opt );
        title(sprintf( '%s averaged across frequencies', opt_prop ));
        out.xf = struct( 't', t, 'c', c, 'p', p );
        
        subplot(3,3,o+3); 
        wav.xfdist( p, c );
        
    
    % Third row: power across channels
    o = 6;
        
        subplot(3,3,o+[1 2]); 
        [t,f,p] = wav.xcplot( opt_prop, opt_fs, opt );
        title(sprintf( '%s averaged across channels', opt_prop ));
        out.xc = struct( 't', t, 'f', f, 'p', p );
        
        subplot(3,3,o+3); 
        wav.xcdist( p, f );
        
        % show spectral magnitude pattern instead
        subplot(3,3,3);
        out.smp = ant.mat.setdiag(corrcoef(p),0);
        dk.ui.image( out.smp, 'title', 'Spectral Magnitude Pattern' );
        
    % Save analysis data
    fig.UserData = out;
    dk.fig.resize( fig, [800 1200] );
        
end
