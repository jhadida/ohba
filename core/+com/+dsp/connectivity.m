function out = connectivity( tfs, fcnames, varargin )
%
% out = com.dsp.connectivity( tfs, fcnames, varargin )
%
%   Compute functional connectivity on input TFSeries object(s).
%   Accepts either scalar TFSeries, or cell thereof.
%
%   fcnames is a string or cellstr with connectivity methods:
%       corr, cov, pcorr, mcorr, coherency, pcoh, icoh, rcoh
%
%
% OPTIONS
% -------
%
%   nosc    DEFAULT: 0
%           If positive, run connectivity analysis on an adaptive sliding-window.
%           The size of the window is set to fit the specified number of oscillations
%           given the centre frequency of the signal considered.
%
%   swin    DEFAULT: []
%           Run connectivity analysis on sliding-window (cf ant.priv.win_parse).
%           This option overrides 'nosc'.
%
%   edge    DEFAULT: []
%           If non-empty, should be a Nx2 array with node-indices in each row.
%           The connectivity analysis is then run only for the specified pairs of 
%           nodes. This option is compatible with sliding-window options.
%
%   norm    DEFAULT: []
%           Coherence weighted normaliser; typically the std of broadband envelope.
%
%
% OUTPUT
% ------
%
%   With N input TFSeries objects, output is a 1xN struct-array with fields:
%       freq    centre frequency of the TFSeries object
%         fs    sampling frequency of the corresponding series
%       +...    one field for each connectivity method in 'fcnames'
%
%   For the static version, the connectivity fields are square matrices, unless
%   the edge option is specified, in which case they are column vectors.
%
%   For the sliding versions (fixed and adaptive), the connectivity fields are 
%   nested structures with fields:
%       time    centre timepoint for each window
%       mats    NxT matrix with vectorised connectivity matrices in each column, 
%               or edge-connectivities if specified
%
%
% NOTES
% -----
%
%   1. For the sliding versions, the NxT matrices returned for each connectivity
%      methods can be converted to a 3D "stack" of usual (square symmetric) matrices
%      using: ant.mat.col2sym( M, true ).
%
%   2. Currently, the coherence-weighted connectivity cannot be computed for 
%      specific edges. If the 'edge' option is specified, then the output is a 
%      small square matrix for the unique subset of nodes in the given edges. 
%      For example, if you specify: 
%           com.dsp.connectivity( ... 'edge', [1,2; 3,4] )
%      then the output is a 4x4 matrix in the static case (vectorised in the sliding 
%      case), whereas it would be a 2x1 vector with other connectivity methods.
%
%
% See also: com.conn.fconz, ant.mat.sym2col
%
% JH

    % default options
    opt = dk.obj.kwArgs(varargin{:});
    nosc = opt.get( 'nosc', 0 );
    swin = opt.get( 'swin', [] );
    edge = opt.get( 'edge', [] );
    
    % wrap fc names
    fcnames = dk.wrap(fcnames);
    assert( iscellstr(fcnames), 'FC name(s) should be string or cell-string.' );
    
    % wrap input time-freq object(s)
    if isa(tfs,'ant.dsp.TFSpectrum')
        tfs = tfs.sig;
    else
        tfs = dk.wrap(tfs);
    end
    assert( all(cellfun( @(x) isa(x,'ant.dsp.TFSeries'), tfs )), 'Bad input time-frequency object(s).' );
    
    % prepare output
    ntf = numel(tfs);
    nfc = numel(fcnames);
    out = dk.struct.repeat( [{'freq','fs'},fcnames], 1, ntf );
    
    % iterate over TF objects
    for i = 1:ntf
        
        signal = tfs{i};

        out(i).freq = signal.freq;
        out(i).fs   = signal.fs;

        % iterate over FC measures
        for j = 1:nfc
        
            % name of current measure
            fcname = fcnames{j};
            
            % fixed sliding window version
            if ~isempty(swin)
                [fc,t] = sliding_connectivity( signal, fcname, edge, swin ); 
                out(i).(fcname) = struct('time',t,'mats',fc);

            % adaptive sliding window version
            elseif nosc > 0
                [fc,t] = adaptive_connectivity( signal, fcname, edge, nosc ); 
                out(i).(fcname) = struct('time',t,'mats',fc);

            % static version
            else
                out(i).(fcname) = static_connectivity( signal, fcname, edge );
            end
        end
    end

end

%--------------------------------------------------------------------------------
%--------------------------------------------------------------------------------

function fc = static_connectivity( sig, name, edge, tmask, ascol )

    if nargin < 5, ascol = false; end
    if nargin < 4, tmask = 1:sig.nt; end
    if nargin < 3, edge= []; end
    
    % lower triangular mask
    persistent lowerT;
    if ~dk.is.matrix(lowerT,sig.ns)
        lowerT = tril(true(sig.ns),-1);
    end
    
    if isempty(edge)
    
        fc = com.conn.fconz( sig.vals(tmask,:), name, sig.pnorm );
        if ascol, fc = fc(lowerT); end
        
    else
        
        ne = size(edge,1);
        fc = zeros(ne,1);
        for i = 1:ne
            fc(i) = com.conn.xfconz( sig.vals(tmask,edge(i,1)), sig.vals(tmask,edge(i,2)), name, self.pnorm );
        end
        
    end
    
end

function [fc,t] = sliding_connectivity( sig, name, edge, swin )

    [fc,t] = ant.dsp.slidingfun( @(~,ti,te) static_connectivity(sig,name,edge,ti:te,true), sig, swin );
    fc = horzcat(fc{:}); % Nsig*(Nsig-1) x Nwin
    
end

function [fc,t] = adaptive_connectivity( sig, name, edge, nosc, varargin )

    awin = ant.priv.win_adaptive( sig.cenfrq, nosc, varargin{:} );
    [fc,t] = sliding_connectivity( sig, name, edge, awin );

end
