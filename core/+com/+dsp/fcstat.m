function data = fcstat( tfs, fcname, varargin )
%
% DRAFT VERSION
%
%
% data = com.dsp.fcstat( tfs, fcname, nosc, burn=0, ovr=0.5 )
%
% Summary given PSD and adaptive connectivity stats.
% Output is a structure with fields:
%   freq  copied from self.freq
%   swin  processed from adaptive parameters
%    psd  PSD computed on a sliding window
%     fc  mean, sdev, skew of FC matrices across windows
%         also contains the FC matrix computed on the full timecourse
%
% JH

    error('Not implemented');

    if isa(tfs,'ant.dsp.TFSpectrum')
        tfs = tfs.sig;
    else
        tfs = dk.wrap(tfs);
    end
    assert( all(cellfun( @(x) isa(x,'ant.dsp.TFSeries'), tfs )), 'Expected time-frequency object(s).' );
    assert( ischar(fcname), 'FC name should be a string.' );

    % initialise output with full connectivity matrices
    full = com.dsp.connectivity( tfs, fcname );
    
    % output
    nf = numel(tfs);
    out = dk.struct.repeat( {'freq','swin',fcname}, 1, nf );
    to_matrix = @(x)ant.mat.col2sym(x,true);
    
    for i = 1:nf
        signal = tfs{i};
        
        swin = ant.priv.win_adaptive( signal.cenfrq, varargin{:} );
        
        out(i).freq = signal.freq;
        out(i).fs   = signal.fs;
        out(i).full = com.dsp.connectivity
        
        [fc,t,s] = self.sig{i}.adaptive_connectivity( method, varargin{:} );
        
    end
    

    % sliding average PSD 
    [p.vals,p.time] = tfs.sliding_psd([],swin);

    % stats across adaptive sliding window
    fc = ant.stat.summary( com.dsp.connectivity( tfs, fcname, 'swin', swin ), 2 );
    fc.mean = to_matrix(fc.mean);
    fc.sdev = to_matrix(fc.sdev);
    fc.skew = to_matrix(fc.skew);
    fc.full = com.dsp.connectivity( tfs.mask(tmask), fcname );

    % wrap up
    data.freq = tfs.freq;
    data.swin = swin;
    data.psd  = p;
    data.fc   = fc;

end

% % Functional connectivity stats
% function [stat,tc] = connectivity_stats( self, method, varargin )
% 
%     n = self.nf;
%     stat = cell(1,n);
%     tc = cell(1,n);
%     to_matrix = @(x) ant.mat.col2sym(x,true);
% 
%     for i = 1:n
%         [fc,t,s] = self.sig{i}.adaptive_connectivity( method, varargin{:} );
% 
%         % average connectivity across edges for each window
%         x.time = t;
%         x.vals = mean(fc,1)';
%         tc{i} = x;
% 
%         % average connectivity across windows for each edge
%         fc = ant.stat.summary( fc, 2 );
%         fc.swin = s;
%         fc.freq = self.sig{i}.freq;
%         fc.mean = to_matrix(fc.mean);
%         fc.sdev = to_matrix(fc.sdev);
%         fc.skew = to_matrix(fc.skew);
% 
%         stat{i} = fc;
%     end
% 
%     % convert to struct-array
%     stat = [stat{:}];
%     tc = [tc{:}];
% 
% end
% 
% function data = summary( self, varargin )
%     data = dk.mapfun( @(x) x.summary(varargin{:}), self.sig, false );
%     data = [data{:}];
% end
