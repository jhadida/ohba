function ts_out = leakage_correction( ts_in, method )
%
% ts_out = leakage_correction( ts_in, method )
%
% Orthogonalise input signals using a method in com.math.orth.*
%
% JH

    ts_out = ts_in.clone();
    try
        ts_out.vals = com.math.orth.( method )(dk.bsx.sub( ts_out.vals, ts_out.mean ));
        ts_out.demean();
    catch 
        warning('Could not perform orthogonalisation.');
    end
    
end
