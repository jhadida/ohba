function out = spstat( tfs, varargin )
%
% out = com.dsp.spstat( tfs, Name, Value ... )
%
% Spectral statistics on input analytic signal(s).
% Mean and std of specified property are computed for each signal.
% Input time-series are analysed using the Wavelet pipeline.
%
% INPUTS:
%
%       tfs : Raw time-series, time-frequency series, or analytic signal
%
% OPTIONS (see com.pipe.Wavelet):
%
%      prop : Spectral property on which to compute stats (default: amplitude)
%
%      burn : Remove initial timepoints (optional)
%  resample : Downsampling frequency (preprocessing, optional)
%      orth : Orthogonalisation method (preprocessing, optional)
%      freq : Vector of wavelet frequncies
%      npts : Number of points per oscillation (for resampling)
%      nosc : Number of oscillations per window (triggers sliding version)
%
%        fs : If set, overrides npts to manually force sampling rates in each band.
%      swin : Override nosc to specify a fixed sliding window across bands (in SEC).
%
% OUTPUTS:
%
%    out : Struct-array (one struct for each frequency) with fields:
%           freq  scalar representing the wavelet frequency 
%             fs  Sampling frequency for this wavelet spectra
%      mean,sdev  Static case : 1xN vector with corresp. temporal statistic 
%                 Sliding case: struct with fields {time,vals}
%                               where vals is a TxN matrix with corresp. statistic in each row
%
% See also: com.pipe.Wavelet
%
% JH

    % parse options
    opt = dk.obj.kwArgs( 'verb', false ).merge( varargin{:} );
        
    % short-circuit analysis if pre-processing is provided
    if isa(tfs,'ant.TimeSeries')
        wlp = com.pipe.Wavelet(opt);
        tfs = wlp.process(tfs);
    end
    if isa(tfs,'ant.dsp.TFSpectrum')
        tfs = tfs.sig;
    else
        tfs = dk.wrap(tfs);
    end
    assert( all(cellfun( @(x) isa(x,'ant.dsp.TFSeries'), tfs )), 'Expected time-frequency object(s).' );
    
    % output
    nf = numel(tfs);
    out = dk.struct.repeat( {'freq','fs','mean','sdev'}, 1, nf );
    nosc = opt.get('nosc',0);
    swin = opt.get('swin',[]);
    prop = opt.get('prop','amplitude');
    
    for i = 1:nf
        signal = tfs{i};
        
        out(i).freq = signal.freq;
        out(i).fs   = signal.fs;
       
        % select sliding or static version
        statfun = @(x) ant.stat.summary(x,1);
        unwrap  = @(x,name) vertcat( x.(name) );
        if ~isempty(swin)
            [stat,t] = signal.sliding_prop( prop, statfun, swin ); 
            
            out(i).mean = struct('time',t,'vals',unwrap(stat,'mean'));
            out(i).sdev = struct('time',t,'vals',unwrap(stat,'sdev'));
            out(i).skew = struct('time',t,'vals',unwrap(stat,'skew'));
        elseif nosc > 0
            [stat,t] = signal.adaptive_prop( prop, statfun, nosc );
            
            out(i).mean = struct('time',t,'vals',unwrap(stat,'mean'));
            out(i).sdev = struct('time',t,'vals',unwrap(stat,'sdev'));
            out(i).skew = struct('time',t,'vals',unwrap(stat,'skew'));
        else
            stat = statfun(signal.(prop));
            
            out(i).mean = stat.mean;
            out(i).sdev = stat.sdev;
            out(i).skew = stat.skew;
        end
        
    end

end
