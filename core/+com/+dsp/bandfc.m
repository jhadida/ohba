function out = bandfc( in, fcnames, varargin )
%
% out = com.dsp.bandfc( in, fcnames, Name, Value ... )
%
%   Compute band-specific functional connectivity on input time-series.
%
%
% INPUTS
% ------
%
%        in : Raw time-series to process, or result of pipeline processing
%   fcnames : Cell-string with names of different FC meeasures to compute
%
%
% OPTIONS 
% -------
%
%     bands : REQUIRED option with the frequency bands in which to compute
%             connectivity matrices (cell of 1x2 vectors)
%
%    reduce : DEFAULT: @dk.forward
%             Function handle to "reduce" the connectivity results computed in each 
%             band. Input is a cell of matrices (static) or structs (sliding).
%
% For additional options, see:
%   - com.dsp.connectivity (connectivity options);
%   - com.pipe.MultiBand (pipeline options, only for time-series inputs).
%
%
% OUTPUT
% ------
%
%       out : Struct-array (one struct for each matched band) with fields
%                 band  1x2 vector representing the frequency band
%                 name  string representing the band for plotting
%                 <fc>   Static case: NxN matrix
%                       Sliding case: struct-array with fields {time,mats}
%                                     where mats is N*(N-1)/2 x T
%
%
% See also: com.dsp.connectivity, com.pipe.preproc, com.pipe.MultiBand, com.dsp.TFSpectrum
%
% JH

    opt = dk.obj.kwArgs( 'verb', false ).merge( varargin{:} );
    
    
    % force FC names to a cell
    fcnames = dk.wrap(fcnames);
    assert( iscellstr(fcnames), 'Second input should be a cell-string.' );
    
    % reduce function
    reduce = opt.get('reduce', @dk.forward);
    
    % get frequency bands
    bands = opt.get('bands');
    if ischar(bands)
        bands = com.meg.bands(bands);
    end
    bands = dk.wrap(bands);
    
    isband = @(x) numel(x)==2 && x(1)<x(2);
    assert( all(cellfun( isband, bands )), 'Bad input bands.' );
    nb = numel(bands);
    
    
    % get processing results
    if isa(in,'ant.TimeSeries')
        mb = com.pipe.MultiBand(opt);
        proc = mb.process(in);
    else
        assert( dk.is.struct(in,{'tf','env'}), 'Input should be the result of pipeline processing.' );
        proc = in;
    end
    

    % for each band, find subset of signals with matching frequency, and 
    % compute connectivity between them
    out = cell(1,nb);
    for i = 1:nb
        b = bands{i};
        k = find(proc.tf.band_match(b));
        if isempty(k)
            warning( 'No match for band: %.0f-%.0f', b(1), b(2) );
        else
            dk.debug( 'Found %d matches for band: %.0f-%.0f', numel(k), b(1), b(2) );
            con = com.dsp.connectivity( proc.tf.sig(k), fcnames, opt );
            out{i} = mergeconn( con, b, fcnames, reduce );
        end
    end
    out = [out{:}];
    
end

function out = mergeconn(con,band,fc,reduce)
%
% Average connectivity matrices at indices idx of struct-array con.
% Output is a struct with fields:
%   band
%   name
%   ...     (connectivity methods)
%

    % initialise output
    out.band = band;
    out.name = sprintf( '%.0f-%.0f Hz', band(1), band(2) );
    
    % iterate on each connectivity field
    nc = numel(fc);
    for i = 1:nc
        f = fc{i};
        c = { con.(f) };
        out.(f) = reduce(c);
    end
    
end
