function [ts_out,irf] = hemodyn_response( ts_in, len, step, burn )
% 
% [ts_out,irf] = com.dsp.hemodyn_response( ts_in, len=25, step=200e-3, burn=0 )
%
% Convolve input signal with impulse response function (using double-gamma function).
% 
% JH

    if nargin < 2, len  = 25; end
    if nargin < 3, step = 200e-3; end
    if nargin < 4, burn = 0; end

    % impulse response function
    dt  = ts_in.dt;
    irf = double_gamma();
    irf = irf( 0:dt:len );
    
    % convert to number of steps
    [wsize,wstep,wburn] = ant.priv.win_time2steps( ts_in, len, step, burn );
    assert( numel(irf) == wsize, 'Size problem with the convolution frame.' );
    
    ts_out = ant.TimeSeries( ...
        ant.priv.win_times( ts_in, [wsize, wstep, wburn], false ), ...
        ant.mex.convolution( ts_in.vals, irf, wstep, wburn ) ...
    );

end

function h = double_gamma( t1, s1, t2, s2, alpha )
% Compute the double-Gamma Impulse Response Function with:
% - p1, s1 the peak occurence and full-width at half-maximum of first Gamma function
% - p2, s2 idem for second peak
% The final function is G1 - d*G2.
%
% The output is a function handle accepting vector-valued time inputs.
% We assume that the impulse occurs at t=0, therefore any negative input is mapped to a null response.

	% Glover 1999 parameters
	if nargin<1, t1 = 5.4; end
	if nargin<2, s1 = 5.2; end
	if nargin<3, t2 = 10.8; end
	if nargin<4, s2 = 7.35; end
	if nargin<5, alpha  = 0.35; end
	
	h = @irf;
	
	function y = irf(t)
		
		t(t<0) = 0;
		
		b1 = s1*s1/(t1*8*log(2));
		a1 = t1/b1;
		
		b2 = s2*s2/(t2*8*log(2));
		a2 = t2/b2;
		
		y  = (t/t1).^a1 .* exp( -(t-t1)/b1 )  -  alpha*(t/t2).^a2 .* exp( -(t-t2)/b2 );
		
	end

end

