function out = varstats(x)
%
% Summarise the contents of array variable x.
%

    s = size(x);
    num = numel(x);
    nz = nnz(x);
    ni = nnz(isinf(x));
    nn = nnz(isnan(x));
    sp = 100*round(1 - nz/num,2);
    
    vmin = min(x(:));
    vmax = max(x(:));
    vmean = mean(x(:));
    vsdev = std(x(:));
    
    q = prctile( x(:), [1,10,50,90,99] );
    
    out.numel = num;
    out.sparsity = sp;
    
    out.size = s;
    out.nnz  = nz;
    out.ninf = ni;
    out.nnan = nn;
    out.mean = vmean;
    out.std  = vsdev;
    out.min  = vmin;
    out.p01  = q(1);
    out.p10  = q(2);
    out.med  = q(3);
    out.p90  = q(4);
    out.p99  = q(5);
    out.max  = vmax;

    if nargout == 0
        dk.print( 'size : %s', dk.util.vec2str(s) );
        dk.print( 'num  : %d', num );
        dk.print( 'nnz  : %d (%d %% sparse)', nz, sp );
        dk.print( 'ninf : %d', ni );
        dk.print( 'nnan : %d', nn );
        dk.print( 'mean : %g', vmean );
        dk.print( 'std  : %g', vsdev );
        dk.print( 'min  : %g', vmin );
        dk.print( 'p01  : %g', q(1) );
        dk.print( 'p10  : %g', q(2) );
        dk.print( 'med  : %g', q(3) );
        dk.print( 'p90  : %g', q(4) );
        dk.print( 'p99  : %g', q(5) );
        dk.print( 'max  : %g', vmax );
    end
    
end
