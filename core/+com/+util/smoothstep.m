function s = smoothstep( k )
%
% s = com.util.smoothstep( k )
%
% Print Horner representation of Smoothstep function of order k and return a handle to it.
%
% Source:
% https://en.wikipedia.org/wiki/Smoothstep#Generalization_of_higher-order_equations
%
% JH
    
    syms x;
    
    coef = zeros(1,k);
    ind  = 0:k-1;
    for i = ind
        coef(i+1) = binomial(-k,i) * binomial(2*k-1,k-i-1);
    end
    
    s = dot( coef, x.^(k+ind) );
    disp(s);
    disp(horner(s));
    s = matlabFunction(s);

end

function b = binomial( n, k )
%
% https://arxiv.org/pdf/1105.3689.pdf

    if (n >= 0) && (k >= 0)
        b = nchoosek(n,k);
    elseif (k >= 0)
        b = (-1)^k * nchoosek( k-1-n, k );
    elseif (k <= n)
        b = (-1)^(n-k) * nchoosek( -k-1, n-k );
    else
        b = 0;
    end

end