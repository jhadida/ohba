function out = imresize3( in, out_size, method )
%
% out = com.util.imresize3( in, out_size, method )
%
% Resize a 3d volume using interpolation.
%
% Note: Matlab 2017a introduced imresize3 in the image analysis toolbox.
%
% JH

    if nargin < 3, method = 'linear'; end
    
    assert( ndims(in)==3 && isnumeric(in), 'Input volume should be a numeric 3D array.' );
    assert( numel(out_size)==3 && all(out_size>=1), 'Sizes should be positive.' );
    
    % input/output dimensions
    in_size  = size(in);
    out_size = round(out_size);
    
    % corresponding grids
    [outI,outJ,outK] = ndgrid( ...
        linspace(1,in_size(1),out_size(1)), ...
        linspace(1,in_size(2),out_size(2)), ...
        linspace(1,in_size(3),out_size(3)) );
    
    % resample image
    out = interp3( in, outJ, outI, outK, method );
    
end