function [h,x,y] = compact_sigmoid( npts )

    x = linspace( -1, 1, npts );
    x = x(2:end-1);
    y = [0,exp( 1./(x.^2-1) )];
    y = [cumsum(y)/sum(y),1];
    x = linspace( 0, 1, npts );
    
    p = pchip(x,y);
    h = @(t) ppval(p,t);
    
end
