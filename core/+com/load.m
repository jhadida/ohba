function varargout = load(fname,varargin)
%
% Facade to dk.load using prefix to data/ folder
%

    varargout = cell(1,nargout);
    [varargout{:}] = dk.load( jh_path('data',fname), varargin{:} );
end