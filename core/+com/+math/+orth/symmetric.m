function L = symmetric(X)
%SYMMETRIC_ORTHOGONALISE closest orthogonal matrix
% 
% L = SYMMETRIC_ORTHOGONALISE(A) returns orthonormal matrix L which
%   is closest to A, as measured by the Frobenius norm of (L-A). 
%
%   The orthogonal matrix is constructed from a singular value decomposition
%   of A. 
%
% L = SYMMETRIC_ORTHOGONALISE(A, KEEP_MAGNITUDES) returns the orthogonal
%   matrix L, whose columns have the same magnitude as the respective
%   columns of A, and which is closest to A, as measured by the Frobenius
%   norm of (L-A), if KEEP_MAGNITUDES is TRUE. 
%
%   The orthogonal matrix is constructed from a singular value decomposition
%   of A. 

% References: Naidu, A. R. "Centrality of Lowdin Orthogonalizations",
%   arXiv 1105.3571v1, May 2011. 
%   Available at: http://arxiv-web3.library.cornell.edu/pdf/1105.3571v1.pdf

%	$LastChangedBy: giles.colclough@gmail.com $
%	$Revision: 239 $
%	$LastChangedDate: 2014-08-15 14:58:49 +0100 (Fri, 15 Aug 2014) $
%	Contact: giles.colclough 'at' magd.ox.ac.uk
%	Originally written on: GLNXA64 by Giles Colclough, 31-Oct-2013 13:30:05

    [U,S,V] = svd(X,'econ');
        
    % we need to check that we have sufficient rank
    S   = diag(S);
    tol = max(size(X)) * S(1) * eps(class(X));
    r   = sum(S > tol);

    if r >= size(X,2)
        L = U * ctranspose(V); % polar factors of A
    else
        % not enough degrees of freedom
        error([mfilename ':RankError'], ...
              ['The input matrix is not full rank. \n', ...
               '    There are not enough degrees of freedom to perform a sensible orthogonalisation. \n', ...
               '    Try a different orthogonalisation method. \n']);
    end

end
