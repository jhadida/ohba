function [O,Q,R] = householder( X )
%HOUSEHOLDER_ORTHOGONALISE	orthogonalisation using householder method
% O = HOUSEHOLDER_ORTHOGONALISE(A) Computes orthogonal set of vectors O 
%    from input A. Vectors must form columns of A. 
%
% [O, Q, R] = HOUSEHOLDER_ORTHOGONALISE(A) Computes orthonormal vectors
%    in Q and R such that A = Q*R. 

%	$LastChangedBy: giles.colclough@gmail.com $
%	$Revision: 239 $
%	$LastChangedDate: 2014-08-15 14:58:49 +0100 (Fri, 15 Aug 2014) $
%	Contact: giles.colclough@eng.ox.ac.uk
%	Originally written on: GLNXA64 by Giles Colclough, 06-Nov-2013 13:35:17

    [Q,R] = qr( X, 0 );
    
    if issparse(Q)
        O = Q * diag(diag(R));
    else
        O = ant.mat.scale_cols( Q, diag(R) );
    end
end
