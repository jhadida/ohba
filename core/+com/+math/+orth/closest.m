function [L,D,rho] = closest(X,tol)
%CLOSEST_ORTHOGONAL_MATRIX  Computes closest orthogonal matrix
%
% L = CLOSEST_ORTHOGONAL_MATRIX(A) returns orthogonal matrix L which
%   is closest to A, as measured by the Frobenius norm of (L-A), such that
%   transpose(L) * L is non-negative diagonal. 
%
% [L, D, RHO] = CLOSEST_ORTHOGONAL_MATRIX(A) also returns the scaling
%   factors for each column of A, D, and the final converged square
%   distance between L and A, RHO. 

%	References:
%	R. Evans,
%	"http://empslocal.ex.ac.uk/people/staff/reverson/uploads/Site/procrustes.pdf"
%   "Orthogonal, but not Orthonormal, Procrustes Problems", 1997

%	$LastChangedBy$
%	$Revision$
%	$LastChangedDate$
%	Contact: giles.colclough@eng.ox.ac.uk
%	Originally written on: MACI64 by Giles Colclough, 07-Aug-2014 13:27:52

    MAX_ITER = 50;
    if nargin < 2
        %tol = max(1, max(size(X)) * com.math.svds(X,1)) * eps(class(X)); % same as Matlab
        tol = 1e-6; % relative difference
    end
    
    % tests for convergence
    reldiff     = @(a,b) 2*abs(a-b) / (abs(a)+abs(b));
    absdiff     = @(a,b) abs(a-b);
    convergence = @(cur,prev) reldiff(cur,prev) <= tol;

    % declare memory and initialise
    iter = 0;
    X_b  = conj(X);
    D    = sqrt(dot( X, X_b, 1 ));
    %D    = ones( 1, size(X,2) );
    rho  = nan(MAX_ITER,1);

    while iter < MAX_ITER
        iter = iter + 1;

        % find orthonormal polar factor
        V = com.math.orth.symmetric(ant.mat.scale_cols(X,D));

        % minimise rho = |X - V D|^2 w.r.t D
        D = dot( X_b, V, 1 );

        % new best orthogonal estimate
        L = ant.mat.scale_cols(V,D);

        % compute error term
        E = X - L;
        rho(iter) = sqrt(sum(dot( E, conj(E), 1 )));

        if iter > 1 && convergence(rho(iter), rho(iter - 1))
            break
        end
    end

    % tidy vector
    rho = rho(1:iter);

    if isequal(iter, MAX_ITER)
        warning([mfilename ':MaxIterationsHit'],                              ...
                ['%s: hit max iterations: %d. ',                              ...
                 'You may not have found the optimal orthogonal matrix. \n'], ...
                 mfilename, MAX_ITER);
        figure('name','[com.math.orth.closest] Debug');
            plot(rho); xlabel('iteration'); ylabel('convergence ratio');
    end

end
