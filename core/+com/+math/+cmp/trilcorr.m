function s = trilcorr( A, B )
%
% s = com.math.cmp.trilcorr( A, B )
%
% Correlation between lower triangular values of two matrices.
%
% JH

    assert( ismatrix(A) && ismatrix(B), 'Inputs must be matrices.' );

    T = tril( true(size(A)), -1 );
    s = com.math.cmp.corr( A(T), B(T) );

end
