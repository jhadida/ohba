function s = corr( a, b, dim )
%
% s = com.math.cmp.corr( a, b, dim=1 )
%
% Normalised cross-correlation (aka Pearson correlation) along given dimension.
%
% JH

    if nargin < 3, dim = 1; end
    
    sa = nanstd(a,[],dim);
    sb = nanstd(b,[],dim);
    s  = dk.bsx.rdiv( com.math.cmp.cov(a,b,dim), max(eps,sa.*sb) );

end
