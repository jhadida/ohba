function s = cov( a, b, dim )
%
% s = com.math.cmp.cov( a, b, dim=1 )
%
% Covariance along given dimension.
%
% JH

    if nargin < 3, dim = 1; end
    
    ma = nanmean( a, dim );
    mb = nanmean( b, dim );
    s  = nanmean( dk.bsx.sub(a,ma) .* conj(dk.bsx.sub(b,mb)), dim );
    
end
