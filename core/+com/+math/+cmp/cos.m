function s = cos( a, b, dim )
%
% s = com.math.cmp.cos( a, b, dim=1 )
%
% Cosine similarity along given dimension.
%
% JH

    if nargin < 3, dim = 1; end

    na = sqrt(nansum( a.*conj(a), dim ));
    nb = sqrt(nansum( b.*conj(b), dim ));
    s  = dk.bsx.rdiv( nansum(a.*conj(b),dim), max(eps,na.*nb) );
    
end
