function L = linkage_threshold( fc, pct )
%
% Hierarchical clustering on thresholded functional connectivity matrix.
%
% JH

    if nargin < 2, pct = 0.9; end

    % transform connectivity into distance matrix "a la fslnets"
    fc(fc < 0) = 0; % remove negative values
    
    msk = ~eye(size(fc),'logical');
    pct = prctile( fc(msk), pct );
    dst = (1 - min(fc/pct,1))/2; % correlations above pct have 0-distance
    
    % do linkage
    L = linkage( dk.torow(ant.mat.vtril(dst)), 'ward' );
    
end
