function [nettc,weights,covmats,vstats] = roi_nets( data, rois, method, varargin )
%
% [nettc,weights,covmats] = roi_nets( data, rois, method, varargin )
%
% Compute individual node timecourses within each ROI of a brain network.
%
% INPUT:
%
%   data        Ntimes x Nsources array or MEEG object
%   rois        Nrois x Nsources array of ROI weights
%   method      One of the methods accepted by com.math.roi_tc
%   varargin    Additional options to com.math.roi_tc
%
% OUTPUT:
%
%   nettc       Ntimes x Nrois matrix of timecourse in each ROI
%   weights     Nrois x Nsources matrix of weights for each source in each ROI
%   covmats     1 x Nrois cell of covariance matrices between signals included in each ROI.
%   vstats      Structure with fields { mean, sdev, freq, med, p01, p10, p90, p99 }
%
% SEE ALSO:
%   com.math.roi_tc
%
% JH

    % Wrap MEEG objects
    if isa(data,'meeg')
        data = com.dsp.MEEG_TimeSeries(data);
    end

    % Allocate outputs
    nr = size(rois,1);
    [nt,ns] = size(data); 
    
    weights = zeros(size(rois));
    covmats = cell(1,nr);
    vstats  = cell(1,nr);
    nettc   = zeros(nt,nr);
    timer   = dk.time.Timer();
    
    dk.print(['[com.math.roi_nets] ' ...
        'Extraction of %d node t.s. from %d input channels (%d timepoints) using method "%s" (please wait)...'], ...
        nr, ns, nt, method ); 
    
    % prevent NetLab from shadowing stats/ppca
    removed = resolve_ppca();
    
    % Iterate on each ROI
    dt = data.dt(true);
    for i = 1:nr
        [nettc(:,i),weights(i,:)] = com.math.roi_tc( data, rois(i,:), method, varargin{:} );
        rmask = abs(rois(i,:)) > eps;
        x = data(:,find(rmask)); %#ok
        covmats{i} = cov(x); 
        vstats{i} = compute_stats(x, dt);
        dk.print('\t- ROI #%d done, timeleft %s',i,timer.timeleft_str(i/nr));
    end
    
    % restore path
    restore_path(removed);

end

function r = resolve_ppca()

    m = matlabroot;
    w = fileparts(which('ppca'));
    r = {};
    
    while isempty(strfind(w,m))
        r{end+1} = w;
        rmpath(w);
        w = fileparts(which('ppca'));
        assert( ~isempty(w), 'Function ppca not found.' );
    end

end

function restore_path(r)
    
    if ~isempty(r)
        path( [strjoin(fliplr(r),pathsep) pathsep path] );
    end

end

function y = compute_stats(x,dt)
    y.mean = mean(x,1); 
    y.sdev = std(x,[],1);
    y.freq = meanfreq(x,1/dt);
    p = prctile(x,[1,10,50,90,99]);
    y.p01 = p(1,:);
    y.p10 = p(2,:);
    y.med = p(3,:);
    y.p90 = p(4,:);
    y.p99 = p(5,:);
end
