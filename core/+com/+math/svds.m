function [U, S, V] = svds(X, N)
%FAST_SVDS RAM/time-efficient version of SVDS, singular value decomposition
% 
% S = FAST_SVDS(X, N) computes N components of the singular value
%   decomposition of X: X = U * S * V' where S is diagonal, and returns the
%   singular values in S.
%
% [U, S, V] = FAST_SVDS(X, N) computes N components of the singular value
%   decomposition of X: X = U * S * V' where S is diagonal. 
%   If N <= 0, rank(X) - abs(N) components are estimated. 
%
%   Note: no demeaning of X takes place within this function

%	Copyright 2013-4 OHBA, FMRIB
%	This program is free software: you can redirstribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%	
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%	
%	You should have received a copy of the GNU General Public License
%	along with this program.  If not, see <http://www.gnu.org/licenses/>.

    [nr,nc] = size(X);

    % Compute svd from eigenvalue decomposition of X'*X. 
    % Choose which decomposition to take to maximise efficiency. 
    if nr < nc        
        if N < nr
            [U,D] = eigs(X * X', N);
        else
            [U,D] = eig(X * X'); 
            U     = fliplr(U); 
            D     = rot90(D, 2);
        end

        S = sqrt(abs(D));
        V = X' * (U * diag(1.0 ./ diag(S))); 

    else
        if N < nc
            [V,D] = eigs(X' * X, N);
        else
            [V,D] = eig(X' * X); 
            V     = fliplr(V); 
            D     = rot90(D, 2);
        end

        S = sqrt(abs(D));
        U = X * (V * diag(1.0 ./ diag(S))); 

    end

    % change output based on required behaviour
    if nargout == 1
        U = S;
    end
    
end
