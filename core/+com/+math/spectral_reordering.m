function [S,Rr,Rc] = spectral_reordering( M )
%
% [S,Rr,Rc] = spectral_reordering( M )
%
% S is the reordered matrix
% Rr is the new order of rows
% Rc is the new order of columns
%
% S Jbabdi 2012

    tr = diag(sqrt( 1./sum(M,1) ));
    tc = diag(sqrt( 1./sum(M,2) ));
    
    W = tc * M * tr;
    [U,~,V] = svd(W);
    
    Pr = tc * U(:,2);
    Pc = tr * V(:,2);

    [~,Rr] = sort(Pr);
    [~,Rc] = sort(Pc);
    
    S = M(Rr,Rc);
    
end
