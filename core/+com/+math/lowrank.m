function L = lowrank( M, k )
%
% L = com.math.lowrank( M, k )
%
% Rank-k approximation of matrix M using SVD.
%
% JH

    [U,S,V] = com.math.svds( M, k );
    L = U*S*V';
    
end
