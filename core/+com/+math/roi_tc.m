function [tc,weights] = roi_tc( data, roi, method, varargin )
%
% [tc,weights] = roi_tc( data, roi, method, varargin )
%
% Summary timecourse of a time-series with large number of channels in given 
% region of interest (roi) using a specified method.
%
%
% INPUTS
% ------
%
%       data    Time-by-channel matrix, or wrapper overloading subsref and size methods.
%
%        roi    1-by-Ns matrix of logical or real-valued weights. 
%               See summary methods below.
%
%     method    Choice of summary method, see below for details.
%
%   varargin    Pre-processing arguments for PCA methods.
%               Currently: demean, regmean, detrend, zscores
%
%
% OUTPUTS
% -------
%
%         tc    Nt-by-1 column vector with summary timecourse.
%
%    weights    1-by-Nr vector with weights for each input source.
%
%
% METHODS
% -------
%
% == Method PEAK
%   roi should be logical.
%   Select channel with highest energy.
%
% == Method MEAN
%   roi should be logical.
%   Average timecourse across roi.
%
% == Method WEIGHTS
%   roi should be positive, will be normalised to sum to 1.
%   Weighted sum of timecourses within roi.
%
% == Method PCA
%   roi should be real.
%   Compute principal component of roi-weighted timecourses.
%   The variance of the output timecourse is manually rescaled.
%
% == Method PPCA
%   Same as PCA, but using probabilistic PCA instead.
%   
% == Method DR
%   roi should be real.
%   Dual regression for given roi.
%
%
% Based on ROInets by Giles Colclough, part of OSL.
% JH

    [nt,ns] = size(data);
    assert( size(roi,2)==ns && size(roi,1)==1, 'Bad roi dimensions.' );

    % process roi
    roi_mask  = abs(roi) > eps;
    roi_index = find(roi_mask);
    roi_vals  = roi(roi_mask);
    roi_nnz   = numel(roi_index);
    weights   = zeros(size(roi));
    
    if roi_nnz == 0
        warning('Input ROI is empty, returning null timecourse.');
        tc = zeros(nt,1);
        return;
    end
    
    % compute summary timecourse
    switch lower(method)
    case 'peak' % most powerful signal within roi

        energy = data(:,roi_index);
        energy = dot(energy,energy,1);
        
        [~,k] = max(energy);
        tc    = data(:,roi_index(k));
        
        wvals = zeros(1,roi_nnz);
        wvals(k) = 1;
        
    case 'mean' % average across each roi
        
        wvals = 1/roi_nnz;
        tc    = mean( data(:,roi_index), 2 );

    case 'weights' % signal weighting

        assert( all(roi_vals > 0), 'Negative weights are not allowed.' );
        wvals = roi_vals / sum(roi_vals); % normalise weigths to sum to 1
        tc    = data(:,roi_index) * wvals(:);

    case 'pca' % principal component

        % prepare data
        X = data( :, roi_index );
        X = pca_preproc( X, varargin{:} );
        X = bsxfun( @times, X, roi_vals );
        
        if roi_nnz > 1
        
            % run PCA
            [U,S,V] = com.math.svds( X, 1 );

            scores = S * U; % principal component
            wvals  = V';

            wstd = abs(wvals) / sum(abs(wvals)); % normalised weights
            wstd = dot( wstd, std(X,[],1) ); % weighted std

            tc = wstd * scores / max( std(scores), eps ); % rescaled variance
            
        end
        
    case 'ppca' % probabilistic principal component

        % prepare data
        X = data( :, roi_index );
        X = pca_preproc( X, varargin{:} );
        X = bsxfun( @times, X, roi_vals );
        
        if roi_nnz > 1
        
            % run pPCA
            [wvals,scores,pcvar] = ppca( X, 1 );

            wvals = wvals';
            wstd  = abs(wvals) / sum(abs(wvals)); % normaised weights
            wstd  = dot( wstd, std(X,[],1) ); % weighted std

            tc = wstd * scores / max( sqrt(pcvar), eps ); % rescaled variance
            
        end

    case 'dr' % dual regression

        values = detrend(data( :, roi_index ));
        
        tc    = values / roi_vals;
        wvals = tc \ values;
        
    otherwise
        error('Unknown method "%s".',method);

    end
    
    % normalise sign (don't do that for now)
    if false
        
        nsign = sign(mean(wvals));
        wvals = nsign * wvals;
        tc    = nsign * tc;
        
    end
    
    % post-process weights
    weights(roi_mask) = wvals;

end

% Pre-processing prior to PCA-like methods
function vals = pca_preproc( vals, method )

    if nargin < 2 || isempty(method)
        method = 'default';
    end
    
    switch lower(method)
            
        case {'demean','default'}
            vals = bsxfun( @minus, vals, mean(vals,1) );
            
        case {'regmean'} % regress mean timecourse
            avg  = mean(vals,2);
            vals = vals - (avg * (pinv(avg)*vals));
            
        case {'detrend'}
            vals = detrend(vals);
            
        case {'zscores','normalise'}
            vals = zscore(vals);
            
        otherwise
            error('Unknown method "%s".',method);
        
    end

end
