function n = medabs( x, nodiag )
%
% n = com.math.medabs( x, nodiag )
%
% Median of absolute values in x.
% If nodiag is set to true, the diagonal values are ignored (matrix input expected).
%
% JH

    if nargin < 2, nodiag = false; end
    
    if nodiag
        assert( ismatrix(x), 'Can only remove diagonal terms on matrices.' );
        x = x(~eye(size(x),'logical'));
    end
    n = nanmedian(abs(x(:)));

end
