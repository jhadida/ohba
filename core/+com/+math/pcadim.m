function [d,lambda,paxes,coord] = pcadim( x, threshold )
%
% Return the "linear embedding dimension" using PCA.
% This corresponds to the smallest dimension required to explain at least threshold % of the data (default: 0.95).
%
% Since a PCA is run to obtain that dimension, the additional outputs return
%
% JH

    if nargin < 2, threshold = 0.95; end

    if isa(x,'ant.TimeSeries')
        assert( x.is_arithmetic() );
        x = x.vals;
    end
    
    [paxes,coord,lambda] = pca( x );
    d = find( cumsum(lambda)/sum(lambda) >= threshold, 1, 'first' );

end
