function lap = graph_laplacian(adj)

    adj = ant.mat.setdiag(adj, 0); % remove diagonal
    deg = sum(adj,1);
    lap = diag(deg) - adj;

end
