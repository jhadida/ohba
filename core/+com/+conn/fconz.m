function M = fconz( vals, method, pnorm )
%
% M = com.conn.fconz( vals, method, pnorm=1 )
%
% Pairwise functional connectivity on complex time-series (typically spectral signals).
% Input vals should be a Ntimes x Nsignals complex matrix.
% Output is a square matrix of size Nsignals.
%
% METHODS:
%
%   corr    Correlation coefficient
%   pcorr   Partial correlation (using inverse covariance estimate)
%   mcorr   Mixed corr (lower triangle) and pcorr (upper triangle)
%   coh     Coherency (cross-spectral cosine)
%   pcoh    Phase coherence (phase-locking pairs)
%   icoh    Imaginary coherency
%
% The third input 'pnorm' is a normalisation vector, such that:
%     dk.bsx.rdiv( vals .* conj(vals), pnorm )
% yields an estimate of the PSD. It defaults to 1, but you should 
% provide a value if you can.
%
% JH

    if nargin < 3, pnorm = 1; end

    assert( ismatrix(vals) && ~isreal(vals), ...
        'Input should ba a Ntime x Nsignals complex matrix.' );

    ns = size(vals,2); % number of signals
    
    pnorm = dk.torow(pnorm);
    %pfun = @(x) dk.bsx.rdiv( x.*conj(x), pnorm );
    %pfun = @(x) dk.bsx.rdiv( abs(x), sqrt(pnorm) );
    pfun = @(x) abs(x);
    
    switch lower(method)
        
        case {'cor','corr','correlation'} % correlation
            M = corrcoef(pfun(vals));
            
        case {'cov','covariance'} % correlation
            M = cov(pfun(vals));
            
        case {'pcorr','icov','partial'} % partial correlation
            M = -inv(cov(pfun(vals)));
            D = max(eps,sqrt(abs(diag( M ))));
            M = M ./ (D * D');
            
        case {'mcorr','mixed'} % mixed corr\pcorr
            C = cov(pfun(vals));
            M = -inv(C);
            D = max(eps,sqrt(abs(diag( M ))));
            M = tril(corrcov(C)) + triu(M ./ (D * D'));
            
        case {'coherency'}
            M = coherency(vals); % coherency is a complex matrix
            
        case {'pcoh','coherence'} % coherence = coherency magnitude
            M = abs(coherency(vals));
            
        case {'icoh'} % imaginary coherency
            M = imag(coherency(vals));
        case {'rcoh'} % real coherency
            M = real(coherency(vals));
            
    end
    
    % set diagonal elements to 0
    M(1:(ns+1):ns^2) = 0;

end

function M = coherency( vals )
    M = ctranspose(vals) * vals;
    D = max(eps,sqrt(diag( M )));
    M = M ./ (D * D');
end
