function M = xfconz( A, B, method, pnorm )
%
% M = com.conn.xfconz( A, B, method, pnorm=1 )
%
% Cross-functional connectivity between matched spectral signals.
% Inputs A and B should be Ntimes x Nsignal complex matrices.
%
% The third input 'method' is one of:
%   corr    Correlation coefficient
%   pcorr   Partial correlation (using inverse covariance estimate)
%   mcorr   Mixed correlation: lower triangle is the correlation, upper triangle is the partial correlation
%   coh     Coherency (cross-spectral cosine)
%   pcoh    Phase coherence (phase-locking pairs)
%   icoh    Imaginary coherency
%
% The third input 'pnorm' is a normalisation vector, such that:
%     dk.bsx.rdiv( A .* conj(A), pnorm )
% yields an estimate of the PSD. It defaults to 1, but you should 
% provide a value if you can.
% Values for A and B should be the same (same sampling properties).
%
% JH

    if nargin < 4, pnorm = 1; end

    assert( ismatrix(A) && ~isreal(A) && ~isreal(B) && all(size(A) == size(B)), ...
        'Inputs must be real matrices with the same size.' );
    
    pnorm = dk.torow(pnorm);
    %pwe = @(x) dk.bsx.rdiv( x.*conj(x), pnorm );
    pwe = @(x) dk.bsx.rdiv( abs(x), sqrt(pnorm) );
    
    switch lower(method)
        
        case {'cor','corr','correlation'} % correlation
            M = com.math.cmp.corr( pwe(A), pwe(B), 1 );
        
        case {'cov','covariance'} % covariance
            M = com.math.cmp.cov( pwe(A), pwe(B), 1 );
            
        case {'coherency'}
            M = coherency(A,B);
            
        case {'pcoh','coherence'} % coherence = coherency magnitude
            M = abs(coherency(A,B));
            
        case {'icoh','imaginary'} % imaginary coherency
            M = imag(coherency(A,B));
        case {'rcoh','real'} % real coherency
            M = real(coherency(A,B));
        
    end

end

function M = coherency( A, B )
    mhp = @(x,y) mean(x.*conj(y),1);
    M = mhp(A,B) ./ sqrt( mhp(A,A) .* mhp(B,B) );
end
