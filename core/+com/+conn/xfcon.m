function M = xfcon( A, B, method )
%
% M = com.conn.xfcon( A, B, method )
%
% Cross functional connectivity between matched signal pairs.
% Output is a vector with a connectivity value for each matched pair of signals.
%
% METHODS:
%
%     corr  Pearson correlation
%      cov  Covariance
%    rcorr  Correlation after regressing mean timecourse
%
% JH

    assert( ismatrix(A) && isreal(A) && isreal(B) && all(size(A) == size(B)), ...
        'Inputs must be real matrices with the same size.' );
    
    regr = @(x,y) x - (y * (pinv(y)*x));
    
    switch lower(method)

        case {'cor','corr','correlation'}
            M = com.math.cmp.corr( A, B, 1 );
            
        case {'cov','covariance'}
            M = com.math.cmp.cov( A, B, 1 );

        case {'rcorr'} % corr() after regressing out mean timecourse
            
            rA = nanmean(A,2);
            rB = nanmean(B,2);
            M  = com.math.cmp.corr( regr(A,rA), regr(B,rB), 1 );

        otherwise
            dk.print('Unknown method "%s"',method);
    end
    
end
