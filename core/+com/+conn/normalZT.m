function z = normalZT( fc, mu, sigma )
%
% S = com.conn.normalZT( fc, mu, sigma )
%
% fc: connectivity matrix to be transformed
% mu, sigma: params of empirical null-distribution
%
% Compute a significance matrix for pairwise functional connectivity between input signals.
% Elements in output mask are z-scores under an empirical null distribution.
%
% The returned mask contains values theoretically in R+, which correspond to normal
% inverse cdf values under the assumption of a normally distributed null-hypothesis.
%
% In practice, this means that you can extract those elements in the raw FC matrix with a 
% p-value of at most P with the following command:
%   fc .* (S >= norminv(1-P))
%
% JH based on code by RA

    z = abs( fc - mu ) ./ max(sigma,eps);
    z = ant.mat.setdiag(z,0);

end
