function M = fcon( vals, method, varargin )
%
% M = com.conn.fcon( vals, method, varargin )
%
% Pairwise functional connectivity between signals.
% Input vals should be a Ntimes x Nsignals real matrix.
% Output is a square positive matrix of size Nsignals.
% It is symmetric for methods other than 'mcorr'.
%
% METHODS:
%
%     corr  Pearson correlation
%      cov  Covariance
%    rcorr  Correlation after regressing mean timecourse
%    pcorr  Partial correlation
%    mcorr  Mixed corr (lower triangle) and pcorr (upper triangle)
%   ridgep  Regularised partial correlation
%           Additional parameter RHO controls regularisation (default: 0.1)
%
% JH

    assert( ismatrix(vals) && isreal(vals), ...
        'Input should be a Ntimes x Nsignals real matrix.' )

    ns = size(vals,2); % number of signals
    
    switch lower(method)

        case {'cor','corr','correlation'}
            M = corrcoef(vals);
        case {'cov','covariance'}
            M = cov(vals);

        case {'rcorr'} % corr() after regressing out mean timecourse
            M = mean(vals,2);
            M = vals - (M * (pinv(M)*vals));
            M = corrcoef(M); 
            
        case {'pcorr','icov','partial'}
            M = -inv(cov(vals));
            D = max(eps,sqrt(abs(diag(M))));
            M = M ./ (D * D');
            
        case {'mcorr','mixed'} % mixed corr\pcorr
            C = cov(vals);
            M = -inv(C);
            D = max(eps,sqrt(abs(diag( M ))));
            M = tril(corrcov(C)) + triu(M ./ (D * D'));

        case {'ridgep','regularized'}
            M = cov(vals); 
            M = M/sqrt(mean(diag(M).^2));

            if nargin == 2
                rho=0.1;
            else
                rho=varargin{1};
            end
            M = -inv(M+rho*eye(ns));
            D = max(eps,sqrt(abs(diag(M))));
            M = M ./ (D * D');

        otherwise
            dk.print('Unknown method "%s"',method);
    end

    % set diagonal elements to 0
    M(1:(ns+1):ns^2) = 0;
    
end
