function [mu,sigma] = phaserand( vals, nsamples, varargin )
% 
% [mu,sigma] = com.conn.phaserand( vals, nsamples, varargin )
%
% Evaluate null distribution of functional connectivity using phase randomisation method.
% Additional arguments are forwarded to com.conn.fcon
%
% For each sample, the Fourier phase of the inpu timt-series is shuffled and a FC matrix is computed.
% The average and std across samples is then evaluated for each element in the FC matrix.
%
% Recommended order of magnitude for nsamples is between n_signals^2 and n_signals^3.
%
% See also: com.conn.fcon, ant.ts.phaserand
%
% JH

    ns = size(vals,2);
    fc = zeros(ns*(ns+1)/2,nsamples);
    
    for i = 1:nsamples
       tmp     = ant.ts.phaserand(vals); 
       fc(:,i) = ant.mat.sym2col(com.conn.fcon( tmp, varargin{:} ));
    end
    
    mu = ant.mat.col2sym(mean( fc, 2 ));
    sigma = ant.mat.col2sym(std( fc, [], 2 ));

end
