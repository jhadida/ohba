function cmat = edit( cmat, varargin )
%
% cmat = com.conn.edit( cmat, varargin )
%
% Edit connectivity matrix using one (or several) of the following methods:
%
%       norm_max  Divide by maximum element
%       norm_eig  Divide by largest eigenvalue
%      norm_mean  Divide by average value
%   norm_meandeg  Divide by average degree (column sum)
%    norm_maxdeg  Divide by maximum degree
%      norm_cols  Divide each column by its sum
%      norm_rows  Divide each row by its sum
%        symrand  Symmetric shuffling of elements
%
% Note that the elements of the connectivity matrix are expected to be non-negative. 
%
% JH

    if iscellstr(varargin)
        
        % set diagonal to 0
        assert( dk.is.square(cmat), 'Expected square matrix in input.' );
        cmat = ant.mat.setdiag(cmat,0);
        assert( all(cmat(:) >= 0), 'Connectivity values should be non-negative.' );
        
        % process string arguments in sequence
        n = numel(varargin);
        for i = 1:n
            method = varargin{i};
            switch lower(method)

                case {'','none'}
                    % don't do anything

                case {'norm_max','max'}
                    cmat = cmat / max(cmat(:));
                    
                case {'norm_eig','eig'}
                    pe = ant.mat.setdiag(cmat,1);
                    pe = eigs( pe, 1 );
                    cmat = cmat / pe;

                case {'norm_mean','mean'}
                    cmat = cmat / mean(cmat(:));
                    
                case {'norm_meansum','norm_meandeg','meandeg'}
                    cmat = cmat / mean(sum(cmat));
                    
                case {'norm_maxsum','norm_maxdeg','maxdeg'}
                    cmat = cmat / max(sum(cmat));

                case {'norm_cols','cols'}
                    cmat = bsxfun( @rdivide, cmat, sum(cmat,1) );

                case {'norm_rows','rows'}
                    cmat = bsxfun( @rdivide, cmat, sum(cmat,2) );
                    
                case {'symrand'} % symmetric randomisation
                    idx = find(tril( true(size(rmat)), -1 ));
                    ord = randperm(length(idx));
                    
                    cmat(idx) = cmat(idx(ord));
                    cmat = transpose(cmat);
                    cmat(idx) = cmat(idx(ord));
                    cmat = transpose(cmat);

                otherwise
                    error('Unknown method "%s".',method);

            end
        end
        
    elseif nargin == 2 && iscellstr(varargin{1})
        
        % expand cell of strings in input
        cmat = com.conn.edit( cmat, varargin{1}{:} );
        
    else
        error( 'Expects string arguments in input.' );
    end
    
end
