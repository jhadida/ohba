function [B,M] = load_bundle( nstream, nregion, wnorm, varargin )
%
% B = com.load_bundle( nstream=10, nregion=68, wnorm=fs, varargin )
%
%   Load connectivity bundle with specified number of streamlines (in million units), 
%   number of regions, and weight normalisation. 
%
%   Additional inputs are used to further normalise the weights matrix.
%   The length matrix corresponds to average streamline length in *METERS*.
%   Available normalisations are: mean and frac
%
% EXAMPLE
%
%   B = com.load_bundle( 10, 68, 'fs' )
%
%
% See also: com.load_mrtrix, lsbm.cfg.connproc
%
% JH

    if nargin < 3, wnorm = 'fs'; end
    
    % load data
    M = com.load_mrtrix(nstream,nregion);
    
    % average connectivity matrices across subjects, and restore symmetric form
    B = rmfield( M.sconn, 'subject' );
    B.count  = ant.mat.col2sym( mean(B.count,2), true );
    B.weight = ant.mat.col2sym( mean(B.weight,2), true );
    B.length = ant.mat.col2sym( mean(B.length,2), true );
    
    B.disp = M.disp;
    B.group = M.group;
    
    % apply normalisation and assign to field 'wnorm'
    switch lower(wnorm)
        
        % divide accumulated weights for each edge by sum of all weights
        case {'m','mean'}
            B.wnorm = B.weight / sum(ant.mat.vtril( B.weight ));
            
        % fractional scaling
        case {'fs','frac','fractional'}
            d = sum(B.count,1);
            d = dk.bsx.add( d(:), d );
            B.wnorm = B.weight ./ max(1,d);
            
        otherwise
            error( 'Unknown normalisation: %s', norm );
    end
    
    % apply further normalisation
    if nargin > 3
        B.wnorm = lsbm.cfg.connproc( B.wnorm, varargin{:} );
    end
    
end