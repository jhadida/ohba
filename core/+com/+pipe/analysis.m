function [ana,pre,tf] = analysis( ts, popt, mopt, wopt )
%
% [ana,pre,tf] = analysis( ts, popt, mopt, wopt )
%
% Run preprocessing and analysis on input time-series.
% The analysis combines MultiBand and Wavelet pipelines, and outputs
% multiband functional connectivity and wavelet spectral statistic.
%
% INPUTS
%
%     ts  Time-series to be processed.
%   popt  Preprocessing options (cf com.pipe.preproc)
%       + Field:burn to discard initial timepoints (default: 0).
%   mopt  Multiband analysis options (bands required, cf com.dsp.bandfc)
%       + Field:fc to specify desired connectivity measure(s) (default: cor).
%   wopt  Wavelet analysis options (freq required, cf com.dsp.spstat)
%       + Field:fold to compute spstat on a sliding window (default: []).
%
%       OPTION INPUTS SHOULD BE STRUCTURES, KEY/VALUE CELLS OR kwArgs
%       Note 1: Field:fs overrides npts to set sampling rates manually.
%       Note 2: tf.wav will not be set if Field:fold is too large.
%       Note 3: Field:fc can be a string or a cell-string.
%
% OUTPUTS
% 
%    ana  Structure with fields {con,sps}, each with fields {out,opt}.
%    pre  Preprocessing results.
%     tf  Spectral aggregates for multiband and wavelet analyses.
%
%       Note: don't keep tf in memory too long, it can be big!
%
% See also: com.dsp.bandfc, com.dsp.spstat, ant.dsp.hilbert, ant.dsp.wavelet
%
% JH
    
    popt = dk.obj.kwArgs(popt);
    mopt = dk.obj.kwArgs(mopt);
    wopt = dk.obj.kwArgs(wopt);
    
    % pre-processing
    pre = com.pipe.preproc( ts, popt );
    
    % burn-in
    burn = popt.get( 'burn', 0 );
    if burn > 0
        burn = pre.preproc.time(1) + burn;
        pre.env.burn(burn);
    end
    
    
    % connectivity analysis
    dk.print( '[com.pipe.analysis] Multi-band analysis...' );
    opt = process_sampling(mopt);
    opt = dk.struct.set( opt, 'fc', 'cor', false );
    
    proc = pre; % short-circuit analysis
    proc.tf = multiband_analysis( pre.preproc, opt, burn );
    
    con = com.dsp.bandfc( proc, opt.fc, opt );
    if nargout > 2, tf.mb = proc.tf; end
    
    ana.con.out = con;
    ana.con.opt = opt;
    
    
    % spectral analysis
    dk.print( '[com.pipe.analysis] Wavelet analysis...' );
    opt = process_sampling(wopt);
    
    fold = dk.struct.get( opt, 'fold', [] );
    if ~isempty(fold)
        % force burn-in into fold
        fold(3) = burn;
        
        % analysis on a sliding window
        [sps.data,sps.time] = ant.dsp.slidingfun( ...
            @(x,ki,ke) windowed_wavelet_analysis(x.between_k(ki,ke), opt), pre.preproc, fold );
        
        % unreasonable to save spectra for sliding version (way too large)
        tf.wav = [];
    else
        proc = pre; % short-circuit analysis
        proc.tf = wavelet_analysis( pre.preproc, opt, burn );
        
        sps = com.dsp.spstat( proc, opt );
        if nargout > 2, tf.wav = proc.tf; end
    end
    ana.sps.out = sps;
    ana.sps.opt = opt;
    
end

function opt = process_sampling( opt )

    p.npts = opt.get( 'fs', 0 ); % fixed sampling rate, overrides npts
    if p.npts > 0
        p.force = true;
    else
        p.force = false;
        p.npts  = opt.get( 'npts', 5 );
    end
    opt = dk.struct.merge( opt.parsed, p );

end

function agg = multiband_analysis( pre, opt, burn )

    assert( isfield(opt,'bands'), '[com.pipe.analysis:multiband] Missing field: bands' );
    agg = ant.dsp.hilbert( pre, opt.bands, opt.npts, opt.force );
    if burn > 0, agg.burn(burn); end

end

function agg = wavelet_analysis( pre, opt, burn )

    assert( isfield(opt,'freq'), '[com.pipe.analysis:wavelet] Missing field: freq' );
    agg = ant.dsp.wavelet( pre, opt.freq, opt.npts, opt.force );
    if burn > 0, agg.burn(burn); end

end

function sp = windowed_wavelet_analysis( pre, opt )

    proc.tf = ant.dsp.wavelet( pre, opt.freq, opt.npts, opt.force );
    sp = com.dsp.spstat( proc, opt );
    
end
