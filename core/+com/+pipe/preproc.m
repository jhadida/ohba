function out = preproc( raw_ts, varargin )
%
% out = com.pipe.preproc( ts, param )
% 
% where INPUT param is expected to be a structure with fields:
%       
%   verb 
%       If true, output progress messages during processing.
%       DEFAULT: false (no message)
%
%   resample
%       If greater than 0, input time-course will be resampled to that frequency.
%       See ant.dsp.do.resample for details.
%       DEFAULT: 0 (no resampling)
%
%   filter 
%       Name of the filter to apply before analysis.
%       See ant.dsp.do.filter for details.
%       DEFAULT: '' (no filter)
%
%   norm
%       Normalise signals before further analysis.
%       See ant.dsp.do.normalise for details.
%       DEFAULT: '' (no normalisation)
%
%   orth 
%       Name of the orthogonalisation method to use (typically for leakage correction).
%       See com.dsp.leakage_correction for details.
%       DEFAULT: '' (no correction)
%
%   hrf 
%       If evaluates to true, compute hemodynamic response function. 
%       See com.dsp.hemodyn_response for details.
%       DEFAULT: false (no convolution)
%
%   envfs
%       If greater than 0, envelope time-course will be resampled to that frequency.
%       See ant.dsp.do.envelope for details.
%       DEFAULT: 0 (no resampling)
%
%
% OUTPUT is a structure with fields:
%
%     input  Input time-series (shallow copy)
%     param  Parsed inputs
%   preproc  Processed time-series
%       env  Corresponding Hilbert envelope
%
%
% See also: ant.dsp.do.*
%
% JH

    param = dk.obj.kwArgs(varargin{:});

    % print progress messages or not
    if param.get('verb',true)
        local_print = @(fmt,varargin) dk.print(['[com.pipe.preproc] ' fmt],varargin{:});
    else
        local_print = @dk.pass;
    end
    
    
    %--------------------------------------------------
    local_print('Preprocessing...');
    pre_ts = raw_ts;

    % resampling
    Prawfs = param.get( 'resample', 0 );
    if Prawfs > 0
        local_print('Resampling raw timecourses at %.2f Hz.',Prawfs);
        pre_ts.resample( Prawfs, 0.01 );
    end
    
% DEPRECATED: 
% It affects spectral analysis badly to burn-in during preprocessing, so we do it after.
%
%     % burn
%     burn = dk.struct.get( param, 'burn', 0 );
%     if burn > 0
%         tstart = pre_ts.time(1) + burn;
%         local_print('Removing timepoints before t=%g.',tstart);
%         pre_ts = pre_ts.burn(tstart);
%     end

    % filtering
    Pfilt = param.get( 'filter', '' );
    if ~isempty(Pfilt)
        local_print('Filtering raw time-courses.');
        pre_ts = ant.dsp.do.filter( pre_ts, Pfilt );
    end
    
    % orthogonalisation
    Porth = param.get( 'orth', '' );
    if ~isempty(Porth)
        local_print('Orthogonalising raw time-courses using method "%s".',Porth);
        pre_ts = com.dsp.leakage_correction( pre_ts, Porth );
    end
    
    % normalisation
    Pnorm = param.get( 'norm', '' );
    if ~isempty(Pnorm)
        local_print('Normalising time-courses using method "%s".',Pnorm);
        pre_ts = ant.dsp.do.normalise( pre_ts, Pnorm );
    end

    % hrf convolution
    if param.get( 'hrf', false )
        local_print('Running HRF convolution.');
        pre_ts = com.dsp.hemodyn_response( pre_ts );
    end
    
    % Hilbert envelope
    env_ts = ant.dsp.do.envelope( pre_ts );
    Penvfs = param.get( 'envfs', 0 );
    if Penvfs > 0
        env_ts.downsample(Penvfs);
    end
    
    
    %--------------------------------------------------
    % OUTPUT
    out.param   = param.parsed;
    out.input   = raw_ts;
    out.preproc = pre_ts;
    out.env     = env_ts;
    
end
