classdef MultiBand < com.pipe.Abstract
%
% Multi-band analysis pipeline.
%
% OPTIONS
% -------
%
%    bands  DEFAULT: rest-overlap
%           Frequency bands in which to compute analytic signals.
%
%     npts  DEFAULT: 5
%           Number of timepoints per oscillation for analytic downsampling.
%
%       fs  DEFAULT: 0
%           If set, overrides npts to specify explicit sampling frequencies.
%           Can be either scalar or vector (one for each band).
%
%    envfs  DEFAULT: 0
%           If nonzero, resample broadband envelope.
%
%     burn  DEFAULT: 0
%           Discard specified time-duration at the beginning of envelope and 
%           spectral time-courses. This is done AFTER processsing.
%
%   tf_obj  DEFAULT: []
%           If nonempty, use provided object instead of recomputing the multi-band 
%           analysis. Object should be a ant.dsp.TFSpectrum.
%
% For additional options, see:
%   - com.pipe.preproc (preprocessing)
%
%
% See also: com.pipe.preproc, ant.dsp.hilbert, ant.dsp.TFSpectrum
%
% JH

    properties (SetAccess=private)
        tf_obj;
    end
    properties
        bands;
    end
    
    methods
        
        % constructor
        function self = MultiBand( varargin )
            
            self.param  = struct();
            self.editor = dk.widget.PropertyEditor();
            self.configure(varargin{:});
            
        end
        
        function configure(self,varargin)
            
            % parse inputs
            opt = dk.obj.kwArgs(varargin{:});
            
            % apply config
            p.verb      = opt.get('verb',true);
            p.resample  = opt.get('resample',0);
            p.burn      = opt.get('burn',0);
            p.filter    = opt.get('filter','');
            p.norm      = opt.get('norm','');
            p.orth      = opt.get('orth','');
            p.hrf       = opt.get('hrf','');
            p.envfs     = opt.get('envfs',0);
            
            % deal with fs override
            p.npts = opt.get('fs',0);
            if p.npts > 0
                p.force = true;
            else
                p.force = false;
                p.npts  = opt.get('npts',5);
            end
                        
            self.tf_obj = opt.get('obj',[]);
            self.bands  = opt.get('bands','rest-overlap');
            
            if ischar(self.bands)
                self.bands = com.meg.bands(self.bands);
            end
            
            self.param = p; % assign only once for property observers
            self.ui_refresh();
            
        end
        
        function ui_open(self,parent)
            
            if nargin < 2, parent = figure('name','NSL Pipeline Editor'); end
            
            self.editor = dk.widget.PropertyEditor() ...
                .set_field_logical ('verb')      ...
                .set_field_numeric ('resample')  ...
                .set_field_numeric ('burn')      ...
                .set_field_string  ('filter')    ...
                .set_field_string  ('norm')      ...
                .set_field_string  ('orth')      ...
                .set_field_string  ('hrf')       ...
                .set_field_numeric ('npts')      ...
                .set_field_logical ('force')     ...
                .set_field_numeric ('envfs')     ...
            ;
            self.editor.build( ...
                uipanel( 'parent', parent, 'title', 'Settings' ), ...
                self.param, @self.cb_update ...
            );
            self.editor.set_prop( 'ColumnWidth', {120,120} );
            
        end
        
        function out = process(self,raw_ts,varargin)
            
            % configure first, if parameters are supplied
            if nargin > 2
                self.configure(varargin{:});
            else
                self.tf_obj = [];
            end
            
            % preprocessing
            out = com.pipe.preproc( raw_ts, self.param );
            
            % multi-band analysis
            if isempty(self.tf_obj)
                
                dk.print('[MultiBand pipeline] Processing...');
                self.tf_obj = ant.dsp.hilbert( out.preproc, self.bands, self.param.npts, self.param.force );
                dk.print('[MultiBand pipeline] Done!');
                
            end
            
            % apply burn-in after spectral transform
            burn = self.param.burn;
            if burn > 0
                self.tf_obj.burn(burn);
                out.env.burn(burn);
            end
            
            out.tf    = self.tf_obj;
            out.param = self.param;
            
        end
        
    end
    
end
