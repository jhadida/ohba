classdef Abstract < handle
    
    properties (SetObservable = true)
        param;
    end
    
    properties (SetAccess = protected)
        editor;
    end
    
    methods (Abstract)
        
        % open editor in a new figure or using the parent handle
        ui_open(self,parent);
        
        % configure the pipeline using key/value pairs, or a structure in input
        configure(self,varargin);
        
        % run the pipeline on provided input (typically data is a time-series)
        % output should typically be a structure of data fields corresponding to different features
        output = process(self,input);
        
    end
    
    methods (Hidden)
        
        % set ui state from self.param
        function ui_refresh(self)
        if self.editor.is_open()
            self.editor.set_data(self.param);
        end
        end
        
        % import settings from editor
        function cb_update(self,data)
            self.configure(data);
        end
        
    end
    
end
