function pa = build( name, opts, plots )
%
% pa = com.pipe.build( name, opts, varargin )
%
% Build pipeline with options, bind to com.ui.Pipeline object, and attach plots.
%
% INPUTS:
%
%    name   Name of the pipeline to use (cf com.pipe.*)
%    opts   Options for the pipeline (anything dk.obj.kwArgs can parse)
%   plots   Struct array with fields
%                 name: Name of the tab
%               handle: Plot handle (cf com.ui.plot.*)
%                 args: Plot options (anything dk.obj.kwArgs can parse)
%
% See also: com.pipe.preproc, com.pipe.MultiBand, com.pipe.Wavelet
%
% JH

    if nargin < 3, plots = []; end

    % build pipeline and bind to pipeline analyser
    pa = com.pipe.(name)( opts );
    pa = com.ui.Pipeline(pa);
    
    % add plots iteratively
    n = numel(plots);
    for i = 1:n
        p = plots(i);
        pa.add_tab( p.name, p.handle, p.args );
    end

end
