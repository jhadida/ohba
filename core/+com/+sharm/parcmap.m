function score = parcmap( weights, reduce )
%
% score = com.sharm.parcmap( weights, reduce=@mean )
%
% Compute scores for each region in the parcellation (specified by pname).
% The reduce function is invoked with a 1xN vector for each region independently, corresponding 
% to the weighted sum of the SH basis functions for each vertex in that region (determined using
% aparc.left.vlabel or aparc.right.vlabel), and should return a SCALAR.
%
% Output is a Px1 column vector with a concatenation of scores across left and right hemispheres.
% Visualise with: com.sharm.showmap( score )
%
% JH

    if nargin < 2, reduce = @mean; end
    
    % load data
    [surf,aparc] = load_data();
    
    % compute SH sums for left and right hem (ensuring chirality)
    %Lscore = process_hemisphere( surf.left.vertices, aparc.left, weights, reduce ); % this makes them symmetric
    Lscore = process_hemisphere( dk.bsx.mul( surf.left.vertices, [-1,1,1] ), aparc.left, weights, reduce );
    Rscore = process_hemisphere( surf.right.vertices, aparc.right, weights, reduce );
    
    % concatenate results
    score = [Lscore(:); Rscore(:)];

end

function s = process_hemisphere( x, a, w, r )

    % convert coordinates
    x = dk.bsx.sub( x, mean(x,1) );
    x = dk.bsx.rdiv( x, sqrt(dot(x,x,2)) );
    [p,t] = cart2sph( x(:,1), x(:,2), x(:,3) );
    t = pi/2 - t;
    
    % evaluate weighted sum of basis functions at all vertices
    h = com.sharm.wsum( w, t, p );
    
    % compute weighted sum of components for each parcel
    n = a.nlabel;
    s = zeros(1,n);
    for i = 1:n
        s(i) = r(h( a.vlabel == a.label(i) ));
    end

end

function [s,p] = load_data()

    data = com.load_mrtrix();
    
    s.left.vertices = data.left.coord.sphere;
    s.left.faces = data.left.faces;
    s.left.atlas = data.left.atlas;
    
    s.right.vertices = data.right.coord.sphere;
    s.right.faces = data.right.faces;
    s.right.atlas = data.right.atlas;
    
    p.left = data.left.parc;
    p.right = data.right.parc;

end
