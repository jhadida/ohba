function Y = wsum( weight, theta, phi )
%
% Y = com.sharm.wsum( weight, theta, phi )
%
% Weighted sum of spherical harmonics basis functions.
% The numer of weights should be a perfect square (n+1)^2, which implies the number of harmonics n.
% The basis functions are normalised across vertices to have largest magnitude 1.
%
% The output is a 1xN vector where N corresponds to the number of points in theta and phi.
%
% JH

    n = sqrt(numel(weight)) - 1;
    assert( dk.is.integer(n), 'Bad number of weights (should be a perfect square).' );
    
    Y = com.sharm.real( n, theta, phi, false );
    Y = normalise(vertcat(Y{:}));
    Y = dk.bsx.dot( weight(:), Y, 1 );

end

% normalise magnitude across all vertices
function y = normalise(x)
    y = max(abs(x),[],2);
    y = dk.bsx.rdiv( x, y );
    %y = ant.math.rescale( x, [0,1], 2 );
end
