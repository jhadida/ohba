function Y = real( order, theta, phi, isnorm )
%
% Y = com.sharm.real( order, theta, phi, isnorm=true )
%
% Real-form spherical harmonics up to specified order
%
% theta=polar angles (1xN or Nx1)
% phi=azimuth angles (1xN or Nx1)
%
% Y = 1xM cell where M = order+1
% Cell k contains a (2k+1) x N matrix, where k is the harmonic number and N the number of points.
%
% Originally by Saad
% Modified by JH

    if nargin < 4, isnorm=true; end
    
    % force points to row
    theta = theta(:)';
    phi = phi(:)';

    % compute basis functions for each harmonic
    Y = cell(1,order+1);
    for k = 0:order
        Y{k+1} = spherical_harmonic(k,theta,phi,isnorm);
    end

end

function Y = spherical_harmonic( order, theta, phi, isnorm )

    index = (0:order)';
    if isnorm
        C = sqrt((2*order+1)/4/pi * factorial(order-index)./factorial(order+index));
    else
        C = ones(size(index));
    end
    
    P = legendre(order,cos(theta),'norm'); % (l+1) x n
    C = C * ones(size(theta)); % (l+1) x n
    
    if order == 0
        Y = C .* P;
    else
        Y_pos = sqrt(2)*C .* P .* cos(index*phi);
        Y_neg = sqrt(2)*C .* P .* sin(index*phi);

        Y = [Y_neg(end:-1:2,:); Y_pos(1,:)/sqrt(2); Y_pos(2:end,:)];
    end

end