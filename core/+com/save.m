function save(fname,varargin)
%
% Facade to dk.save using prefix to data/ folder
%

    dk.save( jh_path('data',fname), varargin{:} );
end