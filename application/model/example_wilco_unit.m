
% get an instance of lsbm.model.wc.Unit using parameters for unit B
u = wilco.get_unit('B');

% show the phase-space along with the nullclines of the system
%
% Note 1: 
%   The unit parameters have been normalised to ensure the oscillatory threshold
%   for the excitatory input is equal to 1. So any input below 1 should yield a
%   fixed point, and any input above one should yield a periodic orbit.
%   
% Note 2:
%   The figure opened to display the phase plane includes an additional menu 
%   element which reads "Select". Clicking on it lets you select an initial point
%   for a short simulation.
%
u.phase_plane(2);

% run a simulation of 1sec with 1ms time-step and input 1.5
% result is wrapped as an instance of lsbm.model.wc.Result
r = u.input(1.5).run(1,1e-3);
r.show_unit();

% compute a "sweep" for relative changes in excitatory coupling, and increasing 
% excitatory input
%
% for each combination of parameters, run a simulation and record the st-dev and
% frequency mode of the excitatory signal, then plot the results as a 3D surface
% where 
%   x=excitatory input 
%   y=excitatory coupling
%   z=std
%   color=frequency
%
u.sweep( 'pe', linspace(0.8,10,45), 'c-e/i', linspace(0.5,3,25) );
xlabel('Exc. Input'); ylabel('Exc. Coupling'); caxis([0,40]); % prettier
