function obj = get_unit(name,varargin)
%
% obj = wilco.get_unit(name,varargin)
%
% Return instance of lsbm.model.wc.Unit with parameters of chosen unit.
% For valid unit names, see files: +wilco/unit*.m
%
% EXAMPLE
%   u = wilco.get_unit('B')
%   r = u.input(1.5).run(1,1e-3);
%   r.show_unit();
%
%
% See also: lsmb.model.wc.Unit
%
% JH
    
    obj = lsbm.model.wc.Unit( wilco.([ 'unit' name ])(varargin{:}) );

end