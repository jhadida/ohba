function u = unitA()
% Operating point given p.18 of the Wilson-Cowan paper (limit-cycle)

    % Excitatory
    E.am    = 1e-3; 
    E.tau   = 10e-3;
    
    E.S.name  = 'logistic';
    E.S.mu    = 4.12;
    E.S.sigma = 0.79;
    
    % Inhibitory
    I.am    = 1e-3; 
    I.tau   = 10e-3;
    
    I.S.name  = 'logistic';
    I.S.mu    = 4.12;
    I.S.sigma = 0.56;
    
    % normalisation
    %E.norm = false;
    %I.norm = false;
    
    % local couplings
    u.cee =  16.49;
    u.cei =  16.71;
    u.cie = -12.36;
    u.cii = - 3.34;
    
    u.E = E;
    u.I = I;

end