function u = unitB()
% Normalised fmod2

    % Excitatory
    E.am    = 0; 
    E.tau   = 10e-3;
    
    E.S.name  = 'logistic';
    E.S.mu    = 3.06;
    E.S.sigma = 0.61;
    
    % Inhibitory
    I.am    = 0; 
    I.tau   = 10e-3;
    
    I.S.name  = 'logistic';
    I.S.mu    = 3.06;
    I.S.sigma = 0.61;
    
    % local couplings
    u.cee =  11.03;
    u.cei =   9.19;
    u.cie = -12.26;
    u.cii =   0;
    
    u.E = E;
    u.I = I;

end