function u = unitDeco()
% Given in Deco2009 PNAS SI

    % Excitatory
    E.am    = 0; 
    E.tau   = 2e-3;
    E.mu    = 4;
    E.sigma = 1;
    
    % Inhibitory
    I.am    = 0; 
    I.tau   = 10e-3;
    I.mu    = 4;
    I.sigma = 1;
    
    % normalisation
    %E.normalise = false;
    %I.normalise = false;
    
    % local couplings
    u.cee =  14.15;
    u.cei =  15.17;
    u.cie = -10.11;
    u.cii =  0;
    
    u.E = E;
    u.I = I;

end
