function u = unitD(tau)
% Used in the NeuroImage paper

    if nargin < 1, tau = 10e-3; end

    % Excitatory
    E.am    = 0; 
    E.tau   = tau;
    
    E.S.name  = 'logistic';
    E.S.mu    = 4.92;
    E.S.sigma = 0.82;
    
    % Inhibitory
    I.am    = 0; 
    I.tau   = tau;
    
    I.S.name  = 'logistic';
    I.S.mu    = 4.92;
    I.S.sigma = 0.82;
    
    % local couplings
    u.cee =  46;
    u.cei =  12;
    u.cie = -58;
    u.cii =   0;
    
    u.E = E;
    u.I = I;

end
