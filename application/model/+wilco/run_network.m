function out = run_network( param, stim, induce )
%
% out = wilco.run_network( param, stim, induce=300 )
%
% Run network simulation of Wilson-Cowan model.
% Output is an instance of lsbm.model.wc.Result.
%
%
% INPUTS
%
%   param
%   
%   stim
%   
%   induce  Induction length ()
%               
%
%
%
%
% See also: wilco.get_network_uniform, wilco.get_network_sharm, lsbm.model.wc.Result
%
% JH

    dp = 1e-3; % delay precision
    if nargin < 3, induce=300e-3; end

    % transform params
    [p,wait] = format_param(param,stim,dp,induce);
    
    % set processing options
    p.options.enable_isp = false;
    p.options.cache_delays = true;
    p.options.num_threads = 4;
    
    % sampling info
    s.span = stim.time([1,end]);
    s.step = struct('min',1e-9,'max',dp);
    s.tol  = struct('abs',1e-6,'rel',1e-5);
    
    % initialisation
    tini = stim.time(1);
    dt = stim.time(2) - stim.time(1);
    maxdel = dt * ceil( max(param.del(:)) / dt );
    t = (tini-maxdel) : dt : tini;
    
    h.time = t(:);
    h.vals = zeros(numel(t),numel(p.nodes));
    
    % create config
    cfg = lsbm.Config('quiet');
    cfg.set_system(p);
    
    % induction
    ind.wait = wait;
    ind.length = induce;
    cfg.set_induction(ind);
    
    % run simulation
    out = lsbm.run( cfg, @lsbm.mex.wc_network, @lsbm.model.wc.Result, s, h );

end

function [out,wait] = format_param( in, stim, dp, wt )

    n = size(in.con,1);
    
    % convert network matrices
    local.del = [];
    local.con = zeros(2,2,n);
    local.con(1,1,:) = reshape( in.unit.cee, [1,1,n] );
    local.con(1,2,:) = reshape( in.unit.cie, [1,1,n] );
    local.con(2,2,:) = reshape( in.unit.cii, [1,1,n] );
    local.con(2,1,:) = reshape( in.unit.cei, [1,1,n] );
    
    nonloc.con.mat  = in.con;
    nonloc.con.mask = [ 1,0; 0,0 ];
    nonloc.del = in.del;
    
    [con,del] = lsbm.cfg.network( n, 2, 1, local, nonloc );
    edges = lsbm.cfg.net2edge( con, del, dp );
    
    % create nodes
    tframe = stim.time([1,end]);
    nodes = dk.struct.repeat( {'am','tau','S','P'}, 2*n, 1 );
    for u = 1:n
        i = 2*u;
        e = i-1;
        
        nodes(e).am = in.unit.E.am(u);
        nodes(e).tau = in.unit.E.tau(u);
        nodes(e).S = lsbm.make.sigmoid( 'logistic', in.unit.E.mu(u), in.unit.E.sig(u) );
        nodes(e).P = lsbm.make.stim_tc( tframe, stim.time, stim.vals(:,u) );
        
        nodes(i).am = in.unit.I.am(u);
        nodes(i).tau = in.unit.I.tau(u);
        nodes(i).S = lsbm.make.sigmoid( 'logistic', in.unit.I.mu(u), in.unit.I.sig(u) );
        nodes(i).P = lsbm.make.stim_const( tframe, 0 );
    end
    
    % output
    out.con = con;
    out.del = del;
    out.nodes = nodes;
    out.edges = edges;
    
    % estimate minimum waiting time for induction
    wait = max( 100*max([ nodes.tau ]), max(del(:)), dp*ceil(wt/max(eps,dp)) );

end