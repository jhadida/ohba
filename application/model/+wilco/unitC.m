function u = unitC()
% Normalised fmod4

    % Excitatory
    E.am    = 0; 
    E.tau   = 5e-3;
    
    E.S.name  = 'logistic';
    E.S.mu    = 3.11;
    E.S.sigma = 0.67;
    
    % Inhibitory
    I.am    = 0; 
    I.tau   = 10e-3;
    
    I.S.name  = 'logistic';
    I.S.mu    = 3.11;
    I.S.sigma = 0.67;
    
    % local couplings
    u.cee =  11.10;
    u.cei =   5.55;
    u.cie = -22.20;
    u.cii =   0;
    
    u.E = E;
    u.I = I;

end
