
% load mrtrix bundle
data = com.load_mrtrix(5,68);

% define color for each vertex
gray = [1,1,1]/2;
col = [ gray; jet(data.left.parc.nlabel) ];
col = col( 1+data.left.parc.vlabel, : );

% show parcellated left hemisphere 
patch( 'Vertices', data.left.coord.inflated, 'Faces', data.left.faces, ...
    'FaceVertexCdata', col, 'EdgeColor', 'none', 'FaceColor', 'flat' );

grid off; box off; axis equal tight off vis3d; lighting phong;
