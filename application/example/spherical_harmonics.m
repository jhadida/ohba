
% for example with one harmonic

GM = 0.0; % global mean
AP = 1.0; % anterior-posterior
SI = 0.7; % superior-inferior
DM = 0.1; % distal-medial
weights = [ GM, AP, SI, DM ];

parcellated_map = com.sharm.parcmap(weights);
com.sharm.showmap(parcellated_map);
