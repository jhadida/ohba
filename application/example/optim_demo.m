
% Run optimisation on random 2D mixture with 5 peaks, using Expected Improvement strategy
% and Simplicial Tessellation partitioning, with a budget of 125 evaluations and maximum
% partition depth of 6.

opt = tbo.demo( 'mixture-5', 'ei2', 'site' ).run( 'EvalCount', 125, 'MaxDepth', 6 );
