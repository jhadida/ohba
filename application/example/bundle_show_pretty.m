
% load mrtrix bundle and normalise conmats
bundle = com.load_bundle(10,68,'fs','meandeg');
r = bundle.disp.ord;

% show matrices
figure('name','Structural connectivity from tractography (10M streamlines)');

dk.ui.image( log1p(bundle.count(r,r)), 'title', 'Log-counts', 'subplot', {2,2,1} ); axis equal off;
dk.ui.image( bundle.wnorm(r,r), 'title', 'Fractional-scaling (degree-normalised)', 'subplot', {2,2,2} ); axis equal off;
dk.ui.image( bundle.weight(r,r), 'title', 'SIFT2 weights', 'subplot', {2,2,3} ); axis equal off;
dk.ui.image( 100*bundle.length(r,r), 'title', 'Streamline length (cm)', 'subplot', {2,2,4} ); axis equal off;
