
% show parcellated brain
S = com.ui.SurfaceBrain('inflated');
S.showall('type','sync');
set(gcf,'name','Parcellated brain');

% show random scalar map with one value per region
S.showmap( randn(1,68), 'type', 'both' );
set(gcf,'name','Random map');

% there are many other options that can be used in the SurfaceBrain class
% see the methods and helptext by typing: edit com.ui.SurfaceBrain
