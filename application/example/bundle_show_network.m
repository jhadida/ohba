
% load mrtrix bundle and normalise conmats
bundle = com.load_bundle(10,68,'fs');

% create groups for each lobe
grp = bundle.group;
grp = { grp.temporal, grp.occipital, grp.parietal, grp.frontal };

% call ui function for display
com.ui.radialconn( bundle.wnorm, 'group', grp, 'prctile', 93, 'bend', 0.3 );
