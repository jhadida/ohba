function p = jh_path( sel, varargin )
%
% Usage:
%   
%   p = jh_path()
%   Return a struct with absolute paths to different folders within the library.
%   Example: p.core, p.data, etc.
%
%   p = jh_path( select, varargin )
%   Return path to selected file or folder, accepting additional segments like fullfile.
%   Example: 
%       jh_path( 'data', 'my_experiment', 'Jan13', 'result.mat' )
%       where first input 'data' selects the path to the data folder
%       see the fields of the output struct in usage above for valid selectors
%
% JH

    % local paths
    rootdir = fileparts(mfilename('fullpath'));
    extdir = fullfile( rootdir, 'external' );
    
    % libraries
    p.root  = rootdir;
    p.core  = fullfile( rootdir, 'core' );
    p.data  = fullfile( rootdir, 'data' );
    p.deck  = fullfile( extdir, 'deck' );
    p.emd   = fullfile( extdir, 'emd' );
    p.bayte = fullfile( extdir, 'bayte' );
    p.lsbm  = fullfile( extdir, 'lsbm', 'usr' );
    
    % select path if requested
    if nargin > 0
        p = fullfile( p.(sel), varargin{:} );
    end

end
