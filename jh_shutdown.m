
% shutdown external libraries
bt_shutdown();
lsbm_shutdown();
dk_shutdown();

% remove all added paths, except root folder
p_ = jh_path();
rmpath( p_.core, p_.deck, p_.lsbm, p_.bayte );
clear p_
