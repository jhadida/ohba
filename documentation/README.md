
# Jonathan's work at OHBA

This repository aggregates several of my projects at OHBA between 2014-2019. 
Here is a brief overview of the folder contents:

```
    application/        Most of my code consists of toolboxes and libraries, which
                        don't do anything on their own. What I call "application"
                        is a folder which contains a set of scripts / modules to 
                        actually answer a question, implement a model or analysis,
                        or simply provide examples of how my tools can be used.

    core/               This folder contains general tools that didn't really fit
                        in any of the external toolboxes and library, either because
                        they are too specialised, because they are experimental, or
                        because they are dealing with housekeeping of the codebase.

    data/               Data which is used by some of the applications and core scripts.
                        See also the datasets backed up on the file-server at OHBA.

    documentation/      This documentation is written as a set of Markdown files, built
                        on-the-fly, and served using docsify. It is also available online
                        at: https://jhadida.gitlab.io/ohba

    jh_startup.m        Call jh_startup() from Matlab every time you use this library, 
    jh_shutdown.m       and jh_shutdown() after use if needed. jh_compile() is used to 
    jh_path.m           recompile all Mex files within the repo (there are quite a few).
    jh_compile.m  

    install.sh         Call ./install.sh from a terminal the first time you download 
    update.sh          this library (see installation).

```

## Quick install

**No Windows**, Unix only, sorry.

Ensure you have `rsync` installed on your machine (on OSX you may need to install [Homebrew](https://brew.sh/) and then `brew install rsync`). 
Finally, make sure you have setup your Matlab installation to compile Mex files (for OSX see e.g. [these tips](https://ohba-analysis.github.io/osl-docs/pages/overview/troubleshooting.html#compiling-mex-files-on-osx)).

Make sure you have an [account on Gitlab](https://gitlab.com/users/sign_up), and that you have [setup your SSH keys](https://gitlab.com/profile/keys) correctly. 
If all previous requirements are met, open a terminal and type:
```
git clone git@gitlab.com:jhadida/ohba.git
cd ohba 
./install.sh
```

Then open Matlab (or move into the `ohba/` folder), and type:
```
jh_startup();
jh_compile();
```

If there are errors, send me an email.

## Additional resources

To send me an e-mail, <a href="mailto:jhadida.ox@gmail.com?subject=[OHBA-repo]%20Question&body=Hi%20Jonathan,">click here</a>.

Selected projects:

 - Implementation of the EMD in Matlab ([repo](https://gitlab.com/jhadida/emd))
 - Exploration of brainwaves in MEG data ([website](https://jhadida.gitlab.io/brainwaves))
 - Gaussian Process Surrogate Optimisation, NeuroImage 2018 ([repo](https://gitlab.com/jhadida/gpso))
 - Latest C++ libraries for simulations ([drayn (new)](https://gitlab.com/jhadida/drayn), [disol](https://gitlab.com/jhadida/disol), [lsbm](https://gitlab.com/jhadida/lsbm))
 - Old C++ libraries for simulations ([drayn (old)](https://bitbucket.org/jhadida/drayn/), [osd](https://bitbucket.org/jhadida/osd/), [nsl](https://bitbucket.org/jhadida/nsl/))
 - Bash library to manage git repositories, multi-part data sync, and SSH config ([repo](https://gitlab.com/jhadida/setup))
 - Tools for computing clusters ([repo](https://gitlab.com/jhadida/cluster))

Documents:

 - [Thesis](https://ora.ox.ac.uk/objects/uuid:5abd962a-b798-4530-947a-24eeafd568f3)
 - [NeuroImage 2018](https://www.sciencedirect.com/science/article/pii/S1053811918301708)
 - [PLOS CB 2018](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1006007)

Web & social:

 - [ORCID](https://orcid.org/0000-0001-6216-9600)
 - [Gitlab (preferred)](https://gitlab.com/jhadida), [Github (academic)](https://github.com/jhadida), [Github (personal)](https://github.com/sheljohn/)
 - [LinkedIn](https://www.linkedin.com/in/jhadida/)
 - [StackExchange](https://stackexchange.com/users/660089/sheljohn)
 - [Facebook](https://www.facebook.com/jhadida)
