<!-- No ".md" in the links -->
* Getting Started

  * [Installation](start/install)
  * [Documentation](start/docs)
  * [Dependencies](start/deps)
  * [Data](start/data)

* General

  * [Analysis](gen/analysis)
  * [Tractography](gen/tract)
  * [EMD](gen/emd)

* Biophysical Models

  * [Toolbox](biophys/toolbox)
  * [Simulation](biophys/simu)
  * [Optimisation](biophys/optim)
  * [Ideas](biophys/ideas)

* Theory

  * [Wilson-Cowan](theory/wc)
  * [Conductance based](theory/cb)
  * [Bayes opt](theory/bopt)
  * [Partitioning](theory/part)
